(TeX-add-style-hook
 "hw1"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref")
   (TeX-add-symbols
    '("ddvec" 1)
    '("dvec" 1)
    '("deriv" 2)
    '("diff" 2)
    '("del" 1))
   (LaTeX-add-labels
    "sec:orgfd5a271"
    "sec:org1bf45d3"
    "sec:org83b2946"
    "sec:orgf466d0e"
    "sec:org556e633"
    "sec:orgbd9e7df"
    "sec:orgd23d951"
    "sec:org636d965"
    "sec:orgdba65c2"
    "sec:orgd484ece"
    "sec:org5baefa6"
    "sec:orged64ffd"
    "sec:org6572acb"
    "sec:orge6d3bdd"
    "sec:org741c87d"
    "sec:org3961623"
    "sec:org75ba7bc"
    "sec:org2645c8f"
    "sec:org1994c11"
    "sec:org1fb9f55"
    "sec:org5f6a39b"))
 :latex)

