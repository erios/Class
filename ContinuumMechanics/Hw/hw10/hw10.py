#!/usr/env python3
# -*- coding: utf-8 -*-
'''
# * To run:
import hw10

# * To import from same directory:
from hw10 import *

# * To import from sibling directory:
from sys import path as syspath
from os import path as ospath
from os import getcwd
if not getcwd() + "/.." in syspath:
    syspath.append(ospath.join(getcwd(), ".."))
from importlib import reload
# Import routines from homework 4
from hw4.hw4 import *

# * To reload
import hw10; reload(hw10)
'''

# To pretty print:
# a2ps hw10.py hw10.res --pro=color --columns=2 -B -E
#   --font-size=8 -o - | ps2pdf - hw10_code.pdf

# Libraries to import...
from sys import path as syspath
from os import path as ospath
from os import getcwd
if not getcwd() + "/.." in syspath:
    syspath.append(ospath.join(getcwd(), ".."))
#  ... and reload modules
from importlib import reload
# --end
# Utilities
from numpy import matrix, diag, cross, sqrt, array,\
    dot, product, vstack, tan
# Singular value decomposition, inverse, determinant, eigencouples
from numpy.linalg import svd, inv, det, eigh
# Plotting
from matplotlib import pyplot as pl
from matplotlib import rc
# Import Mohr plotting
from hw7.hw7 import multiMohr


def nuidentities(nu, E, order=None):
    if order == 1:
        # Betti-Maxwell
        # ν_21 = E1/E2*v_12
        nu[1][0] = E[0] / E[1] * nu[0][1]
        # ν_31 = E1/E3*v_13
        nu[2][0] = E[0] / E[2] * nu[0][2]
        # ν_32 = E1/E3*v_23
        nu[2][1] = E[1] / E[2] * nu[1][2]
    else:
        # Betti-Maxwell
        # ν_12 = E2/E1*v_21
        nu[0][1] = E[1] / E[0] * nu[1][0]
        # ν_13 = E3/E1*v_31
        nu[0][2] = E[2] / E[0] * nu[2][0]
        # ν_23 = E3/E2*v_32
        nu[1][2] = E[2] / E[1] * nu[2][1]

    return nu


def orthotropicS(E, G, nu):
    nu = nuidentities(nu, E)
    # Compliance (empty 6x6 zero matrix). TODO: sparse
    S = matrix(diag([float() for I in range(6)]))
    # * Fill diagonal
    # ** Upper diagonal
    for I in range(3):
        S[I, I] = 1 / E[I]
    # ** Lower diagonal
    for I in range(3, 6):
        S[I, I] = 1 / G[0][1]
    # * Fill upper sparse matrix (manually = faster)
    # First row
    S[0, 1] = -nu[1][0] / E[1]
    S[0, 2] = -nu[2][0] / E[2]
    # Second row
    S[1, 0] = -nu[0][1] / E[0]
    S[1, 2] = -nu[2][1] / E[2]
    # Third row
    S[2, 0] = -nu[0][2] / E[0]
    S[2, 1] = -nu[1][2] / E[1]
    return S, nu


def symtensor2vec(sigma):
    vector = matrix([float() for I in range(6)])
    vector[0, :3] = diag(sigma)
    vector[0, 3:] = [sigma[0, 1],
                     sigma[1, 2],
                     sigma[0, 2]]
    return vector.reshape(6, 1)


def p1a(E, G, nu, sigma):
    # Orthotropic compliance tensor
    S, nu = orthotropicS(E, G, nu)
    # Vectorise stress
    sigma_vec = symtensor2vec(sigma)
    # Voight vector
    epsilon_gamma = dot(S, sigma_vec)
    # Infinitesimal strain (ε = γ/2)
    epsilon_vec = vstack([epsilon_gamma[:3],
                          epsilon_gamma[3:] / 2])
    print("The orthotropic compliance tensor is:")
    print(S)
    print("The infinitesimal strain is:")
    print(epsilon_vec)
    return S, epsilon_vec, nu


def vec2symtensor(vec):
    tensor = matrix(diag([float()
                          for I in range(3)]))
    # Off-diagonal
    # Nomenclature (left here for historical reasons)
    # ↑ e2         ↑ e1          ↑ e3
    # | → e21      | → e13       | → e32
    # |  ↑e12      |  ↑e31       |  ↑e23
    # +-----→ e1   +------→ e3   +-----→e2
    tensor[0, 1] = vec[3]  # e12
    tensor[1, 2] = vec[4]  # e23
    tensor[0, 2] = vec[5]  # e13
    tensor += tensor.transpose()
    # Set diagonal (first three elements)
    for I in range(3):
        tensor[I, I] = vec[I]
    return tensor


def p1b(epsilon_vec):
    # This is not the strain tensor, but a matrix that I
    # use to  plot

    # Turn into matrix
    epsilon = vec2symtensor(epsilon_vec)
    # Get eigencouples
    eigvl, eigvc = eigh(epsilon)
    # Make combinations of eigenvalues
    l = [[eigvl[0], eigvl[1]],
         [eigvl[1], eigvl[2]],
         [eigvl[0], eigvl[2]]]
    pl.clf()
    multiMohr(l, "hw10P1b.svg", markers=True)

    return epsilon, eigvl, eigvc


def p1c(S):
    epsilon1 = matrix(diag([float()
                            for I in range(3)]))
    epsilon1[1, 1] = 0.02

    epsilon2 = matrix(diag([float()
                            for I in range(3)]))
    epsilon2[0, 0] = 0.025
    epsilon2[1, 1] = -0.031
    epsilon2[2, 2] = -0.011

    # Calculate the stress by multiplying the inverse of S, which is
    # C, by the strain
    sigma_vec = dot(S.I, symtensor2vec(epsilon1))
    sigma1 = vec2symtensor(sigma_vec)

    sigma_vec = dot(S.I, symtensor2vec(epsilon2))
    sigma2 = vec2symtensor(sigma_vec)

    print("The first stress tensor is:")
    print(sigma1)

    print("\nThe second stress tensor is:")
    print(sigma2)


def p2c():
    # Data
    # Lamé coefficients and Poisson ratio
    # (from p2a and p2b)
    lamda = 160
    mu = 160
    nu = 1 / 4
    # Tension strain from 0 to 0.05
    epsilon_o = 0
    epsilon_f = 0.05
    # Increments of 0.001
    delta_epsilon = 0.001
    # How many (inclusive) points are these?
    points = int(epsilon_f / delta_epsilon + 1)

    # Create strains on the 1-face in the 1-direction
    epsilon11 = [delta_epsilon * I
                 for I in range(points)]
    # Calculate perpendicular strains (necessary for trace)
    #               epsilon
    #                      j, j
    #    nu     = - -----------
    #      i, j     epsilon
    #                      i, i
    # This can be faster, but let's be explicit
    epsilon22 = [-nu * I for I in epsilon11]
    epsilon33 = [-nu * I for I in epsilon11]
    trace = [1 / 3 * (epsilon11[I] +
                      epsilon22[I] +
                      epsilon33[I])
             for I in range(points)]
    # Calculate sigma based on the Lamé coefficients and
    # the strains
    sigma11 = [lamda * trace[I] + 2 * mu * epsilon11[I]
               for I in range(points)]
    pl.clf()
    pl.plot(epsilon11, sigma11)
    pl.grid()
    pl.xlabel(r"$\varepsilon_{1,1} (\frac{\mathrm{mm}}{\mathrm{mm}})$")
    pl.ylabel(r"$\sigma_{1,1} (\mathrm{MPa})$")
    pl.savefig("hw10P2c.svg")
    pl.show()


def p2d(G12):
    pi = 3.1415926535897323846
    # Angle from 0 to 45°
    theta_o = 0
    theta_f = 45 / 180 * pi
    # Increments of 1°
    delta_theta = 1 / 180 * pi
    # How many (inclusive) points are these?
    points = int(theta_f / delta_theta + 1)
    # Create angles
    epsilon12 = [delta_theta * I
                 for I in range(points)]
    sigma12 = [G12 * tan(I) for I in epsilon12]
    pl.clf()
    pl.plot(epsilon12, sigma12)
    pl.grid()
    pl.xlabel(r"$\varepsilon_{1,2} (\frac{\mathrm{mm}}{\mathrm{mm}})$")
    pl.ylabel(r"$\sigma_{1,2} (\mathrm{MPa})$")
    pl.savefig("hw10P2d.svg")
    pl.show()


def extra(S, sigma):
    # Get Cauchy stress, by inverting S (definition)
    C = S.I
    Q = matrix([[0, 1, 0], [-1, 0, 0], [0, 0, 1]])

    # def transformcolvec(a, Q):
    #     res = [[Q[i, j] * a[j]
    #             for j in range(3)]
    #            for i in range(3)]
    #     return res
    #
    # def transformtensor(A, Q):
    # ABORT! - Need to transform C into 3 by 3 by 3 by 3


def main():
    # * Create empty arrays (allocate memory)
    # ** Poisson ratio
    nu = [[float() for j in range(3)] for i in range(3)]
    # ** Shear modulus
    G = [[float() for j in range(3)] for i in range(3)]
    # ** Young's modulus (unnecessary in Python)

    # * Initialise arrays
    # ** Poisson
    nu[1][0] = 0.31
    nu[2][1] = 0.29
    nu[2][0] = 0.33
    # ** Shear
    G[0][1] = 76.3
    G[1][2] = 38.8
    G[0][2] = 56.4
    # ** Young's modulus
    E = [200, 50, 150]
    # ** Cauchy stress
    sigma = matrix([[0,  1, 0],
                    [-1, -2, 0],
                    [0,  0, 0]])

    # Solve problem 1a
    S, epsilon_vec, nu = p1a(E, G, nu, sigma)

    # Solve problem 1b
    epsilon, eigvl, eigvc = p1b(epsilon_vec)

    # Solve problem 1c
    p1c(S)

    # Solve problem 2c
    p2c()

    # Solve problem 2d
    p2d(160)

    # Solve extra credit
    extra(S, sigma)


if __name__ == "__main__":
    main()
