#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# To pretty print:
# a2ps hw11.py --pro=color --columns=2 -B -E --font-size=8 -o -
# | ps2pdf - hw11_code.pdf

# Libraries to import...
from sys import path as syspath
from os import path as ospath
from os import getcwd
if not getcwd() + "/.." in syspath:
    syspath.append(ospath.join(getcwd(), ".."))
#  ... and reload modules
from importlib import reload
# --end
# Utilities
from numpy import matrix, sqrt, array, dot, NaN, exp, inf
# Singular value decomposition, inverse, determinant, eigencouples
from numpy.linalg import svd, inv, det, eigh
# Regression
from scipy import odr
# Plotting
from matplotlib import pyplot as pl
from matplotlib import rc
# Read data
from hw7.openData import readByCols


def regr_odr(x, y, func, estim, full=0):
    '''Calculates the coefficients that best fit a function for a given
    set of data using Orthogonal distance regression with scipy.odr
    x: array-like (see scipy.odr documentation)
    y: array-like (see scipy.odr documentation)
    func: a defined function that can be evaluated
    estim: estimated values for the coefficients

    Example:  Get the linear regression of a set of data
    >>> def linea(c, x): return c[0]*x + c[1]
    >>>
    >>> # m, b = regr_odr
    >>> regr_odr([1.1, 0.8, 1.4], [2.3, 1.2, 2.9], linea, [1, 1])
    array([ 2.90618533, -1.06347053])
    >>>
    '''
    # Total least squares or Orthogonal distance regression
    # Orthogonal distance regression (scipy.odr)
    # Cookbook/Least Squares Circle - - SciPy
    # http://www.scipy.org/Cookbook/Least_Squares_Circle
    # Python for the Advanced Physics Lab
    # http://www.physics.utoronto.ca/~phy326/python/odr_fit_to_data.py
    data = odr.Data(x=x, y=y)
    model = odr.Model(func)
    proc = odr.ODR(data, model, estim)
    out = proc.run()
    # Get all the statistical information from the regression:
    if not full == 0:
        return odr.odr(func, estim, y, x, full_output=1)
    return out.beta


def lagrange2stretch(epsilon_lag):
    '''Calculate stretch from Lagrange strain
    lambda = √(2*ε_lagrange + 1)'''
    return [sqrt(2 * I + 1) for I in epsilon_lag]


def lagrange2infstrain(epsilon_lag):
    '''Calculate infinitesimal strain from Lagrange strain
    strain = lambda - 1
    strain = √(2*ε_lagrange + 1) - 1'''
    return [sqrt(2 * I + 1) - 1 for I in epsilon_lag]


def stressMooneyRivlin1D(coef, L):
    '''Calculates the first component of stress (sigma11) for a
    Mooney-Rivlin material subject to a 1D stretch (L) using the
    equation:

    sigma11 = 2*c1 * (L^2 - 1/L) + 2 * c2 * (L - 1/L^2)

    Keyword Arguments:
    coef -- list containing one value of each coefficient such
            that:
            c1 = coef[0], c2 = coef[1]
    L    -- a single value of stretch
    '''
    L2 = L * L
    return 2 * coef[0] * (L2 - 1 / L) + 2 * coef[1] * (L - 1 / L2)


def stressOrthotropicLinear1D(coef, e):
    '''Calculates the first component of stress (sigma11) for an
    orthotropic linear (elastic) material subject to a 1D strain (e)
    using the equation:

    sigma11 = E1 * e

    Keyword Arguments:
    coef -- list containing one value of each coefficient such
            that:
            E1 = coef[0] (one value)
    L    -- a single value of strain

    '''
    return coef[0] * e


def stressTransIsoIncomp1D(coef, L):
    '''Calculates the first component of stress (sigma11) for transversely
    hyperelastic and incompressible material subject to a 1D stretch
    (L) using the custom equation:

    sigma11 = 2 * mu * L^2 + exp(c5 * L) + c6

    Keyword Arguments:
    coef -- list containing one value of each coefficient such
            that:
            mu = coef[0] (one value)
            c5 = coef[1] (one value)
            c6 = coef[2] (one value)
    L    -- a single value of stretch

    '''
    # return 2 * coef[0] * L * L + L * coef[1] + coef[2]
    L2 = L * L
    L4 = L2 * L2
    return 2 * coef[0] * L2 + L4 * coef[1] + coef[2]


def formatStrainStressPlot():
    # Set the LaTeX output
    # http://matplotlib.org/users/usetex.html
    pl.rc('text', usetex=True)
    # Legend with transparency and in the upper-left corner
    pl.legend(framealpha=0.5, loc='upper left')
    pl.xlabel(r"$\mathrm{strain\quad(\frac{mm}{mm}})$")
    pl.ylabel(r"$\mathrm{stress\quad(kPa})$")
    pl.grid(True)


def p3a(datafnameL="data_ramp (longitudinal).txt", sep=" "):
    # Read data
    strain_stress = readByCols(datafnameL, sep=sep)
    # Extract first column (strain)
    epsilon_lag = [I[0] for I in strain_stress]
    # Extract second column (stress)
    stress = [I[1] for I in strain_stress]
    # Calculate stretch (lambda = √(2*ε_lagrange + 1))
    stretch11 = lagrange2stretch(epsilon_lag)
    # Plot for different initial guess values
    inivalfit = [[500, -500], [-100, 100], [20, 500],
                [5000, -0.5], [20000, 500]]
    # Clear figure
    pl.clf()
    # Plot experimental data
    pl.plot(epsilon_lag, stress, label="Experiment")
    for I in inivalfit:
        # Fit parameters
        c1, c2 = regr_odr(stretch11, stress, stressMooneyRivlin1D, [I[0],
                                                                    I[1]])
        # Format label
        lab = r"$c_1$: {0:10.3e}; $c_2$: {1:10.3e}".format(c1, c2)
        # Plot the results
        pl.plot(epsilon_lag, [stressMooneyRivlin1D([c1, c2], I)
                              for I in stretch11], label=lab)
    # Save last values
    res = (c1, c2)
    # Format the plot
    formatStrainStressPlot()
    pl.suptitle(r"Mooney-rivlin stress-strain curves for different" +
                " fitted coefficients")
    pl.title(r"Longitudinal test")
    # Save figure
    fname = "p3aLongitudinal.svg"
    pl.savefig(fname)
    print("Plot saved as {0:s}".format(fname))

    # Transversal data
    # Clear figure
    pl.clf()
    # Read data
    strain_stress = readByCols("data_ramp (transverse).txt", sep=" ")
    # Extract first column (strain)
    epsilon_lag = [I[0] for I in strain_stress]
    # Extract second column (stress)
    stress = [I[1] for I in strain_stress]
    # Calculate stretch (lambda = √(2*ε_lagrange + 1))
    stretch22 = lagrange2stretch(epsilon_lag)
    # Plot experimental data
    pl.plot(epsilon_lag, stress, label="Experiment")
    # Plot the results
    lab = r"$c_1$: {0:10.3e}; $c_2$: {1:10.3e}".format(c1, c2)
    pl.plot(epsilon_lag, [stressMooneyRivlin1D([c1, c2], I)
                          for I in stretch22], label=lab)
    # Format the plot
    formatStrainStressPlot()
    pl.suptitle(r"Mooney-rivlin stress-strain curves for different" +
                " a fitted coefficient")
    pl.title(r"Transversal test")
    # Save figure
    fname = "p3aTransversal.svg"
    pl.savefig(fname)
    print("Plot saved as {0:s}".format(fname))
    return res


def p3b():
    # Read data
    strain_stress = readByCols("data_ramp (longitudinal).txt", sep=" ")
    # Extract first column (strain)
    epsilon_lag = [I[0] for I in strain_stress]
    # Extract second column (stress)
    stress = [I[1] for I in strain_stress]
    # Calculate strain (strain = lambda - 1 = √(2*ε_lagrange + 1) - 1)
    strain11 = lagrange2infstrain(epsilon_lag)
    # Plot for different initial guess values
    inivalfit = [[500], [-100], [20],
                [5000], [20000]]
    # Clear figure
    pl.clf()
    # Plot experimental data
    pl.plot(epsilon_lag, stress, label="Experiment")
    for I in inivalfit:
        # Fit parameters
        E1, = regr_odr(strain11, stress, stressOrthotropicLinear1D, [I[0]])
        # Format label
        lab = r"$E_1$: {0:10.3e}".format(E1)
        # Plot the results
        pl.plot(epsilon_lag, [stressOrthotropicLinear1D([E1], I)
                              for I in strain11], label=lab)
    # Save last values
    res = E1
    # Format the plot
    formatStrainStressPlot()
    pl.suptitle(r"Incompressible, orthotropic and linear" +
                " stress-strain curves for different fitted coefficients")
    pl.title(r"Longitudinal test")
    # Save figure
    fname = "p3bLongitudinal.svg"
    pl.savefig(fname)
    print("Plot saved as {0:s}".format(fname))

    # Transversal data
    # Clear figure
    pl.clf()
    # Read data
    strain_stress = readByCols("data_ramp (transverse).txt", sep=" ")
    # Extract first column (strain)
    epsilon_lag = [I[0] for I in strain_stress]
    # Extract second column (stress)
    stress = [I[1] for I in strain_stress]
    # Calculate strain (strain = lambda - 1 = √(2*ε_lagrange + 1) - 1)
    strain22 = lagrange2infstrain(epsilon_lag)
    # Plot experimental data
    pl.plot(epsilon_lag, stress, label="Experiment")
    # Plot the results
    lab = r"$E_1$: {0:10.3e}".format(E1)
    pl.plot(epsilon_lag, [stressOrthotropicLinear1D([E1], I)
                          for I in strain22], label=lab)
    # Format the plot
    formatStrainStressPlot()
    pl.suptitle(r"Incompressible, orthotropic and linear" +
                "stress-strain curves for a fitted coefficient")
    pl.title(r"Transversal test")
    # Save figure
    fname = "p3bTransversal.svg"
    pl.savefig(fname)
    print("Plot saved as {0:s}".format(fname))
    return res


def extra():
    # Read data
    strain_stress = readByCols("data_ramp (longitudinal).txt", sep=" ")
    # Extract first column (strain)
    epsilon_lag = [I[0] for I in strain_stress]
    # Extract second column (stress)
    stress = [I[1] for I in strain_stress]
    # Calculate stretch (lambda = √(2*ε_lagrange + 1))
    stretch11 = lagrange2stretch(epsilon_lag)
    # Plot for different initial guess values
    inivalfit = [[0, 1e-3, 10], [-100, 200, 50], [1000, 50, 10]]
    # Clear figure
    pl.clf()
    # Plot experimental data
    pl.plot(epsilon_lag, stress, label="Experiment")
    for I in inivalfit:
        # Fit parameters using incompressible, transversely isotropic
        # 1D
        mu, c5, c6 = regr_odr(
            stretch11, stress, stressTransIsoIncomp1D, I)
        # Format label
        lab = r"$\mu$: {0:10.3e}; $c5$: {1:10.3e}; $c6$: {2:10.3e}".format(
            mu, c5, c6)
        # Plot the results
        pl.plot(epsilon_lag, [stressTransIsoIncomp1D([mu, c5, c6], I)
                              for I in stretch11], label=lab)
    # Save last values
    res = mu, c5, c6
    # Format the plot
    formatStrainStressPlot()
    pl.suptitle(r"Incompressible, transversely isotropic" +
                " stress-strain curves for different" +
                " fitted coefficients")
    pl.title(r"Longitudinal test")
    # Save figure
    fname = "pExtraLongitudinal.svg"
    pl.savefig(fname)
    print("Plot saved as {0:s}".format(fname))

    # Transversal data
    # Clear figure
    pl.clf()
    # Read data
    strain_stress = readByCols("data_ramp (transverse).txt", sep=" ")
    # Extract first column (strain)
    epsilon_lag = [I[0] for I in strain_stress]
    # Extract second column (stress)
    stress = [I[1] for I in strain_stress]
    # Calculate stretch (lambda = √(2*ε_lagrange + 1))
    stretch22 = lagrange2stretch(epsilon_lag)
    # Plot experimental data
    pl.plot(epsilon_lag, stress, label="Experiment")
    # Plot the results
    lab = r"$\mu$: {0:10.3e}; $c5$: {0:10.3e}; $c6$: {0:10.3e}".format(
        mu, c5, c6)
    pl.plot(epsilon_lag, [2 * mu * I * I
                          for I in stretch22], label=lab)
    # Format the plot
    formatStrainStressPlot()
    pl.suptitle(r"Incompressible, transversely isotropic" +
                " stress-strain curves for different" +
                " fitted coefficients")
    pl.title(r"Transversal test")
    # Save figure
    fname = "pExtraTransversal.svg"
    pl.savefig(fname)
    print("Plot saved as {0:s}".format(fname))
    return res


# print(p3a())
#
# print(p3b())

# print(extra())
