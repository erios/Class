#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Libraries to import...
from sys import path as syspath
from os import path as ospath
from os import getcwd
if not getcwd() + "/.." in syspath:
    syspath.append(ospath.join(getcwd(), ".."))
#  ... and reload modules
from importlib import reload
# --end
# Utilities
from numpy import matrix, sqrt, array, dot, NaN,\
    exp, row_stack
# Singular value decomposition, inverse, determinant, eigencouples
from numpy.linalg import svd, inv, det, eigh
# Regression
from scipy import odr
# Plotting
from matplotlib import pyplot as pl
from matplotlib import rc
# Read data
from hw7.openData import readByCols

# Consider this:
# http://www.physics.nyu.edu/pine/pymanual/html/chap9/chap9_scipy.html


def stressVoigt1D(coef, e_e_dot):
    """Calculates the first component of stress
    (sigma11) for a Voigt viscoelastic material
    subject to a 1D strain (e) using the equation:

       sigma11  = k * e + c * e_dot

    where e_dot is the derivative of e with
    respect to time, k is a linear constant
    (spring equivalent), c is a viscosity constant
    and e is the strain.

    Keyword Arguments:
    coef  -- k = coef[0], c = coef[1]
    e     -- (float) value of infinitesimal strain
    e_dot -- (float) value of derivative of strain
    """
    k = coef[0]
    c = coef[1]
    e = e_e_dot[0]
    e_dot = e_e_dot[1]
    return k * e + c * e_dot


def sigma_dotVoigt(epsilon_dot, sigma, k, c):
    return k * (epsilon_dot - sigma / c)


def sigma_maxwell(deltaEpsilon, deltaT, sigma_1, k, c):
    return k * deltaEpsilon - k / c * deltaT +\
        sigma_1


# Strain ########################################


def deriv_epsilon(epsilon_inf, time):
    # Calculate strain differences (add initial value
    # of 0; particular to this case)
    deltaEpsilon = [0] + [epsilon_inf[I] - epsilon_inf[I - 1]
                          for I in range(1, len(epsilon_inf))]
    # Calculate time differences (add initial
    # value of 1; particular to this case)
    deltaT = [1] + [time[I] - time[I - 1]
                    for I in range(1, len(time))]
    # Estimate derivative
    epsilon_dot = [deltaEpsilon[I] / deltaT[I]
                   for I in range(len(deltaT))]
    return epsilon_dot

# Read strain data
time_strain = readByCols(
    "data_relax (strain vs time).txt",
    sep=" ")
# Extract first column (time)
time = [I[0] for I in time_strain]
# Extract second column (strain)
epsilon_inf = [I[1] for I in time_strain]
epsilon_dot = deriv_epsilon(epsilon_inf, time)

# Stress ########################################
time_stress = readByCols(
    "data_relax (stress vs time).txt",
    sep=" ")
# Extract second column (stress)
sigma11 = [I[1] for I in time_stress]

# Stress fit ####################################
# Voigt #########################################


def finiteDifFit(epsilon_inf,
                 sigma11, guesses=None):
    '''Example of using finite differences and ODR
    with two independent variables (epsilon_inf
    and its derivative: epsilon_dot)
    '''
    # Plot experimental data
    pl.plot(sigma11, label="Experiment")

    # Try different initial guesses
    # [±10^n, ±10^m], m = n = {1, 3, 5}
    if guesses is None:
        guesses = [[[[[k1 * 10 ** I, k2 * 10 ** J]
                      for k2 in [-1, 1]]
                     for k1 in [-1, 1]]
                    for J in [3]]
                   for I in [3]]
    for I in guesses:
        for J in I:
            for k1 in J:
                for beta0 in k1:
                    # Estimate stress constants,
                    # using the Voigt model by
                    # finite differences by means
                    # of a multivariate fit (as if
                    # epsilon_dot was a secondary
                    # variable)
                    # https://stackoverflow.com/questions/23995768/scipy-odr-python#23997001
                    # Independent variables Chop
                    # first value
                    X = epsilon_inf
                    Y = epsilon_dot
                    lsc_data = odr.Data(
                        row_stack([X, Y]),
                        y=sigma11)
                    lsc_model = odr.Model(stressVoigt1D)
                    lsc_odr = odr.ODR(lsc_data, lsc_model, beta0)
                    lsc_out = lsc_odr.run()
                    k, c = lsc_out.beta
                    # * Plot fitted data
                    # ** Wrap epsilon_inf and epsilon_dot
                    wrap = [[epsilon_inf[I],
                             epsilon_dot[I]]
                            for I in
                            range(len(epsilon_dot))]
                    # lab = str(beta0[0]) +\
                    #     " " + str(beta0[1]) +\
                    lab = "k: " + str(k) +\
                          "; c: " + str(c)
                    dat = [stressVoigt1D([k, c],
                                         I)
                           for I in wrap]
                    pl.plot(dat, label=lab)
    pl.legend()
    fname = "p1a.svg"
    pl.savefig(fname)
    print("File saved: " + fname)
    pl.show()


finiteDifFit(epsilon_inf, sigma11)

# Maxwell #######################################
# https://stackoverflow.com/questions/16223283/optimize-constants-in-differential-equations-in-python
# See also:
# https://scipy-cookbook.readthedocs.io/items/FittingData.html
from scipy.integrate import odeint
from scipy.optimize import curve_fit


def sigmadot(e_dot, sigma11, k, c):
    '''Maxwell model:
    diff(sigma, t) = k * (e - sigma)/c'''
    return k * (e_dot - sigma11 / c)


def sigma_odesol(epsilon_dot, sigma0,
                 k, c):
    '''ODE solution to Maxwell model'''
    # First order system (for numerical solution)
    sigma11 = odeint(sigmadot, sigma0,
                     epsilon_dot,
                     args=(k, c), printmessg=False)
    return sigma11.ravel()


def odeFit(sigma11, epsilon_dot, ini_guess=None):
    '''Example of fitting an ODE (not recommended:
    slow and sensitive)'''
    # Boundary condition
    # y(0) = (peak - ini_val) / dt
    dt = sigma11.index(max(sigma11)) - 0
    sigma11_0 = (max(sigma11) - sigma11[0]) / dt
    # Initial guesses for fitting
    if ini_guess is None:
        a_guess = 10e3
        b_guess = 50
        ini_guess = [a_guess, b_guess]
    # Fit
    popt, cov = curve_fit(sigma_odesol,
                          epsilon_dot,
                          sigma11,
                          [sigma11_0,
                           ini_guess[0],
                           ini_guess[1]])
    # Results
    sigma0_opt, k_opt, c_opt = popt
    return sigma0_opt, k_opt, c_opt


def fitmanyode():
    # Boundary condition
    # y(0) = (peak - ini_val) / dt
    dt = sigma11.index(max(sigma11)) - 0
    sigma11_0 = (max(sigma11) - sigma11[0]) / dt
    # /# Try different initial guesses
    # /# [±10^n, ±10^m], m = n = {1, 3, 5}
    guesses = [[[[[k1 * I, k2 * J]
                  for k2 in [1]]
                 for k1 in [1]]
                for J in [-100, -500]]
               for I in [91.84, 88.94]]
    for I in guesses:
        for J in I:
            for k1 in J:
                for ini_guess in k1:
                    res = odeFit(sigma11, epsilon_dot,
                                 ini_guess)
                    k = res[1]
                    c = res[2]
                    # k = 88.94
                    # c = -522.46
                    # * Plot fitted data
                    lab = "k: " + str(k) +\
                          "; c: " + str(c)
                    sigma_ini = sigma11_0
                    dat = sigma_odesol(epsilon_dot,
                                       sigma_ini,
                                       k, c)
                    pl.plot(dat, label=lab)
    pl.legend()
    print("File saved: " + fname)
    pl.show()

# fitmanyode()

deltaT = [1] + [time[I] - time[I - 1]
                for I in range(1, len(time))]
deltaEpsilon = [0] + [epsilon_inf[I] - epsilon_inf[I - 1]
                      for I in range(1,
                                     len(epsilon_inf))]
# Plot original data
pl.clf()
pl.plot(sigma11, label="Experiment")
#!!! There may be a bug here !!!! (check indices,
# initial values and similar)
# Set values from fit
k = 88.9439
c = -522.47
sigmai = [
    sigma_maxwell(deltaEpsilon[I], deltaT[I],
                  sigma11[I], k, c)
    for I in range(len(deltaEpsilon) - 1)]
# Set label
lab = "k: " + str(k) +\
      "; c: " + str(c)
pl.plot(sigmai, label=lab)
# Set filename to save
fname = "p1b.svg"
pl.legend()
pl.savefig(fname)
print("File saved: " + fname)
pl.show()

# Standard linear model #########################
# http://www.sciencedirect.com/science/article/pii/S0020768307000741
# https://hal.archives-ouvertes.fr/hal-00776729/document
