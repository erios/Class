#!/bin/bash

for i in 1 2 3; do
    for L in C A B; do
        for ii in 1 2 3; do
            if [ $ii == 3 ]; then
                case $L in
                    C)
                        sym="=";
                        ;;
                    A)
                        sym="+";
                        ;;
                    B)
                        sym='\\';
                        ;;
                esac
            else
                sym="+";
            fi
            printf "%s" "${L}_{$i$ii}\,\\vec{e_{$i}}\,\\vec{e_{$ii}}$sym";
        done;
    done;
    printf "\n";
done

for i in 1 2 3; do
    for ii in 1 2 3; do
        echo "C_{$i$ii}\,\begin{bmatrix}0 & 0 & 0 \\\\ 0 & 0 & 0 \\\\ 0 & 0 & 0 \\\\ \end{bmatrix} +";
    done;
done;
