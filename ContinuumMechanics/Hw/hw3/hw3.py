#!/bin/env python3
# -*- coding: utf-8 -*-

from numpy import cos, sin, matrix, dot, pi
from numpy import linspace as lsp
# from numpy import linalg as la
from matplotlib import pyplot as pl
from matplotlib import rc


def Q_trans(a, b, g):
    """This function builds the inverse of Q (the coordinate system
    transformation matrix) from Euler angles alpha (a), beta (b) and gamma
    (g)
    Keyword Arguments:
    a -- Euler angle 1
    b  -- Euler angle 2
    g -- Euler angle 3
    """
    res = matrix([
        [cos(a)*cos(g),
            -cos(a)*sin(b) - cos(b)*cos(g)*sin(a),
            sin(a)*sin(b)],
        [cos(g)*sin(a) + cos(a)*cos(b)*sin(g),
            cos(a)*cos(b)*cos(g) - sin(a)*sin(g),
            -cos(a)*sin(b)],
        [sin(b)*sin(g),
            cos(g)*sin(b),
            cos(b)]
    ])

    return res


def matQ(a = None, b = None, c = None):
    """This function builds the coordinate system transformation matrix Q
    from Euler angles alpha (a), beta (b) and gamma (g)
    Keyword Arguments:
    a -- Euler angle 1
    b  -- Euler angle 2
    g -- Euler angle 3
    """
    if not a: a = 0
    if not b: b = 0
    if not c: c = 0
    return Q_trans(a, b, c).transpose()

def main():
    # b. Using values given below, have the program calculate vector
    # components for vB. Sketch the problem to confirm that your solution
    # makes physical sense.
    vA = [0, 4, 0]
    print("vB = ", dot(matQ(0, pi, 0), vA))
    # vB = [[ 2.82842712  2.82842712  0.        ]]

    # c. Using values given below, have the program calculate tensor
    # components for TB.
    TA = matrix([
        [-4, 3, 0],
        [3, 2, 0],
        [0, 0, 1]
        ])

    alpha = pi/2
    beta = -pi/2
    gamma = pi/4

    Q = matQ(alpha, beta, gamma)
    TB = dot(dot(Q, TA), Q.transpose())
    print("TB = ", TB)

    # d. Plot the changing values for T11 (plot on the x-axis) and T21 (plot
    # on the y-axis) when α rotates from 0 to π (β = 0, and γ = 0). What
    # does the resulting plot remind you of from mechanics of materials?
    T11A = TA[0, 0]
    T21A = TA[1, 0]
    T11B = TB[0, 0]
    T21B = TB[1, 0]

    # # https://github.com/matplotlib/matplotlib/issues/
    # # 3945#issuecomment-68475206
    # x = array([0, T11A, 0, T11B])
    # y = array([0, 0, 0, 0])
    # pl.quiver(x[:-1], y[:-1], diff(x), diff(y), angles='xy',
    #           scale_units='xy', scale=1)

    # Create linear array with 100 points for the angle alpha
    alpha = lsp(0, pi, 100)
    beta = 0
    gamma = 0

    # Calculate the values of Q for each alpha
    Q = [matQ(alpha_i, beta, gamma) for alpha_i in alpha]
    # Calculate the values of the transformed tensor
    TB = [dot(dot(Q_i, TA), Q_i.transpose()) for Q_i in Q]
    # Extract the first element (0,0) from all the results
    T11B = [T_i[0, 0] for T_i in TB]
    # Extract the fourth element (1,0) from all the results
    T21B = [T_i[1, 0] for T_i in TB]

    # Set the LaTeX output
    # http://matplotlib.org/users/usetex.html
    pl.rc('text', usetex=True)
    # Plot all the first elements v.s. all the fourth elements
    pl.plot(T11B, T21B, label=r"${T_{11}}^B$ v.s. ${T_{21}}^B$")
    pl.legend()
    # Set equal axes
    pl.axis("equal")
    # Set axes labels
    pl.xlabel("${T_{11}}^B$")
    pl.ylabel('${T_{21}}^B$')
    # Export picture to PDF
    pl.savefig("hw3_fig.pdf")
    # Clear figure
    pl.clf()
    # # I had understood something different
    # x = [TA[0, 0], TB[0, 0], 0, 0]
    # y = [0, 0, TA[1, 0], TB[1, 0]]
    # c = ['red', 'blue', 'magenta', 'orange']
    # fig = pl.figure()
    # ax = fig.gca()
    # for i in range(len(x)):
    #     xi = x[i-1]
    #     yi = y[i-1]
    #     ax.arrow(0, 0, xi, yi, length_includes_head=True, head_width=0.05,
    #              edgecolor=c[i], width=0.02, facecolor=c[i])
    # ax.axis("scaled")
    # ax.axis([min(x), max(x), min(y), max(y)])
    # fig.show()
    # fig.clf()
