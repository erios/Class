#!/usr/env python3
# -*- coding: utf-8 -*-
'''
* To run:
from hw4 import main as hw4

* To import from same directory:
from hw4 import *

* To import from sibling directory:
from sys import path as syspath
from os import path as ospath
from os import getcwd
if not getcwd() + "/.." in syspath:
    syspath.append(ospath.join(getcwd(), ".."))
#  ... and reload modules
from importlib import reload
# Import routines from homework 4
from hw4.hw4 import *
'''

# To pretty print:
# a2ps hw4.py hw4.res --pro=color --columns=2 -B -E
#   --font-size=8 -o hw4_code.ps && ps2pdf hw4_code.ps

# Libraries to import...
from sys import path as syspath
from os import path as ospath
from os import getcwd
if not getcwd() + "/.." in syspath:
    syspath.append(ospath.join(getcwd(), ".."))
#  ... and reload modules
from importlib import reload
# Mathematical functions
from numpy import matrix, pi
# Eigenvalues and eigenvectors of a Hermitian or symmetric
# matrix
from numpy.linalg import eigh
# Import routines from homework 3
from hw3.hw3 import matQ


def multiTransf(T, a, b, c, prFlag=None):
    """
    Calculates multiple transformations
    T -- Tensor to transform
    a -- List of Euler angles alpha
    b -- List of Euler angles beta
    g -- List of Euler angles gamma
    """
    # Create a container for the results
    res = [T for i in a]
    for I in range(len(a)):
        alpha = a[I]
        beta = b[I]
        gamma = c[I]
        Q = matQ(alpha, beta, gamma)
        res[I] = Q * T * Q.transpose()

        if prFlag:
            msg = "\nalpha = {0:0.3f}, beta = %{1:0.3f}, "\
                + "gamma = {2:0.3f}"
            print(msg.format(alpha, beta, gamma))
            print("[Q].[T].transpose([Q]) = ")
            print(res[I])

    return res


def eigInvar(eigval):
    """Calculate the invariants of a matrix by means of
    eigenpairs (numpy.linalg.eigh, numpy.linalg.eig)
    eigval -- list with the eigenvalues
    """
    I1 = eigval[0] + eigval[1] + eigval[2]
    # VoyiadjisKattani05 - Damage mechanics
    I2 = eigval[0] * eigval[1] + eigval[1] * eigval[2] +\
        eigval[2] * eigval[0]
    I3 = eigval[0] * eigval[1] * eigval[2]
    return I1, I2, I3


def testPosDef(eigval):
    """Tests if a matrix is positive definite, based on its
    eigenvalues.
    eigval -- list with the eigenvalues"""
    invar = eigInvar(eigval)
    res = all([i > 0 for i in list(invar) + eigval])
    return res


def main():
    qb = """"
    b. Use your HW3 program to calculate the tensor
    component after counterclockwise transformations about
    e3, defined by α. Sketch the column vectors on e1 and
    e2 surfaces."""
    T = matrix([[10, 0, 0], [0, 0, 0], [0, 0, 0]])

    print(qb)
    # alpha = 0, alpha = 40°, alpha = 60°, alpha = 90°,
    # alpha = 150°, alpha = 180°
    a = [0, 45, 60, 90, 150, 180]
    for I in a:
        alpha = I / 180 * pi
        Q = matQ(alpha)
        T_trans = Q * T * Q.transpose()
        print("\nalpha = " + str(I))
        print("[T]^" + str(I) + "° = ")
        print(T_trans)

    qc = """
    c. Mark locations in your Mohr’s circle that
    represent the normal and shear component of the surface
    normal to the e1 unit basis as T is transformed in part
    (b)."""
    print(qc)
    # Change last value of angles to 12
    a[len(a) - 1] = 12
    alpha = [i / 180 * pi for i in a]
    z = [0 for i in a]
    T = matrix([
               [5, 2, 0],
               [2, -4, 0],
               [0, 0, 0]
               ])
    multiTransf(T, alpha, z, z, prFlag=True)

    q7a = """
    7.a) Include code to calculate the invariants, eigenvalues
    and eigenvectors of T. To calculate eigenvectors you can use
    the built-in function ‘eig’ (this will give eigenvectors as
    column vectors, see “help” in Matlab). Include screen shots
    of the “published” code and the “published” results.
    """
    print(q7a)

    T = matrix([
               [1, 0, 0],
               [0, 3, 1],
               [0, 1, 3]
               ])

    eigval, eigvec = eigh(T)
    invarT = eigInvar(eigval)
    print("[T]")
    print(T)
    print("The invariants of the matrix are:")
    print("I1, I2, I3")
    print(invarT)
    print("The eigenvalues of the matrix are")
    print(eigval)
    print("The corresponding eigenvectors are")
    print(eigvec)

    q7b = """
    7.b) Include code to calculate whether tensor T is positive
    definite. Include screen shots of the results.
    """
    print(q7b)
    if testPosDef(eigval):
        print("[T] is positive definite")

    q7c = """
    7.c) If the coordinate system of T is rotated by α=30° and
    α=-45° (β = 0, and γ = 0), use your program to calculate the
    invariants, eigenvalues, and eigenvectors of T in these new
    coordinate systems?
    """
    print(q7c)
    alpha = [i / 180 * pi for i in [30, -45]]
    z = [0 for i in a]
    T_trans = multiTransf(T, alpha, z, z, prFlag=True)

    for Ti in T_trans:
        eigval, eigvec = eigh(Ti)
        invarT = eigInvar(eigval)
        print("[T]")
        print(Ti)
        print("The invariants of the matrix are:")
        print("I1, I2, I3")
        print(invarT)
        print("The eigenvalues of the matrix are")
        print(eigval)
        print("The corresponding eigenvectors are")
        print(eigvec)
        print()
