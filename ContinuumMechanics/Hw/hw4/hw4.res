Python 3.4.3 (default, Nov 17 2016, 03:08:31)
[GCC 4.8.4] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> >>> >>> >>> >>>
>>> from hw4 import main as hw4
>>> hw4()
"
    b. Use your HW3 program to calculate the tensor
    component after counterclockwise transformations about
    e3, defined by α. Sketch the column vectors on e1 and
    e2 surfaces.

alpha = 0
[T]^0° =
[[ 10.   0.   0.]
 [  0.   0.   0.]
 [  0.   0.   0.]]

alpha = 45
[T]^45° =
[[ 5. -5.  0.]
 [-5.  5.  0.]
 [ 0.  0.  0.]]

alpha = 60
[T]^60° =
[[ 2.5        -4.33012702  0.        ]
 [-4.33012702  7.5         0.        ]
 [ 0.          0.          0.        ]]

alpha = 90
[T]^90° =
[[  3.74939946e-32  -6.12323400e-16   0.00000000e+00]
 [ -6.12323400e-16   1.00000000e+01   0.00000000e+00]
 [  0.00000000e+00   0.00000000e+00   0.00000000e+00]]

alpha = 150
[T]^150° =
[[ 7.5         4.33012702  0.        ]
 [ 4.33012702  2.5         0.        ]
 [ 0.          0.          0.        ]]

alpha = 180
[T]^180° =
[[  1.00000000e+01   1.22464680e-15   0.00000000e+00]
 [  1.22464680e-15   1.49975978e-31   0.00000000e+00]
 [  0.00000000e+00   0.00000000e+00   0.00000000e+00]]

    c. Mark locations in your Mohr’s circle that
    represent the normal and shear component of the surface
    normal to the e1 unit basis as T is transformed in part
    (b).

alpha = 0.000, beta = %0.000, gamma = 0.000
[Q].[T].transpose([Q]) =
[[ 5.  2.  0.]
 [ 2. -4.  0.]
 [ 0.  0.  0.]]

alpha = 0.785, beta = %0.000, gamma = 0.000
[Q].[T].transpose([Q]) =
[[ 2.5 -4.5  0. ]
 [-4.5 -1.5  0. ]
 [ 0.   0.   0. ]]

alpha = 1.047, beta = %0.000, gamma = 0.000
[Q].[T].transpose([Q]) =
[[-0.01794919 -4.89711432  0.        ]
 [-4.89711432  1.01794919  0.        ]
 [ 0.          0.          0.        ]]

alpha = 1.571, beta = %0.000, gamma = 0.000
[Q].[T].transpose([Q]) =
[[-4. -2.  0.]
 [-2.  5.  0.]
 [ 0.  0.  0.]]

alpha = 2.618, beta = %0.000, gamma = 0.000
[Q].[T].transpose([Q]) =
[[ 1.01794919  4.89711432  0.        ]
 [ 4.89711432 -0.01794919  0.        ]
 [ 0.          0.          0.        ]]

alpha = 0.209, beta = %0.000, gamma = 0.000
[Q].[T].transpose([Q]) =
[[  5.42442785e+00  -3.22397856e-03   0.00000000e+00]
 [ -3.22397856e-03  -4.42442785e+00   0.00000000e+00]
 [  0.00000000e+00   0.00000000e+00   0.00000000e+00]]

    7.a) Include code to calculate the invariants, eigenvalues
    and eigenvectors of T. To calculate eigenvectors you can use
    the built-in function ‘eig’ (this will give eigenvectors as
    column vectors, see “help” in Matlab). Include screen shots
    of the “published” code and the “published” results.

[T]
[[1 0 0]
 [0 3 1]
 [0 1 3]]
The invariants of the matrix are:
I1, I2, I3
(7.0, 14.0, 8.0)
The eigenvalues of the matrix are
[ 1.  2.  4.]
The corresponding eigenvectors are
[[ 1.          0.          0.        ]
 [ 0.         -0.70710678  0.70710678]
 [ 0.          0.70710678  0.70710678]]

    7.b) Include code to calculate whether tensor T is positive
    definite. Include screen shots of the results.

[T] is positive definite

    7.c) If the coordinate system of T is rotated by α=30° and
    α=-45° (β = 0, and γ = 0), use your program to calculate the
    invariants, eigenvalues, and eigenvectors of T in these new
    coordinate systems?


alpha = 0.524, beta = %0.000, gamma = 0.000
[Q].[T].transpose([Q]) =
[[ 1.5        0.8660254  0.5      ]
 [ 0.8660254  2.5        0.8660254]
 [ 0.5        0.8660254  3.       ]]

alpha = -0.785, beta = %0.000, gamma = 0.000
[Q].[T].transpose([Q]) =
[[ 2.         -1.         -0.70710678]
 [-1.          2.          0.70710678]
 [-0.70710678  0.70710678  3.        ]]
[T]
[[ 1.5        0.8660254  0.5      ]
 [ 0.8660254  2.5        0.8660254]
 [ 0.5        0.8660254  3.       ]]
The invariants of the matrix are:
I1, I2, I3
(6.9999999999999982, 13.999999999999995, 7.9999999999999947)
The eigenvalues of the matrix are
[ 1.  2.  4.]
The corresponding eigenvectors are
[[ 0.8660254   0.35355339  0.35355339]
 [-0.5         0.61237244  0.61237244]
 [ 0.         -0.70710678  0.70710678]]

[T]
[[ 2.         -1.         -0.70710678]
 [-1.          2.          0.70710678]
 [-0.70710678  0.70710678  3.        ]]
The invariants of the matrix are:
I1, I2, I3
(7.0, 14.000000000000004, 8.0000000000000036)
The eigenvalues of the matrix are
[ 1.  2.  4.]
The corresponding eigenvectors are
[[ -7.07106781e-01   5.00000000e-01  -5.00000000e-01]
 [ -7.07106781e-01  -5.00000000e-01   5.00000000e-01]
 [  1.66533454e-16   7.07106781e-01   7.07106781e-01]]

>>>
