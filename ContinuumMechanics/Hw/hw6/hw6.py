#!/usr/env python3
# -*- coding: utf-8 -*-
'''
* To run:
import hw6

* To import from same directory:
from hw6 import *

* To import from sibling directory:
from sys import path as syspath
from os import path as ospath
from os import getcwd
if not getcwd() + "/.." in syspath:
    syspath.append(ospath.join(getcwd(), ".."))
#  ... and reload modules
from importlib import reload
# Import routines from homework 4
from hw6.hw6 import *
'''

# To pretty print:
# a2ps hw6.py hw6.res --pro=color --columns=2 -B -E
#   --font-size=8 -o hw6_code.ps && ps2pdf hw6_code.ps

# Libraries to import...
from sys import path as syspath
from os import path as ospath
from os import getcwd
if not getcwd() + "/.." in syspath:
    syspath.append(ospath.join(getcwd(), ".."))
#  ... and reload modules
from importlib import reload
# --end
# # Import routines from homework 4
# from hw4.hw4 import *
# Singular value decomposition, inverse, determinant, eigencouples
from numpy.linalg import svd, inv, det, eigh
# Utilities
from numpy import matrix, pi, diag, cross, sqrt, array
# Plotting
from matplotlib import pyplot as pl
from matplotlib import rc



def J3x3(F):
    """
    Does nothing!
    Calculate the Jacobian of a 3x3 (may be more, feeling brave?)
    deformation gradient tensor

    Keyword Arguments:
    F -- Deformation gradient tensor
    """


def defGradVec(cX1, cX2, cX3, cx1, cx2, cx3):
    """
    Calculates the deformation gradient based on the coordinates of
    the parallelepipid coordinates
    Keyword Arguments:
    cX1 -- coordinates of node 1 at reference configuration
    cX2 -- coordinates of node 2 at reference configuration
    cX3 -- coordinates of node 3 at reference configuration
    cx1 -- coordinates of node 1 at deformed state
    cx2 -- coordinates of node 2 at deformed state
    cx3 -- coordinates of node 3 at deformed state
    """
    dx1 = array(cx1)/cX1[0]
    dx2 = array(cx2)/cX2[1]
    dx3 = array(cx3)/cX3[2]
    F = matrix([dx1, dx2, dx3]).transpose()
    return F


def defGradPt(dX, dx):
    """
    Function to calculate the (3x3) deformation gradient (F) at a
    point from local increments. F[i,j] = dx[i]/dX[j]

    Keyword Arguments:
    dX -- local increment in the reference configuration (3x1 matrix)
    dx -- local increment in the deformed state (3x1 matrix)
    """
    F = matrix([[dx[I]/dX[J] for J in range(3)]
               for I in range(3)])
    return F


def RUDecSVD(F):
    """
    Polar decomposition by means of a singular value decomposition

    Keyword Arguments:
    F -- deformation gradient (or anything else; check svd)

    """
    P, S, V = svd(F)
    # Right stretch
    U = V.transpose() * diag(S) * V
    R = F * inv(U)
    return R, U


def VRDecSVD(F, u_bol=None):
    """Left polar decomposition by means of a right polar
    decomposition (not efficient, but saves me code).

    Keyword Arguments:
    F -- deformation gradient
    """
    U, R = RUDecSVD(F)
    V = F * inv(R)
    if u_bol is True:
        res = (V, R, U)
    else:
        res = (V, R)
    return res


def cauchyGreen(F):
    """
    Calculates the right Cauchy-Green deformation tensor from the
    deformation gradient tensor.
    Keyword Arguments:
    F -- deformation gradient
    """
    # Right Cauchy-Green tensor
    C = F.transpose()*F
    return C


def UPrinDec(F, q_bol=None):
    """
    Polar decomposition by means of a singular value decomposition

    Keyword Arguments:
    F -- deformation gradient (or anything else; check svd)

    """
    C = cauchyGreen(F)
    # Eigenvalues and eigenvectors (as columns)
    dum, Qt = eigh(C)
    # Set eigenvectors in rows as the transformation matrix
    Q = Qt.transpose()
    # Make a matrix out of the eigenvalues (principal Cauchy-Green)
    Cp = Q * C * Qt
    # I don't need the dummy variable
    del dum
    # Get right stretch from Cauchy-Green in principal configuration
    Up = sqrt(Cp)
    if q_bol is True:
        res = (Up, Cp, Q, Qt)
    else:
        res = (Up, Cp)
    return res


def RUDec(F, p_bol=None, q_bol=None):
    """
    Right polar decomposition through the Cauchy-Green tensor

    Keyword Arguments:
    F -- deformation gradient

    """
    # Get the right stretch tensor and the Cauchy-Green tensor in the
    # principal configuration, together with the transformation matrix
    Up, Cp, Q, Qt = UPrinDec(F, q_bol=True)
    U = Qt*Up*Q
    R = F*inv(U)
    return R, U, Q


def defDiffA(F, dX1, dX2):
    """
    Deformed area differential

    Keyword Arguments:
    F   -- deformation gradient
    dX1 -- vector of the first basis deformation in the reference
           configuration (1x3)
    dX2 -- vector of the second basis deformation in the reference
           configuration (1x3)
    """
    return det(F)*inv(F.transpose())*matrix(cross(dX1, dX2)).transpose()

def main():
    """
    Write a Python (which I, Trevor Lujan, admit to be a superior language)
    program where you input dX1, dX2, dX3, dx1, dx2, and dx3, and
    calculate the deformation gradient tensor, F. Using this program,
    calculate F at point A.
    """
    cX1 = [1, 0, 0]
    cX2 = [0, 1, 0]
    cX3 = [0, 0, 1]

    cx1 = [2, 0.5, 0]
    cx2 = [1, 1, 0]
    cx3 = [0, 0, 1.8]

    F = defGradVec(cX1, cX2, cX3, cx1, cx2, cx3)
    print("F = ", F)


    """
    Using Python, calculate the Jacobian, J, and the change in surface
    area, dA (shaded gray), and interpret the answers. [2 pts]
    """
    J = det(F)
    print("\nJacobian: J = ", J)

    dA = defDiffA(F, [1, 0, 0], [0, 1, 0])
    print("\nChange in area differential (deformed state):")
    print("dA = ", dA)


    """
    Using Python, calculate U and V for point A in the principal basis
    (use earlier Python code that computes eigenvalues).
    """
    R, U = RUDecSVD(F)
    V, R, U = VRDecSVD(F, True)
    eigvalU, eigvecU = eigh(U)
    Up = diag(eigvalU)
    print("\nRight stretch in the principal direction:")
    print("Up = ", Up)

    eigvalV, eigvecV = eigh(V)
    Vp = diag(V)
    print("\nLeft stretch in the principal direction:")
    print("Vp = ", Vp)


    """
    Using Python, calculate U and V for point A in the lab basis (use
    earlier Python code that computes eigenvectors and transforms 2ndorder
    tensors).
    """
    print("\nRight stretch in the original (lab) configuration:")
    print("U = ", U)

    print("\nLeft stretch in the original (lab) configuration:")
    print("V = ", V)


    """
    Using Python, calculate R at point A.[2 pts]
    """
    print("\nOrthogonal (unitary) matrix of the polar decomposition:")
    print("R = ", R)


    """
    4 Using  an  ellipse,  sketch  the  in-plane  principal stretch directions in the  current  and  reference
    configuration. Explain whether this sketch make physical sense when comparing to the in-plane
    components of R calculated using Matlab? [4 pts]
    """
    eigvl, eigvc = eigh(U)
    fig = pl.figure()
    ax1 = fig.add_subplot(121)
    # Set the LaTeX output
    # http://matplotlib.org/users/usetex.html
    pl.rc('text', usetex=True)
    # pl.plot([0, eigvc[0,0]], [0, eigvc[1,0]])
    ax1.arrow(0, 0, eigvc[0, 0], eigvc[1, 0], length_includes_head=True,
              head_width=0.05)
    ax1.annotate(r"$G_{1}$", xy=(eigvc[0, 0], eigvc[1, 0]),
                 xytext=(eigvc[0, 0]*1.1, eigvc[1,0]*1.1))
    ax1.arrow(0, 0, eigvc[0, 2], eigvc[1, 2], length_includes_head=True,
              head_width=0.05)
    ax1.annotate(r"$G_{2}$", xy=(eigvc[0, 2], eigvc[1, 2]),
                 xytext=(eigvc[0, 2]*1.3, eigvc[1, 2]*1.1))
    ax1.axis("scaled")
    ax1.axis([-1.5, 0.5, -1.5, 1])
    ax1.grid()
    pl.title(r"Reference (undeformed)")

    ax2 = fig.add_subplot(122)
    # # Set the LaTeX output
    # # http://matplotlib.org/users/usetex.html
    # pl.rc('text', usetex=True)
    # pl.plot([0, eigvc[0,0]], [0, eigvc[1,0]])
    ax2.arrow(0, 0, eigvc[0, 0]*eigvl[0], eigvc[1, 0]*eigvl[0], length_includes_head=True,
              head_width=0.05)
    ax2.annotate(r"$g_{1}$", xy=(eigvc[0, 0]*eigvl[0], eigvc[1, 0]*eigvl[0]),
                 xytext=(eigvc[0, 0]*eigvl[0]*1.2, eigvc[1,0]*eigvl[0]*1.2))
    ax2.arrow(0, 0, eigvc[0, 2]*eigvl[2], eigvc[1, 2]*eigvl[2], length_includes_head=True,
              head_width=0.05)
    ax2.annotate(r"$g_{2}$", xy=(eigvc[0, 2]*eigvl[2], eigvc[1, 2]*eigvl[2]),
                 xytext=(eigvc[0, 2]*eigvl[2]*1.1, eigvc[1, 2]*eigvl[2]*1.1))
    ax2.axis("scaled")
    ax2.axis([-1.5, 0.5, -1.5, 1])
    ax2.grid()
    pl.title(r"Current (deformed)")

    pl.savefig("hw6_fig.svg")
    pl.show()
    # Set equal axes
    pl.axis("equal")
    fig.clf()
