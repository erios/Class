Python 3.4.3 (default, Nov 17 2016, 01:08:31)
[GCC 4.8.4] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> F =  [[ 2.   1.   0. ]
 [ 0.5  1.   0. ]
 [ 0.   0.   1.8]]

Jacobian: J =  2.7

Change in area differential (deformed state):
dA =  [[ 0. ]
 [ 0. ]
 [ 1.5]]

Right stretch in the principal direction:
Up =  [[ 0.82199494  0.          0.        ]
 [ 0.          1.          0.        ]
 [ 0.          0.          1.15079291]]

Left stretch in the principal direction:
Vp =  [ 0.98639392  0.98639392  1.        ]

Right stretch in the original (lab) configuration:
U =  [[ 0.98639392  0.16439899  0.        ]
 [-0.16439899  0.98639392  0.        ]
 [ 0.          0.          1.        ]]

Left stretch in the original (lab) configuration:
V =  [[ 0.98639392  0.16439899  0.        ]
 [-0.16439899  0.98639392  0.        ]
 [ 0.          0.          1.        ]]

Orthogonal (unitary) matrix of the polar decomposition:
R =  [[ 1.89058835  0.82199494  0.        ]
 [ 0.82199494  1.15079291  0.        ]
 [ 0.          0.          1.8       ]]
>>> 
