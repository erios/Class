#!/usr/env python3
# -*- coding: utf-8 -*-
'''
* To run:
import hw7

* To import from same directory:
from hw7 import *

* To import from sibling directory:
from sys import path as syspath
from os import path as ospath
from os import getcwd
if not getcwd() + "/.." in syspath:
    syspath.append(ospath.join(getcwd(), ".."))
from importlib import reload
# Import routines from homework 4
from hw7.hw7 import *

* To reload
import hw7; reload(hw7)
'''

# To pretty print:
# a2ps hw7.py hw7.res --pro=color --columns=2 -B -E
#   --font-size=8 -o hw7_code.ps && ps2pdf hw7_code.ps

# Libraries to import...
from sys import path as syspath
from os import path as ospath
from os import getcwd
if not getcwd() + "/.." in syspath:
    syspath.append(ospath.join(getcwd(), ".."))
#  ... and reload modules
from importlib import reload
# --end
# Utilities
from numpy import matrix, diag, cross, sqrt, array, dot, product
# Singular value decomposition, inverse, determinant, eigencouples
from numpy.linalg import svd, inv, det, eigh
# Gradient functions from homework 6
from hw6.hw6 import defGradVec, cauchyGreen, RUDecSVD
# Plotting
from matplotlib import pyplot as pl
from matplotlib import rc
# Reading data
from .openData import readByCols


def greenLagrangeTensor(F):
    C = cauchyGreen(F)
    E = 1 / 2 * (C - matrix(diag([1, 1, 1])))
    return E


def uPrinc(F):
    R, U = RUDecSVD(F)
    eigvalU, eigvecU = eigh(U)
    Up = diag(eigvalU)
    return Up, R


def engLagrangeTensor(F):
    R, U = RUDecSVD(F)
    E_eng = U - diag([1, 1, 1])
    return E_eng


def infinitesimalStrain(F):
    R, U = RUDecSVD(F)
    tmp = U.transpose() \
        * R.transpose()
    I = diag([1, 1, 1])
    # epsilon = 0.5*(R*U + tmp)\
    #           - diag([1, 1, 1])
    epsilon = 0.5 * ((F - I) + (F - I).transpose())
    epsilon_eigvl, tmp = eigh(epsilon)

    return epsilon, epsilon_eigvl


def rescaleMohrEig(l_min, l_max):
    ax = pl.gcf().gca()
    r = abs((l_max - l_min) / 2)
    # Set axes
    ax.axis("scaled")
    ax.axhline(y=0, color='gray')
    ax.axvline(x=0, color='gray')
    sign1 = -1 if l_min < 0 else 1
    sign2 = -1 if l_max < 0 else 1
    # Make the axes range bigger
    xo = (1 - 0.2 * sign1) * l_min
    xf = (1 + 0.2 * sign2) * l_max
    yo = -r * 1.2
    yf = r * 1.2
    ax.axis([xo, xf, yo, yf])


def plotorsave(fname):
    if fname is None:
        pl.show()
    elif fname is False:
        pass
    else:
        pl.savefig(fname)


def plotPlaneMohrEig(l1, l2, fname=None, markers=True):
    eigvl = (l1, l2)
    l_max = max(eigvl)
    l_min = min(eigvl)
    # Centre
    c = [(l1 + l2) / 2, 0]
    # Radius
    r = abs((l2 - l1) / 2)

    fig = pl.gcf()
    ax = fig.gca()
    # Set the LaTeX output
    # http://matplotlib.org/users/usetex.html
    pl.rc('text', usetex=True)
    # Use centre and radius to plot circle
    circ = pl.Circle(c, r, fill=False)
    ax.add_artist(circ)

    if markers:
        # Max tangent marker
        ax.annotate(r"Max shear:\\%0.3e" % r,
                    xy=(c[0], r),
                    xytext=(c[0], r * 1.2),
                    arrowprops=dict(facecolor='black', shrink=0.03))
        ax.plot([c[0]], [r], 'o')
        # Max normal marker
        sign2 = -1 if l_max < 0 else 1
        xf = (1 + 0.2 * sign2) * l_max
        ax.annotate(r"Max normal:\\%0.3e" % l_max,
                    xy=(l_max, 0),
                    xytext=(xf, 0),
                    arrowprops=dict(facecolor='black', shrink=0.03))
        ax.plot([l_max], [0], 'o')

    # Re-scale plot and show axes
    rescaleMohrEig(l_min, l_max)

    # Show figure or save
    plotorsave(fname)


def multiMohr(l, fname, markers=True):
    for [l1, l2] in l:
        # Make plot, don't show figure nor markers
        plotPlaneMohrEig(l1, l2, False, markers)
    l_min = min(min(I) for I in l)
    l_max = max(max(I) for I in l)
    rescaleMohrEig(l_min, l_max)
    # Show figure or save
    plotorsave(fname)


def squareLengthFromPoints(p1, p2=None):
    """
    innefficient (this is better:
    a = matrix([[1], [2], [3]])
    b = matrix([[5], [7], [11]])
    a.transpose()*b
    )
    """
    if p2 is None and (isinstance(p1, list)
                       or isinstance(p1, type(array([])))):
        l = p1[1] - p1[0]
    else:
        l = array(p2) - array(p1)
    return dot(l, l)


def dxMat2D(dx1, dx2, dx3):
    return matrix([[dx1[0, 0] * dx1[0, 0],
                    2 * dx1[0, 0] * dx1[1, 0],
                    dx1[1, 0] * dx1[1, 0]],
                   [dx2[0, 0] * dx2[0, 0],
                    2 * dx2[0, 0] * dx2[1, 0],
                    dx2[1, 0] * dx2[1, 0]],
                   [dx3[0, 0] * dx3[0, 0],
                    2 * dx3[0, 0] * dx3[1, 0],
                    dx3[1, 0] * dx3[1, 0]]])


def p1a():
    cX1 = [1, 0, 0]
    cX2 = [0, 1, 0]
    cX3 = [0, 0, 1]

    cx1 = [2, 0.5, 0]
    cx2 = [1, 1, 0]
    cx3 = [0, 0, 1.8]

    F = defGradVec(cX1, cX2, cX3, cx1, cx2, cx3)
    E = greenLagrangeTensor(F)
    E_p = eigh(E)

    print("Deformation tensor (F):")
    print(F)
    print("Green-Lagrange tensor (E):")
    print(E)
    print("Eigenvalues of E:")
    print(E_p[0])

    plotPlaneMohrEig(E_p[0][0], E_p[0][1], "hw7P1a.svg")
    print("See hw7P1a.svg")

    return F, E, E_p


def p1b(F):
    """Using Matlab, calculate the engineering strain tensor, E eng , at
    point A. Calculate the eigenvalues of E eng .  Plot the in-plane
    Mohr’s circle for E eng , and label the max normal strain and max
    shear strain
    """
    E_eng = engLagrangeTensor(F)
    E_eng_eigvl = eigh(E_eng)

    print("Engineering strain tensor (E_eng):")
    print(E_eng)
    print("Eigenvalues of E_eng:")
    print(E_eng_eigvl[0])

    plotPlaneMohrEig(E_eng_eigvl[0][0],
                     E_eng_eigvl[0][2],
                     "hw7P1b.svg")
    print("See hw7P1b.svg")
    return E_eng, E_eng_eigvl[0]


def p1c(F):
    """Using Matlab, calculate the infinitesimal strain tensor, ε, at
    point A. Calculate the eigenvalues of ε. Plot the in-plane Mohr’s
    circle for ε at point A, and label the max normal strain and max shear
    strain.  Compare your results to E eng , and interpret any differences

    """
    epsilon, epsilon_eigvl = infinitesimalStrain(F)
    print("The infinitesimal strain (\epsilon) is:")
    print(epsilon)
    print("The eigenvalues of \epsilon are:")
    print(epsilon_eigvl)

    plotPlaneMohrEig(epsilon_eigvl[0],
                     epsilon_eigvl[2],
                     "hw7P1c.svg")
    print("See hw7P1c.svg")

    return epsilon, epsilon_eigvl


def p2d():
    """
    Using python, calculate E at point A (problem 1) using the method
    in (2c)
    """
    # See the figure to get the points

    # Get three lines for material (reference; initial) coordinates
    # * First line
    # ** Points
    p1l1 = [0, 0]
    p2l1 = [1, 0]
    # ** Line as a vector
    dX1 = matrix(array(p2l1) - array(p1l1)).transpose()
    # ** Length
    dS1 = squareLengthFromPoints(p1l1, p2l1)         # Overkill

    # * Second line
    # ** Points
    p1l2 = [0, 0]
    p2l2 = [0, 1]
    # ** Line as a vector
    dX2 = matrix(array(p2l2) - array(p1l2)).transpose()
    # ** Length
    dS2 = squareLengthFromPoints(p1l2, p2l2)

    # * Third line length (diagonal from p2l1 to p2l2)
    # ** Line as a vector
    dX3 = matrix(array(p2l2) - array(p2l1)).transpose()
    dS3 = squareLengthFromPoints([1, 0], [0, 1])

    # Get three lines for spatial (current; deformed) coordinates
    p1 = [0, 0]
    p2 = [2, 0.5]
    p3 = [1, 1]
    ds1 = squareLengthFromPoints(p2, p1)
    ds2 = squareLengthFromPoints(p3, p1)
    ds3 = squareLengthFromPoints(p3, p2)

    # Build column vector of squared lengths
    # (the segments correspond)
    ds_dS = matrix([[ds1 - dS1],
                    [ds2 - dS2],
                    [ds3 - dS3]])
    print("The vector of the differences of square lengths is:")
    print(ds_dS)
    dXmat = 2 * dxMat2D(dX1, dX2, dX3)
    print("The coefficient matrix for strain is:")
    print(dXmat)
    E_vec = inv(dXmat) * ds_dS
    print("The resulting Green-Lagrange strain vector is:")
    print(E_vec)
    return E_vec


def p3():
    lamb = [I * 4 / 100 for I in range(10, 100)]
    pl.rc('text', usetex=True)
    pl.plot(lamb, [l - 1 for l in lamb], label="Engineernig")
    pl.plot(lamb, [(l ** 2 - 1) / 2 for l in lamb], label="Lagrange")
    pl.plot(lamb, [1 - 1 / l for l in lamb], label="True")
    pl.plot(lamb, [(1 - 1 / l ** 2) / 2 for l in lamb], label="Euler")
    pl.legend()
    pl.savefig("hw7P3.svg")
    # pl.show()
    pl.clf()


def p5a():
    # width = 20 mm
    # height = 10 mm
    # thickness = 1.5 mm
    # vertical displacement = 7.5 mm
    # linear force response
    # maximum force response coincides with maximum strain
    # maximum force = 5 N
    # data collected at 30 Hz
    # milimeters
    # E13 E23 are zero
    # Assume incompressibility (det(F) = 1)
    #
    # Calculate out-of-plane normal strain E33

    # a = readByCols("data.txt", sep="|", cab="2")

    # Frequency (data collected at 30 Hz)
    freq = 30.0

    # Read data
    data = readByCols("simple_shear_video.txt", cab=0)
    dataRan = range(len(data))
    # Unpack data as points [x, y] in 4 variables (u_: upper; l_:lower;
    # _l:left; _r:right)
    ul, ur, ll, lr = [array([[J[2 * I], J[2 * I + 1]] for J in data])
                      for I in range(4)]
    del data
    # *Calculate strains
    # *Calculate dx for each line (left-right, down-up) at each instant
    dxi = [
        # **Bottom line (0)
        lr - ll,
        # **Right vertical (1)
        ur - lr,
        # **Top line (2)
        ur - ul,
        # **Left vertical (3)
        ul - ll,
        # **Diagonal going up (4)
        ur - ll,
        # **Diagonal going down (5)
        lr - ul,
    ]
    # **Reference coordinates are at t = 0
    dXi = [dx[0] for dx in dxi]
    # **Calculate dS (magnitude of dX) for each line
    dSi = [sqrt(I[0] ** 2 + I[1] ** 2) for I in dXi]
    # **Calculate ds for each line at each instant
    dsi = [[sqrt(J[0] ** 2 + J[1] ** 2) for J in I]
           for I in dxi]
    # **Calculate the vector of ds^2 - dS^2 at each instant
    # (could be faster if I didn't calculate the sqrt of ds and dS before)
    ds2_dS2 = [matrix([ds[J] ** 2 - dSi[I] ** 2 for I, ds in enumerate(dsi)]).T
               for J in dataRan]
    # **Build single coefficient matrix (6x3)
    dXmat = 2 * \
        matrix([[dX[0] ** 2, 2 * dX[0] * dX[1], dX[1] ** 2] for dX in dXi])
    # **Build assmebled inverted matrix (3x6)
    aMatInv = inv(dXmat.T * dXmat) * dXmat.T
    # **Calculate resulting planar Lagrange strain for each instant
    # (E11 = E_vec[0,0], E12 = E_vec[1,0], E21 = E11, E22 = E_vec[2,0])
    E_vec = [aMatInv * v for v in ds2_dS2]
    # **Calculate The E33 component
    # ***det(2E + I) = 1
    """
    E_33 = (calculate based on det(F) = 1 → det(C) = → det(2E + I)=1)
    ((2*E11+1)*((2*E22+1)*(2*E33+1)
                -4*E23*E32)
     -2*E12*(2*E21*(2*E33+1)
             -4*E23*E31)
     +2*E13*(4*E21*E32-2*(2*E22+1)*E31)) = 1

    E13 = 0
    E23 = 0
    E31 = 0
    E32 = 0
    E33 = ((-2*E11-1)*E22+2*E12^2-E11)/((4*E11+2)*E22-4*E12^2+2*E11+1)
    (E11 = v[0,0], E12 = v[1,0], E21 = E11, E22 = v[2,0])
    """
    E33 = [((-2 * v[0, 0] - 1) * v[2, 0]
            + 2 * v[1, 0] ** 2 - v[0, 0]) / ((4 * v[0, 0] + 2) * v[2, 0]
                                             - 4 * v[1, 0] ** 2 + 2 * v[0, 0] + 1)
           for v in E_vec]
    pl.rc('text', usetex=True)
    # Plot
    time = [I * (1 / freq) for I in dataRan]
    lines = pl.plot(time, [[I[J, 0] for J in range(3)] for I in E_vec])
    lines = lines + pl.plot(time, E33)
    pl.legend(lines, [r"$E_{1,1}$",
                      r"$E_{1,2}$",
                      r"$E_{2,2}$",
                      r"$E_{3,3}$"])

    # pl.xticks([I for I in range(133)],
    #           [I/(freq*len(dataRan)) for I in dataRan])
    ticks = pl.xticks()
    pl.savefig("hw7P5a.svg")
    # pl.show()
    pl.clf()
    # pl.close()
    return E_vec, E33


def p5b():
    # * Lagrange strain by means of deformation gradient
    # 1. E = 0.5 * (C - I)
    # 2. C = transpose(F) * F
    # 3. F[i, j] = dx[i]/dX[j]
    # ** Estimate F[i,j] by dx[i]/dX[j]
    #          [ dx1  dx1  dx1 ]
    #          [ ---  ---  --- ]
#          [ dX1  dX2  dX3 ]
#          [               ]
#          [ dx2  dx2  dx2 ]
# F[i,j] = [ ---  ---  --- ]
#          [ dX1  dX2  dX3 ]
#          [               ]
#          [ dx3  dx3  dx3 ]
#          [ ---  ---  --- ]
#          [ dX1  dX2  dX3 ]
# Reported frequency
# freq = 30 Hz
# Number of samples
    N = 133
    # Reference vector (2D, because I need )
    dX = [20, 10, 1.5]
    # Final y-displacement
    h = 7.5
    # y-displacement increment
    dh = h / (N - 1)
    # vector of the deformed (horizontal; only in reference) line at
    # each instant (could be 2D)
    dx1 = [dX[0], 0, 0]
    # vertical component (could be 2D)
    # dx2 = [[dh*I, dX[1], 0] for I in range(N)]
    # theta = atan...?
    dx2 = [[dh * I, dX[1], 0] for I in range(N)]
    # *** Calculate area (to get volume, and solve for dx3)
    # A = cross(dx1, dx2), dx3_z = 1/A
    # z-component (to be calculated by means of the incompressibility;
    # V = 20*10*1.5)
    V = product(dX)
    dx3 = [[0, 0, V / cross(dx1[:2], dx2[I][:2])] for I in range(N)]

    # *** Deformation gradient

    def singleDefGrad(dX, dx1, dx2, dx3):
        F = matrix([[dx1[0] / dX[0], dx1[1] / dX[1], dx1[2] / dX[2]],
                    [dx2[0] / dX[0], dx2[1] / dX[1], dx2[2] / dX[2]],
                    [dx3[0] / dX[0], dx3[1] / dX[1], dx3[2] / dX[2]]])
        return F

    F = [singleDefGrad(dX, dx1, dx2[I], dx3[I]) for I in range(N)]
    # ** Cauchy stress
    C = [I.transpose() * I for I in F]
    # ** Lagrange strain
    E = [0.5 * (I - diag([1, 1, 1])) for I in C]
    return E


def main():
    # F, E, E_p = p1a()
    # E_eng, E_eng_eigvl = p1b(F)
    # epsilon, epsilon_eigvl = p1c(F)
    #
    # multiMohr([[E_eng_eigvl[0], E_eng_eigvl[2]],
    #            [epsilon_eigvl[0], epsilon_eigvl[2]]])
    # pl.savefig("hw7P1comp.svg")
    # print("See hw7pP1comp.svg")
    # pl.close()
    #
    # E_vec = p2d()
    E_vecs, E33s = p5a()
    E = p5b()

    freq = 1 / 30
    time = [I * (1 / freq) for I in range(len(E))]
    # lines = pl.plot([[I[J, 0] for J in range(3)] for I in E_vecs],
    #                 marker="+")
    # lines = pl.plot(E33s)
    # lines = lines + pl.plot(E33s)
    lines = lines + pl.plot([I[0, 0] for I in E],
                            [I[0, 1] for I in E],
                            [I[1, 1] for I in E],
                            [I[2, 2] for I in E])
    # pl.legend(lines, [r"$E_{1,1}$ from markers",
    #                   r"$E_{1,2}$ from markers",
    #                   r"$E_{2,2}$ from markers",
    #                   r"$E_{3,3}$ from markers",
    #                   r"$E_{1,1}$ from clamp",
    #                   r"$E_{1,2}$ from clamp",
    #                   r"$E_{2,2}$ from clamp",
    #                   r"$E_{3,3}$ from clamp"])
