Process Python finished
Python 3.4.3 (default, Nov 17 2016, 01:08:31)
[GCC 4.8.4] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> main()
Deformation tensor (F):
[[ 2.   1.   0. ]
 [ 0.5  1.   0. ]
 [ 0.   0.   1.8]]
Green-Lagrange tensor (E):
[[ 1.625  1.25   0.   ]
 [ 1.25   0.5    0.   ]
 [ 0.     0.     1.12 ]]
Eigenvalues of E:
[-0.30823201  1.12        2.43323201]
See hw7P1a.png
Engineering strain tensor (E_eng):
[[ 0.89058835  0.82199494  0.        ]
 [ 0.82199494  0.15079291  0.        ]
 [ 0.          0.          0.8       ]]
Eigenvalues of E_eng:
[-0.38069719  0.8         1.42207845]
See hw7P1b.png
The infinitesimal strain (\epsilon) is:
[[ 1.    0.75  0.  ]
 [ 0.75  0.    0.  ]
 [ 0.    0.    0.8 ]]
The eigenvalues of \epsilon are:
[-0.40138782  0.8         1.40138782]
See hw7P1c.png
See hw7pP1comp.png
The vector of the differences of square lengths is:
[[ 3.25]
 [ 1.  ]
 [-0.75]]
The coefficient matrix for strain is:
[[ 2  0  0]
 [ 0  0  2]
 [ 2 -4  2]]
The resulting Green-Lagrange strain vector is:
[[ 1.625]
 [ 1.25 ]
 [ 0.5  ]]
>>> 
