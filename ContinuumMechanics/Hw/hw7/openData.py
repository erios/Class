#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Abrir selección de archivo

def readWoHead(arch, cab=2, sep="\t", lin="\n"):
    '''Lee un archivo y quita las primeras cab cabeceras
    arch: literal indicando el nombre del archivo
    cab: entero número de lineas para saltar
    sep: separador de campos (columnas)
    lin: separador de líneas (el último caracter de la línea)'''
    with open(arch) as f:
        # max([0, cab -1]): asegurar que no se empieza en -1
        # Leer todas las líneas, quitarles el último caracter de línea
        #~ con lin y separar las columnas con sep
        res = [ I.replace(lin, "").split(sep)
          for I in f.readlines()[max([0, cab]):]]
        return res

def readByCols(arch, cols=None, **args):
    '''Lee un archivo y quita las primeras cab cabeceras
    arch: literal indicando el nombre del archivo
    cab: entero número de lineas para saltar
    sep: separador de campos (columnas)
    lin: separador de líneas (el último caracter de la línea)
    cols: lista de columnas a leer o vacío para todas las columnas'''
    if isinstance(cols, list):
        tmp = readWoHead(arch, **args)
        res = [ [ float() for II in cols] for I in tmp]
        for I in range(len(tmp)):
            for II in range(len(cols)):
                val = tmp[I][cols[II]]
                if isinstance(val, str) and len(val) > 0:
                    res[I][II] = float(val)
                else:
                    res[I][II] = float("nan")
    elif cols == None:
        tmp = readWoHead(arch, **args)
        res = [ [ float() for II in I] for I in tmp]
        for I in range(len(tmp)):
            for II in range(len(tmp[I])):
                val = tmp[I][II]
                if isinstance(val, str) and len(val) > 0:
                    res[I][II] = float(val)
                else:
                    res[I][II] = float("nan")
    return res
