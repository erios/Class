# Libraries to import...
from sys import path as syspath
from os import path as ospath
from os import getcwd
if not getcwd() + ".." in syspath:
    syspath.append(ospath.join(getcwd(), ".."))
    syspath.append(ospath.join(getcwd(), "../Hw"))
# ... and reload modules
from importlib import reload
# For regression
from scipy import odr
from scipy.integrate import odeint
from scipy.stats import linregress, pearsonr
# Numerical functions
from numpy import exp, inf, log10, mean, sqrt, ediff1d, dot, gradient
# Plot
# http://orgmode.org/worg/org-contrib/babel/languages/ob-doc-python.html
from matplotlib import pyplot as pl
from matplotlib.lines import lineStyles, lineMarkers
from itertools import cycle
# Utilities
# Read data
from hw7.openData import readByCols
# Fit data
from hw11.hw11 import lagrange2stretch, formatStrainStressPlot, lagrange2infstrain, stressMooneyRivlin1D, stressOrthotropicLinear1D


def resetplot():
    try:
        pl.clf()
    except:
        pass
    # fig_width_pt = 600.0  # Get this from LaTeX using \showthe\columnwidth
    # inches_per_pt = 1.0 / 72.27               # Convert pt to inch
    # golden_mean = (5 ** 0.5 - 1.0) / 2.0         # Aesthetic ratio
    # fig_width = fig_width_pt * inches_per_pt  # width in inches
    # fig_height = fig_width * golden_mean      # height in inches
    # fig_size = [fig_width, fig_height]
    params = {'backend': 'ps',
              'axes.labelsize': 16,
              'text.fontsize': 16,
              'legend.fontsize': 14,
              'xtick.labelsize': 14,
              'ytick.labelsize': 14,
              'text.usetex': True,
              # 'figure.figsize': fig_size
              }
    pl.rcParams.update(params)


def regr_odr(x, y, func, estim, extra=None, full=0):
    '''Calculates the coefficients that best fit a function for a given
    set of data using Orthogonal distance regression with scipy.odr
    x: array-like (see scipy.odr documentation)
    y: array-like (see scipy.odr documentation)
    func: a defined function that can be evaluated
    estim: estimated values for the coefficients
    extra: other values which need to be passed to
           the function. Note that the function needs to receive these
           _after_ the values of the independent variable.

    Example:  Get the linear regression of a set of data
    >>> def linea(c, x): return c[0]*x + c[1]
    >>>
    >>> # m, b = regr_odr
    >>> regr_odr([1.1, 0.8, 1.4], [2.3, 1.2, 2.9], linea, [1, 1])
    array([ 2.90618533, -1.06347053])
    >>>

    Ex 2:
    >>> x = [I for I in range(10)]
    >>> y = [I*2 for I in x]
    >>> def line_offset(coef, L, e):
    ...   return coef[0]*L + coef[1] - e
    >>> data = odr.RealData(x, y, sx=1e-99, sy=0.01)
    >>> model = odr.Model(prueba, extra_args=tuple([3]))
    >>> proc = odr.ODR(data, model, [2, 0])
    >>> out = proc.run()
    >>> out.beta
    array([ 2.,  3.])

    '''
    # Total least squares or Orthogonal distance regression
    # Orthogonal distance regression (scipy.odr)
    # Cookbook/Least Squares Circle - - SciPy
    # http://www.scipy.org/Cookbook/Least_Squares_Circle
    # Python for the Advanced Physics Lab
    # http://www.physics.utoronto.ca/~phy326/python/odr_fit_to_data.py
    data = odr.RealData(x, y, sx=1e-99, sy=0.01)
    if extra is None:
        model = odr.Model(func)
    elif isinstance(extra, tuple):
        # https://docs.scipy.org/doc/scipy/reference/generated/
        # scipy.odr.Model.html
        model = odr.Model(func, extra_args=extra)
    elif isinstance(extra, list):
        model = odr.Model(func, extra_args=tuple(extra))
    proc = odr.ODR(data, model, estim)
    out = proc.run()
    if full == 0:
        # Get the parameters only
        return out.beta
    if full == 1:
        # Get some useful information
        return out
    if full == 2:
        # Get all the statistical information from the regression:
        return odr.odr(func, estim, y, x, full_output=1)


def stressUndBlancoEtAl(coef, L):
    '''Calcualtes the undamaged stress of a uniaxial
    fibre, based on BlancoEtAl eq. 75

    k1 -- coef[0]
    k2 -- coef[1]'''
    L2 = L * L
    L4 = L2 * L2
    L2_12 = (L2 - 1) * (L2 - 1)
    # Make sure that the exponent is not bigger
    # than 200
    # k2 cannot be negative, otherwise, the energy is negative
    # k2 cannot be zero, otherwise the energy is infinite
    if isinstance(coef[1], float) and (coef[1] > 200 or coef[1] <= 0):
        # The exponent is greater than 0 or k2 is negative
        res = inf
    elif isinstance(coef[1], float):
        # The exponent is lower than 0 and k2 is positive
        res = 2 * coef[0] * (L4 - L2) * exp(coef[1] * L2_12)
    return res


def r_dot(t, k1, k2, chi, m, b):
    '''Calculates g/A = dr/dt. See project_report.org to understand g
    '''
    # TODO: Create array for L instead of calculating each time?
    L = m * t + b
    L2 = L * L
    L2_1 = L2 - 1
    L2_12 = L2_1 * L2_1
    return -2 * k1 * m * (L) * L2_1 * exp(k2 * L2_12) \
        / sqrt(k1 * (exp(k2 * L2_12) - 1) / k2)


def q_dotfunc(q, t, k1, k2, chi, A, m, b):
    '''Derivative of q (BlancoEtAl eq 46). See
    project_report.org to understand g'''
    g = r_dot(t, k1, k2, chi, m, b) * A
    # * Make sure that the exponent is not too big
    # ** Get the log10 of each variable
    if chi == 0:
        res = g
    elif g == 0 or q == 0:
        res = 0
    elif log10(abs(chi)) * log10(abs(q)) + log10(abs(g)) > 5:
        res = inf
    else:
        res = g * q ** chi
    return res


def q_ode_sol(k1, k2, chi, A, m, b, q0, t):
    '''Solves for the god-damn q (stress-like
    internal variable with internal hardening BlancoEtAl p.23)

    Keyword Arguments:
    t -- (list, numpy.array) monotonically increasing values of time
    m -- (float) slope of stretch.
    b -- (float) intercept of stretch.
    q0 -- (float) initial guess for the differential equation solver
    all the rest are parameters of the equation.
    '''
    # http://www.danham.me/r/2015/10/29/differential-eq.html
    sol_q = odeint(q_dotfunc, q0, t, args=(k1, k2, chi, A, m, b))
    return sol_q


def energyBlancoEtAl1D(coef, L):
    k1 = coef[0]
    k2 = coef[1]
    L2_1 = (L * L - 1)
    ex = k1 * L2_1 * L2_1
    if isinstance(ex, float):
        if ex > 200:
            res = inf
        else:
            res = 0.5 * k1 / k2 * (exp(ex) - 1)
    else:
        # Assume this is a numpy array
        res = ex.copy()
        for I, val in enumerate(res):
            # Replace values in array by addecuate values
            if val < 200:
                # If the exponent is lower than 200, calculate
                res[I] = 0.5 * k1 / k2 * (exp(val) - 1)
            else:
                # If the exponent is not lower than 200, set to inf
                # (otherwise it becomes too big)
                res[I] = inf
    return res


def rBlancoEtAl(coef, L):
    k1 = coef[0]
    k2 = coef[1]
    ro = coef[2]
    # k2 cannot be negative, otherwise, the energy
    # is negative
    # k2 cannot be zero, otherwise the energy is
    # infinite
    if k2 > 0:
        E = energyBlancoEtAl1D([k1, k2], L)
    else:
        E = inf
    if isinstance(E, float):
        res = max(ro, sqrt(2 * E))
    elif not isinstance(E, list):
        # Assume E is a numpy array
        # Create an array of the same length
        res = E.copy()
        # Set all values of res by the max between each E or ro
        res[:] = [max(ro, sqrt(2 * I)) for I in E]
    return res


def damageBlancoEtAl(q, r):
    return 1 - q / r


def stressDamBlancoEtAl(coef, L, k1, k2, m, b, time, stressUnd):
    '''
    chi = coef[0]
    A = coef[1]
    q0 = coef[2]
    ro = coef[3]

    k1 -- (float) elastic constant (Pa)
    k2 -- (float) elastic constant (Pa/Pa)
    m -- (float) slope of the stretch (stretch needs to be
         monotonically increasing, according to
         BlancoEtAl). Calculate with scipy.stats.linregress
    b -- (float) intercept of the stretch (stretch needs to be
         monotonically increasing, according to
         BlancoEtAl). Calculate with scipy.stats.linregress
    time -- (list) monotonic values of time to
            calculate monotonic stretch
    stretch11 -- (list) monotonically increasing stretch
    '''
    chi = coef[0]
    A = coef[1]
    q0 = coef[2]
    ro = coef[3]
    # * Calculate r (waviness parameter)
    r = rBlancoEtAl([k1, k2, ro], L)
    # * Get current value of q
    # ** Solve non-linear differential equation for q
    # TODO: this is very inefficient (calculates
    #       the ode each time the parameters are fit)
    q = q_ode_sol(k1, k2, chi, A, m, b, q0, time)
    if isinstance(L, float):
        # ** Find the index of the current stretch
        ind_L = stretch11.index(L)
        # ** Set current value of q from the
        #    current value of stretch
        q_cur = q[ind_L]
        # * Calculate damaged stress based on
        #   undamaged stress, q and r
        #   TODO: this is slow. I already have
        #         some values of undeformed stress
        res = damageBlancoEtAl(q_cur, r) \
            * stressUndBlancoEtAl([k1, k2], L)
    else:
        # Assume L is a numpy array (or list)
        # Use dot product to multiply element by
        # element (note the .T for transpose)
        res = dot(damageBlancoEtAl(q, r),
                  stressUndBlancoEtAl([k1, k2],
                                      L).T)
    return res


def sqrtmse(a, b):
    """Calculates the squared root of the mean of the squared errors
    (good luck with missing data)
    Keyword Arguments:
    a -- list
    b -- list (same length as a)
    """
    dif = [a[I] - b[I] for I in range(len(a))]
    sqrtmse = sqrt(mean([I * I for I in dif]))
    return sqrtmse


def stressMyDamage(coef, L):
    L2 = L * L
    L4 = L2 * L2
    L2_12 = (L2 - 1) * (L2 - 1)
    # Make sure that the exponent is not bigger
    # than 200
    # When a fit is run, this is an array, not a
    # number
    if isinstance(coef[1], float) and coef[1] > 200:
        res = inf
    else:
        res = (1 - coef[2] * log10(L * coef[3])) * \
            2 * coef[0] * (L4 - L2) * exp(coef[1] * L2_12)
    return res


def nonLinearCorr(a, b, div=None):
    """Calculates the correlation coefficient between
       non-linear data (eta squared)

    Keyword Arguments:
    a   -- list (independent variable; ordered;
           increasing)
    b   -- list (dependent variable; same length as a)
    div -- (default None) "no fewer than 6
           categories and no more than 12
           [HinkleWiersmaJurs]"
    """
    if div is None:
        div = 8
    # length of a
    len_a = len(a)
    # Splitting indices (independent variable)
    nsplit = int(len_a / div)
    mod_split = len_a % div
    if nsplit == 0:
        split_ind = [len_a]
    elif nsplit * div < len_a:
        split_ind = [(I + 1) * nsplit for I in range(div)] + [len_a]
    elif nsplit * div == len_a:
        split_ind = [(I + 1) * nsplit for I in range(div)]

    # Total mean for dependent variable
    mean_all = mean(b)
    # Initialise values
    SSB = 0
    SST = 0

    # Calculate weighted residual by group
    def bygroupSSB(b, ini, last):
        group = b[ini:last]
        ssb = (mean(group) - mean_all)
        return (last - ini) * ssb * ssb
    for I in range(len(split_ind)):
        ini = I * nsplit
        last = split_ind[I]
        SSB += bygroupSSB(b, ini, last)

    # Calcualte residuals
    # By individual measurement
    SST = sum([(I - mean_all) * (I - mean_all) for I in b])
    return SSB / SST


def find_ini_jump(a, thres=None):
    if thres is None:
        thres = 5e-5
    # * Calculate the differences in the data from one instant to the
    #   next
    deltas = ediff1d(a)
    # * Find the first where there is an increase greater than the
    #   threshold
    delta = 0
    ini = 0
    # **** Loop over the list until a value higher
    #      than or equal to the threshold is found
    while delta < thres and ini < len(deltas):
        delta = abs(deltas[ini])
        ini += 1
    return min([len(deltas) - 1, ini + 1])


def mono_stretch_linreg(stretch11, dt=None):
    '''Create a monotonic stretch from the experimental data
    dt -- if None, set to 0.01 automatically (independent var)
    '''
    if dt is None:
        dt = 0.01
    # ** Create fake time (approximation)
    #    TODO: get real time (I set 0.01 by looking at the data)
    time = [dt * I for I in range(len(stretch11))]
    # ** Get slope and intercept of the ramp of stretch
    m, b = linregress(time, stretch11)[:2]
    return m, b, time


def stressVoigt1D(coef, e, e_dot):
    """Calculates the first component of stress
    (sigma11) for a Voigt viscoelastic material
    subject to a 1D strain (e) using the equation:

       sigma11  = k * e + c * e_dot

    where e_dot is the derivative of e with
    respect to time, k is a linear constant
    (spring equivalent), c is a viscosity constant
    and e is the strain.

    Keyword Arguments:
    coef  -- k = coef[0], c = coef[1]
    e     -- (float) value of infinitesimal strain
    e_dot -- (float) value of derivative of strain
    """
    k = coef[0]
    c = coef[1]
    return k * e + c * e_dot


def stress_dotMaxwell(sigma11, e_dot, k, c):
    '''Maxwell model:
    diff(sigma, t) = k * (e - sigma)/c'''
    return k * (e_dot - sigma11 / c)


def stressMaxwell_odesol(coef, epsilon_dot, sigma0):
    '''ODE solution to Maxwell model'''
    # First order system (for numerical solution)
    k, c = coef
    sigma11 = odeint(stress_dotMaxwell, sigma0, epsilon_dot,
                     args=(k, c), printmessg=False)
    return sigma11[:, 0]


def stressMaxwellLaplace(coef, epsilon_inf, t):
    '''Calculate the analytical solution to Maxwell's
    model:

    sigma = k * (dirac(t) - k/c * exp(-k/c*t)) * e(t)

    where sigma is Cauchy stress and e(t) is the
    infinitesimal strain. Dirac's delta dirac(t)
    is not considered at all

    Keyword Arguments:
    k = coef[0] -- elasticity constant
    c = coef[1] -- damping constant
    epsilon_inf -- infinitesimal strain
    t           -- time (same type as epsilon_inf)

    TODO: implement Dirac's delta
    '''
    k = coef[0]
    c = coef[1]
    tau = - k / c
    # Make sure that the exponent is not too big
    if isinstance(t, float) and tau * t > 200:
        res = inf
    elif isinstance(t, float) and tau * t < 200:
        res = k * tau * exp(tau * t) * epsilon_inf
    elif isinstance(t, list) \
            and not isinstance(epsilon_inf, list):
        # Assume that epsilon_inf is a numpy array
        # (ODR)
        # Make a copy
        res = epsilon_inf.copy()
        for I in range(len(res)):
            if tau * t[I] > 200:
                res[I] = inf
            else:
                res[I] = k * tau * exp(tau * t[I]) \
                    * epsilon_inf[I]
    return res


def maxwell(coef, t, c1, m):
    k = coef[0]
    c = coef[1]
    tau = k / c
    # Make sure that the exponent is not too big
    if isinstance(t, float) and abs(tau * t) > 200:
        res = inf
    elif isinstance(t, float) and abs(tau * t) <= 200:
        res = exp(-tau * t) * (c * m * exp(tau *
                                           t) + c1)
    elif not isinstance(t, list):
        # Assume that epsilon_inf is a numpy array
        # (ODR)
        # Make a copy
        res = t.copy()
        for I in range(len(res)):
            if tau * t[I] > 200:
                res[I] = inf
            else:
                res[I] = exp(-tau * t[I]) \
                    * (c * m
                       * exp(tau * t[I])
                       + c1)
    return res

# ************************************************
# *                  Undamaged                   *
# ************************************************
# Read data
datafname = "./F28LMTL7X3.stress-strain.csv"
strain_stress = readByCols(datafname, sep="\t", cab=5)
# Extract second column (stress)
stress = [I[1] for I in strain_stress]
max_stress = stress.index(max(stress))
stress = stress[:max_stress]
# Extract first column (strain)
epsilon_lag = [I[0] for I in strain_stress[:max_stress]]
# Calculate stretch (lambda = √(2*ε_lagrange + 1))
stretch11 = lagrange2stretch(epsilon_lag)
# Plot for different initial guess values
inivalfit = [[3, 0.0001]]
# Clear figure
resetplot()
# Plot experimental data
pl.plot(epsilon_lag, stress, label="Experiment")
for I in inivalfit:
    # Fit parameters
    k1, k2 = regr_odr(stretch11, stress, stressUndBlancoEtAl, I)
    # Calculate estimated values with the coefficients
    stressUnd = [stressUndBlancoEtAl([k1, k2], I) for I in stretch11]
    # Calcualte correlation coefficient
    # Pearson
    # Rsq, p_val = pearsonr(stress, res)
    # Correlation coefficient (eta squared)
    # etasq = nonLinearCorr(stress, res)
    # Calculate squared root of the mean of the squared differences
    sqmse = sqrtmse(stress, stressUnd)
    # Format label
    lab = r"$k_1: {0:0.3f}$; $k_2: {1:0.3f}$".format(k1, k2) +\
          r"; $\sqrt{MSE}:" + " {0:0.3f}$".format(sqmse) + "\n" +\
          r"$R^{2}:$ " +\
          r"${0:0.3f}$".format(pearsonr(stress, stressUnd)[0])

    # r"; $R^2: {0:0.3f}$".format(Rsq) +\
    # r"; $\eta^2: {0:0.3f}$".format(etasq)
    # Plot the results
    pl.plot(epsilon_lag, stressUnd, label=lab)
# Format the plot
formatStrainStressPlot()
pl.suptitle(r"Stress fitted to BlancoEtAl uniaxial stress")
# pl.title(r"Longitudinal test")
# Save figure
fname = "./stressUndBlancoEtAl.png"
pl.savefig(fname)

# Sensitivity analysis for undamaged **********
# Clear figure
resetplot()
# Plot experimental data
pl.plot(epsilon_lag, stress, label="Experiment")
# Factors for each coefficient
F = [-0.5, 0.5, 1]
# Values of the coefficients to be changed
K = [k1, 0.5]
labels = [r"k_1", r"k_2"]
styles = cycle([":", "-.", "--"])
markers = cycle(["*", "+", "x", ".", "o", "d"])
for f in F:
    # Temporarily store the values for future modification
    tmpk = [I for I in K]
    for II in range(len(K)):
        # Modify coefficient values
        tmpk[II] = (1 + f) * K[II]
        # Calculate estimated values with the coefficients
        tmpstress = [stressUndBlancoEtAl(tmpk, I) for I in stretch11]
        # Calcualte correlation coefficient
        # Pearson
        # Rsq, p_val = pearsonr(stress, tmpstress)
        # Correlation coefficient (eta squared)
        # etasq = nonLinearCorr(stress, tmpstress)
        # Calculate squared root of the mean of the squared differences
        sqmse = sqrtmse(stress, tmpstress)
        # Format label
        lab = r"$k_1: {0:0.3f}$; $k_2: {1:0.3f}$".format(tmpk[0], tmpk[1]) +\
            r"; $\sqrt{MSE}:" + " {0:0.3f}$".format(sqmse) + "\n" +\
            r"$R^{2}:$ " +\
            r"${0:0.3f}$".format(pearsonr(stress, stressUnd)[0])
        # r"; $\eta^2: {0:0.3f}$".format(etasq)
        # Plot the results
        pl.plot(epsilon_lag, tmpstress, label=lab, linewidth=2,
                ls=styles.__next__()  # , marker=markers.__next__()
                )
# Format the plot
formatStrainStressPlot()
pl.suptitle(r"Stress fitted to BlancoEtAl uniaxial stress")
pl.title(r"Longitudinal test")
# Save figure
fname = "./stressUndBlancoEtAlComp" + ".png"
pl.savefig(fname)


# *******************************
# *  Other models (undamaged)   *
# *******************************
# Calculate infinitesimal strain from Lagrange strain
# strain_inf = √(2*ε_lagrange + 1) - 1
epsilon_inf = lagrange2infstrain(epsilon_lag)
# Estimate time (from the data, it seems that dt = 0.01)
dt = 0.01
# Estimate time derivative of strain with numpy.gradient
# (central difference theorem)
epsilon_inf_dot = gradient(epsilon_inf, dt)
# Clear figure
resetplot()
# https://stackoverflow.com/questions/16150819/common-xlabel-ylabel-for-matplotlib-subplots
# pl.gcf().text(0.5, 0.04, r'Lagrange strain (mm/mm)', ha='center')
# pl.gcf().text(0.04, 0.5, r'Cauchy stress (kPa)',
#               ha='center', rotation='vertical')
# Voigt ***********************
# Fit paramters
k, c = regr_odr(
    epsilon_inf, stress, stressVoigt1D, [10, 10],
    extra=[epsilon_inf_dot])
# Calcualte stress based on fitted parameters
tmpstress = [stressVoigt1D([k, c], epsilon_inf[I], epsilon_inf_dot[I])
             for I in range(len(epsilon_inf))]
# Format label
lab = r"Voigt " + "\n" +\
      r"$k={0:0.3f}$, ".format(k) + "\n" +\
      r"$c={0:0.3f}$".format(c) + "\n" +\
      r"$R^{2}:$ " +\
      r"${0:0.3f}$".format(pearsonr(stress, tmpstress)[0])
# Set subplot
pl.subplot(311)
# Plot experimental data
pl.plot(epsilon_lag, stress, linewidth=2)
# Plot model data
pl.plot(epsilon_lag,
        tmpstress,
        label=lab)
pl.legend(framealpha=0.5, loc='upper left')

# Maxwell ********************
# Fit paramters
# k, c = regr_odr(epsilon_inf_dot, stress, stressMaxwell_odesol, [1, 10],
#                 extra=[stress[0]])
# Calcualte stress based on fitted parameters
# tmpstress = odeint(stress_dotMaxwell, stress[0], epsilon_inf_dot,
#                    args=(k, c), printmessg=False)
# Format label
# lab = r"Maxwell " + "\n" +\
#       r"$k: {0:10.3f}$".format(k) + "\n" +\
#       r"$c: {0:10.3f}$".format(c) + "\n" +\
#       r"$R^{2}:$ " +\
#       r"${0:0.3f}$".format(pearsonr(stress, tmpstress.T[0])[0])
# Set subplot
# pl.subplot(222)
# Plot experimental data
# pl.plot(epsilon_lag, stress, linewidth=2)
# Plot model data
# pl.plot(epsilon_lag,
#         tmpstress,
#         label=lab)
# pl.legend(framealpha=0.5, loc='upper left')

# Mooney-Rivlin **************************
# Fit parameters
guess = [10, 100]
c1, c2 = regr_odr(stretch11, stress, stressMooneyRivlin1D, guess)
# Calcualte stress based on fitted parameters
tmpstress = [stressMooneyRivlin1D([c1, c2], I)
             for I in stretch11]
# Format label
lab = r"Mooney-Rivlin" + "\n" +\
      r"$c_1: {0:10.3f}; $".format(c1) + "\n" +\
      r"$c_2: {0:10.3f}$".format(c2) + "\n" +\
      r"$R^{2}:$ " +\
      r"${0:0.3f}$".format(pearsonr(stress, tmpstress)[0])
# Set subplot
pl.subplot(312)
# Plot experimental data
pl.plot(epsilon_lag, stress, linewidth=2)
# Plot model data
pl.plot(epsilon_lag,
        tmpstress,
        label=lab)
pl.legend(framealpha=0.5, loc='upper left')

# Linear elastic ********************
# Fit parameters
guess = [1000]
E1, = regr_odr(epsilon_inf, stress, stressOrthotropicLinear1D, guess)
# Calcualte stress based on fitted parameters
tmpstress = [stressOrthotropicLinear1D([E1], I)
             for I in epsilon_inf]
# Format label
lab = r"Linear elastic: " + "\n" +\
      r"$E_1: {0:10.3f}$".format(E1) + "\n" +\
      r"$R^{2}:$ " +\
      r"${0:0.3f}$".format(pearsonr(stress, tmpstress)[0])
# Set subplot
pl.subplot(313)
# Plot experimental data
pl.plot(epsilon_lag, stress, linewidth=2)
# Plot model data
pl.plot(epsilon_lag,
        tmpstress,
        label=lab)
pl.legend(framealpha=0.5, loc='upper left')

for I in [1, 2, 3]:
    pl.subplot(310 + I).grid(True)
# * Format the plot
# formatStrainStressPlot()
# pl.suptitle(r"Comparison to other models")
# pl.title(r"Longitudinal test")
# Save figure
pl.tight_layout()
fname = "./stressDamOtherComp" + ".svg"
pl.savefig(fname)


# ************************************************
# *    Show effect of epsilon_dot on Voigt       *
# ************************************************
pl.clf()
pl.subplot(211)
lab = r"$k\," +\
      r"\sqrt{2\,\varepsilon_{\mathrm{lag}} + 1} - 1$"
pl.plot([dt * I for I in range(len(epsilon_inf))],
        [k * I for I in epsilon_inf],
        label=lab)
# * String for k epsilon without units
lab_ke = r"$k\,\varepsilon_{\mathrm{inf}}$"
# * String for c epsilon_dot without units
lab_cedot = r"$c\,\frac{d}{dt}" +\
    r"\varepsilon_{\mathrm{inf}}$"
pl.ylabel(lab_ke + r"~$(\mathrm{kPa})$")
pl.legend(framealpha=0.5, loc="upper left")
pl.title(r"$\sigma_{\mathrm{Voigt}} = " + lab_ke[1:-1] +
         r" + " + lab_cedot[1:])
pl.subplot(212)
pl.plot([dt * I
         for I in range(len(epsilon_inf_dot))],
        [c * I for I in epsilon_inf_dot])
pl.ylabel(lab_cedot + r"~$(\mathrm{kPa})$")
pl.xlabel(r"time (s)")
pl.suptitle(r"Effect of infinitesimal strain on" +
            r" Voigt's model")
fname = "voigtArtifacts.png"
pl.savefig(fname)

# * Calculate Maxwell's stress with idealised data
# * Create fake epsilon_inf
# ** Create a monotonic stretch from the
# ** experimental data
# m, b, time = mono_stretch_linreg(stretch11, dt=0.01)
# epsilon_inf_fake = [m * I + b for I in time]
# guess = [1000, 100]
# k, c = regr_odr(epsilon_inf_fake,  # independent var
# stress,  # what we try to fit
# stressMaxwellLaplace,  # function to
# fit
# guess,  # initial guess
# extra=tuple([time])  # ODR sends two args
# to the function, the rest are
# specified with this
#                 )
# tmpstress = [stressMaxwellLaplace([k, c],
#                                   epsilon_inf_fake[I],
#                                   time[I])
#              for I in range(len(time))]

# * Calculate Maxwell's stress with idealised data
# * Create fake epsilon_inf
# ** Create a monotonic stretch from the
# ** experimental data
time = [0.01 * I for I in range(len(stress))]
m, b = linregress(time, epsilon_inf)[:2]
res = regr_odr(time,
               stress,
               maxwell,
               [10, 10],
               extra=(0, m))
tmpstress = [maxwell(res,
                     time[I],
                     0, m)
             for I in range(len(time))]
pl.plot(epsilon_lag, stress)
pl.plot(epsilon_lag, tmpstress)
pl.show()

k = 10
c = 10
tau = k / c
pl.subplot(311)
pl.plot([exp(-tau * t) for t in time])
pl.subplot(312)
pl.plot([m / tau * exp(tau * t) for t in time])
pl.subplot(313)
pl.plot([exp(-tau * t) * m / tau * exp(tau * t) for t in time])
pl.show()

# ************************************************
# *                  Damaged 1                   *
# ************************************************
# Read data
datafname = "./F28LMTL7X3.stress-strain.csv"
strain_stress = readByCols(datafname, sep="\t", cab=5)
# Extract first column (strain)
epsilon_lag = [I[0] for I in strain_stress]
# Extract second column (stress)
stress = [I[1] for I in strain_stress]
# Calculate stretch (lambda = √(2*ε_lagrange + 1))
stretch11 = lagrange2stretch(epsilon_lag)

# * Find where the stretch starts to increase
# ** Find where there is the first jump in the difference
#    of stretch bigger than 5e-5 (look at the data, and check what
#    is a good value for this)
stretch_ini = find_ini_jump(stretch11)
# * Find where the stress is no longer flat at the end
# ** Invert the order of the data (assuming that it ends flat)
inv_stress = [I for I in stress]
inv_stress.reverse()
# ** Find where there is the first jump in the difference
# of stress bigger than 0.02 (look at the data, and check what
# is a good value for this)
# stress_fin = find_ini_jump(inv_stress, .03) - 2
# Pick where the material fails
stress_fin = len(stress) - 1900

# * Crop experimental data
epsilon_lag = epsilon_lag[stretch_ini:-stress_fin]
stress = stress[stretch_ini:-stress_fin]
stretch11 = stretch11[stretch_ini:-stress_fin]

# * Create a monotonic stretch from the experimental data
m, b, time = mono_stretch_linreg(stretch11, dt=0.01)

# * Fit coefficients to damaged stress
# ** Set various initial guesses
inivalfit = [[1,   # chi
              1,   # A
              1,   # q0
              1],  # ro
             [1e-3, 100, 10, 100],
             [42, 17, 106, 0.7],  # Thanks Adelle!!
             [100, 1e-3, 100, 10]]
# Set temporary variable to compare variances

# ** By optimization


# def var_from_fit(guess, stretch11, stress, k1, k2, m, b, time):
#     res = regr_odr(
#         stretch11, stress, stressDamBlancoEtAl, guess,
#         (k1, k2, m, b, time, stretch11), full=1)
#     return res.res_var
#
# from scipy.optimize import minimize
# optim_guess = minimize(var_from_fit, inivalfit[1],
#                        args=(stretch11, stress,
#                              k1, k2, m, b, time),
#                        method='COBYLA')
# from numpy import array
# a = stressDamBlancoEtAl(optim_guess.x, array(stretch11), k1, k2, m, b,
#                         time, array(stressUnd))


# def maxwell_odesol(coeff, epsilon_dot, sigma0):
#     '''ODE solution to Maxwell model using ODR and odeint'''
# First order system (for numerical solution)
#     sigma11 = odeint(sigmadot, sigma0,
#                      epsilon_dot,
#                      args=(k, c), printmessg=False)
#     return sigma11.ravel()


# var = inf
# for I, guess in inivalfit:
# ** Fit coefficients:
#     tmp = regr_odr(
#         stretch11, stress, stressDamBlancoEtAl, guess,
#         (k1, k2, m, b, time, stretch11), full=1)
#     if
# chi, A, q0, ro = res_odr.beta
# Wrap coefficients
# coef = chi, A, q0, ro
# Get resulting damaged stress
# from numpy import array
# a = stressDamBlancoEtAl(coef, array(stretch11), k1, k2, m, b,
#                         time, array(stressUnd))

# Damage 2 ******************************


# My damage ******************************
# '''# Set initial guess
# inivalfit = [30, 50, 5, 2]
# Clear figure
# resetplot()
# Plot experimental data
# pl.plot(epsilon_lag, stress, label="Experiment")
# Fit parameters
# k1, k2, k3, k4 = regr_odr(stretch11, stress,
#                           stressMyDamage,
#                           inivalfit)
# Format label
# lab = r"$k_1$: {0:10.3e}; $k_2$: {1:10.3e};\\$k_3$: {2:10.3e}; $k_4$: {3:10.3e}".format(
#     k1, k2, k3, k4)
# Plot the results
# pl.plot(epsilon_lag, [stressMyDamage([k1, k2, k3, k4], I)
#                       for I in stretch11], label=lab)
# Format the plot
# formatStrainStressPlot()
# pl.suptitle(r"Stress fitted to my custom function for uniaxial stress")
# pl.title(r"Longitudinal test")
# Save figure
# fname = "stressMyFunction.png"
# pl.savefig(fname)
# '''

# Energy ******************************
# def energyBlancoEtAl1D(coef, L):
#     k1 = coef[0]
#     k2 = coef[1]
#     tmp = (L * L - 1)
#     ex = k1 * tmp * tmp
#     if isinstance(k1, float) and ex > 200:
#         res = inf
#     else:
#         res = 0.5 * k1 / k2 * (exp(ex) - 1)
#     return res
#
#
# def rho(Psi):
#     return [sqrt(2 * I) for I in Psi]
#
#
# def chainRuleDeriv(f, g, x):
#     """Calculates the chain rule derivative:
#     d[f(g(x))]/d[g(x)] · d[g(x)]/dx
#     Keyword Arguments:
#     f -- (list, array) f(g(x))
#     g -- (list, array) g(x)
#     x -- (list, array) x
#
#     TODO: make this central difference theorem
#     """
#     dx = ediff1d(x)
#     dg = ediff1d(g)
#     df = ediff1d(f)
#     dfdg = df / dg
#     dgdx = dg / dx
#     return dfdg * dgdx
