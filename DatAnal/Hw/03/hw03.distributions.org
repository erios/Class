#+TITLE: Homework 3.
#+SUBTITLE: Data Analysis (MATH 471)
#+AUTHOR: Edgar Rios
* Configuration                                            :noexport:ARCHIVE:
** General options

- Export table of contents, export equations as pictures,
  export author
  #+OPTIONS: toc:nil author:t date:t tex:t LaTeX:t -:t e:t

- Do not evaluate R blocks, and export results only
  #+PROPERTY: header-args:R :session *R* :eval no-export :exports results

- Style, symbols, etc.
  # #+INCLUDE: "../Reports/headers.org"
  #+LATEX_HEADER: \input{/home/edgar/.emacs.d/plugins/symbols.tex}
* Documentation                                   :noexport:
#+BEGIN_QUOTE
The skewness can also be computed as g1 = the average value of z3, where z is the familiar z-score, z = (x−x̅)/σ. Of course the average value of z is always zero, but what about the average of z3? Suppose you have a few points far to the left of the mean, and a lot of points less far to the right of the mean. Since cubing the deviations gives the big ones even greater weight, you’ll have negative skewness. It works just the opposite if you have big deviations to the right of the mean.
#+END_QUOTE
https://brownmath.com/stat/shape.htm

* P1

A consumer testing agency wants to evaluate the claim made by a manufacturer of discount tires. The manufacturer claims that their tires can be driven at least 35,000 miles before wearing out. To determine the average number of miles that can be obta ined from the manufacturer’s tires, the agency randomly selects 60 tires from the manufactu rer’s warehouse and places the tires on 15 cars driven by test drivers on a 2-mile oval track. T he number of miles driven (in thousands of miles) until the tires are determined to be worn ou t is given in the following table.

#+BEGIN_SRC R :results none
  x <- c(25, 27, 35, 42, 28, 37, 40, 31,
         29,33, 30, 26, 31, 28, 30)
#+END_SRC

** a
Describe the descriptive statistics about the variable ’Miles driven’. Also describe the shape of its distribution.

[[pdfview:/home/edgar/Documentos/Descargas/Ricci-distributions-en.pdf::7][Skewness]] can be [[http://www.itl.nist.gov/div898/handbook/eda/section3/eda35b.htm][calculated]] in R in the [[pdfview:/home/edgar/Documentos/Biblio/Mate/Applied_Multivariate_Statistics_with_R%5BDr.Soc%5D.pdf::145][following way]] (eq. [[ref:prueba]])[fn::run ~install.packages("e1071")~ and see ~help(skewness)~] (see [[file:~/Documentos/Biblio/Physics%20Complete/Modern%20Physics/Mathematical%20Physics/Fundamental%20Numerical%20Methods%20and%20Data%20Analysis%20-%20G.%20Collins.pdf][/Fundamental numerical methods and data analysis/ by George W. Collins, II, 2003]]).

#+NAME: prueba
\begin{align}
  m(x, r)
  & = \frac{\sum_{i=1}^{n}{\left(\left(x_{i} - \bar{x}\right)^{r}\right)}}{n}
  \\
  \gamma_{1}
  & = \frac{\sum_{i=1}^{n}{\frac{(Y_{i} - \bar{Y})^{3}}{n}}} {s^{3}}
  \\
  & = \frac{m(x, 3)}{m(x, 2)^{ \frac{3}{2}}}
  \\
  G_{1}
  & = \gamma_{1}\, \frac{\sqrt{n \, (n-1)}}{n - 2}
\end{align}

where G_{1} is for a sample, and \gamma_{1} is for [[https://brownmath.com/stat/shape.htm][population]].

#+BEGIN_SRC R :results output
  ## http://www.r-tutor.com/elementary-statistics/numerical-measures/kurtosis
  library(e1071)
  ## From help(skewness) and
  ## http://www.itl.nist.gov/div898/handbook/eda/section3/eda35b.htm
  ## G_1 = g_1 * sqrt(n(n-1)) / (n-2)
  m.r <- function (x, r){
      sum((x - mean(x))^r) / length(x)
  }
  g.1 <- function (x){
      m.r(x, 3) / m.r(x, 2)^(3/2)
  }
  G.1 <- function (x){
      n <- length(x)
      g.1 <- m.r(x, 3) / m.r(x, 2)^(3/2)
      g.1 * sqrt(n * (n-1)) / (n-2)
  }
  res <- c(mean(x), sd(x), skewness(x, type = 2),
           kurtosis(x, type = 2))
  names(res) <- c("mean", "std. dev.", "skewness", "kurtosis")
  res
#+END_SRC

#+RESULTS:
:        mean   std. dev.    skewness    kurtosis
: 31.46666667  5.04078603  0.88762739 -0.01005782

From the skewness (greater than 0), we see that the the right tail is longer than the left (*skewed to the right*; data is concentrated to the left). The value between \(\frac{1}{2}\) and 1 indicates that the data is *moderately skewed*.

The negative kurtosis (platykurtic) indicates *shorter tails* than a normal distribution (the frequencies of the extreme values are below the normal distribution). The value close to 0 indicates a *small kurtosis*. Some people refer to this measure of kurtosis as /excess kurtosis/, because it is the difference to the normal distribution.

#+NAME: p1.a.quantiles.R
#+BEGIN_SRC R :results output
  summary(x)
#+END_SRC

The quantiles are
#+RESULTS: p1.a.quantiles.R
:    Min. 1st Qu.  Median    Mean 3rd Qu.    Max.
:   25.00   28.00   30.00   31.47   34.00   42.00

The inter-quantile range (IQR) is src_R[:exports results]{IQR(x)} {{{results(=6=)}}}.

#+BEGIN_SRC R :file p1-a.png :results output graphics
  hist(x)
#+END_SRC

#+ATTR_LATEX: :width 0.5\textwidth
#+RESULTS:
[[file:p1-a.png]]

** b

Estimate a population distribution of ’Miles driven’ using Q-Q plot

#+HEADER: :file p1.b.png :results output graphics
#+BEGIN_SRC R
  qqnorm(x)
  qqline(x)
#+END_SRC

#+NAME: fig-p1.b.Q-Q.plot
#+ATTR_LATEX: :width 0.5\textwidth
#+RESULTS:
[[file:p1.b.png]]

It seems that the data (fig. [[fig-p1.b.Q-Q.plot]]) belongs to a gamma distribution, because the right tail is longer (above the Q-Q line in the upper region), and the lower tail is shorter (above the Q-Q line in the lower region).

To test this hypothesis, let us create a Q-Q plot for the gamma distribution by means of the [[file:~/Documentos/Class/DatAnal/Notes/notes-DatAnal.org::#distr.definition.of.u][definition of u]]:

 \(u = \frac{i - \frac{1}{2}}{n}\), i: ordered index

and plot the sorted data against the gamma quantile function of \(u\).

#+NAME: p1.b.gamma.Q-Q.plot.R
#+BEGIN_SRC R :file p1.b.gamma.Q-Q.plot.png :results output graphics
  u <- (seq(1:length(x)) - 0.5)/length(x)
  Q.G <- qgamma(u, 5)

  plot(sort(x), Q.G, main = "Gamma Q-Q plot", ylab = "Q(gamma(u))")
#+END_SRC

The data looks rather linear (fig. [[fig-p1.b.gamma.Q-Q.plot]]), which means that the gamma distribution is a good pick.
#+NAME: fig-p1.b.gamma.Q-Q.plot
#+ATTR_LATEX: :width 0.5\textwidth
#+RESULTS: p1.b.gamma.Q-Q.plot.R
[[file:p1.b.gamma.Q-Q.plot.png]]

* P2

Read [[pdfview:/home/edgar/Documentos/Biblio/Mate/R.%20Lyman%20Ott,%20Micheal%20T.%20Longnecker-An%20Introduction%20to%20Statistical%20Methods%20and%20Data%20Analysis-Brooks%20Cole%20(2015).pdf::198][Ch.4.10]] in the textbook and do the following questions from the textbook: pp. 222-223: 4.65, 4.66, 4.70, 4.72

** 4.65

Let y be a random variable having a normal distribution with a mean equal to 250 and a standard deviation equal to 50. Find the following probabilities

a. \(P(y > 250)\)
   #+BEGIN_SRC R
     1 - pnorm(250, 250, 50)
   #+END_SRC

   #+RESULTS:
   : 0.5

b. \(P(y > 150)\)
   #+BEGIN_SRC R
     1 - pnorm(150, 250, 50)
   #+END_SRC

   #+RESULTS:
   : 0.977249868051821

c. \(P(150 < y < 350)\)
   #+BEGIN_SRC R
     pnorm(350, 250, 50) - pnorm(150, 250, 50)
   #+END_SRC

   #+RESULTS:
   : 0.954499736103642

d. [[file:P2.xoj][Find k such that]] \(P(250 - k < y < 250 + k) = 0.60\)

   p = 0.6

   \(P(\mu - k <= y <= \mu + k) = p\)
   \(P(y <= \mu + k) - P(y <= \mu - k) = p\)

   Q_{N}\left(\frac{1-p}{2}\right) ?

   #+NAME: p2d.R
   #+BEGIN_SRC R
     p2d.res <-qnorm((1 - 0.6)/2, 250, 50)
     p2d.res
   #+END_SRC

   The value of k should be
   #+RESULTS: p2d.R
   : 207.918938321354

   Test:
   #+BEGIN_SRC R
     1 - 2 * pnorm(207.9189, 250, 50)
   #+END_SRC

   #+RESULTS:
   : 0.600000429140659

** 4.66

Suppose that y is a random variable having a normal distribution with a mean equal to 250 and a standard deviation equal to 10.
a. Show that the event \(y < 260\) has the same probability as \(z < 1\).

   \begin{align}
   z & = \frac{x_{i}-\mu}{\sigma}
   \\
   z & = \frac{260-250}{10}
   \\
   z & = 1
   \end{align}

b. Convert the event \(y > 230\) to the z-score equivalent.

   #+BEGIN_SRC maxima
     eq: z = (x[i] - mu)/sigma$
     tex(subst([x[i]=230, mu=250, sigma = 10], (eq)))$
   #+END_SRC

   #+RESULTS:
   : $$z=-2$$

   #+NAME: p2.4.66.R
   #+BEGIN_SRC R
     1 - pnorm(-2)
   #+END_SRC

   The z-score would be
   #+RESULTS: p2.4.66.R
   : 0.977249868051821

c. Find \(P(y < 260)\) and \(P(y > 230)\)

   #+BEGIN_SRC R :results output
     p2.4.66.res <- c(pnorm(260, 250, 10), 1 - pnorm(230, 250, 10))
     names(p2.4.66.res) <- c("(P(y < 260))", "P(y > 230)")
     p2.4.66.res
   #+END_SRC

   #+RESULTS:
   :  P(y < 260) P(y > 230)
   :   0.8413447   0.9772499

d. Find \(P(y > 265)\), \(P(y < 242)\), and \(P(242 < y < 265)\).

   #+BEGIN_SRC R :results value
     paste(1 - pnorm(265, 250, 10), pnorm(242, 250, 10),
         pnorm(265, 250, 10) - pnorm(242, 250, 10))
   #+END_SRC

   #+ATTR_LATEX:
   #+RESULTS:
   : 0.0668072012688581 0.211855398583397 0.721337400147745
** 4.70

The College Boards, which are administered each year to many thousands of high school students, are scored so as to yield a mean of 513 and a standard deviation of 130. These scores are close to being normally distributed. What percentage of the scores can be expected to satisfy each of the following conditions?
a. Greater than 600

   #+NAME: p4.70.a.R
   #+BEGIN_SRC R :var xi = 600
     mu <- 513
     s <- 130
     z <- (xi - 513)/s
     p <- 1 - pnorm(z)
     paste(100 * p, "%")
   #+END_SRC

   #+RESULTS: p4.70.a.R
   : 25.1674140677811 %

b. Greater than 700

   #+CALL: p4.70.a.R(xi=700)

   #+RESULTS:
   : 7.51515714100521 %

c. Less than 450

   #+BEGIN_SRC R
     p <- pnorm(450, 513, 130)
     paste(100 * p, "%")
   #+END_SRC

   #+RESULTS:
   : 31.3974599262192 %

d. Between 450 and 600

   #+BEGIN_SRC R
     p <- pnorm(600, 513, 130) - pnorm(450, 513, 130)
     paste(100 * p, "%")
   #+END_SRC

   #+RESULTS:
   : 43.4351260059997 %

** 4.72

Refer to Exercise 4.70. An honor society wishes to invite those scoring in the top 5% on the College Boards to join their society.
a. What score is required to be invited to join the society?

   *None*, having good contacts in such society is usually enough.

   #+BEGIN_SRC R
     qnorm(0.95, 513, 130)
   #+END_SRC

   #+RESULTS:
   : 726.830971503691

   From [[pdfview:/home/edgar/Documentos/Biblio/Mate/R.%20Lyman%20Ott,%20Micheal%20T.%20Longnecker-An%20Introduction%20to%20Statistical%20Methods%20and%20Data%20Analysis-Brooks%20Cole%20(2015).pdf::1104][tables]], find corresponding probability and solve for x in \( \frac{x - \mu}{\sigma} = z\)
   #+BEGIN_SRC R
     1.64 * 130 + 513
   #+END_SRC

   #+RESULTS:
   : 726.2

b. What score separates the top 75% of the population from the bottom 25%? What do we call this value?

   #+BEGIN_SRC R
     paste("First quantile: ", qnorm(0.25, 513, 130))
   #+END_SRC

   #+RESULTS:
   : First quantile:  425.316332474509
