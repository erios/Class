// Gmsh project created on Tue Apr 11 15:50:36 2017
// Merge "dbNarrow.brep";
// To generate mesh:\n
// gmsh -3 -optimize_ho -format msh -o dbNarrow.msh dbNarrow.geo

// Units are dimensionless, but need to be
//consistent. Consider millimeters

/////////////////////////////////////////////////
//                  Parameters                 //
/////////////////////////////////////////////////
// Point tolerance //////////////////////////////
lc = 10e-3;

// Origin ///////////////////////////////////////
orgn_x = 0;
orgn_y = 0;
orgn_z = 0;
// Thickness ////////////////////////////////////
thick = 0.5;
// Total height /////////////////////////////////
total_hei = 12.5;

// Mid substance ////////////////////////////////
// ** Mid substance width
mid_subst_wid = 0.6;
// ** Mid substance height
mid_subst_hei = 6;
// ** Mid substance number of points in x
mid_subst_x_div = 5;
// ** Mid substance number of points in z
mid_subst_z_div = mid_subst_x_div * 4;

// Hole /////////////////////////////////////////
// ** Ratio between hole radius and width of mid substance
hole_ratio = 1/3;
// ** Length of the side of the area surrounding the hole
hole_len = mid_subst_wid;
// ** Hole radius
hole_rad = (hole_len) * hole_ratio ;
// ** Width of the area surrounding the hole
hole_wid = mid_subst_wid;
// ** Height of the area surrounding the hole
hole_hei = hole_len;
// Meshing parameters ///////////////////////////
// ** Number of points for diagonal
hole_d_div = 5;
hole_d_mr = 0.825;
// ** Number of points for each arc
hole_r_div = mid_subst_x_div;
// ** Number of points for long sides
hole_long_div = hole_r_div;
hole_l_mr = 1;//0.9;
// ** Number of points for short sides
hole_short_div = hole_d_div;
hole_s_mr = 0.85;

// Chamfer //////////////////////////////////////
// * Arc of chamfer
// ** Radius
chmf_R = 3.5;
// ** Centre of arc (same height as bottom line;
//    it needs to be tangent to the vertical line
//    of the mid-substance).
chmf_Cx = chmf_R + mid_subst_wid;
chmf_Cz = mid_subst_hei;
// ** End point of arc
// *** To get the position of this point, go to
//     FreeCad, find the index # of the point
//     (in the sketch) and do:
// App.ActiveDocument.Sketch014.getPoint(#, 1)
//     I'm sure that you can also calculate
//     the tangency conditions of the two arcs
//     comprising the chamfer and clamping
//     regions ;) ...
chmf_P3_x = 2.323068841391659;
chmf_P3_z = 9.015359609265856;
// Meshing parameters ///////////////////////////
// * Number of points in x
chmf_x_div = mid_subst_x_div;
// * Number of points in z
chmf_z_div = 7;
chmf_z_mr = 0.9;

/////////////////////////////////////////////////
//                   Hole                      //
/////////////////////////////////////////////////
// * Bottom line of hole
// ** Start point
hole_P1 = newp;
Point(hole_P1) = {hole_rad, orgn_y, orgn_z};
// ** End point
hole_P2 = newp;
Point(hole_P2) = {hole_wid, orgn_y, orgn_z};
// ** Bottom line of hole
hole_L1 = newl;
Line(hole_L1) = {hole_P1, hole_P2};
// * Arc1 of hole
// ** Centre (origin)
hole_C = newp;
Point(hole_C) = {orgn_x, orgn_y, orgn_z, lc};
// ** Start point (above)
hole_P3 = newp;
Point(hole_P3) = {
    Cos(Pi/4) * hole_rad,
    orgn_y,
    Cos(Pi/4) * hole_rad,
    lc};
// ** Arc1 (line) of hole. Reuse point from bottom line
hole_A1 = newl;
Circle(hole_A1) = {hole_P3, hole_C, hole_P1};
// * Vertical line of hole
// ** End point
hole_P4 = newp;
Point(hole_P4) = {hole_wid, orgn_y, hole_hei};
// ** Vertical line of hole. Reuse point from bottom line
hole_L2 = newl;
Line(hole_L2) = {hole_P2, hole_P4};
// * Top line of hole
// ** End point
hole_P5 = newp;
Point(hole_P5) = {orgn_x, orgn_y, hole_hei};
// ** Top line of hole. Reuse point from vertical line
hole_L3 = newl;
Line(hole_L3) = {hole_P4, hole_P5};
// * Arc2 (line) of hole.
// ** Start point (above)
hole_P6 = newp;
Point(hole_P6) = {orgn_x, orgn_y, hole_rad};
// ** Arc2 (line) of hole. Reuse point from Arc1
hole_A2 = newl;
Circle(hole_A2) = {hole_P6, hole_C, hole_P3};
// * TopVert line of hole
// ** TopVert line of hole. Reuse points
hole_L4 = newl;
Line(hole_L4) = {hole_P5, hole_P6};
// * Diagonal line of hole
// ** Diagonal line of hole. Reuse points
hole_L5 = newl;
Line(hole_L5) = {hole_P4, hole_P3};
// * Discretise lines (for a controlled mesh)
// Transfinite Line{hole_L4} = hole_r_div; // Using Bump 0.9;
// ** Arcs
Transfinite Line{hole_A1} = hole_r_div; // Using Progression 1.1;
Transfinite Line{hole_A2} = hole_r_div; // Using Progression 1.1;
// ** Long
Transfinite Line{-hole_L2, hole_L3} = hole_long_div Using Progression hole_l_mr;
// ** Short
Transfinite Line{-hole_L1, hole_L4} = hole_short_div Using Progression hole_s_mr;
// ** Diagonal
Transfinite Line{hole_L5} = hole_d_div Using Progression hole_d_mr;
// * Create surfaces
// *** Closed line for lower part of hole
hole_Lp1 = newll;
Line Loop(hole_Lp1) = {hole_A1, hole_L1,
  hole_L2, hole_L5};
hole_S1 = news;
Plane Surface(hole_S1) = {hole_Lp1};
// *** Closed line for upper part of hole
hole_Lp2 = newll;
Line Loop(hole_Lp2) = {hole_L3, hole_L4,
  hole_A2, -hole_L5};
hole_S2 = news;
Plane Surface(hole_S2) = {hole_Lp2};
// ** Structured mesh request for the surfaces
Transfinite Surface {hole_S1, hole_S2};

// vol[] = Extrude {0,1,0} {
//   Surface{hole_S1, hole_S2};
//   // Number of divisions along the extrusion
//   Layers{4};
//   // Ask for hexas
//   Recombine;
// }


/////////////////////////////////////////////////
//                Mid substance                //
/////////////////////////////////////////////////
// ** Mid substance bottom line
// *** Points of the bottom line of the mid
// substance
// **** Reuse point from the hole
mid_subst_P1 = hole_P5;
mid_subst_P2 = hole_P4;
// ** Mid substance top points
Translate {0, 0, mid_subst_hei - hole_hei} {
  Duplicata { Point{mid_subst_P1, mid_subst_P2}; }
}
// Get the indices of the new two points from the Duplicata.
// (hack: get the index of a new point, and go back two)
tmp = newp;
mid_subst_P3 = tmp - 2;
mid_subst_P4 = tmp - 1;
// ** Mid substance vertical and top lines. Reuse point of hole plate
mid_subst_L2 = newl;
Line(mid_subst_L2) = {hole_P5, mid_subst_P3};
mid_subst_L3 = newl;
Line(mid_subst_L3) = {mid_subst_P3, mid_subst_P4};
mid_subst_L4 = newl;
Line(mid_subst_L4) = {mid_subst_P4, mid_subst_P2};
// ** Join lines for mid substance. Reuse top line of hole plate
mid_subst_L1 = hole_L3;
mid_subst_Lp1 = newll;
Line Loop(mid_subst_Lp1) = {-mid_subst_L1,
  -mid_subst_L4, -mid_subst_L3, -mid_subst_L2};
// *** Print line number (example)
Printf("(This is a printing example)");
Printf(" Line number mid_subst_Lp1: ’%g’", mid_subst_Lp1);
// ** Discretise lines (for a controlled mesh)
Transfinite Line{mid_subst_L3} = mid_subst_x_div;
Transfinite Line{mid_subst_L2, mid_subst_L4} = mid_subst_z_div;
// ** Create surface
mid_subst_S1 = news;
Plane Surface(mid_subst_S1) = {mid_subst_Lp1};
// *** Discretise surface by specifying the four
//corners of the interpolation
// forces meshing to contain structured triangles
Transfinite Surface{mid_subst_S1} = {mid_subst_P1,
  mid_subst_P3, mid_subst_P4, mid_subst_P2};

/////////////////////////////////////////////////
//                   Chamfer                   //
/////////////////////////////////////////////////
// * Bottom line of chamfer
// ** Reuse top points of mid-substance as bottom
//    points of the chamfer
chmf_P1 = mid_subst_P4;
chmf_P2 = mid_subst_P3;
// ** Reuse top line of mid-substance as bottom
//    line of chamfer
chmf_L1 = mid_subst_L3;
// * Arc of chamfer
// ** Centre
chmf_C = newp;
Point(chmf_C) = {chmf_Cx, orgn_y, chmf_Cz, lc};
// ** End point of arc
chmf_P3 = newp;
Point(chmf_P3) = {chmf_P3_x, orgn_y, chmf_P3_z};
// ** Arc (line) of chamfer
chmf_A = newl;
Circle(chmf_A) = {chmf_P1, chmf_C, chmf_P3};
// * Top line of chamfer
// ** End point
chmf_P4 = newp;
Point(chmf_P4) = {orgn_x, orgn_y, chmf_P3_z};
// ** Top line of chamfer
chmf_L3 = newl;
Line(chmf_L3) = {chmf_P3, chmf_P4};
// * Vertical line of chamfer
chmf_L4 = newl;
Line(chmf_L4) = {chmf_P4, chmf_P2};
// * Closed line for chamfer
chmf_Lp1 = newll;
Line Loop(chmf_Lp1) = {chmf_L1, chmf_A,
  chmf_L3, chmf_L4};

// ** Discretise lines (for a controlled mesh)
Transfinite Line{chmf_L1, chmf_L3} = chmf_x_div;
Transfinite Line{chmf_L4} = chmf_z_div; // Using Bump 0.9;
Transfinite Line{chmf_A} = chmf_z_div Using Progression 1.05;
// ** Create surface
chmf_S1 = news;
Plane Surface(chmf_S1) = {chmf_Lp1};
// *** Discretise surface by specifying the four
//corners of the interpolation
// forces meshing to contain structured triangles
// Transfinite Surface{chmf_S1};  // works
Transfinite Surface{chmf_S1} = {chmf_P1,
  chmf_P2, chmf_P4, chmf_P3};


Recombine Surface{chmf_S1, hole_S1, hole_S2, mid_subst_S1};

/////////////////////////////////////////////////
//                   Extrude                   //
/////////////////////////////////////////////////
vol[] = Extrude {0, thick, 0} {
  Surface{hole_S1, hole_S2, mid_subst_S1, chmf_S1};
  Layers{2};
  Recombine;
};
//+
Physical Volume("couponVol") = {1, 2, 3, 4};
//+
Physical Surface("zSym") = {34};
//+
Physical Surface("xSym") = {56, 86, 108};
//+
Physical Surface("ySym") = {43, 65, 87, 109};
//+
Physical Surface("top") = {104};
