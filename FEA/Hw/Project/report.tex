% Created 2017-12-15 Fri 11:52
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[QX]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{adjustbox}
\usepackage[usenames,dvipsnames,table]{xcolor}
\usepackage{multirow}
\usepackage{booktabs,array,tabularx}
\usepackage{mathtools}
\usepackage{listings}
\usepackage[
  % The type of reference is included in the hyperlink
  nameinlink
  %% The type of reference is not abbreviated (eq becomes equation)
  % noabbrev
]{cleveref}
\usepackage[
  % Link to the top of the image
  hypcap,
  % Centre last line of the caption only
  justification=centerlast,
  % Justify the text which is after the type of caption
  format=hang,
  % Make the font of label small and italics
  textfont={small},
  % Put the type of caption in versalitas
  labelfont={sc,small}
]{caption}
\usepackage[
  % Put the type of caption in small and slanted shape
  labelfont={footnotesize,sl},
  % Put the text of the caption in versalitas and really small
  textfont={scriptsize}
]{subcaption}
\usepackage{autonum}
\usepackage{pdfcomment}
\usepackage{stackengine}
\input{../../symbols.tex}
\usepackage{siunitx} \sisetup{per-mode=fraction,fraction-function=\tfrac}
\renewcommand\lstlistingname{Block}
\renewcommand\lstlistlistingname{Code blocks}
\def\lstlistingautorefname{Blk.}
\usepackage[style=numeric-comp,
sorting=none,hyperref=true,backref=true,
url=true,backend=biber,natbib=true]{%
biblatex}
\addbibresource{../../References.bib}
\usepackage{pdflscape,longtable,tabu,booktabs}
\input{../../externalapps.tex}
\author{Edgar Rios}
\date{\today}
\title{2D FEA transversely isotropic solver\\\medskip
\large FEA - Programming}
\hypersetup{
 pdfauthor={Edgar Rios},
 pdftitle={2D FEA transversely isotropic solver},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 25.2.2 (Org mode 9.1.4)},
 pdflang={English},
 %
 % Enable coloring links
 colorlinks=true,
 % Change color of links
 linkcolor=[HTML]{2E4D2A}, % Very dark green inner links
 citecolor=Blue,           % Blue citations
 urlcolor=[HTML]{23294A},  % Very dark blue external links
 filecolor=[HTML]{400940}, % Very dark purple for file links
 % To enable autonum (see texdoc autonum; autonum.pdf)
 hypertexnames=false}\begin{document}

\maketitle

\section{Abstract}
\label{sec:org6418ba0}

The present work presents the finite element analysis (FEA) of a meniscus specimen. It shows the derivation of this analysis from hand-calculation to its implementation on a FEA suite. The same code that had been used for previous work was adapted here. Results show a good correspondence to widely available  (FEBio) and restricted code (Abaqus). The objective is to show the applicability of FEA in the biomechanical study of the meniscus.

\section{Introduction}
\label{sec:orgf96003e}

The meniscus is a fibrous tissue which lies on the distal plateau of the tibio-femoral joint (\cref{fig:knee}). It (1) can withstand a life-time of 30 -- 70 years, (2) has almost no friction, (3) is poro-, visco-, and hyper-elastic, and (4) is fibre-reinforced\cite{Lujan07}. Although it is well established that the growth, maintenance and degeneration of the meniscus depends on mechanical factors, their relationship to the latter is still unclear\cite{McnulGuilak15}. The increasing understanding of how meniscus works has proven to be effective in improving the quality of life of people\cite{MakrisHadidAthan11}.

\begin{figure}[ht]
  \centering
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[height=0.25\textheight]{%
      MakrisHadidiAthanasiou11-Figure1(kneeligaments).png}
    \caption{Coronal view of the knee with the meniscal position and connections}
    \label{fig:knee-front}
  \end{subfigure}
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{%
      MakrisHadidiAthanasiou11-Figure2(meniscussuperior).png}
    \caption{Top view of the tibial plateau with the meniscus position}
    \label{fig:knee-top}
  \end{subfigure}
  \caption{Location and geometry of the meniscus\cite{MakrisHadidAthan11}}
\label{fig:knee}
\end{figure}

However, trial and error studies still provide the bulk of knowledge regarding injury prevention and rehabilitation therapy\cite{McnulGuilak15}. This can be costly, risky and not widely applicable. In contrast, theoretical models which can predict the mechanical behaviour of meniscus could be used for computer simulations with a variety of loading scenarios. These can serve to develop better therapies or prevent unfavorable loading conditions for individuals.

In this work, we present the finite element analysis (FEA) of a meniscus specimen as the ones prepared for experimental tensile testing in the Northwest Tissue Mechanics Laboratory (NTM Lab). The objective is to show the applicability of FEA in the creation of (otherwise) difficult analyses. To this end, the development is taken from a simplified tensorial mechanics hand-calculation to a full FEA of a complex geometry with an intermediate step of a custom Python\cite{Rossum95} implementation.

\section{Previous investigations}
\label{sec:org248a0ea}

\begin{sidewaystable}[htbp]
\centering
\begin{tabu}{lXXX}
\toprule
Ref. & Usefulness & Conclusions & Shortcomings\\
\midrule
\cite{WeissMakerGovin96} & Provides coupled and uncoupled formulation of fibred transversely isotropic materials. & Allows fully compressible and incompressible material. & Low-order interpolation may lead to hourglass.\\
 & Can be found in FEBio (verifiable). &  & Purely elastic (non-recoverable effects disregarded).\\
\cite{NimsDurneyCigan15} & Provides classical continuum damage formulation for fibred materials. & Damage mechanics framework that (a) employs observable state variables, instead of internal hidden variables, (b) is applicable to anisotropic materials without tensorial damage, (c) can deal with multi-directional fibres and (c) growth can lead to damage. & Implementation is not completely clear\\
 & Implemented in FEBio (verifiable). &  & No algorithmic implementation (how to implement without FEBio)\\
\cite{PeloqSantarElliot16} & Provides experimental data & Quantified the meniscus’ yield point, peak stress and strain. & Disregards influence of stress concentration due to  geometry.\\
\cite{CalvoPenaMartin07} & Full derivation of damage model applicable to fibred anisotropic materials with cycling loading. & Damage is characterized by the maximum value previously attained by the strain energy of the undamaged material. & No experimental data (unverifiable).\\
 &  & Decomposition of the deformation gradient into iso-choric and dilatational parts. & Algorithm is unclear.\\
\cite{CalvoPenaMartin09} & Adds initial on-set fibre deformation & Reproduces experimental data accuately & Idem\\
\cite{MaScheidBargm16} & Inspirational & Anisotropic hierarchical damage model for tension and compression. & Too advanced for me at this stage\\
\bottomrule
\end{tabu}
\caption{\label{tbl:condensed-ref}
Condensed comparative table with references (Ref: reference).}

\end{sidewaystable}

Many researchers have addressed the topic of soft fibrous tissue as a material from different perspectives (this situation is synthesised in table \ref{tbl:condensed-ref}). However, there are only a handful of publications on meniscus as related to finite element analysis\cite{McnulGuilak15}. For instance, Peña et al.\cite{PenaCalvoMartin08} idealised and used it only as a means to calculate intermediate boundary conditions. A similar situation happened with Nims et al.\cite{NimsDurneyCigan15}. In other words, the present literature provides generic models\cite{PenaMartinCalvo08}, serves as a starting point\cite{PenaCalvoMartin08} or confronts common sense\cite{PeloqSantarElliot16} (see table \ref{tbl:condensed-ref} for details).

\section{Methods}
\label{sec:org3ef1b2d}

The analysis was separated into (1) a hand calculation, (2) a simplified FEA programmed in Python\cite{Rossum95}, Numpy\cite{StefanVaroq11} and Mayavi\cite{ramachandran2011mayavi} and (3) its FEBio implementation. For (1), the geometry was simplified to a \uline{cube}, and the material was modeled as a \uline{transversely isotropic material with a Weibull damage} distribution\cite{NimsDurneyCigan15}. The simplified FEA in Python (2) increased the geometrical complexity to represent \(\frac{1}{8}\) of the real specimen (see details below). However, the material was simplified to be \uline{isotropic and linearly elastic}. The results from the latter were benchmarked with Abaqus. Finally, (3) consisted of a three-fold symmetrical problem with the same material similar to (1). The latter was performed in FEBio (www.febio.org). The reader may consult the appendices for the sample hand-calculation and the source files.

\subsection{Geometry simplifications for FEA}
\label{sec:orgaf679dd}

In real life, a meniscus coupon looks very similar to what is shown in figure \ref{fig:dbWidePunchWithDimensions}. There, the total geometry and the simplification by means of symmetry which are used for this work are shown. Although it is not explicit in the picture, a plane of symmetry parallel to the page was also considered. This results in 3 orthogonal planes (bottom, left and back) mirroring the geometry. It is also assumed that the clamping area provides a uniform pull at the top of the mesh. The result is a working region similar to figure \ref{fig:originalCoupon}.  The depth of the whole specimen was \(\SI{1}{mm}\), for which the mesh thickness was only \(\SI{0.5}{mm}\).

\begin{figure}[ht]
  \centering
  \begin{subfigure}[t]{0.6\textwidth}
    \centering
    \includegraphics[width=0.7\textwidth]{dbWidePunchWithDimensions.png}
    \caption{Depiction of the dimensions of a coupon with a symmetrycal simplification}
    \label{fig:dbWidePunchWithDimensions}
  \end{subfigure}
  \begin{subfigure}[t]{0.3\textwidth}
    \centering
    \includegraphics[width=\textwidth]{originalCoupon.png}
    \caption{Working geometry of a coupon}
    \label{fig:originalCoupon}
  \end{subfigure}
  \caption{Geometry of a meniscus coupon}
\label{fig:dbWide-geom}
\end{figure}

To simulate a condition costly to reproduce experimentally, a \uline{hole defect was included} in the specimen. If the width of the real specimen at the narrowest region was \(w\), a hole with a \_radio of \(\frac{w}{6}\) was added to the centre of the unsplit geometry (looks as a notch in Fig. \ref{fig-undeformed}).

Since the loading condition is axial displacement, \uline{linear hexahedra} were chosen for the analysis (no hourglassing effects are expected due to bending). Also, this type of elements are less computationally expensive than having the equivalent mesh with tetrahedra, or quadratic elements.

The final geometry for the FEA is shown in figure \ref{fig-undeformed} as a structured mesh. This was generated by means of Gmsh\cite{Geuza09}. The reader should be aware that in this document, any orientation is considered as in figure \ref{fig:dbWide-geom}

\begin{figure}[htbp]
\centering
\includegraphics[width=\textwidth]{./undeformed.png}
\caption{\label{fig-undeformed}
Undeformed mesh}
\end{figure}

\subsection{FEA programming}
\label{sec:org8410178}

\subsubsection{Material simplification}
\label{sec:org381d279}

The material was simplified to be a linearly isotropic material. The material properties for this portion of the work are thus only the elasticity modulus (\(E = \SI{18.409}{MPa}\)) and the Poisson's ratio (\(nu = \SI{0.45568}{\mm\per\mm}\)). These were fitted to experimental values.

\subsubsection{Boundary conditions}
\label{applyBC}
As noted before, the geometry was simplified so that there was symmetry normal to the \(x\), \(y\) and \(z\) axes. Therefore, the left side (of the gray area shown in Fig. \ref{fig:dbWidePunchWithDimensions}) was fixed in the \(x\) direction, the bottom in \(z\) and the back in \(y\). This is depicted with arrows in Fig. \ref{fig-GmshBC}.

\begin{figure}[htbp]
\centering
\includegraphics[height=0.3\textheight]{dbWide-hole-GmshBC.png}
\caption{\label{fig-GmshBC}
Symmetry boundary conditions.}
\end{figure}

A \uline{displacement was applied to the top} (hidden in Fig. \ref{fig-GmshBC}) of the geometry, corresponding to the yield point of an experimental test. The recorded value was approximately \(\SI{2.17152}{mm}\).

\subsubsection{Mesh size}
\label{sec:orgacc6036}

A brief convergence study was performed with the Python implementation by means of three sample meshes, as shown in Fig. \ref{fig-coarse-mesh},\ref{fig-fine-mesh} and \ref{fig-refined-mesh}. In each case, the number of layers was kept the same. The difference between them was in the parameters of the region near the hole, which also impacted the rest of the mesh.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{dbWide-hole-fine0.png}
\caption{\label{fig-coarse-mesh}
Coarse mesh}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{dbWide-hole-fine1.png}
\caption{\label{fig-fine-mesh}
Fine mesh}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{dbWide-hole-fine2.png}
\caption{\label{fig-refined-mesh}
Refined mesh}
\end{figure}

With these meshes, the maximum strain was captured and plotted (Fig. \ref{fig-convergence-plot}). The variation due to the size of the mesh seems irrelevant, and thus, the coarser mesh was used for the study.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./ConvergencePlot.png}
\caption{\label{fig-convergence-plot}
Convergence for the three types of mesh}
\end{figure}

\subsubsection{Python results}
\label{sec:org1f04baa}

The components of normal strain are shown in Fig. \ref{fig-py-eXX},\ref{fig-py-eYY} and \ref{fig-py-eZZ}. There, one can see the values at the nodes and integration points (labelled with \_iPt in the figures). As it can be seen, these values have a slight variation, but are in agreement with what Abaqus shows (Fig. \ref{fig-abaqus-e11},\ref{fig-abaqus-e22} and \ref{fig-abaqus-e33}). The difference is more evident for the components of strain which are perpendicular to the pull.

The values were aggregated by taking the average for all the elements sharing each node. Each component shows a band almost perpendicular to the pull, and a concentration in the hole area, as expected. Although it is not immediately evident, there is a reduction of cross-sectional area in different regions. as it happens in real circumstances. The reader is reminded that the negative values (in blue) correspond to more strain (contraction). This normally happens perpendicular to the extension of the material (Fig. \ref{fig-py-eXX} and \ref{fig-py-eYY}).

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./strainXX-displ.png}
\caption{\label{fig-py-eXX}
Strain in the \$xx\$-direction from Python.}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./strainYY-displ.png}
\caption{\label{fig-py-eYY}
Strain in the \$yy\$-direction from Python.}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./strainZZ-displ.png}
\caption{\label{fig-py-eZZ}
Strain in the \$zz\$-direction from Python.}
\end{figure}

What results interesting is to see the considerable effect that choosing integration points or nodal values has on the reported stress. In Fig. \ref{fig-py-sZZ}, the maximum stress for the nodal values (top) is reported as \(\SI{48}{MPa}\), while the value from integration points is \(\SI{19}{MPa}\) (more than a 2-fold difference). The latter seems to be in accordance with Abaqus (Fig. \ref{fig-abaqus-s33}). Only the values for the \(ZZ\) component are reported, since they are the primary components.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./stressZZ-displ-hole.png}
\caption{\label{fig-py-sZZ}
\(ZZ\)-stress in the hole region from Python.}
\end{figure}

All of these plots were produced by means of ParaView\cite{Ayachit:2015:PGP:2789330} and TVTK from Mayavi\cite{ramachandran2011mayavi}.

\subsubsection{Abaqus plots (benchmarking)}
\label{sec:org9c61257}

The stress and strain plots from Abaqus are reported in Fig. \ref{fig-abaqus-s33},\ref{fig-abaqus-e11},\ref{fig-abaqus-e22} and \ref{fig-abaqus-e33}. They lie in their own section to prevent confusion with the Python results.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./Abaqus/strain11.png}
\caption{\label{fig-abaqus-e11}
Strain in the \$xx\$-direction from Abaqus.}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./Abaqus/strain22.png}
\caption{\label{fig-abaqus-e22}
Strain in the \$yy\$-direction from Abaqus.}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./Abaqus/strain33.png}
\caption{\label{fig-abaqus-e33}
Strain in the \$zz\$-direction from Abaqus.}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./Abaqus/stress33.png}
\caption{\label{fig-abaqus-s33}
Stress in the \$zz\$-direction from Abaqus.}
\end{figure}

\subsection{FEBio implementation}
\label{sec:org4fafcad}

For the FEBio implementation coupons with and without a hole are presented. The reason is that, in combination with the full damage model\cite{NimsDurneyCigan15}, the analysis becomes rather unstable, and does not provide a good result. Physically, this is the result of the material failing completely and never being able to recover. This can be appreciated in Fig. \ref{fig-febio-hole-fit}. It is also evident that the mesh was refined with the objective of providing more material elements which could withstand the load (Fig. \ref{fig-febio-hole-strain}). Also, the converging tolerances had to be relaxed.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./dbWide-hole-long-dam-dud-mu5_alpha2_xi53_dtol0.01_exp_vs_sim.png}
\caption{\label{fig-febio-hole-fit}
Force v.s. displacement with manually fitted parameters (the material can never recover).}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{dbWide-hole-long-dam-dud-mu5_alpha2_xi53_dtol0.01_exp_vs_sim-Postview.png}
\caption{\label{fig-febio-hole-strain}
Lagrange strain field on the specimen with a refined mesh and relaxed converging tolerance.}
\end{figure}

The results from the simulation without a hole are shown in Fig. \ref{fig-febio-fit} and \ref{fig-febio-strain}. The former shows that the selection of parameters (tweaked manually) gives a good fit. The second picture shows the distribution of Lagrange strain across the specimen. As it is shown, the concentration of strain no longer happens near base, because the hole has been removed. The most affected zone is in the necking.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{dbWide-noclamp-long-dam-dud-mu4.39799_alpha0.5551875_xi53_exp_vs_sim.png}
\caption{\label{fig-febio-fit}
Parameter fit for FEBio.}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[height=0.3\textheight]{dbWide-noclamp-long-dam-dud-mu4.39799_alpha0.5551875_xi53_exp_vs_sim-Postview.png}
\caption{\label{fig-febio-strain}
Lagrange strain field on the specimen.}
\end{figure}

\subsection{Discussion}
\label{sec:org1859306}

The hand-analysis presented here is rather limited, and can only serve to assess the validity of the material model. When compared to the original source\cite{NimsDurneyCigan15}, it shows that the implementation is correct. Although no presented here, a preliminary analysis had to be performed to verify the validity of the FEBio model.

The most illustrative part of the project came from comparing the results from the Python program to Abaqus. There, we could see the big effect that the post-processing has in the validity and interpretation of the results. In other words, we should keep in mind that these are simulations and understand the shortcomings of the method (averaging a non-continuous data across elements from interpolated data). The material model was very simple, and it would be interesting to develop a FEA for a non-linear compliance matrix (not for this course).

It was rather anti-climatic to see the results from FEBio for the specimen. Unfortunately, the results for a sample with a hole could not be presented (it is an ongoing research project).

As a side note, a partial result showed that \texttt{get\_nforce.py} will only provide a set of forces, but trying to extract the values from a simulation and applying them to another FEA code will fail misserably. This is likely related to the fact that all the internal forces should be considered and set as boundaries (which makes for a very dull problem).

The results from the FEBio implementation shows that the damage model is rather sensitive (to mostly anything). Finding the right set of parameters will be a very interesting task, because the traditional optimisation techniques fail misserably. Also, this shows that the refinement of the mesh is dependent on the conditions of the problem (the linear material had no need for it).

As a final remark, remember that the extended length of the report is due to the many pictures, and that the format of my text creates more readable, but also narrower pages (instead of lowering my grade on page length, please, consider the time that it takes you to go over this document as compared to others).

\subsection{Acknowledgements}
\label{sec:org2fd2c1b}

This work was supported by Dr. Trevor Lujan's NSF CAREER grant, by which my tuition is waived. None of this would have been possible without the lectures from Dr. Clare Fitzpatrick. Except for Abaqus (for which the couse is designed), this whole work depended on the titanic job of the free software community, and is a kind offering to them. The author wishes to condemn the new tax bill proposed by the U.S. government which is going to shower billions of dollars to the richest people in the country and possibly ruin any future plans towards my studies in Boise.

\section*{References}
\label{sec:org7899aee}
\printbibliography[heading=none]
\end{document}
