#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Calculation of deflection of a beam with 1D elements
# Copyright (C) 2017  Edgar Rios
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

import numpy as np
from matplotlib import pyplot as pl


def lin_assembly_stiff(en, dims, k, KBC):
    '''Puts together the individusl stiffness matrices into
    the global stiffness matrix

    en -- (list) indices of the nodes of an element to be
        assembled
    dims -- (int) spatial dimensions of the coordinate
    system
    k -- (numpy.matrix) individual stiffness matrix
    KBC -- (numpy.matrix) global stiffness matrix
        (pre-allocated or partially filled)

    Values used to get the slice the first and last position
    of DOF pertaining to the element, relative to the global
    matrix. Example: 2D displacements, 2 nodes per element,
    from node 2 to node 0 (starting on 0)
    elems = [[0, 1],
             [1, 2],
             [0, 3],
             [3, 1],
             [1, 4],
             [4, 2],
             [3, 4]]
    en = elems[3]
    k = np.matrix([[ 0,  1,  2,  3],
                   [ 4,  5,  6,  7],
                   [ 8,  9, 10, 11],
                   [12, 13, 14, 15]])
    dims = 2
    numn = 5
    KBC = np.zeros([numn * dims, numn * dims])

    # Result:
    array([[  0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.],
           [  0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.],
           [  0.,   0.,  10.,  11.,   0.,   0.,   8.,   9.,   0.,   0.],
           [  0.,   0.,  14.,  15.,   0.,   0.,  12.,  13.,   0.,   0.],
           [  0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.],
           [  0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.],
           [  0.,   0.,   2.,   3.,   0.,   0.,   0.,   1.,   0.,   0.],
           [  0.,   0.,   6.,   7.,   0.,   0.,   4.,   5.,   0.,   0.],
           [  0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.],
           [  0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.]])
    '''

    # TODO: this loop is inefficient (may be this:
    # https://docs.scipy.org/doc/numpy-1.13.0/reference/
    # arrays.indexing.html)
    for ind_i, IND_I in enumerate(en):
        ind_i *= dims
        IND_I *= dims
        end_i = ind_i + dims
        END_I = IND_I + dims
        for ind_j, IND_J in enumerate(en):
            ind_j *= dims
            IND_J *= dims
            end_j = ind_j + dims
            END_J = IND_J + dims
            submat = k[ind_i:end_i, ind_j:end_j]
            KBC[IND_I:END_I, IND_J:END_J] += submat
    return KBC


# Geometrical definition
# Diagonal height
h = np.sqrt(1 - (1 / 2) ** 2)

# Nodes
# Coordinates of nodes
# [x, y] (2D)
# [x, y, z] (3D)
nodes = np.matrix([[0, 0],
                   [1, 0],
                   [2, 0],
                   [0.5, h],
                   [1.5, h]])

# Nodes corresponding to elements
# [node_start, node_end]
elems = np.matrix([[0, 1],
                   [1, 2],
                   [0, 3],
                   [3, 1],
                   [1, 4],
                   [4, 2],
                   [3, 4]])

# Properties
# Young's modulus
# (200 GPa)
E = [200e9 for I in elems]

# Cross-sectional area of each element
# 1.9634954084936207e-05
A = [np.pi * (5e-3 / 2) ** 2 for I in elems]

# Lengths
lens = [1 for I in elems]

# Set which DOF of which nodes are restricted.
# Qualifying boundary conditions (reduces the terms in the global matrix; set 1 when the DOF equals 0)
# [node xDof_restricted? yDof_restricted?]
qbc = [[0, True, True],
       [2, False, True]]

# Applied loads
# (any other are zero)
# [node, Fx, Fy ...]
f = [[1, 0, -10e3]]

# Number of elements
nume = len(elems)

# Nodes per element (take first row, and count elements)
npe = len(elems[0])

# Dimensionality (2D, 3D). Each row of ~nodes~ has a number
# of columns equal to the number of coordinates. The number
# of coordinates indicates the number of dimensions. The
# number of columns is the second elment given by ~shape~
dims = nodes.shape[1]

# Total number of nodes (one node per row in nodes). The
# number of rows is the first elment given by ~shape~
numn = nodes.shape[0]

# Global load vector
F = np.zeros([numn * dims, 1])

# Global stiffness matrix
KBC = np.matrix(np.zeros([numn * dims, numn * dims]))

# Temporary variables for spatial coordinates (why do we need a list?)
x = [float()] * nume
y = [float()] * nume
b = [float()] * nume

# Range with as long as the number of elements
rang_els = range(nume)

# Element stiffness matrices
# Loop over the number of elements
for I in rang_els:
    # Spatial coordinates (2D)
    x[I] = nodes[elems[I, 1], 0] - nodes[elems[I, 0], 0]
    y[I] = nodes[elems[I, 1], 1] - nodes[elems[I, 0], 1]
    # Angle
    b[I] = np.arctan2(y[I], x[I])

    # local stiffness matrix
    # (slide 3/26, 170828 + 19/26, 170828)
    c = np.cos(b[I])
    c2 = c * c
    s = np.sin(b[I])
    s2 = s * s
    cs = c * s
    k = (A[I] * E[I] / lens[I]) * \
        np.matrix([[c2, cs, -c2, -cs],
                   [cs, s2, -cs, -s2],
                   [-c2, -cs, c2, cs],
                   [-cs, -s2, cs, s2]])

    # Nodes of current element
    en = list((elems[I] * dims).flat)

    # Values used to get the slice the first and last
    # position of DOF pertaining to the element, relative to
    # the global matrix. Example: 2D element, from node 2 to
    # node 0 (starting on 0)
    # en = [2, 0]
    # k = np.matrix([[ 0,  1,  2,  3],
    #                [ 4,  5,  6,  7],
    #                [ 8,  9, 10, 11],
    #                [12, 13, 14, 15]])
    # numn = 5
    # dims = 2
    # KBC = np.zeros([numn * dims, numn * dims])
    # Result:
    # >>> KBC
    # [[ 10.  11.   8.   9.   0.   0.   0.   0.   0.   0.]
    #  [ 14.  15.  12.  13.   0.   0.   0.   0.   0.   0.]
    #  [  2.   3.   0.   1.   0.   0.   0.   0.   0.   0.]
    #  [  6.   7.   4.   5.   0.   0.   0.   0.   0.   0.]
    #  [  0.   0.   0.   0.   0.   0.   0.   0.   0.   0.]
    #  [  0.   0.   0.   0.   0.   0.   0.   0.   0.   0.]
    #  [  0.   0.   0.   0.   0.   0.   0.   0.   0.   0.]
    #  [  0.   0.   0.   0.   0.   0.   0.   0.   0.   0.]
    #  [  0.   0.   0.   0.   0.   0.   0.   0.   0.   0.]
    #  [  0.   0.   0.   0.   0.   0.   0.   0.   0.   0.]]
    # TODO: this loop is inefficient
    for ind_i, IND_I in enumerate(en):
        ind_i *= dims
        IND_I *= dims
        end_i = ind_i + dims
        END_I = IND_I + dims
        for ind_j, IND_J in enumerate(en):
            ind_j *= dims
            IND_J *= dims
            end_j = ind_j + dims
            END_J = IND_J + dims
            submat = k[ind_i:end_i, ind_j:end_j]
            KBC[IND_I:END_I, IND_J:END_J] += submat

    KBC = lin_assembly_stiff(en, dims, k, KBC)

# Application of boundary conditions from selected nodes
for I in qbc:
    # I = [node xDof_restricted? yDof_restricted?]
    node = I[0]
    # Nodal DOF
    # I[1:] = [xDof_restricted? yDof_restricted? ...]
    for nDof, val in enumerate(I[1:]):
        if val:
            # Node starting position in the global matrix
            nodePos = node * dims
            # Position of the current DOF of the node in the
            # global matrix
            dofPos = nodePos + nDof
            # All the columns of the present row (the whole
            # row)
            KBC[dofPos, :] = 0
            # The whole column
            KBC[:, dofPos] = 0
            # The position of the node in the matrix
            KBC[dofPos, dofPos] = 1

# Applied loads from selected nodes
for I in f:
    # val = [node, Fx, Fy ...]
    node = I[0]
    for nDof in range(dims):
        # Node starting position in the global matrix
        nodePos = node * dims
        # Position of the current DOF of the node in the
        # global matrix
        dofPos = nodePos + nDof
        F[dofPos, 0] = I[nDof + 1]
    # F[f[I + 1][0]] = f[I][2]

# Displacements result (solve linear system)
# TODO Check conditioning, etc.
resDisp = np.linalg.solve(KBC, F)
# Reshape to the same form as nodes
displ = np.reshape(resDisp, [numn, dims])

# Reaction forces (load)
resLoad = np.dot(KBC, resDisp)

# Stretch
# lambda = L / l_0
# Deformed lengths
# L = l_0 + displ
L = np.matrix(np.zeros([nume, 1]))
for I in rang_els:
    # Nodes of current element
    en = elems[I].tolist()[0]
    # Coordinates of the deformed elements
    coords = np.add(nodes[en], displ[en])
    # Lengths of the deformed elements (subtract
    # coordinates). Use the matricial norm.
    L[I] = np.linalg.norm(np.diff(coords.T))
# Make a column vector out of the initial lengths
lens_vec = np.reshape(np.matrix(lens), [nume, 1])
# Divide by the values of the initial lengths
stretch = np.divide(L, lens_vec)

# Engineering strain and Cauchy stresses
# epsilon = stretch - 1
# sigma = E * epsilon
epsilon = np.subtract(stretch, 1)
E_vec = np.reshape(np.matrix(E), [nume, 1])
sigma = np.multiply(epsilon, E_vec)

dsf = 40

# plot mesh and deformed configuration
for I in rang_els:
    # Nodes of current element
    en = elems[I].tolist()[0]
    # Coordinates of the element's nodes
    coords = nodes[en]

    # Plot each element with a node (o marker), linewidth of
    # 3 and semitransparency (alpha = 0.75)
    pl.plot(coords[:, 0], coords[:, 1], marker="o", lw="3",
            alpha=0.75)
    # Coordinates of the deformed elements, scaled for
    # visualisation
    coords = np.add(nodes[en], displ[en] * dsf)
    pl.plot(coords[:, 0], coords[:, 1], marker="o", lw="3",
            alpha=0.75)
    pl.axes().set_aspect('equal', 'datalim')

outfig = "trussResults-hw01.png"

pl.savefig(outfig, bbbox_inces="tight")
