#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Calculation of deflection of a beam with 2D triangular
# elements
# Copyright (C) 2017  Edgar Rios
#
# This program is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# **************************************************
#                External modules
# **************************************************
import numpy as np
from matplotlib import pyplot as pl
import subprocess as sp
import shlex
import os


# **************************************************
#                 Local functions
# **************************************************
def get_inp_nodes(inpFile, sep=None):
    '''Retrieve nodal values for the first part in an inp
    file using awk through the terminal with Python.

    '''
    if sep is None:
        sep = ","

    # Build command for awk
    # Call awk with sep to separate fields
    cmd = "awk -F'" + sep
    # Set the output field separator to ~,~, because Python
    # separates elements with the same separator (to be
    # consistent, could be something else)
    cmd += "' 'BEGIN{OFS=\"" + sep + "\"} "
    # If we find the tag for element, exit (hopefully comes
    # after the nodes definition)
    cmd += "/^[[:space:]]*\\*/ && a==1{exit} "
    # As long as the previous condition is not met, and the
    # Node tag has been previously found, print all the
    # values from column 2 to the the last field
    cmd += "a==1{for (I=2; I<=NF; I++){" \
        + "printf \"%s%s\", $I, (I<NF?OFS:ORS)}}"
    # Set the flag when the Node tag is found
    cmd += "/^[[:space:]]*\\*Node/{a=1}' "
    # Set the file to scan
    cmd += inpFile

    # Call awk, and split the result at each newline
    # character (getoutput returns a string)
    inpNodes = sp.getoutput(cmd).split("\n")
    # For each element of the list, split the string at the
    # symbol used to print the fields (sep; OFS), and
    # convert each value to a single precision real
    # number
    nodes = [[float(II) for II in I.split(sep)]
             for I in inpNodes]
    return nodes


def get_inp_elems(inpFile, sep=None):
    '''Retrieve elements definition for the first part in an
    inp file using awk through the terminal with Python.

    '''
    if sep is None:
        sep = ","

    # Build command for awk
    # Call awk with sep to separate fields
    cmd = "awk -F'" + sep
    # Set the output field separator to ~,~, because Python
    # separates elements with the same separator (to be
    # consistent; could be something else)
    cmd += "' 'BEGIN{OFS=\"" + sep + "\"} "
    # Exit if we find a tag after having found the element
    # tag (hopefully comes after the elements' definition)
    cmd += "/^[[:space:]]*\\*/ && a==1{exit} "
    # As long as (1) the previous condition is not met, and
    # the Node tag has been previously found, and the line
    # starts with any number (including none) of spaces
    # followed by an integer number print all the values
    # from column 2 to the the last field
    cmd += "a==1 && /^[[:space:]]*[[:digit:]]+,/" \
           + "{for (I=2; I<=NF; I++){" \
           + "printf \"%s%s\", $I, (I<NF?OFS:ORS)}}"
    # Set the flag when the Element tag is found
    cmd += "/^[[:space:]]*\\*Element/{a=1}' " \
        # Set the file to scan
    cmd += inpFile

    # Call awk, and split the result at each newline
    # character (getoutput returns a string)
    inpElems = sp.getoutput(cmd).split("\n")
    # For each member of the list, split the string at the
    # symbol used to print the fields (sep; OFS), and
    # convert each value to a single precision integer
    # (indices in Python start with 0)
    nodes = [[int(II) - 1 for II in I.split(sep)]
             for I in inpElems]
    return nodes


def grad_interp_lin_tri2D(adjoint_matA, det_matA):
    """If [N] is the augmented interpolation matrix of a
    planar CST triangle, this function calculates its
    gradient [B]. The calculation is based on the values of
    the adjoint matrix from where that vector is derived
    (see inv_tri_coord_matrix_2D)

    Keyword Arguments:
    adjoint_matA -- adjoint matrix of the matrix which links
      the weights of the displacement function to the
      spatial coordinates (which was called [A] in the FEA
      course).

    Requires
    import numpy as np

    """
    # beta
    b = adjoint_matA[1, :]
    # gamma
    g = adjoint_matA[2, :]
    matB = np.matrix(
        [[b[0, 0], 0, b[0, 1], 0, b[0, 2], 0],
         [0, g[0, 0], 0, g[0, 1], 0, g[0, 2]],
         [g[0, 0], b[0, 0], g[0, 1],
          b[0, 1], g[0, 2], b[0, 2]]]) / det_matA
    return matB


def inv_coord_matrix_tri2D(coords, adjoint_mat, fullres=None):
    """Inverse of the matrix which links the weights of the
    displacement function to the spatial coordinates (which
    was called [A] in the FEA course)

    Keyword Arguments:
    coords -- (numpy.matrix) each row of the matrix
         contains the coordinates in each axis of the
         geometry Ex: For a 2D element with three nodes
         [[x1, y1], [x2, y2], [x3, y3]].
    adjoint_mat -- 3x3 matrix (will be replaced)

    Requires:
    import numpy as np

    Returns:
    if fullres: [inverse([A]), adjoint([A]), det([A])]
    else: inverse([A])
    """
    coords_lst = coords.tolist()
    x = coords_lst[0]
    y = coords_lst[1]
    alpha = [x[1] * y[2] - y[1] * x[2],
             x[0] * y[2] - y[0] * x[2],
             x[0] * y[1] - y[0] * x[1]]
    beta = [y[1] - y[2],
            y[2] - y[0],
            y[0] - y[1]]
    gamma = [x[2] - x[1],
             x[0] - x[2],
             x[1] - x[0]]
    # Row by row
    adjoint_mat[0, :] = alpha
    adjoint_mat[1, :] = beta
    adjoint_mat[2, :] = gamma
    det_matA = sum([x[I] * beta[I] for I in range(3)])
    if fullres:
        return adjoint_mat / det_matA, adjoint_mat, det_matA
    else:
        return adjoint_mat / det_matA


def lin_assembly_stiff(en, dims, k, KBC):
    '''Puts together the individusl stiffness matrices into
    the global stiffness matrix

    en -- (list) indices of the nodes of an element to be
        assembled
    dims -- (int) spatial dimensions of the coordinate
    system
    k -- (numpy.matrix) individual stiffness matrix
    KBC -- (numpy.matrix) global stiffness matrix
        (pre-allocated or partially filled)

    Values used to get the slice the first and last position
    of DOF pertaining to the element, relative to the global
    matrix. Example: 2D displacements, 2 nodes per element,
    from node 2 to node 0 (starting on 0)
    elems = [[0, 1],
             [1, 2],
             [0, 3],
             [3, 1],
             [1, 4],
             [4, 2],
             [3, 4]]
    en = elems[3]
    k = np.matrix([[ 0,  1,  2,  3],
                   [ 4,  5,  6,  7],
                   [ 8,  9, 10, 11],
                   [12, 13, 14, 15]])
    dims = 2
    numn = 5
    KBC = np.zeros([numn * dims, numn * dims])

    # Result:
    array([[  0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.],
           [  0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.],
           [  0.,   0.,  10.,  11.,   0.,   0.,   8.,   9.,   0.,   0.],
           [  0.,   0.,  14.,  15.,   0.,   0.,  12.,  13.,   0.,   0.],
           [  0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.],
           [  0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.],
           [  0.,   0.,   2.,   3.,   0.,   0.,   0.,   1.,   0.,   0.],
           [  0.,   0.,   6.,   7.,   0.,   0.,   4.,   5.,   0.,   0.],
           [  0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.],
           [  0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.]])
    '''

    # TODO: this loop is inefficient (may be this:
    # https://docs.scipy.org/doc/numpy-1.13.0/reference/
    # arrays.indexing.html)
    for ind_i, IND_I in enumerate(en):
        ind_i *= dims
        IND_I *= dims
        end_i = ind_i + dims
        END_I = IND_I + dims
        for ind_j, IND_J in enumerate(en):
            ind_j *= dims
            IND_J *= dims
            end_j = ind_j + dims
            END_J = IND_J + dims
            submat = k[ind_i:end_i, ind_j:end_j]
            KBC[IND_I:END_I, IND_J:END_J] += submat
    return KBC


def plot_def_tri_lin_mesh(nodes, elems, displ, field, dsf,
                          title=None):
    '''Plot the original, deformed and elements values (constant
    across the elements)

    Arguments
    nodes -- (numpy.matrix) coordinates of nodes
    elems -- (list) each line indicates which nodes comprise
        a 2D element
    displ -- (numpy.matrix) same shape as
        nodes. Displacements from the original coordinates
        (nodes)
    field -- (numpy.matrix) each element has a value
        assigned to it. This serves to add a color with a
        palette onto the element.
    title -- (string) Top title for the subfigure

    See:
    http://matplotlib.org/1.5.0/examples/pylab_examples/triplot_demo.html
    http://matplotlib.org/examples/pylab_examples/tripcolor_demo.html
    '''
    # Plot undeformed mesh
    # Coordinates of the original shape
    x = np.degrees(nodes[:, 0].T.flat)
    y = np.degrees(nodes[:, 1].T.flat)
    # Nodal indices of the triangle
    elem_arr = np.asarray(elems)
    # http://matplotlib.org/1.5.0/examples/pylab_examples/triplot_demo.html
    pl.triplot(x, y, elem_arr, 'ko-')

    # Plot deformed mesh
    # Add original coordinates to the deformed coordinates
    coords_def = np.add(nodes, displ * dsf)
    x = np.degrees(coords_def[:, 0].T.flat)
    y = np.degrees(coords_def[:, 1].T.flat)
    # http://matplotlib.org/1.5.0/examples/pylab_examples/triplot_demo.html
    pl.triplot(x, y, elem_arr, 'go-', alpha=0.5)

    # Plot field contour (strain, stress, whatever)
    # http://matplotlib.org/examples/pylab_examples/tripcolor_demo.html
    field_lst = np.asarray(field.flat)
    pl.tripcolor(x, y, elem_arr, facecolors=field_lst)
    pl.colorbar()
    if not title is None: pl.title(title)

    # Hide the axes values
    # https://stackoverflow.com/a/9295472
    pl.axis("off")


def compliance_iso_el_2D(E, nu):
    matD = E / (1 - nu * nu) \
        * np.matrix([[1, nu, 0],
                     [nu, 1, 0],
                     [0, 0, (1 - nu) * 0.5]])
    return matD


# **************************************************
#                   Main body
# **************************************************
np.set_printoptions(precision=3, linewidth=105)
inpFile = "./hw02/Abaqus/cantilever20Elems.inp"

# # Nodes
# # Coordinates of nodes. For each row:
# # [x, y] (2D)
# # [x, y, z] (3D)
# nodes = np.matrix(get_inp_nodes(inpFile))
# 
# # Nodes corresponding to elements
# # [node_start, node_end]
# elems = get_inp_elems(inpFile)

nodes = np.matrix([[0.,  0.],
                   [0.015,  0.],
                   [0.03,  0.],
                   [0.045,  0.],
                   [0.06,  0.],
                   [0.075,  0.],
                   [0.09,  0.],
                   [0.105,  0.],
                   [0.12,  0.],
                   [0.135,  0.],
                   [0.15,  0.],
                   [0.,  0.005],
                   [0.015,  0.005],
                   [0.03,  0.005],
                   [0.045,  0.005],
                   [0.06,  0.005],
                   [0.075,  0.005],
                   [0.09,  0.005],
                   [0.105,  0.005],
                   [0.12,  0.005],
                   [0.135,  0.005],
                   [0.15,  0.005]])

elems = [[0, 1, 12], [12, 11, 0], [1, 2, 13], [13, 12, 1], [2, 3, 14], [14, 13, 2], [3, 4, 15], [15, 14, 3], [4, 5, 16], [16, 15, 4], [5, 6, 17], [17, 16, 5], [6, 7, 18], [18, 17, 6], [7, 8, 19], [19, 18, 7], [8, 9, 20], [20, 19, 8], [9, 10, 21], [21, 20, 9]]

# Properties
# Young's modulus
# (60 GPa; 60e3 MPa)
E = [60e9 for I in elems]
# Poisson ratio
nu = 0.4

# Set which DOF of which nodes are restricted.
# Qualifying boundary conditions (reduces the terms in the
# global matrix; set 1 when the DOF equals 0)
# [node xDof_restricted? yDof_restricted? ]
# nodes at (0, -2.5) mm and (0, 2.5) mm
qbc = [[0, True, True],
       [11, True, True]]

# Applied loads
# (any other are zero)
# [node, Fx, Fy ...]
f = [[10, 0, -10]] # 10 N downward; right, bottom corner

# Thickness
t = 3e-3

# Number of elements
nume = len(elems)

# Nodes per element (take first row, and count elements)
npe = len(elems[0])

# Dimensionality (2D, 3D). Each row of ~nodes~ has a number
# of columns equal to the number of coordinates. The number
# of coordinates indicates the number of dimensions. The
# number of columns is the second elment given by ~shape~
dims = nodes.shape[1]

# Total number of nodes (one node per row in nodes). The
# number of rows is the first elment given by ~shape~
numn = nodes.shape[0]

# Global load vector
F = np.zeros([numn * dims, 1])

# Global stiffness matrix
KBC = np.matrix(np.zeros([numn * dims, numn * dims]))

# Pre-allocate empty 3x3 matrix
adj_matA = np.matrix(np.zeros([3, 3]))

# Range as long as the number of elements
rang_els = range(nume)

# Pre-allocate areas
A = [float() for I in rang_els]

# Lists with the values of engineering strain and Cauchy
# stress
eng_strain = [np.zeros([3,1]) for I in range(numn)]
stress = [np.zeros([3,1]) for I in range(numn)]

# plot mesh and deformed configuration
for I in rang_els:
    # Nodes of current element
    en = elems[I]
    # Coordinates of the element's nodes
    coords = nodes[en]

    # Nodes of current element
    inv_matA, adj_matA, det_matA = \
        inv_coord_matrix_tri2D(coords.T, adj_matA,
                               fullres=True)
    A[I] = det_matA * 0.5
    matB = grad_interp_lin_tri2D(adj_matA, det_matA)

    matD = compliance_iso_el_2D(E[I], nu)
    k = t * A[I] * np.dot(np.dot(matB.T, matD), matB)

    KBC = lin_assembly_stiff(en, dims, k, KBC)

# Application of boundary conditions from selected nodes
for I in qbc:
    # I = [node xDof_restricted? yDof_restricted?]
    node = I[0]
    # Nodal DOF
    # I[1:] = [xDof_restricted? yDof_restricted? ...]
    for nDof, val in enumerate(I[1:]):
        if val:
            # Node starting position in the global matrix
            nodePos = node * dims
            # Position of the current DOF of the node in the
            # global matrix
            dofPos = nodePos + nDof
            # All the columns of the present row (the whole
            # row)
            KBC[dofPos, :] = 0
            # The whole column
            KBC[:, dofPos] = 0
            # The position of the node in the matrix
            KBC[dofPos, dofPos] = 1

# Applied loads from selected nodes
for I in f:
    # val = [node, Fx, Fy ...]
    node = I[0]
    for nDof in range(dims):
        # Node starting position in the global matrix
        nodePos = node * dims
        # Position of the current DOF of the node in the
        # global matrix
        dofPos = nodePos + nDof
        F[dofPos, 0] = I[nDof + 1]
    # F[f[I + 1][0]] = f[I][2]

# Displacements result (solve linear system)
# TODO Check conditioning, etc.
resDisp = np.linalg.solve(KBC, F)
# Reshape to the same form as nodes
displ = np.reshape(resDisp, [numn, dims])

# plot mesh and deformed configuration
for I in rang_els:
    # Nodes of current element
    en = elems[I]
    # Coordinates of the element's nodes
    coords = nodes[en]

    # Nodes of current element
    inv_matA, adj_matA, det_matA = \
        inv_coord_matrix_tri2D(coords.T, adj_matA,
                               fullres=True)
    A[I] = det_matA * 0.5
    matB = grad_interp_lin_tri2D(adj_matA, det_matA)

    # Get displacements from the current nodes
    nDof = np.asarray([[I * dims, I * dims + 1] for I in
                       en]).flatten()
    vec_d = resDisp[nDof]
    
    # Calculate and store strain per element
    eng_strain[I] = np.dot(matB, vec_d)
    # Calculate and store stress per element
    matD = compliance_iso_el_2D(E[I], nu)
    stress[I] = np.dot(matD, eng_strain[I])

dsf = 40

# Plot results
# Prepare plot
pl.figure()
# Split the frame in 2 (horizontal plots) and take the top
# one
pl.subplot(211)
# Set a 1:1 ratio for vertical and horizontal axes. Leave
# some room for the geometry.
pl.gca().set_aspect('equal', 'datalim')

# Plot the x-strain
exx = np.matrix([I[0, 0] for I in eng_strain])
plot_def_tri_lin_mesh(nodes, elems, displ, exx, dsf,
                      "x-Strain + deformed mesh")
# New plot under the other one
pl.subplot(212)
pl.gca().set_aspect('equal', 'datalim')
eyy = np.matrix([I[1, 0] for I in eng_strain])
plot_def_tri_lin_mesh(nodes, elems, displ, eyy, dsf,
                      "y-Strain + deformed mesh")

outfig = "cantileverStrain-hw02.png"
pl.savefig(outfig)

# Stress ########################################
pl.clf()
pl.subplot(311)
# Set a 1:1 ratio for vertical and horizontal axes. Leave
# some room for the geometry.
pl.gca().set_aspect('equal', 'datalim')

# Plot the x-stress
sxx = np.matrix([I[0, 0] for I in stress])
plot_def_tri_lin_mesh(nodes, elems, displ, sxx, dsf,
                      "x-Stress + deformed mesh")
# New plot under the other one
pl.subplot(212)
pl.gca().set_aspect('equal', 'datalim')
syy = np.matrix([I[1, 0] for I in stress])
plot_def_tri_lin_mesh(nodes, elems, displ, syy, dsf,
                      "y-Stress + deformed mesh")

outfig = "cantileverStress-hw02.png"
pl.savefig(outfig)
