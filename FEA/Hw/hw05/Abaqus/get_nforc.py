# Gets the nodal forces in a nodal set within a part from an
# Abaqus ODB output file.

from odbAccess import *


class OdbReader:

    def __init__(self, odbFile):
        self.odbFile = odbFile
        self.odb = openOdb(path=self.odbFile)

    def run(self):

        stepName = self.odb.steps['tractionStep']
        count = 1

        fw = open("NFORC.results", 'w')
        fw.write("node_number,NFORCE_value\n")

        frame = stepName.frames[1]
        # Since Abaqus sucks (and we all know that), you
        # need to write the name of everything in upper
        # case.
        nforcSet = frame.fieldOutputs['NFORC2'].getSubset(
            region=self.odb.rootAssembly.instances['PLATEHOLEPARTINSTANCE'].nodeSets['TOP_NODES'])

        for nforcValue in nforcSet.values:
            print("Node, force ",
                  nforcValue.nodeLabel, nforcValue.data)
            fw.write("%9d,%10.3e\n" %
                     (nforcValue.nodeLabel, nforcValue.data))

        self.odb.close()


if __name__ == '__main__':

    import sys

    file = sys.argv[1]

    OdbReader(file).run()
