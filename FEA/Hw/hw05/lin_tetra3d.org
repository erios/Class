#+TITLE:
#+DATE:
#+SUBTITLE: FEA - Homework 5
#+AUTHOR: Edgar Rios

* Instructions

1) In Abaqus, construct a

   - [X] 3D geometry of a hole in a plate problem, using

     - [X] the parameters given in lecture slides FEclass_10_27_17;

     - [X] bi-axial symmetry to model 1/4 of the overall geometry.

   - [X] Mesh your geometry using linear tetrahedral elements (C3D4).

   - [ ] Calculate the stress concentration factor.

   You may make the mesh as coarse or fine as you wish (i.e. you don't need to show convergence with analytical solution).

2) In Python, develop a finite element code

   - [X] with the same geometry, boundary and loading conditions described above.

     See lecture slides FEclass_10_27_17 for code to get you started.

   - [X] Calculate and apply the equivalent nodal forces to the distributed pressure applied in Abaqus.

     See CAE_HoleInPate.pdf tutorial and accompanying Python script for information on how to extract nodal force data.

   - [X] Calculate the stress concentration factor.

   - [ ] Plot stress contours to show the maximum stress.

   Note: had you used proprietary software to plot tetrahedrons, you could have used the same "patch" function we used previously. This time there will be x, y, and z values, with each set of values containing 4 entries for each nodal coordinate (i.e. patch(x_values, y_values, z_values, stress_value)).

3) Write a brief report comparing results from your Python and Abaqus solutions.

   - [X] Compare stress concentration factors between
     - [X] your program
     - [ ] Abaqus and
     - [X] from tables.

   Include
   - [X] numerical comparisons
   - [-] stress plots showing max stress for both Python and Abaqus solutions.

Submit your Abaqus ~.inp~ file, Python ~.py~ file, and report as a single zipped file with the following format (HW5,Lastname,Firstname.zip) via Blackboard.

* Python implementation

The undeformed and deformed meshes are shown in figures [[fig-hw05-undeformed]] and [[fig-hw05-deformed]], respectively.

#+NAME: fig-hw05-undeformed
#+CAPTION: Undeformed geometry.
#+ATTR_LATEX: :width 0.85\textwidth
[[file:undeformed.png]]

#+NAME: fig-hw05-deformed
#+CAPTION: Deformed geometry (scaling factor: 50).
#+ATTR_LATEX: :width 0.85\textwidth
[[file:deformed.png]]

** Checking boundary conditions through displacements.

I present the displacements of bottom, left and top surfaces of \(\frac{1}{4}\) of the plate. The bottom is opposite to the direction of the pull and is symmetric with respect to the vertical axis. The left side is symmetric in the $x$-direction.

The results show that indeed, the boundary conditions are applied correctly. The bottom table ( [[tbl-hw05-bottom-displ]]) shows no displacement in the vertical direction, as much as table [[tbl-hw05-left-displ]] reflects no motion horizontally.

#+NAME: tbl-hw05-top-displ
#+CAPTION: Top nodes displacements
#+ATTR_LATEX: :bootabs t :environment longtabu
|      x (m) |     y (m) |      z (m) |
|------------+-----------+------------|
|  0.000e+00 | 4.628e-05 | -2.266e-04 |
|  0.000e+00 | 4.057e-05 | -8.897e-04 |
| -3.416e-06 | 7.429e-05 | -2.559e-04 |
| -7.704e-06 | 2.523e-05 | -8.827e-04 |
| -2.676e-06 | 3.225e-05 | -2.367e-04 |
| -3.571e-06 | 3.774e-05 | -2.430e-04 |
| -4.168e-06 | 4.135e-05 | -2.478e-04 |
| -4.588e-06 | 4.418e-05 | -2.516e-04 |
| -5.002e-06 | 4.626e-05 | -2.541e-04 |
| -5.307e-06 | 4.776e-05 | -2.556e-04 |
| -5.567e-06 | 4.887e-05 | -2.563e-04 |
| -5.730e-06 | 4.966e-05 | -2.564e-04 |
| -5.702e-06 | 5.028e-05 | -2.562e-04 |
| -5.079e-06 | 5.062e-05 | -2.559e-04 |
| -5.268e-06 | 7.718e-05 | -2.552e-04 |
| -5.638e-06 | 7.713e-05 | -2.554e-04 |
| -5.711e-06 | 7.658e-05 | -2.557e-04 |
| -5.688e-06 | 7.594e-05 | -2.556e-04 |
| -5.683e-06 | 7.509e-05 | -2.549e-04 |
| -5.748e-06 | 7.402e-05 | -2.535e-04 |
| -5.804e-06 | 7.257e-05 | -2.509e-04 |
| -5.703e-06 | 7.062e-05 | -2.474e-04 |
| -5.811e-06 | 6.826e-05 | -2.424e-04 |
| -5.456e-06 | 6.478e-05 | -2.361e-04 |
| -3.037e-06 | 5.719e-05 | -2.297e-04 |
| -7.141e-06 | 7.490e-05 | -3.014e-04 |

#+NAME: tbl-hw05-bottom-displ
#+CAPTION: Bottom nodes displacements
#+ATTR_LATEX: :bootabs t :environment longtabu
|      x (m) |     y (m) |      z (m) |
|------------+-----------+------------|
| -4.835e-07 | 0.000e+00 | -1.250e-03 |
| -5.353e-06 | 0.000e+00 | -2.265e-04 |
| -9.045e-06 | 0.000e+00 | -1.254e-03 |
| -8.225e-06 | 0.000e+00 | -8.830e-04 |
| -7.874e-06 | 0.000e+00 | -1.248e-03 |
| -7.144e-06 | 0.000e+00 | -1.247e-03 |
| -6.326e-06 | 0.000e+00 | -1.248e-03 |
| -5.575e-06 | 0.000e+00 | -1.250e-03 |
| -4.693e-06 | 0.000e+00 | -1.253e-03 |
| -3.943e-06 | 0.000e+00 | -1.256e-03 |
| -3.119e-06 | 0.000e+00 | -1.259e-03 |
| -1.669e-06 | 0.000e+00 | -1.264e-03 |
| -8.762e-06 | 0.000e+00 | -1.253e-03 |
| -7.643e-06 | 0.000e+00 | -1.250e-03 |
| -7.226e-06 | 0.000e+00 | -1.248e-03 |
| -6.991e-06 | 0.000e+00 | -1.249e-03 |
| -6.905e-06 | 0.000e+00 | -1.251e-03 |
| -6.601e-06 | 0.000e+00 | -1.254e-03 |
| -6.565e-06 | 0.000e+00 | -1.257e-03 |
| -6.506e-06 | 0.000e+00 | -1.260e-03 |
| -6.138e-06 | 0.000e+00 | -1.265e-03 |
| -9.932e-06 | 0.000e+00 | -1.238e-03 |

#+NAME: tbl-hw05-left-displ
#+CAPTION: Left nodes displacements
#+ATTR_LATEX: :bootabs t :environment longtabu
|      x (m) |     y (m) |      z (m) |
|------------+-----------+------------|
|  0.000e+00 | 1.729e-05 | -1.250e-03 |
|  0.000e+00 | 3.221e-06 | -1.268e-03 |
|  0.000e+00 | 4.628e-05 | -2.266e-04 |
|  0.000e+00 | 4.057e-05 | -8.897e-04 |
|  0.000e+00 | 1.758e-05 | -8.901e-04 |
|  0.000e+00 | 4.641e-05 | -2.570e-04 |
|  0.000e+00 | 4.985e-05 | -3.526e-04 |
|  0.000e+00 | 5.123e-05 | -4.190e-04 |
|  0.000e+00 | 5.125e-05 | -4.919e-04 |
|  0.000e+00 | 4.981e-05 | -5.731e-04 |
| -0.000e+00 | 4.789e-05 | -6.661e-04 |
|  0.000e+00 | 4.460e-05 | -7.711e-04 |
|  0.000e+00 | 3.630e-05 | -9.702e-04 |
|  0.000e+00 | 3.241e-05 | -1.035e-03 |
|  0.000e+00 | 2.885e-05 | -1.087e-03 |
|  0.000e+00 | 2.614e-05 | -1.127e-03 |
|  0.000e+00 | 2.402e-05 | -1.159e-03 |
|  0.000e+00 | 2.206e-05 | -1.184e-03 |
|  0.000e+00 | 2.059e-05 | -1.205e-03 |
|  0.000e+00 | 1.930e-05 | -1.222e-03 |
|  0.000e+00 | 1.827e-05 | -1.236e-03 |
|  0.000e+00 | 2.086e-05 | -7.714e-04 |
|  0.000e+00 | 2.326e-05 | -6.664e-04 |
|  0.000e+00 | 2.455e-05 | -5.734e-04 |
|  0.000e+00 | 2.496e-05 | -4.920e-04 |
|  0.000e+00 | 2.348e-05 | -4.189e-04 |
|  0.000e+00 | 1.947e-05 | -3.520e-04 |
|  0.000e+00 | 1.128e-05 | -2.871e-04 |
|  0.000e+00 | 3.082e-06 | -1.237e-03 |
|  0.000e+00 | 5.333e-06 | -1.222e-03 |
|  0.000e+00 | 6.468e-06 | -1.205e-03 |
|  0.000e+00 | 7.304e-06 | -1.184e-03 |
|  0.000e+00 | 8.190e-06 | -1.159e-03 |
|  0.000e+00 | 9.222e-06 | -1.127e-03 |
|  0.000e+00 | 1.051e-05 | -1.087e-03 |
|  0.000e+00 | 1.224e-05 | -1.036e-03 |
|  0.000e+00 | 1.473e-05 | -9.706e-04 |
|  0.000e+00 | 2.066e-05 | -2.303e-04 |

Other than the forces at the top nodes, the load values are all zero, as shown in [[tbl-hw05-top-force]]

#+NAME: tbl-hw05-top-force
#+CAPTION: Forces at the top nodes
#+ATTR_LATEX: :bootabs t :environment longtabu
| x (kN) | y (kN) | z (kN) |
|--------+--------+--------|
|     0. |  6.944 |     0. |
|     0. |  3.472 |     0. |
|     0. |  3.472 |     0. |
|     0. |  6.944 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |
|     0. | 10.417 |     0. |

It may be confusing to see such large displacements in the $z$ direction, but one needs to remember that this coordinate was not fixed. Therefore, it can displace without much stress.

** Stress values

- Max stress value in the xx direction ::
  1.481e+07 Pa

- Max stress value in the yy direction ::
  2.520e+07 Pa

- Max stress value in the zz direction ::
  1.021e+07 Pa

- Max stress value in the xy direction ::
  1.534e+07 Pa

- Max stress value in the yz direction ::
  1.572e+07 Pa

- Max stress value in the zx direction ::
  6.480e+06 Pa

These values are not the same as the ones in Abaqus (see [[#hw05-abaqus]]), althought the order of magnitude is similar. The question becomes: how did they become diffferent. The elasticity matrix in Voight notation seems to be right:

\[%\frac{E}{(1+\nu)\,(1 - 2\,\nu)}\,
\begin{bmatrix}
  8.890e+09 & 4.379e+09 & 4.379e+09 & 0.000e+00 & 0.000e+00 & 0.000e+00 \\
  4.379e+09 & 8.890e+09 & 4.379e+09 & 0.000e+00 & 0.000e+00 & 0.000e+00 \\
  4.379e+09 & 4.379e+09 & 8.890e+09 & 0.000e+00 & 0.000e+00 & 0.000e+00 \\
  0.000e+00 & 0.000e+00 & 0.000e+00 & 2.256e+09 & 0.000e+00 & 0.000e+00 \\
  0.000e+00 & 0.000e+00 & 0.000e+00 & 0.000e+00 & 2.256e+09 & 0.000e+00 \\
  0.000e+00 & 0.000e+00 & 0.000e+00 & 0.000e+00 & 0.000e+00 & 2.256e+09
\end{bmatrix}\,\si{Pa}\]

** Stress concentration factor

*** With Python

To calculate the stress concetration factor (SCF), the maximum value of stress in the \(yy\)-direction was divided by its mean. All the nodal values were considered. Ideally, this should only consider the bottom side, but I did not have the time to consolidate the nodal values by element. In this way, the value was 3.971. This is completely unrealistic (see below).

*** Analytically

Since the nominal stress is \(\sigma_{n} = \frac{P}{A}\), the stress concentration factor is the ratio between the maximum and the nominal stresses \(K = \frac{\sigma_{max}}{\sigma_{n}}\), and the maximum is given by \(\sigma_{max} = 3\,\sigma\), the
stress concentration factor is 3.

*** Abaqus

Sorry. This takes little time, but I haven't slept.

*** Tables

One can calculate the SCF with help from [[fig-hw05-SCF]]

#+NAME: fig-hw05-SCF
#+CAPTION: Stress concentration factor table
#+ATTR_LATEX: :width 0.85\textwidth
[[file:2017-11-06-113521_683x511_scrot.png]]

** Todo list                                                      :noexport:
*** DONE Extract set of nodes for the BC
CLOSED: [2017-11-06 Mon 10:21]
Based on the coordinates

*** DONE Extract forces depending on nodes
CLOSED: [2017-11-06 Mon 10:22]
Consolidate by nodal number (dumb double loop)

*** DONE Change load (symmetry) in Abaqus file
- Note taken on [2017-11-03 vie 09:32] \\
  Not needed, both surface and load are divided by 2
Divide by 2

* Abaqus
:PROPERTIES:
:CUSTOM_ID: hw05-abaqus
:END:
#+ATTR_LATEX: :width 0.85\textwidth
[[file:Abaqus/plateHoleS11.png]]
#+ATTR_LATEX: :width 0.85\textwidth
[[file:Abaqus/plateHoleS22.png]]
#+ATTR_LATEX: :width 0.85\textwidth
[[file:Abaqus/plateHoleS33.png]]
#+ATTR_LATEX: :width 0.85\textwidth
[[file:Abaqus/plateHoleS12.png]]
#+ATTR_LATEX: :width 0.85\textwidth
[[file:Abaqus/plateHoleS13.png]]
#+ATTR_LATEX: :width 0.85\textwidth
[[file:Abaqus/plateHoleS23.png]]

* Discussion

Beware: ranting ahead.

It was wonderful to see that I could plot the geometry in Python using MayaVi. It was hell to install it (I am still thinking of the two quizes this week and how I haven't slept). At the end, I could not plot the stresses, because the only way that I know of plotting requires indexed nodal values, and the values that I have are indexed only within the element (two elements may share a node).

Also, I learnt how to export CSV to ParaView, VTK from Python, compile VTK with Python3 wrappers, structure C3D4 meshes with Gmsh, export .inp meshes from Gmsh, import Gmsh meshes into Abaqus, and I know that nobody asked me for it... but this is a place for learning! (I know that doesn't count for many).

As I know that nobody else around here uses this software, and that installing it takes a long time (mobody else will possibly be able to produce the plots), I prepared two Python files: one does calculations only (~plateHole.py~) and the other one has the required code (once VTK and MayaVi are installed) to show the undeformed and deformed geometry.

Thank you again for your attention, patience and support. I acknowledge all the effort that is put into this course, and I like the fact that my self torture is actually letting me learn.

* Configuration                                            :noexport:ARCHIVE:
** General options
- Export table of contents, export equations as pictures,
  export author
  #+OPTIONS: toc:t author:t date:t tex:t LaTeX:t

- Show the headlines at start
  #+STARTUP: content

- Footnotes
  #+OPTIONS: f:t

- Use python3, do not evaluate when exporting and create
  a session (all blocks are connected)
  #+PROPERTY: header-args:python :python python3 :eval no-export
  #+PROPERTY: header-args:latex :exports results :results raw :eval no-export

- Load symbols
  #+LaTeX_HEADER: \input{../../symbols.tex}

- SI units
  #+LaTeX_HEADER: \usepackage{siunitx} \sisetup{per-mode=fraction,fraction-function=\tfrac}
#  #+LaTeX_HEADER: \usepackage[mediumspace,squaren,Gray,thinqspace]{}
 # #+LaTeX_HEADER: \sisetup{per-mode=fraction, }

- Substitute "Listings" for "Blocks" when exporting code to LaTeX
  # http://stackoverflow.com/questions/2814714/
  #+LATEX_HEADER: \renewcommand\lstlistingname{Block}
  #+LATEX_HEADER: \renewcommand\lstlistlistingname{Code blocks}
  #+LATEX_HEADER: \def\lstlistingautorefname{Blk.}

- Add support for BibLaTeX
  #+LATEX_HEADER: \usepackage[style=numeric-comp,
  #+LATEX_HEADER:  sorting=none,hyperref=true,backref=true,
  #+LATEX_HEADER:  url=true,backend=biber,natbib=true]{%
  #+LATEX_HEADER:  biblatex}
  #+LATEX_HEADER: \addbibresource{../../References.bib}

- Add support for longtabu
  #+LATEX_HEADER: \usepackage{pdflscape,longtable,tabu,booktabs}

- Add booktabs
  # #+LATEX_HEADER: \usepackage{booktabs}

- SVG support
  #+LaTeX_HEADER: \input{../../externalapps.tex}

- Tables a subfigures
  #+LATEX_HEADER: \usepackage{subcaption}

** Export to DOC
To get a working file, first export to odt with Org-mode
(~C-c C-e o o~), and then run this (you can do ~C-c C-c~
on it; make sure that LibreOffice is not running)
g#+BEGIN_SRC shell :results none
 libreoffice --headless --convert-to doc ./transiso.odt
#+END_SRC

** Export equations (and previews) as pictures
   # #+OPTIONS: LaTeX: t
   # #+OPTIONS: tex:t
   #+OPTIONS: tex:dvipng
   # https://stackoverflow.com/a/20033865

** Others
   # http://orgmode.org/worg/org-contrib/babel/how-to-use-Org-Babel-for-R.html

* Local variables :noexport:ARCHIVE:
   # Local Variables:
   # coding: utf-8
   # bibtex-dialect: biblatex
   # org-latex-pdf-process: ("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"            "biber %b"            "pdflatex -interaction nonstopmode -output-directory %o %f"            "pdflatex -interaction nonstopmode -output-directory %o %f")
   # End:
