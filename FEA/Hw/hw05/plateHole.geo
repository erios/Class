lc = 1e-8;

// * Parameters
// ** Lengths
b = 50e-3;
b2 = b/2;
// ** Radius
r = 5e-3;
// ** Number of nodes, and ratios
// Short
Ns = 10; Rs = 0.8;
// Diagonal
Nd = 10; Rd = 0.825;
// Long
Nl = 10; Rl = 0.9;
// Top
Nx2 = Nl; Rx2 = Rl;
Ny2 = 10; Ry2 = 1.00;
// Circumferential
Nc = 10; Rc = 1.00;

// Origin
//+
Point(1) = {0, 0, 0, lc};
// Right lower corner
//+
Point(2) = {b2, 0, 0, lc};
// Top nodes
//+
Point(3) = {b2, b2, 0, lc};
Point(4) = {0, b2, 0, lc};
// Hole nodes
//+
Point(5) = {r, 0, 0, lc};
Point(6) = {0, r, 0, lc};
//+ Mid
Point(7) = {r*Cos(Pi/4), r*Sin(Pi/4), 0, lc};

//+ Bottom line
Line(1) = {2, 5};
Transfinite Line {1} = Ns Using Progression Rs;
//+ Right line
Line(2) = {3, 2};
Transfinite Line {2} = Nl Using Progression Rl;
//+ Top line
Line(3) = {3, 4};
Transfinite Line {3} = Nl Using Progression Rl;
//+ Left line
Line(4) = {4, 6};
Transfinite Line {4} = Ns Using Progression Rs;

// ** Top square
//+
Translate {0, b2, 0} {
  Duplicata { Point{4, 3}; }
}
//+
Line(5) = {4, 8};
Transfinite Line {5} = Ny2 Using Progression Ry2;
//+
Line(6) = {9, 8};
Transfinite Line {6} = Nx2 Using Progression Rx2;
//+
Line(7) = {9, 3};
Transfinite Line {7} = Ny2 Using Progression Ry2;


// ** Hole
//+
Circle(8) = {5, 1, 7};
Transfinite Line {8} = Nc Using Progression Rc;
//+
Circle(9) = {7, 1, 6};
Transfinite Line {9} = Nc Using Progression Rc;
//+
Line(10) = {3, 7};
Transfinite Line {10} = Nd Using Progression Rd;

// * Surfaces
//+
Line Loop(11) = {8, -10, 2, 1};
//+
Plane Surface(12) = {11};
//+
Line Loop(13) = {-3, -4, 9, 10};
//+
Plane Surface(14) = {13};
//+
Line Loop(15) = {7, 3, 5, -6};
//+
Plane Surface(16) = {15};
//+
Transfinite Surface {12};
//+
Transfinite Surface {14};
//+
Transfinite Surface {16};

// * Extrusion
//+
Extrude {0, 0, 1e-3} {
  Surface{12, 14, 16};
  Layers{1};
}


// * Groups (sets)
//+
Physical Surface("bottom") = {37};
//+
Physical Surface("left") = {59, 77};
//+
Physical Surface("hole") = {55, 25};
//+
Physical Surface("top") = {81};
//+
Physical Surface("right") = {69, 33};
//+
Physical Surface("front") = {82, 60, 38};
//+
Physical Surface("back") = {16, 14, 12};
//+
Physical Volume("plateHolePart") = {3, 2, 1};

//+
// Translate {0, 0, 1e-3} {
//   Duplicata { Surface{16, 14, 12}; }
// }
//
// // Projected surface lines
// //+
// Transfinite Line {31, -26} = Ns Using Progression Rs;
// //+
// Transfinite Line {24} = Nd Using Progression Rd;
// //Right proj
// //+
// Transfinite Line {30} = Nl Using Progression Rl;
// // Top proj
// //+
// Transfinite Line {19} = Nl Using Progression Rl;
//
// // Top square lines
// //+
// Transfinite Line {-21} = Nx2 Using Progression Rx2;
// //+
// Transfinite Line {20, 18} = Ny2 Using Progression Ry2;
//
// // Hole
// //+
// Transfinite Line {28, 25} = Nc Using Progression Rc;
//
// // Surfaces
// //+
// Transfinite Surface {27};
// //+
// Transfinite Surface {22};
// //+
// Transfinite Surface {17};
//
// // "Extrusion" lines
// //+
// Line(32) = {8, 19};
// //+
// Line(33) = {4, 15};
// //+
// Line(34) = {6, 36};
// //+
// Line(35) = {7, 31};
// //+
// Line(36) = {5, 43};
// //+
// Line(37) = {2, 53};
// //+
// Line(38) = {3, 11};
// //+
// Line(39) = {9, 10};
//
// // Lateral (thin faces)
// //+
// Line Loop(40) = {20, -32, -5, 33};
// //+
// Plane Surface(41) = {40};
// //+
// Line Loop(42) = {26, -33, 4, 34};
// //+
// Plane Surface(43) = {42};
// //+
// Line Loop(44) = {25, -34, -9, 35};
// //+
// Ruled Surface(45) = {44};
// //+
// Line Loop(46) = {28, -35, -8, 36};
// //+
// Ruled Surface(47) = {46};
// //+
// Line Loop(48) = {31, -36, -1, 37};
// //+
// Ruled Surface(49) = {48};
// //+
// Line Loop(50) = {30, -37, -2, 38};
// //+
// Ruled Surface(51) = {50};
// //+
// Line Loop(52) = {18, -38, -7, 39};
// //+
// Ruled Surface(53) = {52};
// //+
// Line Loop(54) = {21, -39, 6, 32};
// //+
// Ruled Surface(55) = {54};
//
//
// //+
// Transfinite Line {32, 33, 34, 36, 35, 37, 38, 39} = 1 Using Progression 1;
// //+
// Transfinite Surface {41};
// //+
// Transfinite Surface {43};
// //+
// Transfinite Surface {45};
// //+
// Transfinite Surface {47};
// //+
// Transfinite Surface {49};
// //+
// Transfinite Surface {51};
// //+
// Transfinite Surface {53};
// //+
// Transfinite Surface {55};
//
// // Sets (groups)
// //+
// Physical Surface("bottom") = {49};
// //+
// Physical Surface("left") = {41, 43};
// //+
// Physical Surface("top") = {55};
// //+
// Surface Loop(56) = {17, 53, 51, 27, 47, 45, 22, 43, 41, 55, 16, 14, 12, 49};
//
//
// //+
// Volume(57) = {56};
// //+
// Physical Volume("myPart") = {57};
