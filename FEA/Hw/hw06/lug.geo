// Tolerance to node positions
lc = DefineNumber[ 1e-5, Name "Mesh/point_tol" ];
// origin coordinates
o_x = 0;
o_y = 0;
o_z = 0;

// Bar geometry
length = 0.125;
height = 0.05;
width = 0.02;

// Hole geometry
in_rad = 0.015;
out_rad = 0.025;

// * Hole
// ** Centre of hole
hole_ctr = newp;
Point(hole_ctr) = {o_x, o_y, o_z, lc};
// ** Quarter-circunference 1
// *** Starting point (top)
hole_P1 = newp;
hole_P1_x = o_x - in_rad * Cos(Pi/4);
hole_P1_y = o_y + in_rad * Sin(Pi/4);
Point(hole_P1) = {hole_P1_x, hole_P1_y, o_z, lc};
// *** Ending point (bottom)
hole_P2 = newp;
hole_P2_x = hole_P1_x;
hole_P2_y = o_y - in_rad * Sin(Pi/4);
Point(hole_P2) = {hole_P2_x, hole_P2_y, o_z, lc};
// *** Arc 1 (right)
hole_A = newl;
Circle(hole_A) = {hole_P1, hole_ctr, hole_P2};

// * Conecting section
// ** Corner top point
conn_l_top_P = newp;
Point(conn_l_top_P) = {-out_rad, out_rad, o_z, lc};
// ** Corner bottom point
conn_l_bot_P = newp;
Point(conn_l_bot_P) = {-out_rad, -out_rad, o_z, lc};
// ** Top diagonal
conn_l_top_L = newl;
Line(conn_l_top_L) = {hole_P1, conn_l_top_P};
// ** Vertical
conn_l_vert_L = newl;
Line(conn_l_vert_L) = {conn_l_top_P, conn_l_bot_P};
// ** Bottom diagonal
conn_l_bot_L = newl;
Line(conn_l_bot_L) = {conn_l_bot_P, hole_P2};
// ** Closed loop for surface
conn_l_loop = newl;
Line Loop(conn_l_loop) = {-hole_A, conn_l_top_L, conn_l_vert_L, conn_l_bot_L};
// ** Surface of connector
conn_l_surf = news;
Plane Surface(conn_l_surf) = {conn_l_loop};
// ** Discretise lines
Transfinite Line {hole_A, conn_l_vert_L} = 7 Using Progression 1;
Transfinite Line {conn_l_bot_L, -conn_l_top_L} = 3 Using Progression 0.7;

// * Top connector
// ** Top point
conn_top_top_P = newp;
Point(conn_top_top_P) = {o_x, o_y + out_rad, o_z, lc};
// ** Arc point
conn_top_a_P = newp;
Point(conn_top_a_P) = {o_x, o_y + in_rad, o_z, lc};
// ** Arc
conn_top_A = newl;
Circle(conn_top_A) = {hole_P1, hole_ctr, conn_top_a_P};
// ** Vertical line
conn_top_v_L = newl;
Line(conn_top_v_L) = {conn_top_a_P, conn_top_top_P};
// ** Top line
conn_top_top_L = newl;
Line(conn_top_top_L) = {conn_top_top_P, conn_l_top_P};
// ** Closed loop for surface
conn_top_loop = newl;
Line Loop(conn_top_loop) = {conn_top_A, conn_top_v_L, conn_top_top_L, -conn_l_top_L};
// ** Surface of top connector
conn_top_surf = news;
Plane Surface(conn_top_surf) = {conn_top_loop};
// ** Discretise lines
Transfinite Line {conn_top_A, conn_top_top_L} = 3 Using Progression 1;
Transfinite Line {conn_top_v_L} = 3 Using Progression 1;

// * Bottom connector
// ** Bottom point
conn_bot_bot_P = newp;
Point(conn_bot_bot_P) = {o_x, o_y - out_rad, o_z, lc};
// ** Arc point
conn_bot_a_P = newp;
Point(conn_bot_a_P) = {o_x, o_y - in_rad, o_z, lc};
// ** Arc
conn_bot_A = newl;
Circle(conn_bot_A) = {hole_P2, hole_ctr, conn_bot_a_P};
// ** Vertical line
conn_bot_v_L = newl;
Line(conn_bot_v_L) = {conn_bot_a_P, conn_bot_bot_P};
// ** Bottom line
conn_bot_bot_L = newl;
Line(conn_bot_bot_L) = {conn_bot_bot_P, conn_l_bot_P};
// ** Closed loop for surface
conn_bot_loop = newl;
Line Loop(conn_bot_loop) = {conn_bot_A, conn_bot_v_L, conn_bot_bot_L, conn_l_bot_L};
// ** Surface of bottom connector
conn_bot_surf = news;
Plane Surface(conn_bot_surf) = {conn_bot_loop};
// * Discretise lines
Transfinite Line {conn_bot_A, conn_bot_bot_L} = 3 Using Progression 1;
Transfinite Line {conn_bot_v_L} = 3 Using Progression 1;

// * Semi-ring
// ** Inner semi-circumference
semi_in = newl;
Circle(semi_in) = {conn_bot_a_P, hole_ctr, conn_top_a_P};
// ** Outter semi-circumference
semi_out = newl;
Circle(semi_out) = {conn_bot_bot_P, hole_ctr, conn_top_top_P};
// ** Closed loop for surface
semi_loop = newl;
Line Loop(semi_loop) = {semi_out, -conn_top_v_L, -semi_in, conn_bot_v_L};
// ** Surface of ring
semi_surf = news;
Plane Surface(semi_surf) = {semi_loop};
// ** Discretise lines
Transfinite Line {semi_out, semi_in} = 9 Using Progression 1;
//+
Point(10) = {-length, -out_rad, 0, lc};
Point(11) = {-length, out_rad, 0, lc};
//+
Line(21) = {4, 11};
//+
Line(22) = {11, 10};
//+
Line(23) = {10, 5};
//+
Transfinite Line {21, 23} = 12 Using Progression 1;
//+
Transfinite Line {22} = 7 Using Progression 1;
//+
Line Loop(24) = {21, 22, 23, -3};
//+
Plane Surface(25) = {24};

// * Structured mesh from planes
Transfinite Surface {conn_l_surf, conn_top_surf, conn_bot_surf, semi_surf, 25};
// * Convert triangles to quadrangles
Recombine Surface {conn_l_surf, conn_top_surf, conn_bot_surf, semi_surf, 25};
//+
Extrude {0, 0, width} {
  Surface{25, 6, 11, 20, 16};
  Layers{3};
  Recombine;
}
//+
Physical Volume("myVol") = {1, 3, 2, 5, 4};
