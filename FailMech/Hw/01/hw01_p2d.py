#################################################
#                    Modules                    #
#################################################
import numpy as np
from matplotlib import pyplot as pl


#################################################
#                   Functions                   #
#################################################
def Q_trans(a, b, g):
    """This function builds the inverse of Q (the coordinate
    system transformation matrix) from Euler angles alpha
    (a), beta (b) and gamma (g) Keyword Arguments: a --
    Euler angle 1 b -- Euler angle 2 g -- Euler angle 3

    """
    matrix = np.matrix
    cos = np.cos
    sin = np.sin
    res = matrix([
        [cos(a)*cos(g),
         -cos(a)*sin(b) - cos(b)*cos(g)*sin(a),
         sin(a)*sin(b)],
        [cos(g)*sin(a) + cos(a)*cos(b)*sin(g),
         cos(a)*cos(b)*cos(g) - sin(a)*sin(g),
         -cos(a)*sin(b)],
        [sin(b)*sin(g),
         cos(g)*sin(b),
         cos(b)]
        ])

    return res


def matQ(a = None, b = None, c = None):
    """This function builds the coordinate system transformation
    matrix Q from Euler angles alpha (a), beta (b) and gamma
    (g) Keyword Arguments: a -- Euler angle 1 b -- Euler
    angle 2 g -- Euler angle 3

    """
    if not a: a = 0
    if not b: b = 0
    if not c: c = 0
    return Q_trans(a, b, c).transpose()



#################################################
#                   Main body                   #
#################################################
# Set output precision and line width
np.set_printoptions(precision=3, linewidth=105)

# Set the angles
theta = [0, 45, -45]
theta_rad = np.deg2rad(theta)
# Define the stress tensor
# * Shear components
sigma_A = np.matrix([[0, 3, 0], [3, 0, 0], [0, 0, 0]])

# Calculate the values of Q for each alpha
Q = [matQ(alpha_i, 0, 0) for alpha_i in theta_rad]
# Calculate the values of the transformed tensor
sigma_B = [np.dot(Q_i.dot(sigma_A), Q_i.T)
           for Q_i in Q]

for I in range(len(theta)):
    sigma_i = sigma_B[I]
    theta_i = theta[I]
    print("Angle: {0:0.3f}°".format(theta[I]))
    print("Stress tensor (kPa): ")
    print(sigma_B[I], "\n")
