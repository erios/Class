#################################################
#                    Modules                    #
#################################################
import numpy as np
from matplotlib import pyplot as pl
# eigencouples
from numpy.linalg import eigh


#################################################
#                   Main body                   #
#################################################
# Set output precision and line width
np.set_printoptions(precision=3, linewidth=105)

eigvl, eigvc = eigh(np.matrix([[4.532, 0, 1.528],
                               [0, 0, 0],
                               [1.528, 0, 0]]).T)

print(" {0:>6} {1:>6} {2:>6}".format("s_3", "s_2", "s_1"))
print(eigvl)
