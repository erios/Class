#################################################
#                    Modules                    #
#################################################
import numpy as np
from matplotlib import pyplot as pl


#################################################
#                   Functions                   #
#################################################
def critical_stress_pairs(crit_val, theta_ran):
    """Calculates the components of stress for angles from 0° to
    360° that reach a given critical value (crit_val); a threshold
    Will fail with crit_val = 0

    Keyword Arguments:
    crit_val -- (float)
    theta_ran -- (range) range of values to consider for the
        angle

    Returns
    sigma_all -- (list(float)) A list of paired values. One
        value of each pair is the same as the critical value
    """
    # Exit if the critical value is 0
    if crit_val == 0:
        raise ValueError("Cannot take a critical value equal to 0")
    # List to store pairs of values
    sigma_all = [[float(), float()] for I in theta_ran]
    # Loop over the angle values
    for I, theta in enumerate([np.deg2rad(ang) for ang in theta_ran]):
        # Coordinates of the maximum stresses (stress pairs)
        sigma_A = crit_val * np.cos(theta)
        sigma_B = crit_val * np.sin(theta)
        # Check which is closer to the critical value (in size)
        if abs(crit_val) - abs(sigma_A) < \
           abs(crit_val) - abs(sigma_B):
            # If the first one is bigger, use that as a factor
            k = abs(crit_val / sigma_A)
        else:
            # If the second one is bigger, use that as a factor
            k = abs(crit_val / sigma_B)
        # Make one of the values equal to the critical
        # value, and scale the other one accordingly (keep
        # the angle; increase the magnitude to match
        # critical value)
        sigma_all[I] = [k * sigma_A, k * sigma_B]

    return sigma_all


def split_critical_stress_pairs(crit_val1, theta_ran1,
                                crit_val2 = None,
                                theta_ran2 = None):
    """
    Keyword Arguments:
    crit_val1  -- (float)
    theta_ran1 -- (range)
    crit_val2  -- (float; default None)
    theta_ran2 -- (range; default None)
    """
    if crit_val2 is None or theta_ran2 is None:
        crit_val2 = crit_val1
        theta_ran2 = theta_ran1

    sect1 = critical_stress_pairs(crit_val1, theta_ran1)

    theta_ran1_2 = range(np.max(theta_ran1) + 1,
                         np.min(theta_ran2) + 1)
    val1_2 = crit_val1 * crit_val2
    r1_2 = val1_2/(crit_val2 * np.sin(np.deg2rad(theta_ran1_2)) -
                   crit_val1 * np.cos(np.deg2rad(theta_ran1_2)))
    sect2 = [[val * np.cos(np.deg2rad(theta_ran1_2[I])),
              val * np.sin(np.deg2rad(theta_ran1_2)[I])]
             for I, val in enumerate(r1_2)]

    sect3 = critical_stress_pairs(crit_val2, theta_ran2)

    theta_ran2_1 = range(np.max(theta_ran1) + 180,
                         360 - (np.min(theta_ran1)))
    val2_1 = crit_val1 * crit_val2
    r2_1 = - val2_1/(crit_val1 * np.sin(np.deg2rad(theta_ran2_1)) -
                     crit_val2 * np.cos(np.deg2rad(theta_ran2_1)))
    sect4 = [[val * np.cos(np.deg2rad(theta_ran2_1[I])),
              val * np.sin(np.deg2rad(theta_ran2_1)[I])]
             for I, val in enumerate(r2_1)]

    return sect1 + sect2 + sect3 + sect4


def princ_stress_from_plane_stress(stress_pairs):
    """Sets the first principal value of stress to the
      \max(\sigma_A, \sigma_B) for each (\sigma_A, \sigma_B)
      in stress_pairs. A value of 0 is added to each pair in
      stress_pairs to determine \sigma_3, that is, if
      \min(\sigma_A, \sigma_B) \geq 0, then \sigma_3 =
      0. Otherwise, \sigma_3 = \min(\sigma_A, \sigma_B).

    Keyword arguments
    stress_pairs -- (list(float)) principal values of plane
        stress

    Requires
    import numpy as np
    """
    res = [sorted([VAL[0], 0, VAL[1]], reverse=True)
           for VAL in stress_pairs]
    return res


def check_tresca(stress_vals, strength):
    """Checks a list of pairs representing the values of
    principal stress to the Tresca (maximum shear stress)
    failure criterion for plane stress. A value of 0 is
    added to the pair to determine \sigma_3, that is, if
    \min(\sigma_A, \sigma_B) \geq 0, then \sigma_3 =
    0. Otherwise, \sigma_3 = \min(\sigma_A, \sigma_B)

    Keyword Arguments:
    stress_vals -- (list(float)) Principal
        values of stress ([[sigma_A, sigma_B],
                           [sigma_A, sigma_B], ...])
    strength -- (float) used to check the stress values
        (S_{y})
         \sigma_1 - \sigma_3 \geq strength

    Returns
    factors, check
    factors -- (list(float)) list of factors by which the
        principal plane stress values would reach the
        failure criterion
    check -- (list(int)) list of indices where the failure
        criterion was not reached

    Requires
    import numpy as np
    princ_stress_from_plane_stress
    """
    # Sets principal stresses from plane stress pairs
    sigma_princ = princ_stress_from_plane_stress(stress_vals)

    # Use Tresca criterion (these values are equivalent to
    # the safety factors)
    factors = [strength/(VAL[0] - VAL[2])
              for VAL in sigma_princ]
    # If the value is lower than, it means that the
    # failure criterion has not been reached
    # TODO: this does not work if sigma_princ[0] and
    #       sigma_princ[1] are negative, and strength is
    #       positive
    # TODO: this may be unstable for large numbers
    check = [I
             for I, VAL in enumerate(sigma_princ)
             if VAL[0] - VAL[2] < strength]
    return factors, check


#################################################
#                   Main body                   #
#################################################
# Set output precision and line width
np.set_printoptions(precision=3, linewidth=105)

Sy = 300

# Set initial values of plane stress
# Make a circle
theta = np.deg2rad([I for I in range(361)])
circle = [[np.cos(I), np.sin(I)] for I in theta]

# Get which values are under the Tresca criterion, and get the factors by which it would be reached
factors, check = check_tresca(circle, Sy)

# Scale the values to reach the criterion
MSS_vals = [[val * factors[I]
             for val in circle[I]] for I in check]

# f, c = check_tresca(MSS_vals, Sy)

# Extract each column
sigma_A, sigma_B = list(zip(*MSS_vals))

pl.plot(sigma_A, sigma_B)

pl.title('Maximum Normal Stress')
pl.xlabel("sigma_A")
pl.ylabel("sigma_B")

pl.grid("on")

pl.savefig("./02/p4-1.png")
