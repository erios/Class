* Configuration                           :noexport:ARCHIVE:
** Document options

- No table of contents
  #+OPTIONS: toc:nil

- Footnotes
  #+OPTIONS: f:t

- Style, symbols, etc.
  #+LATEX_HEADER: \input{/home/edgar/.emacs.d/plugins/symbols.tex}

- My external apps
  #+LATEX_HEADER: \input{/home/edgar/.emacs.d/plugins/externalapps.tex}
  - Set path for SVG
    #+LATEX_HEADER: \def\epsdir{./}
    #+LATEX_HEADER: \def\svgdir{./}

- Add support for BibLaTeX
  #+LATEX_HEADER: \usepackage[style=numeric-comp,
  #+LATEX_HEADER:  sorting=none,hyperref=true,backref=true,
  #+LATEX_HEADER:  url=true,backend=biber,natbib=true]{%
  #+LATEX_HEADER:  biblatex}
  #+LATEX_HEADER: \addbibresource{../References.bib}

- Long and rotated tables
  #+LATEX_HEADER:  \usepackage{pdflscape,longtable,tabu}

- Bold math symbols
  #+LATEX_HEADER: \usepackage{bm}

- Do not evaluate when exporting
  #+PROPERTY: header-args:latex :exports results :eval no-export :results raw

- Use python3, do not evaluate when exporting
  #+PROPERTY: header-args:python :python python3 :eval no-export

- Do not evaluate blocks when exporting
  #+PROPERTY: header-args :eval no-export

- Change caption head for listings
  #+LATEX_HEADER: \renewcommand\lstlistingname{Block}
  #+LATEX_HEADER: \renewcommand\lstlistlistingname{Code blocks}
  #+LATEX_HEADER: \def\lstlistingautorefname{Blk.}

- Use smart-quotes
  #+OPTIONS: ':t

** How to use this document

*** Exporting to PDF

#+BEGIN_SRC emacs-lisp
  (setq org-latex-pdf-process
        '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
          "biber %b"
          "pdflatex -interaction nonstopmode -output-directory %o %f"
          "pdflatex -interaction nonstopmode -output-directory %o %f"))

  ;; From org-ref-help
  (setq bibtex-dialect 'biblatex)
#+END_SRC

*** Word-wrapping
#+BEGIN_SRC emacs-lisp
  (use-package visual-fill-column
    :ensure t
    ;; If this is enabled, it fucks up Helm
    ;; (progn
    ;;   (global-visual-fill-column-mode))
    )
  (remove-hook 'org-mode-hook #'turn-on-auto-fill)
  (add-hook 'org-mode-hook 'turn-on-visual-line-mode)
  ;; Change the length of line wrapping
  (add-hook 'org-mode-hook (lambda ()
                             (set-fill-column 60)
                             (auto-fill-mode 0)
                             (visual-fill-column-mode)
                             (setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))
                             ))
#+END_SRC


* P1d

 Calculate the principal stresses using the eigenvalues of the stress tensor (include screenshots of Python code and answer).

** Code
#+INCLUDE: "~/Documentos/Class/FailMech/Hw/02/hw02_p1d.py" src python

#+INCLUDE: "~/Documentos/Class/FailMech/Hw/code-blocks-FailMech.org::res_hw02_p1d"

* P2 Brittle vs Ductile.

Explain key differences in the mechanical behavior between ductile and brittle materials, and how you treat them differently when predicting failure.

#+CAPTION: Mechanical differences between ductile and brittle materials (hw02-P2).
#+ATTR_LATEX: :booktabs t :float sideways :environment longtabu
| Characteristic    | Ductile                        | Brittle                           |
|-------------------+--------------------------------+-----------------------------------|
| Plasticity        | Undergoes plasticity after UTS | Almost none: breaks close to UTS  |
| Failure mode      | Shear stress                   | Tensile stress                    |
| Failure locus     | Region of high strain energy   | Near stress concentration; cracks |
| Failure metric    | Yield strength                 | Ultimate strength                 |
| Impact            | Insensitive                    | Sensitive                         |
| Compression limit | Similar to tension             | Differs from tension              |
* P4
Using Python (or other software), generate plots of the following failure theories for planar stress (σ_{A} on the x-axis, and σ_{B} on the y-axis). Use load lines at 1 degree increments over 360 degrees. Staple code and plots to your assignment. Note: In blackboard, I've provided /example code/ for the Maximum Normal Stress Theory.

** Maximum-Shear-Stress (MSS) Theory

when S_{y} = 300 MPa [1 part]

*** Code
#+INCLUDE: "~/Documentos/Class/FailMech/Hw/02/hw02_p4_1.py" src python

[[file:./p4-1.png]]

** Coulomb-Mohr Theory for Ductile Materials

when S_{yt} = 300 MPa  and S_{yc} = 800 MPa

*** Code
#+INCLUDE: "~/Documentos/Class/FailMech/Hw/02/hw02_p4_2.py" src python

[[file:./p4-2.png]]

** Modified Mohr                                 :noexport:

when S_{yt} = 300 MPa and S_{yc} = 800 MPa

*** Code
#+INCLUDE: "~/Documentos/Class/FailMech/Hw/02/hw02_p4_1.py" src python

[[file:./02/p4-1.png]]

* P5c                                             :noexport:
#+BEGIN_SRC python :results output
  import numpy as np
  A = np.matrix([[3, 2, 1],
                 [1, 4, 5],
                 [0, 1, 2]])

  B = np.matrix([[3, -1, 2],
                 [1, 0, 1],
                 [3, -2, 4]])

  print(A.dot(B))
#+END_SRC

#+RESULTS:
: [[ 14  -5  12]
:  [ 22 -11  26]
:  [  7  -4   9]]
