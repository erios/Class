#################################################
#                    Modules                    #
#################################################
import numpy as np
from matplotlib import pyplot as pl
# eigencouples
from numpy.linalg import eigh


#################################################
#                   Functions                   #
#################################################
def isotropic_compliance(E, nu):
    """Returns a 6x6 matrix for an linear and isotropic elastic
    material (Voight notation)
    Keyword Arguments:
    E  -- (float) modulus of elasticity
    nu -- (float) Poisson's ratio

    Requires
    import numpy as np
    """
    topleft = -nu/E * np.ones([3, 3])
    topleft[[0, 1, 2], [0, 1, 2]] = 1/E
    topright = np.zeros([3, 3])
    botleft = np.zeros([3, 3])
    G = E/(2 + nu + nu)
    botright = np.divide(np.identity(3), G)
    res = np.vstack(
        (np.hstack((topleft, topright)),
         np.hstack((botleft, botright))))
    return np.matrix(res)


def sym3Dtensor2vec(sigma_orig):
    sigma = np.matrix(sigma_orig)
    vector = np.matrix([float() for I in range(6)])
    vector[0, :3] = np.diag(sigma)
    vector[0, 3:] = [sigma[0, 1],
                     sigma[1, 2],
                     sigma[0, 2]]
    return vector.T


def vec2sym3Dtensor(vec):
    tensor = np.matrix(np.diag([float()
                          for I in range(3)]))
    # Off-diagonal
    # Nomenclature (left here for historical reasons)
    # ->: right arrow
    # ^: up arrow
    #
    # ^ e2         ^ e1          ^ e3
    # | -> e21     | -> e13      | -> e32
    # |  ^ e12     |  ^ e31      |  ^ e23
    # +-----> e1   +------> e3   +----->e2
    tensor[0, 1] = vec[3]  # e12
    tensor[1, 2] = vec[4]  # e23
    tensor[0, 2] = vec[5]  # e13
    tensor += tensor.T
    # Set diagonal (first three elements)
    for I in range(3):
        tensor[I, I] = vec[I]
    return tensor


#################################################
#                   Main body                   #
#################################################
# Set output precision and line width
np.set_printoptions(
    precision=3, linewidth=105,
    formatter={'float': lambda x: format(x, ' 0.3e')})

E = 70e9
nu = 0.3

sigma_lst = [
    [[0, 0, 0],
     [0, 0, 0],
     [0, 0, 100e6]],

    [[-50e6, 0, 0],
     [0, 0, 0],
     [0, 0, 100e6]],

    [[0, 50e6, 0],
     [50e6, 0, 0],
     [0, 0, 0]],

    [[0, 50e6, 0],
     [50e6, 0, 0],
     [0, 0, 100e6]]
]

for stress in sigma_lst:
    # Convert the 2D matrix to a vector in Voight's notation
    sigma = sym3Dtensor2vec(stress)
    # Calculate strain in Voight's notation with compliance and
    # stress tensors
    strain_eng_vec = isotropic_compliance(E, nu).dot(sigma)
    # Compensate for real strain (epsilon = gamma)
    strain_eng_vec[3:6] /= 2
    # Convert the vector in Voight's notation to 2D matrix
    strain_eng = vec2sym3Dtensor(strain_eng_vec)
    # Calculate the dilational portion of stress
    # * Calculate the mean of the stress
    strain_eng_mean = np.sum(strain_eng_vec[:3])/3
    # * The dilational portion is the mean as an identity matrix
    strain_eng_dil = strain_eng_mean * np.matrix(np.identity(3))
    # The deviatoric portion is the difference between the full
    # strain and the dilational part
    strain_eng_dev = strain_eng - strain_eng_dil

    print("Stress tensor:")
    print(vec2sym3Dtensor(sigma))
    
    print("Engineering strain tensor:")
    print(strain_eng)
    
    print("Dilational component of strain:")
    print(strain_eng_dil)
    
    print("Deviatoric component of strain:")
    print(strain_eng_dev)
    
    print()
