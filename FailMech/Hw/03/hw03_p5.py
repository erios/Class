#################################################
#                    Modules                    #
#################################################
import numpy as np
from matplotlib import pyplot as pl


#################################################
#                   Functions                   #
#################################################
def check_max_norm_strain(stress_vals, strain_lim, E, nu):
    """Checks a list of pairs representing the values of
    principal stress to the maximum normal strain failure
    criterion for plane stress.

    Keyword Arguments:
    stress_vals -- (list(float)) Principal
        values of stress ([[sigma_A, sigma_B],
                           [sigma_A, sigma_B], ...])
    strain_lim -- (float) used to check the stress values
         max(abs(strain)) \geq strain_lim

    Returns
    factors -- (list(float)) list of factors by which the
        principal plane stress values would reach the
        failure criterion
    check -- (list(int)) list of indices where the failure
        criterion was not reached

    Requires
    import numpy as np
    """
    # Calculate plain strain
    nuE = - nu / E
    epsilon_AB = [np.dot(
        np.matrix([[1, nuE],
                   [nuE, 1]]),
        np.matrix([[VAL[0]],
                   [VAL[1]]]))
                  for VAL in stress_vals]

    # Get maximum between the absolute values of each pair
    epsilon_max = [np.max([np.abs(VAL) for VAL in VALS])
                   for VALS in epsilon_AB]

    # Check where the strain is zero for some reason
    avoid = np.where(np.asarray(epsilon_max) == 0)[0]

    # Calculate the multiplication factor, except for those
    # values of strain which are zero
    factors = [strain_lim / epsilon_max[I]
               if I not in avoid
               else float('nan')
               for I in range(len(stress_vals))]

    # If the value is lower than, it means that the
    # failure criterion has not been reached
    # TODO: this may be unstable for large numbers
    below = [I for I, VAL in enumerate(epsilon_max)
             if VAL < strain_lim]

    above = [I for I, VAL in enumerate(epsilon_max)
             if VAL > strain_lim]
    return factors, below, above


#################################################
#                   Main body                   #
#################################################
# Set output precision and line width
np.set_printoptions(
    precision=3, linewidth=105,
    formatter={'float': lambda x: format(x, ' 0.3e')})

E = 70e9
nu = 0.3
epsilon_f = 2e-3

# Set initial values of plane stress
# Make a circle
theta = np.deg2rad([I for I in range(361)])
circle = [[np.cos(I), np.sin(I)] for I in theta]

# Get which values are off the strain criterion, and get
# the factors by which it would be reached
factors, below, above = check_max_norm_strain(circle,
                                              epsilon_f,
                                              E, nu)
# Scale the values to reach the criterion
# Initialise list to hold results
MNS_vals = [[float('nan') for I in VAL]
            for VAL in circle]

# Calculate adjusted values
Enu2 = E/(1- nu * nu)
MNS_vals = [np.multiply(Enu2 * factors[I], np.dot(
    np.matrix([[1, nu],
               [nu, 1]]),
    np.matrix([[circle[I][0]],
               [circle[I][1]]]))).flatten().tolist()[0]
            for I in below + above]

sigma_A, sigma_B = list(zip(*MNS_vals))

pl.plot(sigma_A, sigma_B)

pl.title('Maximum Normal Stress')
pl.xlabel("sigma_A")
pl.ylabel("sigma_B")

pl.grid("on")

pl.savefig("./03/p5.png")
