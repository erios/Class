* Configuration                           :noexport:ARCHIVE:
** Document options

- No table of contents
  #+OPTIONS: toc:nil

- Footnotes
  #+OPTIONS: f:t

- Style, symbols, etc.
  #+LATEX_HEADER: \input{/home/edgar/.emacs.d/plugins/symbols.tex}

- My external apps
  #+LATEX_HEADER: \input{/home/edgar/.emacs.d/plugins/externalapps.tex}
  - Set path for SVG
    #+LATEX_HEADER: \def\epsdir{./}
    #+LATEX_HEADER: \def\svgdir{./}

- Add support for BibLaTeX
  #+LATEX_HEADER: \usepackage[style=numeric-comp,
  #+LATEX_HEADER:  sorting=none,hyperref=true,backref=true,
  #+LATEX_HEADER:  url=true,backend=biber,natbib=true]{%
  #+LATEX_HEADER:  biblatex}
  #+LATEX_HEADER: \addbibresource{../References.bib}

- Long and rotated tables
  #+LATEX_HEADER:  \usepackage{pdflscape,longtable,tabu}

- Bold math symbols
  #+LATEX_HEADER: \usepackage{bm}

- Do not evaluate when exporting
  #+PROPERTY: header-args:latex :exports results :eval no-export :results raw

- Use python3, do not evaluate when exporting
  #+PROPERTY: header-args:python :python python3 :eval no-export

- Do not evaluate blocks when exporting
  #+PROPERTY: header-args :eval no-export

- Change caption head for listings
  #+LATEX_HEADER: \renewcommand\lstlistingname{Block}
  #+LATEX_HEADER: \renewcommand\lstlistlistingname{Code blocks}
  #+LATEX_HEADER: \def\lstlistingautorefname{Blk.}

- Use smart-quotes
  #+OPTIONS: ':t

** How to use this document

*** Exporting to PDF

#+BEGIN_SRC emacs-lisp
  (setq org-latex-pdf-process
        '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
          "biber %b"
          "pdflatex -interaction nonstopmode -output-directory %o %f"
          "pdflatex -interaction nonstopmode -output-directory %o %f"))

  ;; From org-ref-help
  (setq bibtex-dialect 'biblatex)
#+END_SRC

*** Word-wrapping
#+BEGIN_SRC emacs-lisp
  (use-package visual-fill-column
    :ensure t
    ;; If this is enabled, it fucks up Helm
    ;; (progn
    ;;   (global-visual-fill-column-mode))
    )
  (remove-hook 'org-mode-hook #'turn-on-auto-fill)
  (add-hook 'org-mode-hook 'turn-on-visual-line-mode)
  ;; Change the length of line wrapping
  (add-hook 'org-mode-hook (lambda ()
                             (set-fill-column 60)
                             (auto-fill-mode 0)
                             (visual-fill-column-mode)
                             (setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))
                             ))
#+END_SRC


* P3
** Code

#+INCLUDE: "~/Documentos/Class/FailMech/Hw/04/hw04_p3.py" src python
* P4
*Application*. Explain why or why not the von Mises failure criterion is useful for the following applications:

1. Optimizing geometry of a metal part to reduce failure from fatigue loading.

   - No: v. Mises does not consider frequency

   - Yes: can still be combined with the S-N theory

2. Optimizing geometry of a metal part to reduce failure from static loading.

   - Yes: If the load has a distortional component

   - No: If the load is hydrostatic (or isotropic[fn::isotropic: invariant with respect to direction; same values when measured along axes in all directions; having the same properties in all directions, from iso- + -tropic, from Greek tropikos "belonging to a turning," from tropos "a turning, way, manner," from trepein "to turn"])

3. Reproducing the stress-strain curve observed when testing a specimen to UTS.

   - Yes: If the loading condition can be guaranteed to be uni-axial

   - No: Any other variation

4. Predicting the effect of loading rate on failure.

   - No: v. Mises does not consider time effects

5. Write a new “real world” application that would be suitable.

   Predicting failure of a part subjected to tri-axial loading, namely, a trampoline.

* P5
Give a mathematical explanation for why von Mises failure criterion (a.k.a. distortion energy) is unaffected by pressure and Drucker-Prager failure criterion is affected by pressure (/J_{2}/ is the 2^{nd} invariant of deviatoric stress and /k/ is shear strength).

- Von Mises: J_{2}=k^{2}
- Drucker-Pager: \(\sqrt{J_{2}} = A + B\,I_{1}\)

In short, the second one prescribes a change of length (first invariant), which can only be caused by a pressure term.

* P6

#+BEGIN_SRC bash :exports none :results none
  for i in tresca drucker-pager-maxima vonmises distortional; do mogrify -trim +repage "./$i".png; done
#+END_SRC

** Maximum stress theory, a.k.a. Tresca (S_{yt}=300 MPa)

#+CAPTION: Code for Tresca (S_{yt}=300 MPa).
#+header: :results none
#+BEGIN_SRC maxima programmode: false;
  load(draw)$
  /* http://emp.byui.edu/BrownD/CAS/wxMaxima/Graphics_in_wxMaxima-for-Math-215.pdf */
  print("Tresca")$
  Syt: 300$

  set_draw_defaults(
    view = [60,120],
    colorbox=false,
    xyplane = 0,
    proportional_axes = xyz,
    /* user_preamble = "set autoscale fix" */
    )$

  plot1: gr3d(
    palette = gray,
    implicit(
      max(abs(x-y), abs(y-z), abs(z-x)) = Syt,
      x, -Syt, Syt,
      y, -Syt, Syt,
      z, -Syt, Syt
      )
    )$

  draw(
    file_name = "/home/edgar/Documentos/Class/FailMech/Hw/04/tresca",
    terminal = 'png,
    plot1)$
#+END_SRC

#+CAPTION: Plot of the Tresca contour.
#+ATTR_LATEX: :width 0.5\textwidth
#+RESULTS:
[[file:./tresca.png]]

** Distortional energy (S_{yt}= S_{yc} =300 MPa)

#+CAPTION: Maxima code for distortion energy (S_{yt}= S_{yc} =300 MPa)
#+BEGIN_SRC maxima :results none
  load(draw)$
  /* http://emp.byui.edu/BrownD/CAS/wxMaxima/Graphics_in_wxMaxima-for-Math-215.pdf */
  print("Von Mises")$
  Syt: 300$
  set_draw_defaults(
      enhanced3d=[(x+y+z), x,y,z],
      palette = gray,
      view = [60,60],
      colorbox=false,
      xyplane = 0,
      proportional_axes = xyz
  )$
  plot1: gr3d(
      implicit(
      sqrt(((x - y)^2 + (y - z)^2 + (z -x)^2)/2) = Syt,
      x, -Syt, Syt,
      y, -Syt, Syt,
      z, -Syt, Syt
      )
    )$

  plot2: gr3d(
    view = [55,135],
    implicit(
      sqrt(((x - y)^2 + (y - z)^2 + (z -x)^2)/2) = Syt,
      x, -Syt, Syt,
      y, -Syt, Syt,
      z, -Syt, Syt
      )
    )$

  draw(
    file_name = "/home/edgar/Documentos/Class/FailMech/Hw/04/vonmises",
    terminal = 'png,
    /* plot1)$, plot2, columns=2)$ */
    plot1)$
#+END_SRC

#+CAPTION: Plot of the distortional energy contour.
#+ATTR_LATEX: :width 0.5\textwidth
[[file:./vonmises.png]]

#+CAPTION: Explicit function (ugly)
#+BEGIN_SRC maxima :exports none
  sol: solve(sqrt(((x - y)^2 + (y - z)^2 + (z -x)^2)/2) = 300, z);
  plot3d(
  [
   [rhs(sol[1]), [x, -300, 300], [y, -300, 300]],
   rhs(sol[2]), [x, -300, 300], [y, -300, 300]
  ],
  [x, -300, 300], [y, -300, 300], [z, -300, 300],
  same_xyz
  );
#+END_SRC

** Drucker-pager (S_{yt}=300 MPa, S_{yc} =600 MPa)

#+CAPTION: Maxima code for Drucker-Pager (S_{yt}=300 MPa, S_{yc} =600 MPa).
#+BEGIN_SRC maxima
  load(draw)$
  /* http://emp.byui.edu/BrownD/CAS/wxMaxima/Graphics_in_wxMaxima-for-Math-215.pdf */
  print("Drucker-pager")$
  Syc: 600$
  Syt: 300$
  draw3d(
    terminal = 'png,
    file_name = "/home/edgar/Documentos/Class/FailMech/Hw/04/drucker-pager-maxima",
    enhanced3d = true,
    palette = gray,
    colorbox = false,
    xyplane = 0,
    proportional_axes = xyz,
    implicit(
      /* x^2 + y^2 = 4, */
      (Syc/Syt - 1)/2 * (x + y + z) + (Syc/Syt + 1)/2 * sqrt(((x - y)^2 + (y - z)^2 + (z -x)^2)/2)=Syc,
      x, -Syc, Syc,
      y, -Syc, Syc,
      z, -Syc, Syc
      )
    )$
#+END_SRC

#+CAPTION: Plot of the Drucker-Pager contour.
#+ATTR_LATEX: :width 0.5\textwidth
[[file:./drucker-pager-maxima.png]]

** Distortion energy as a function of 2^{nd} deviatoric stress

#+CAPTION: Maxima code for distortion energy as a function of 2^{nd} deviatoric stress
#+BEGIN_SRC maxima
  print("J2")$
  draw3d(
    file_name = "/home/edgar/Documentos/Class/FailMech/Hw/04/distortional",
    terminal = 'png,
    enhanced3d = true,
    palette = gray,
    colorbox = false,
    xyplane = 0,
    proportional_axes = xyz,
    implicit(
      /* http://homepages.engineering.auckland.ac.nz/~pkel015/SolidMechanicsBooks/Part_II/08_Plasticity/08_Plasticity_02_Stress_Analysis.pdf */
      /* J[2] = 1/6 * ((x - y)^2 + (y - z)^2 + (z -x)^2) */
      subst([k=1], 1/6 * ((x - y)^2 + (y - z)^2 + (z -x)^2) = k),
      x, -2, 2,
      y, -2, 2,
      z, -2, 2
      )
  )$
#+END_SRC

#+ATTR_LATEX: :width 0.5\textwidth
#+CAPTION: Plot of the J_{2} contour.
[[file:./distortional.png]]

** Others                                        :noexport:
Von mises

https://www.researchgate.net/post/how_can_i_plot_the_von_mises_formula_in_a_3d-coordinate-system
#+BEGIN_SRC octave
  clear all;close all;clc
                                 % initialize hydrostatic axis
  Imin=-1000;
  Imax=1000;
  n=100;
  I=linspace(Imin,Imax,n);
                                  % deviatoric plane
  yieldstress=400;
  sqrt(2/3)*yieldstress
  for i=1:n
    theta=(i-1)*2*pi/(n-1);
    y(i)=yieldstress*sqrt(2/3).*cos(theta+45);
    z(i)=yieldstress*sqrt(2/3).*sin(theta+45);
  end
  plot(y,z)
                                  % build 3d plot
  x=ones(n,1)*I;
                                  % paramters for rotation
  ang=45;
  cang=cos(ang*pi/180);
  sang=sin(ang*pi/180);
  ang2=-45;
  cang2=cos(ang2*pi/180);
  sang2=sin(ang2*pi/180);
  figure
                                  % initialize 3d plot
  mises3d=surf([Imin,Imin;Imax,Imax],[Imin,Imin;Imax,Imax],[Imin,Imin;Imax,Imax]);
  set(mises3d,'x',(x*cang+z'*ones(1,n)*sang)*cang2+y'*ones(1,n)*sang2,...
      'y',(x*cang+z'*ones(1,n)*sang)*-sang2+y'*ones(1,n)*cang2,...
      'z',(x*-sang+z'*ones(1,n)*cang)*cang2);
#+END_SRC

Matsuoka-Nakai 1
https://www.researchgate.net/post/how_can_i_plot_mohr-coulomb_yield_criterion_in_3d_using_matlab
#+BEGIN_SRC octave
  clear;
  clc;
  close all;
  m = 50;
  n = 25;
  mm = 6*m-5;
  fr = (30/180)*pi;
  k = 6*sin(fr)/(sqrt(3)*(3-sin(fr)));
  pmin = 0;
  pmax = 10;
                                  % polar angle [0, 2*pi]
  phi = linspace(0, 2*pi, mm);
          % lode angle (periodic (-pi/6, pi/6], (pi/6, -pi/6])
  ith1 = linspace(-pi/6, pi/6, m);
  ith2 = linspace(pi/6, -pi/6, m);
  th1 = ith1(2:end);
  th2 = ith2(2:end);
  th = [ith1, th2, th1, th2, th1, th2];
                                  % hydrostatic stress
  p = linspace(pmin, pmax, n);
                                  % create grids
  Aphi = repmat(phi, n, 1);
  Ath = repmat(th, n, 1);
  P = repmat(p', 1, mm);
                                % Matsuoka-Nakai yield surface
  kmn = (9 - sin(fr)^2)/(1 - sin(fr)^2);
  A1 = (kmn - 3)/(kmn - 9);
  A2 = kmn/(kmn - 9);
  a = 2/sqrt(3)*sqrt(A1);
  b = 1/3;
  c = A2/(A1)^(3/2);
  r = P./(a.*cos(b.*acos(c.*sin(3.*Ath))));
  X = P + r.*cos(Aphi+2*pi/3);
  Y = P + r.*cos(Aphi);
  Z = P + r.*cos(Aphi-2*pi/3);
  figure(1)
  surf(X, Y, Z);
  axis equal;
#+END_SRC

Mansuoka-Nakai 2
https://www.researchgate.net/post/how_can_i_plot_mohr-coulomb_yield_criterion_in_3d_using_matlab
#+BEGIN_SRC octave
  % 3D diagram
  global X CS NNN L AT XI
  global U UI EQ1 EQ NSTEP
  %
  % 3d diagram for time (1) or freqency (2)
  coding = 1;
  if coding == 1
  tdr = U;
  tdrint = UI;
  for i = 1:NSTEP
  %break
  tdr(2,i) = (tdr(4,i)+tdrint(L/2,i))/2;
  tdr(1,i) = (tdr(2,i)+tdrint(L/2,i))/2;
  tdr(3,i) = (tdr(2,i)+tdr(4,i))/2;
  tdr(NNN,i) = tdr(1,i);
  tdr(NNN-1,i) = tdr(2,i);
  tdr(NNN-2,i) = tdr(3,i);
  %tdr(round(NNN/2),i) = tdr(round(NNN/2)-1,i)+(tdr(round(NNN/2)-1,i)-tdr(round(NNN/2)-2,i))/4;
  end
  tdr_tot = [tdrint(1:L/2,:);tdr(:,:);tdrint(L/2+1:end,:)];
  eq_tot = [EQ(:,1:L/2)';EQ1(:,:)';EQ(:,L/2+1:end)'];
  xnew = [XI(1:L/2),X,XI(L/2+1:end)];
  time = AT:AT:NSTEP*AT;
  figure(1)
  hold on
  for i=1:1:length(xnew)
  %break
  plot3(time,xnew(i)*ones(1,length(time))/200,tdr_tot(i,:)/0.001,'k')
  end
  for i=61:1:154
  break
  plot3(time,xnew(i)*ones(1,length(time))/200,eq_tot(i,:)/0.001,'k-')
  end
  view(0,70)
  %[tt,xxx] = meshgrid(time,xnew/X(1));
  %mesh(tt,xxx,tdr_tot./0.001)
  return
  end
  nfft = 2^nextpow2(3000);
  fftsignal = zeros(NNN,nfft);
  fftinter = zeros(L,nfft);
  ffteq1 = zeros(NNN,nfft);
  ffteq = zeros(L,nfft);
  for i = 1 : NNN
  fftsignal(i,:) = fft(U(i,:),nfft);
  ffteq1(i,:) = fft(EQ1(:,i),nfft);
  end
  for i = 1 : L
  fftinter(i,:) = fft(UI(i,:),nfft);
  ffteq(i,:) = fft(EQ(:,i),nfft);
  end
  % correction of sem-sine shaped valley at corner points
  for i = 1:nfft
  %break
  fftsignal(2,i) = (fftsignal(4,i)+fftinter(L/2,i))/2;
  fftsignal(1,i) = (fftsignal(2,i)+fftinter(L/2,i))/2;
  fftsignal(3,i) = (fftsignal(2,i)+fftsignal(4,i))/2;
  fftsignal(NNN,i) = fftsignal(1,i);
  fftsignal(NNN-1,i) = fftsignal(2,i);
  fftsignal(NNN-2,i) = fftsignal(3,i);
  fftsignal(round(NNN/2),i) = fftsignal(round(NNN/2)-1,i)+(fftsignal(round(NNN/2)-1,i)-fftsignal(round(NNN/2)-2,i))/4;
  end
  %Next, calculate the frequency axis, which is defined by the sampling rate
  T = AT;
  fs = 1/T;
  f = fs/2*linspace(0,1,nfft/2+1);
  % dimensionless frequency
  df = 2*X(1)*f./CS;
  % select dimensionless frequency " max "
  sdf_max = 4.0;
  sdf_min = 0.125;
  vec_max = find(df>=sdf_max);
  vec_min = find(df<=sdf_min);
  frq_max = vec_max(1);
  frq_min = vec_min(end);
  dff = df(frq_min:frq_max);
  %
  xnew = [XI(1:L/2),X,XI(L/2+1:end)];
  %
  [ff,xx] = meshgrid(dff,xnew/X(1));
  %
  ffteqn = zeros(NNN,nfft);
  for i=1:NNN
  ffteqn(i,:) = ffteq1(end,:);
  end
  y1new = abs(fftinter(1:L/2,frq_min:frq_max))./(abs(ffteq(1:L/2,frq_min:frq_max))/1);
  y2new = abs(fftsignal(:,frq_min:frq_max))./(abs(ffteqn(:,frq_min:frq_max))/1);
  y3new = abs(fftinter(L/2+1:end,frq_min:frq_max))./(abs(ffteq(L/2+1:end,frq_min:frq_max))/1);
  ynew = [y1new;y2new;y3new];
  %
  figure(2)
  %axis normal
  %colormap('gray')
  meshz(ff,xx,ynew)
  axis([sdf_min sdf_max -4 4 min(min(ynew)) max(max(ynew))])
  %tri = delaunay(ff,xx);
  %trisurf(tri,ff,xx,ynew)
  %surface(ff,xx,ynew)
  %surf(ff,xx,ynew)
  %contourf(xx,ff,ynew,4)
  colorbar
#+END_SRC


#+BEGIN_QUOTE
I improved on @Greg's answer and made a solid 3D cylinder with a top and bottom surface and rewrote the equation so that you can translate in the x, y,and z
#+END_QUOTE
https://stackoverflow.com/a/33688716

#+BEGIN_SRC python
  from mpl_toolkits.mplot3d import Axes3D
  import mpl_toolkits.mplot3d.art3d as art3d
  import matplotlib.pyplot as plt
  import numpy as np
  from matplotlib.patches import Circle

  def plot_3D_cylinder(radius, height, elevation=0, resolution=100, color='r', x_center = 0, y_center = 0):
      fig=plt.figure()
      ax = Axes3D(fig, azim=30, elev=30)

      x = np.linspace(x_center-radius, x_center+radius, resolution)
      z = np.linspace(elevation, elevation+height, resolution)
      X, Z = np.meshgrid(x, z)

      Y = np.sqrt(radius**2 - (X - x_center)**2) + y_center # Pythagorean theorem

      ax.plot_surface(X, Y, Z, linewidth=0, color=color)
      ax.plot_surface(X, (2*y_center-Y), Z, linewidth=0, color=color)

      floor = Circle((x_center, y_center), radius, color=color)
      ax.add_patch(floor)
      art3d.pathpatch_2d_to_3d(floor, z=elevation, zdir="z")

      ceiling = Circle((x_center, y_center), radius, color=color)
      ax.add_patch(ceiling)
      art3d.pathpatch_2d_to_3d(ceiling, z=elevation+height, zdir="z")

      ax.set_xlabel('x-axis')
      ax.set_ylabel('y-axis')
      ax.set_zlabel('z-axis')

      plt.show()

  # params
  radius = 3
  height = 10
  elevation = -5
  resolution = 100
  color = 'r'
  x_center = 3
  y_center = -2

  plot_3D_cylinder(radius, height, elevation=elevation, resolution=resolution, color=color, x_center=x_center, y_center=y_center)
#+END_SRC


http://inside.mines.edu/~vgriffit/pubs/All_J_Pubs/72.pdf
https://en.wikipedia.org/wiki/Mohr%E2%80%93Coulomb_theory

https://eli.thegreenplace.net/2014/meshgrids-and-disambiguating-rows-and-columns-from-cartesian-coordinates/
#+BEGIN_SRC python
  # https://stackoverflow.com/a/2492042
  from sympy import var, plotting
  var('x y')
  plotting.plot3d(-(((-3*y**2)+6*x*y-3*x**2+2)**(0.5)-y-x)/2)

  # https://eli.thegreenplace.net/2014/meshgrids-and-disambiguating-rows-and-columns-from-cartesian-coordinates/
  n = 10
  x = [I/n for I in range(n + 1)]
  y = [I/n for I in range(n + 1)]
  xx, yy = np.meshgrid(x, y)
  zz = -(np.sqrt((-3*yy**2)+6*xx*yy-3*xx**2+2)-yy-xx)/2
  plt.clf()
  fig = plt.figure(1)
  ax = fig.gca(projection='3d')
  ax.plot_surface(xx, yy, zz, alpha=0.3)
  cset = ax.contourf(xx, yy, zz, zdir='z', offset=0,
                     levels=np.linspace(-100, 100, 100))
  cset = ax.contourf(xx, yy, zz, zdir='x', offset=0)
  cset = ax.contourf(xx, yy, zz, zdir='y', offset=0)
  ax.set_xlabel('xx')
  ax.set_xlim(0, 2)
  ax.set_ylabel('yy')
  ax.set_ylim(0, 2)
  ax.set_zlabel('zz')
  ax.set_zlim(0, 2)
  plt.show()

  from mpl_toolkits.mplot3d import axes3d
  import matplotlib.pyplot as plt
  import numpy as np
  plt.clf()
  fig = plt.figure(1)
  ax = fig.gca(projection='3d')
  X, Y, Z = axes3d.get_test_data(0.05)
  ax.plot_surface(X, Y, Z, rstride=8, cstride=8, alpha=0.3)
  cset = ax.contourf(X, Y, Z, zdir='z', offset=-100,
                     levels=np.linspace(-100, 100, 1200), cmap=plt.cm.jet)
  cset = ax.contourf(X, Y, Z, zdir='x', offset=-40, cmap=plt.cm.jet)
  cset = ax.contourf(X, Y, Z, zdir='y', offset=40, cmap=plt.cm.jet)
  ax.set_xlabel('X')
  ax.set_xlim(-40, 40)
  ax.set_ylabel('Y')
  ax.set_ylim(-40, 40)
  ax.set_zlabel('Z')
  ax.set_zlim(-100, 100)
  plt.show()

  # https://stackoverflow.com/a/4687582
  from mpl_toolkits.mplot3d import axes3d
  import matplotlib.pyplot as plt
  import numpy as np

  def plot_implicit(fn, bbox=(-2.5,2.5)):
      ''' create a plot of an implicit function
      fn  ...implicit function (plot where fn==0)
      bbox ..the x,y,and z limits of plotted interval'''
      xmin, xmax, ymin, ymax, zmin, zmax = bbox*3
      fig = plt.figure()
      ax = fig.add_subplot(111, projection='3d')
      A = np.linspace(xmin, xmax, 100) # resolution of the contour
      B = np.linspace(xmin, xmax, 15) # number of slices
      A1,A2 = np.meshgrid(A,A) # grid on which the contour is plotted

      for z in B: # plot contours in the XY plane
          X,Y = A1,A2
          Z = fn(X,Y,z)
          cset = ax.contour(X, Y, Z+z, [z], zdir='z')
          # [z] defines the only level to plot for this contour for this value of z

      for y in B: # plot contours in the XZ plane
          X,Z = A1,A2
          Y = fn(X,y,Z)
          cset = ax.contour(X, Y+y, Z, [y], zdir='y')

      for x in B: # plot contours in the YZ plane
          Y,Z = A1,A2
          X = fn(x,Y,Z)
          cset = ax.contour(X+x, Y, Z, [x], zdir='x')

      # must set plot limits because the contour will likely extend
      # way beyond the displayed level.  Otherwise matplotlib extends the plot limits
      # to encompass all values in the contour.
      ax.set_zlim3d(zmin,zmax)
      ax.set_xlim3d(xmin,xmax)
      ax.set_ylim3d(ymin,ymax)

      plt.show()

  def goursat_tangle(x,y,z):
      a,b,c = 0.0,-5.0,11.8
      return x**4+y**4+z**4+a*(x**2+y**2+z**2)**2+b*(x**2+y**2+z**2)+c

  plot_implicit(goursat_tangle)

  def sphere(x,y,z):
      return x**2 + y**2 + z**2 - 2.0**2

  def translate(fn,x,y,z):
      return lambda a,b,c: fn(x-a,y-b,z-c)

  def union(*fns):
      return lambda x,y,z: np.min(
          [fn(x,y,z) for fn in fns], 0)

  def intersect(*fns):
      return lambda x,y,z: np.max(
          [fn(x,y,z) for fn in fns], 0)

  def subtract(fn1, fn2):
      return intersect(fn1, lambda *args:-fn2(*args))

  plot_implicit(union(sphere,translate(sphere, 1.,1.,1.)), (-2.,3.))


  def vmises(x, y, z, Syt=300):
      return (((x - y)**2 + (y - z)**2 + (z -x)**2)/2)**(0.5) - Syt

  plot_implicit(vmises, bbox=(-300,300))

  def drupag(x, y, z, Syc=600, Syt=300):
      m = Syc / Syt
      return (m - 1)/2 * (x + y + z) + (m + 1)/2 * np.sqrt(((x - y)**2 + (y - z)**2 + (z -x)**2)/2) - Syc

  plot_implicit(drupag, bbox=(-600,600))


  # Animation
  # https://pythonprogramming.net/wireframe-graph-python/?completed=/3d-bar-charts-python-matplotlib/
  from mpl_toolkits.mplot3d import axes3d
  import matplotlib.pyplot as plt
  import numpy as np

  '''
  def get_test_data(delta=0.05):

      from matplotlib.mlab import  bivariate_normal
      x = y = np.arange(-3.0, 3.0, delta)
      X, Y = np.meshgrid(x, y)

      Z1 = bivariate_normal(X, Y, 1.0, 1.0, 0.0, 0.0)
      Z2 = bivariate_normal(X, Y, 1.5, 0.5, 1, 1)
      Z = Z2 - Z1

      X = X * 10
      Y = Y * 10
      Z = Z * 500
      return X, Y, Z

  '''


  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')

  x, y, z = axes3d.get_test_data(0.05)
  ax.plot_wireframe(x,y,z, rstride=2, cstride=2)

  plt.show()

  from __future__ import print_function
  """
  A very simple 'animation' of a 3D plot
  """
  from mpl_toolkits.mplot3d import axes3d
  import matplotlib.pyplot as plt
  import numpy as np
  import time

  def generate(X, Y, phi):
      R = 1 - np.sqrt(X**2 + Y**2)
      return np.cos(2 * np.pi * X + phi) * R

  plt.ion()
  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')

  xs = np.linspace(-1, 1, 50)
  ys = np.linspace(-1, 1, 50)
  X, Y = np.meshgrid(xs, ys)
  Z = generate(X, Y, 0.0)

  wframe = None
  tstart = time.time()
  for phi in np.linspace(0, 360 / 2 / np.pi, 100):

      oldcol = wframe

      Z = generate(X, Y, phi)
      wframe = ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2)

      # Remove old line collection before drawing
      if oldcol is not None:
          ax.collections.remove(oldcol)

      plt.draw()

  print ('FPS: %f' % (100 / (time.time() - tstart)))
#+END_SRC

#+BEGIN_SRC python
  from __future__ import print_function

  from mpl_toolkits.mplot3d import axes3d
  import matplotlib.pyplot as plt
  import numpy as np
  import time


  def generate(X, Y, phi):
      '''
      Generates Z data for the points in the X, Y meshgrid and parameter phi.
      '''
      R = 1 - np.sqrt(X**2 + Y**2)
      return np.cos(2 * np.pi * X + phi) * R


  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')

  # Make the X, Y meshgrid.
  xs = np.linspace(-1, 1, 50)
  ys = np.linspace(-1, 1, 50)
  X, Y = np.meshgrid(xs, ys)

  # Set the z axis limits so they aren't recalculated each frame.
  ax.set_zlim(-1, 1)

  # Begin plotting.
  wframe = None
  tstart = time.time()
  for phi in np.linspace(0, 180. / np.pi, 100):
      # If a line collection is already remove it before drawing.
      if wframe:
          ax.collections.remove(wframe)

      # Plot the new wireframe and pause briefly before continuing.
      Z = generate(X, Y, phi)
      wframe = ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2)
      plt.pause(.001)

  print('Average FPS: %f' % (100 / (time.time() - tstart)))


  from mpl_toolkits.mplot3d import axes3d
  import matplotlib.pyplot as plt

  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')

  # load some test data for demonstration and plot a wireframe
  X, Y, Z = axes3d.get_test_data(0.1)
  ax.plot_wireframe(X, Y, Z, rstride=5, cstride=5)

  # rotate the axes and update
  for angle in range(0, 360):
      ax.view_init(30, angle)
      plt.draw()
      plt.pause(.001)
#+END_SRC

#+BEGIN_SRC python
  #
  x = np.arange(-1, 1.1, 0.01)
  y = np.arange(-1, 1.1, 0.01)
  xx, yy = np.meshgrid(x, y)

  # Von Mises
  zz1 = -(np.sqrt((-3*yy**2)+6*xx*yy-3*xx**2+2)-yy-xx)/2
  zz2 = (np.sqrt((-3*yy**2)+6*xx*yy-3*xx**2+2)+yy+xx)/2
  pl.clf()
  fig = pl.figure(1)
  ax = fig.gca(projection='3d')
  ax.plot_surface(xx, yy, zz1, alpha=0.3)
  ax.plot_surface(xx, yy, zz2, alpha=0.3)
  ax.set_xlabel('xx')
  ax.set_xlim(0, 2)
  ax.set_ylabel('yy')
  ax.set_ylim(0, 2)
  ax.set_zlabel('zz')
  ax.set_zlim(0, 2)
  pl.show()

  # Drucker-Prager
  Syc = 1.5
  Syt = 1
  m = Syc / Syt
  zz1 = abs(xx - yy)
#+END_SRC
