#################################################
#                    Modules                    #
#################################################
import numpy as np
from matplotlib import pyplot as pl
# eigencouples
from numpy.linalg import eigh


#################################################
#                   Main body                   #
#################################################
# Set output precision and line width
np.set_printoptions(
    precision=3, linewidth=105,
    formatter={'float': lambda x: format(x, ' 0.3e')})

sigma_lst = [
    np.matrix(
        [[50, 0, 0],
         [0, 0, 0],
         [0, 0, 0]]),
    np.matrix(
        [[50, 0, 0],
         [0, 50, 0],
         [0, 0, 50]]),
    np.matrix(
        [[0, 50, 50],
         [50, 0, 50],
         [50, 50, 0]]),
    np.matrix(
        [[50, 50, 50],
         [50, 50, 50],
         [50, 50, 50]])
]

for stress in sigma_lst:
    # --- Debugging code ----
    # stress = np.matrix([[57, 0, 24], [0, 50, 0], [24, 0, 43]])
    # stress = sigma_lst[4]
    # --- end Debugging
    
    val, vec = np.linalg.eigh(stress)
    
    sqrt3 = 3**(0.5)
    sqrt3i = 1 / sqrt3
    
    v_oct = (vec[:, 0] + vec[:, 1] + vec[:, 2])
    # n_oct = v_oct / (np.dot(v_oct.T, v_oct)[0, 0])**0.5
    n_oct = np.matrix([[sqrt3i] for I in range(3)])
    
    sigma_p = np.matrix(np.diag(val))
    
    n1 = sqrt3i * np.matrix(np.ones([1, 3]))
    n2 = np.divide(np.matrix([[-1, 2, -1]]), 6**0.5)
    n3 = np.divide(np.matrix([[-1, 0, 1]]), 2**0.5)
    
    Q = np.vstack([n1, n2, n3])
    
    sigma_oct = Q.dot(sigma_p.dot(Q.T))
    tau_oct_mag = (sum([VAL[0, 0] * VAL[0, 0]
                    for VAL in sigma_oct[1:3, 0]])) ** 0.5
    
    vmises = (((val[0] - val[1])**2
               + (val[1] - val[2])**2
               + (val[2] - val[0])**2) / 2)**(0.5)
    
    print("Stress:")
    print(stress)
    print("Magnitude of octahedral shear: {0:0.4e}".format(
        tau_oct_mag))
    print("Von Mises': {0:0.4e}".format(vmises))
    print("Ratio between tau_oct and v. Mises: {0:0.4e}".format(
        tau_oct_mag/vmises))
    print("sqrt(2)/3: %0.4e" % (2**0.5/3))
    print()
