#+Title: Homework 8
#+Subtitle: ME 597 - Failure mechanics
#+Author: Edgar Rios
#+Date: 16th of March, 2018

* Instructions                                    :noexport:
Name:

Homework #8 Assigned: 3/16/18

ME 597, Spring 2018 Due: 3/21/18, *by 6pm*

(Please staple papers. Engineering paper is not required, but work must
be *neat and legible* to receive credit)

*Reading*

lesson\_09, lesson\_10
* Fatigue and* *Weibull distributions
You have been asked to build an S-N curve for a new chromium alloy that your company has developed (/S_{ut}/=200 MPa). You conduct experiments with an R-R Moore machine to get cycles to failure at various levels of stress (8 tests per stress level - data below)

1. Using a Weibull distribution, determine the median cycles to
   failure (50 percentile) at each fatigue strength. Useless Matlab
   functions include ‘wblfit' and ‘wblcdf'.

   #+INCLUDE: ../code-blocks-FailMech.org::#hw08.p1 :only-contents t

2. Use the median cycles to failure to create an S-N plot for this
   material.

   See next

3. Using the S-N plot, estimate the fatigue strength at 100,000
   cycles.

   #+INCLUDE: ../code-blocks-FailMech.org::#hw08.p1.3 :only-contents t

   #+CAPTION: S-N curve with median cycles to failure for problem 1.1 and 1.2
   [[file:./s-n.curve.png]]

4. Using the S-N plot and the Palmgren-Miner rule, estimate the
   number of 120 MPa cycles the material can experience before
   failure if the material has already been subjected to 1500 cycles
   at 160 MPa and 200,000 cycles at 100 MPa.

   See attached

   #+INCLUDE: ../code-blocks-FailMech.org::#hw08.p1.4 :only-contents t

   #+CAPTION: S-N curve with marked values for problem 1.4 (Palmgren-Miner).
   [[file:./s-n.palmner_miner.curve.png]]

5. EXTRA CREDIT (10%): Create unreliability plots for each stress
   level by using the parameters you computed at each stress level
   (i.e. shape parameter, scale parameter). For this extra credit, do
   not use the ‘probplot' Matlab function.

   See figure [[fig:p1.5]].

   #+NAME: fig:p1.5
   #+CAPTION: Problem 1.5 (extra credit).
   [[file:./multi-weibull.png]]

* Damage Derivation.

1. Derive the one-dimensional constitutive equation for stress $\sigma$, strain $\varepsilon$, and damage /D/ below. Start with the basic definition of damage and use sketches of an intact and damaged material to visual the mathematics (see lesson).

   See attached

2. Write the above equation in three dimensions using index notation.  Do not assume that /*D*/ is isotropic).

   See attached

* 1D damage model.
You're asked to create a damage model for ductile copper (/E/=99 GPa). Based on experimental data, you create a graph of damage vs. strain (right).
#+ATTR_LATEX: :height 0.2\textheight
[[./media/image1.png]]

1. Create a stress-strain plot when a steady loading regime is applied to a strain value of 0.9. For the constitutive relation use the model derived in (2a). For the damage evolution model, use a linear function based on the damage vs. strain experimental data (solid black line). The damage initiation strain is 0.35.
   #+INCLUDE: ../code-blocks-FailMech.org::#hw08.p3.1 :only-contents t

   #+CAPTION: Slope damage (problem 3.1).
   [[file:./hw08.p3.1.damage1D.slope.png]]

2. Repeat part (a), but apply a loading regime that pulls the material in tension from 0 to 0.7 strain, unloads to zero strain, and then pulls in tension to 0.9 strain.
   #+INCLUDE: ../code-blocks-FailMech.org::#hw08.p3.2 :only-contents t

   #+CAPTION: Slope damage with two loading cycles (problem 3.2).
   [[file:./hw08.p3.2.damage1D.cycle.png]]

3. Repeat part (a), but instead of using a linear function for the damage evolution function, use a Weibull CDF that best represents the experimental data.
   #+INCLUDE: ../code-blocks-FailMech.org::#hw08.p3.3 :only-contents t

   #+CAPTION: Weibull damage (problem 3.3).
   [[file:hw08.p3.3.damage1D.weibull.png]]

* Configuration                                            :noexport:ARCHIVE:
** General options

- Export table of contents, export equations as pictures,
  export author
  #+OPTIONS: toc:nil author:t date:t tex:t LaTeX:t -:t e:t

- Do not evaluate R blocks, and export results only
  #+PROPERTY: header-args:R :session *R* :eval no-export :exports both

- Style, symbols, etc.
  # #+INCLUDE: "../Reports/headers.org"
  #+LATEX_HEADER: \input{/home/edgar/.emacs.d/plugins/symbols.tex}

- Add support for longtabu
  #+LATEX_HEADER: \usepackage{pdflscape,longtable,tabu,booktabs}
