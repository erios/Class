# LICENSE.
# Copyright (C) 2018  Edgar Rios
#
# This program is free software: you can redistribute
# it and/or modify it under the terms of the GNU
# General Public License as published by the Free
# Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will
# be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General
# Public License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
from scipy import odr
# Genetic algorithms
from deap import base
from deap import creator
from deap import tools
from deap import algorithms
import random
# Parallel computation (for GA)
import multiprocessing
# Numerical package
import numpy as np
# Plots
from matplotlib import pyplot as pl

def regr_odr(x, y, func, estim, full=0, extra=None):
    '''Calculates the coefficients that best fit a function for a given
    set of data using Orthogonal distance regression with scipy.odr
    x: array-like (see scipy.odr documentation)
    y: array-like (see scipy.odr documentation)
    func: a defined function that can be evaluated
    estim: estimated values for the coefficients

    Example:  Get the linear regression of a set of data
    >>> def linea(c, x): return c[0]*x + c[1]
    >>>
    >>> # m, b = regr_odr
    >>> regr_odr([1.1, 0.8, 1.4], [2.3, 1.2, 2.9], linea, [1, 1])
    array([ 2.90618533, -1.06347053])

    Requires:
    from scipy import odr
    '''
    #Total least squares or Orthogonal distance regression
    #Orthogonal distance regression (scipy.odr)
    #Cookbook/Least Squares Circle - - SciPy
    #http://www.scipy.org/Cookbook/Least_Squares_Circle
    #Python for the Advanced Physics Lab
    #http://www.physics.utoronto.ca/~phy326/python/odr_fit_to_data.py
    data = odr.Data(x=x, y=y)
    model = odr.Model(func, extra_args=extra)
    proc = odr.ODR(data, model, estim)
    out = proc.run()
    # Get all the statistical information from the regression:
    if not full==0:
        return out
    return out.beta

def format_plot_latex(family="sans", serif=[],
                      sans_serif=[], mono=[], labelsize=14, size=14,
                      legfontsize=12, ticksize=12, bold=False):
    """Sets a plot with LaTeX fonts"""
    import matplotlib as mpl
    # https://tex.stackexchange.com/a/391078
    # https://3diagramsperpage.wordpress.com/category/matplotlib/
    pgf_with_latex = {                      # setup matplotlib to use latex for output# {{{
        "pgf.texsystem": "xelatex",        # change this if using xetex or lautex
        "text.usetex": True,                # use LaTeX to write all text
        "font.family": family,
        "font.serif": serif,                   # blank entries should cause plots
        "font.sans-serif": sans_serif,              # to inherit fonts from the document
        "font.monospace": mono,
        # LaTeX default is 10pt font.
        "axes.labelsize": labelsize,
        "font.size": size,
        "legend.fontsize": legfontsize,              # Make the legend/label fonts
        "xtick.labelsize": ticksize,              # a little smaller
        "ytick.labelsize": ticksize,
        "text.latex.preamble": [
            r"\usepackage[utf8]{inputenc}",  # use utf8 fonts
            r"\usepackage[QX]{fontenc}",    # plots will be generated
            r"\usepackage{lmodern}",        # using this preamble
            r"\usepackage[detect-all]{siunitx}",
            r"\sisetup{per-mode=fraction, exponent-product = \cdot, list-units=single, list-final-separator = {\ \text{and}\ }, list-pair-separator = {\ \text{and}\ }, scientific-notation=engineering, zero-decimal-to-integer, exponent-to-prefix, round-mode=figures, round-precision=4}",
        ]
    }

    if bold is True:
        mpl.rcParams['text.latex.preamble'] = [r'\boldmath']

    # mpl.rcParams.update({
    #     'font.family':'sans-serif',
    #     'font.sans-serif':['Liberation Sans'],
    # })
    mpl.rcParams.update(pgf_with_latex)

def dam_CalvoPenaMartin09(Psi_k_dev, Xi_k_t, Xi_min_k,
                          Xi_max_k, beta_k):
    # Get new value for Xi_k_t (damage evolution) (eq. 9
    # CalvoPenaMartin09)
    Xi_k_t_new = np.sqrt(2 * Psi_k_dev)
    # Check if the new value is larger than the current maximum
    if Xi_k_t_new > Xi_k_t:
        # If the new value is larger, store it as equivalent
        # strain
        Xi_k_t = Xi_k_t_new

    # Damage criterion (useless for calculations)
    phi_k = Xi_k_t_new == Xi_k_t

    if Xi_k_t < Xi_min_k:
        g_k_dev = 1
    elif Xi_min_k <= Xi_k_t <= Xi_max_k:
        xi = (Xi_k_t - Xi_min_k) / (Xi_max_k - Xi_min_k)
        xi2 = xi * xi
        g_k_dev = 1 - xi2 * (1 - beta_k * (xi2 - 1))
    elif Xi_max_k < Xi_k_t:
        g_k_dev = 0

    return Xi_k_t, g_k_dev


def stress_CalvoPenaMartin09(k, lamda_arr, c2=0, theta=0,
                             full_out=False):
    c1, c3, c4 = k[:3]
    I_4_b_dev, I_4_ref_dev = k[3:5]
    beta_g, beta_f = k[5:7]
    Xi_min_g, Xi_max_g, Xi_min_f, Xi_max_f = k[7:]

    idmat = np.eye(3, 3)
    Psi_dev_g_tmp = -np.inf
    Psi_dev_f_tmp = -np.inf
    Xi_g_t = -np.inf
    Xi_f_t = -np.inf

    # Equivalent to chi when I_4_dev = I_4_ref_dev
    chi_ref = c4 * (I_4_ref_dev - I_4_b_dev)
    # For stress derivative continuity
    c5 = 2 * c3 * I_4_ref_dev ** 0.5 * (
        np.exp(chi_ref) * (c4 * I_4_ref_dev + 1) - 1)
    # For stress continuity
    c6 = c3 * I_4_ref_dev * (
        1 - np.exp(chi_ref) * (2 * c4 * I_4_ref_dev + 1))
    # For energy continuity
    c7 = c3 / c4 * (np.exp(chi_ref) - chi_ref - 1)\
         - (c6 * np.log(I_4_ref_dev)
            + 2 * c5 * I_4_ref_dev ** 0.5)

    a_0 = np.matrix([[np.cos(np.radians(theta))],
                    [np.sin(np.radians(theta))],
                    [0]])

    dat_ran = range(len(lamda_arr))
    # List of stress matrices
    tsigma = [np.matrix(np.zeros([3, 3])) for I in dat_ran]
    tsigma_D = [np.matrix(np.zeros([3, 3])) for I in dat_ran]
    # List of energy values
    Psi_g_mat = [float() for I in tsigma]
    Psi_f_mat = [float() for I in tsigma]
    # List of damage values
    dam_g_vals = [float() for I in tsigma]
    dam_f_vals = [float() for I in tsigma]

    for L in dat_ran:
        # The stress is calculated iteratively for
        # each of the stretch values below this line
        lamda = lamda_arr[L]

        dx_1 = lamda
        dx_2 = 1 / np.sqrt(dx_1)

        tF = np.matrix([[dx_1, 0, 0],
                       [0, dx_2, 0],
                       [0, 0, dx_2]])

        tC = np.dot(tF.T, tF)
        tB = np.dot(tF, tF.T)

        tC2 = np.dot(tC.T, tC)
        trC = np.trace(tC)
        tr2tC = trC * trC

        I_4 = float(np.dot(a_0.T, np.dot(tC, a_0)))
        J = np.linalg.det(tF)

        lamda_f = np.sqrt(I_4)

        J__13 = 1 / (J ** (1 / 3))
        lamda_f_dev = J__13 * lamda_f
        I_4_dev = lamda_f_dev * lamda_f_dev

        if I_4_dev <= I_4_b_dev:
            Psi_f_dev = 0
            Phi_f_dev = 0
        elif I_4_b_dev < I_4_dev <= I_4_ref_dev:
            chi = c4 * (I_4_dev - I_4_b_dev)
            Psi_f_dev = c3 / c4 * (np.exp(chi) - chi - 1)
            Phi_f_dev = 2 * c3 * I_4_dev * (np.exp(chi) - 1)
        elif I_4_ref_dev < I_4_dev:
            chi = c4 * (I_4_dev - I_4_b_dev)
            Psi_f_dev = c6 * np.log(I_4_dev) \
                        + 2 * np.sqrt(I_4_dev) * c5 + c7
            Phi_f_dev = 2 * (c6 + c5 * lamda_f_dev)

        J__23 = J__13 * J__13
        tB_dev = J__23 * tB
        I_1_dev = J__23 * trC

        Psi_g_dev = c1 * (I_1_dev - 3)

        # Fibre unitary vector
        a = np.dot(tF, a_0) / lamda_f

        dPsidI_1_dev = c1
        dPsidI_2_dev = c2

        # Matrix (ground substance)
        tvarsigma_g1_dev = (dPsidI_1_dev
                           + dPsidI_2_dev * I_1_dev) * tB_dev
        tvarsigma_g2_dev = - dPsidI_2_dev \
                          * np.dot(tB_dev, tB_dev)
        tvarsigma_g_dev = tvarsigma_g1_dev + tvarsigma_g2_dev
        # Fibre
        tvarsigma_f_dev = 0.5 * Phi_f_dev * np.outer(a, a)

        tvarsigma_dev = tvarsigma_g_dev \
                       + tvarsigma_f_dev

        tsigma_dev = 2/J * (tvarsigma_dev
                           - np.trace(tvarsigma_dev)
                           / 3 * idmat)

        p = - tsigma_dev[1, 1]

        tsigma[L] = tsigma_dev \
                   + np.matrix([[p, 0, 0],
                                [0, p, 0],
                                [0, 0, p]])

        Xi_g_t, dam_g = dam_CalvoPenaMartin09(Psi_g_dev, Xi_g_t,
                                              Xi_min_g, Xi_max_g,
                                              beta_g)
        Xi_f_t, dam_f = dam_CalvoPenaMartin09(Psi_f_dev, Xi_f_t,
                                              Xi_min_f, Xi_max_f,
                                              beta_f)

        dam_g_vals[L] = dam_g
        dam_f_vals[L] = dam_f

        tsigma_g_dev = 2/J * (tvarsigma_g_dev
                           - np.trace(tvarsigma_g_dev)
                           / 3 * idmat)
        tsigma_g_dev_D = dam_g * tsigma_g_dev
        tsigma_f_dev = 2/J * (tvarsigma_f_dev
                           - np.trace(tvarsigma_f_dev)
                           / 3 * idmat)
        tsigma_f_dev_D = dam_f * tsigma_f_dev

        tsigma_dev_D = tsigma_g_dev_D + tsigma_f_dev_D

        p = - tsigma_dev_D[1, 1]

        tsigma_D[L] = tsigma_dev_D \
                      + np.matrix([[p, 0, 0],
                                   [0, p, 0],
                                   [0, 0, p]])

    if full_out is True:
        return tsigma, tsigma_D, dam_g_vals, dam_f_vals
    return tsigma, tsigma_D


def stress11_CalvoPenaMartin09(k, lamda_arr, c2=0, theta=0,
                               use_dam=False):
    """use_dam: False/True for undamaged/damaged"""
    res = stress_CalvoPenaMartin09(k,
                                   lamda_arr,
                                   c2=c2,
                                   theta=theta
    )
    # sigma11_D = VAL[0, 0] for each tsigma, tsigma_D
    return [np.asarray([VAL[0, 0] for VAL in VALS])
           for VALS in res]


def obj_fun_und(k, xo, c2=0, theta=0):
    """
    k: c1, c3, c4,
       I_4_b_dev, I_4_ref_dev,
       beta_g, beta_f,
       Xi_min_g, Xi_max_g, Xi_min_f, Xi_max_f"""
    tsigma, tsigma_D = stress_CalvoPenaMartin09(k,
                                                xo,
                                                c2=c2,
                                                theta=theta
    )
    res = np.array([VAL[0, 0] for VAL in tsigma])
    return res


def obj_fun_dam(k, xo, und_par, c2=0, theta=0):
    """
    k: c1, c3, c4,
       I_4_b_dev, I_4_ref_dev,
       beta_g, beta_f,
       Xi_min_g, Xi_max_g, Xi_min_f, Xi_max_f"""
    k = np.hstack([und_par, k])
    tsigma, tsigma_D = stress_CalvoPenaMartin09(k,
                                                xo,
                                                c2=c2,
                                                theta=theta
    )
    res = np.array([VAL[0, 0] for VAL in tsigma_D])
    return res


def obj_fun(k, xo, yo, other):
    if other is True:
        # res = obj_fun_und(k, xo, yo)
        k = np.hstack([k, [0, 0, 0, 1, 0, 1]])
        myregr = regr_odr(xo, yo,      # Data to fit
                       obj_fun_und, # Objective function
                       k,           # Guess for parameters
                       full=True    # I want all results
                )
        res = myregr.sum_square
    else:
        # res = obj_fun_dam(k, xo, yo, und_par = other)
        # und_par = other
        # myregr = regr_odr(xo, yo, obj_fun_dam, k, full=True,
        #                extra = [other])
        y = obj_fun_dam(k, xo, und_par=other)
        res = np.sum((yo - y)**2)
    # For DEAP, every result must be an iterable (comma)
    return res,

def ga_min(num_par = 2):
    # number of parameters
    IND_SIZE = num_par
    creator.create("Minimizer", base.Fitness, weights=(-1.0,))
    creator.create("Individual", list, fitness=creator.Minimizer)

    toolbox = base.Toolbox()
    toolbox.register("attribute", random.random)
    toolbox.register("individual", tools.initRepeat, creator.Individual,
                     toolbox.attribute, n=IND_SIZE)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)

    toolbox.register("mate", tools.cxTwoPoint)
    toolbox.register("mutate", tools.mutGaussian, mu=0, sigma=1, indpb=0.1)
    toolbox.register("select", tools.selTournament, tournsize=3)
    return toolbox


import csv

pubLab="Danso14-Fig2b"
pubFname="Danso14-Fig2B.csv"
with open(pubFname) as fp:
    # Skip heading
    fp.readline()
    # Create CSV iterator
    read = csv.reader(fp)
    # Extract columns as text
    # column1: data[0]
    pub_data = list(zip(*read))
    # Convert to single precision number
    pub_stretch = [float(I) for I in pub_data[0]]
    pub_stress = [float(I) for I in pub_data[1]]
    del pub_data, read

# Indices where the undamaged portion happens
und_ind = [I for I, VAL in enumerate(pub_stretch) if VAL <= 1.1]

data_x = [pub_stretch[I] for I in und_ind]
data_y = [pub_stress[I] for I in und_ind]
undamaged = True
other = undamaged

# Genetic algorithm with 11 parameters
# c1, c3, c4,
# I_4_b_dev, I_4_ref_dev,
nparam = 5
# Create the main object to manipulate the GA
toolbox = ga_min(nparam)
# Number of individuals for a population (generation)
popsize = 2
pop = toolbox.population(n=popsize)
# Number of generations
NGEN = 1
# https://deap.readthedocs.io/en/master/api/base.html#deap.base.Toolbox.register
toolbox.register("evaluate", # Ask to define objective funct
                 obj_fun, # Name of defined object. funct.
                 # Data to which we are going to compare
                 xo=data_x, # Anything after the name of
                 yo=data_y, #   function is passed as
                             #   extra arguments (not
                             #   optimised parameters)
                 other=other
                 )
# Keep the best 5 out of all
best = tools.HallOfFame(5)
# Cross over probability, mutation probability
CXPB, MUTPB = 0.5, 0.2
# Register parallel computation
pool = multiprocessing.Pool()
toolbox.register("map", pool.map)

# Use predefined algorithm
res_GA = algorithms.eaSimple(pop, toolbox, CXPB, MUTPB, NGEN,
                              halloffame=best)
# Use the last one (best)
und_par = best[-1]

# Delete!!
und_par = np.array([2.46975723, 1.95005535, 2.78145096, 0.91984471, 1.56879454,
       0.        , 0.        , 0.        , 1.        , 0.        ,
       1.        ])

data_x = pub_stretch
data_y = pub_stress
other = und_par[:5]

#Forget previous fit function
toolbox.unregister("evaluate")

# Genetic algorithm with 6 parameters
# beta_g, beta_f,
# Xi_min_g, Xi_max_g, Xi_min_f, Xi_max_f
nparam = 6
# Create the main object to manipulate the GA
toolbox = ga_min(nparam)
# Number of individuals for a population (generation)
popsize = 2
pop = toolbox.population(n=popsize)
# Number of generations
NGEN = 1
# https://deap.readthedocs.io/en/master/api/base.html#deap.base.Toolbox.register
toolbox.register("evaluate", # Ask to define objective funct
                 obj_fun, # Name of defined object. funct.
                 # Data to which we are going to compare
                 xo=data_x, # Anything after the name of
                 yo=data_y, #   function is passed as
                             #   extra arguments (not
                             #   optimised parameters)
                 other=other
                 )
# Keep the best 5 out of all
best = tools.HallOfFame(5)
# Cross over probability, mutation probability
CXPB, MUTPB = 0.5, 0.2
# Register parallel computation
pool = multiprocessing.Pool()
toolbox.register("map", pool.map)

# Use predefined algorithm
res_GA = algorithms.eaSimple(pop, toolbox, CXPB, MUTPB, NGEN,
                              halloffame=best)

format_plot_latex()
# Save values of stress (to calculate descriptive statistics)
F = [np.zeros(len(data_x)) for I in best]
for I, ind in enumerate(best):
    pars = np.hstack([und_par[:5], ind])
    vals = stress_CalvoPenaMartin09(
        pars, data_x, full_out=True)
    tsigma, tsigma_D, dam_g_vals, dam_f_vals = vals
    tsigma11 = [np.asarray([VAL[0, 0] for VAL in VALS])
                for VALS in tsigma]
    tsigma11_D = [np.asarray([VAL[0, 0] for VAL in VALS])
                  for VALS in tsigma_D]
    F[I] = tsigma11_D

pubLab="DansoHonkanSaarak14-Fig2b"
pl.plot(data_x, tsigma11_D,
        label="damaged fit " + str(I))
pl.plot(data_x, tsigma11,
        label="undamaged fit " + str(I))
pl.plot(data_x, data_y, linestyle="", marker="o",
        fillstyle="none", label=pubLab)

lamda_arr = data_x
pl.subplot(211)
pl.plot(lamda_arr, [VAL[0, 0] for VAL in tsigma],
        label="My undamaged modelling")
pl.plot(lamda_arr, [VAL[0, 0] for VAL in tsigma_D],
        label="My damaged modelling")

import csv


outFig="CalvoPenaMartin09-Fig4a"
pubFname="CalvoPenaMartin09-Fig4-model.csv"
with open(pubFname) as fp:
    # Skip heading
    fp.readline()
    # Create CSV iterator
    read = csv.reader(fp)
    # Extract columns as text
    # column1: data[0]
    pub_data = list(zip(*read))
    # Convert to single precision number
    pub_stretch = [float(I) for I in pub_data[0]]
    pub_stress = [float(I) for I in pub_data[1]]
    del pub_data, read
pl.plot(pub_stretch, pub_stress, linestyle="",
       marker="o", fillstyle="none",
       label=pubLab)

pl.ylabel("Stress (MPa)")
pl.grid(True)
pl.legend()

pl.subplot(212)
pl.plot(lamda_arr, [1 - VAL for VAL in dam_g_vals],
        label="ground substance damage")
pl.plot(lamda_arr, [1 - VAL for VAL in dam_f_vals],
        label="fibre damage")
pl.xlabel("Stretch")
pl.ylabel("Damage")
pl.grid(True)
pl.legend()

pl.tight_layout()
pl.subplots_adjust(hspace=1e-5)

print("Stress")
print(tsigma11_D)
print("\nOriginal values")
print(data_y)
print("\nParameters")
print(pars)
