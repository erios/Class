# File to imitate Fig4B
# Danso, E., Honkanen, J., Saarakkala, S., & Korhonen, R., Comparison of nonlinear mechanical properties of bovine articular cartilage and meniscus, , 47(),  (2014).  http://dx.doi.org/10.1016/j.jbiomech.2013.09.015
# Based on
#Tensor Mechanics tutorial: the basics (with Gmsh)
#Step 3, part 1
#3D simulation of uniaxial tension with J2 plasticity

[GlobalParams]
  displacements = 'disp_x disp_y disp_z'
[]

[Mesh]
  file = tens_mech_tut.msh
  dim = 2
  uniform_refine = 0
[]

[MeshModifiers]
  [./extrude]
    type = MeshExtruder
    extrusion_vector = '0 0 0.5'
    num_layers = 2
    bottom_sideset = 'back'
    top_sideset = 'front'
  [../]
[]

[AuxKernels]
  [./vmStress] # Can have a different name
    type = RankTwoScalarAux
    # This can be (mostly) anything you want
    variable = vmStress
    # This should exist in your Kernel
    rank_two_tensor = stress
    # This should exist in the C source of the AuxKernel
    scalar_type = VonMisesStress
  [../]
[]

[AuxVariables]
  [./vmStress] # Same as in AuxKernel
    family = MONOMIAL
    order = CONSTANT
  [../]
[]


[Modules/TensorMechanics/Master]
  [./block1]
    strain = FINITE
    add_variables = true
    generate_output = 'stress_yy strain_yy stress_zz strain_zz stress_xx strain_xx'
  [../]
[]

[Materials]
  [./elasticity_tensor]
    type = ComputeIsotropicElasticityTensor
    youngs_modulus = 90
    poissons_ratio = 0.499
  [../]
  [./stress]
    type = ComputeMultiPlasticityStress
    ep_plastic_tolerance = 1e-9
    plastic_models = J2
  [../]
[]

[UserObjects]
  [./hardening]
    type = TensorMechanicsHardeningCubic
    value_0 = 7.5
    value_residual = 26
    internal_0 = 0
    internal_limit = 0.005
  [../]
  [./J2]
    type = TensorMechanicsPlasticJ2
    yield_strength = hardening
    yield_function_tolerance = 1E-3
    internal_constraint_tolerance = 1E-9
  [../]
[]

[BCs]
  [./left]
    type = PresetBC
    variable = disp_x
    boundary = left
    value = 0.0
  [../]
  [./back]
    type = PresetBC
    variable = disp_z
    boundary = back
    value = 0.0
  [../]
  [./bottom]
    type = PresetBC
    variable = disp_y
    boundary = bottom
    value = 0.0
  [../]
  [./top]
    type = FunctionPresetBC
    variable = disp_y
    boundary = top
    # Stretch proportionally to time and to a stretch of 1.4
    # (initial length: 4 mm, final time: 1 s, final length:
    # 5.6 mm, displacement: 1.6 mm)
    function = '1.6*t'
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Executioner]
  type = Transient
  # Up to 1 s in at least 10 steps
  num_steps = 20
  end_time = 1

  solve_type = 'PJFNK'

  petsc_options = '-snes_ksp_ew'
  petsc_options_iname = '-pc_type -sub_pc_type -pc_asm_overlap -ksp_gmres_restart'
  petsc_options_value = 'asm lu 1 101'
[]

[Postprocessors]
  [./ave_stress_yy]
    type = SideAverageValue
    variable = stress_yy
    boundary = bottom
  [../]
  [./ave_strain_yy]
    type = SideAverageValue
    variable = strain_yy
    boundary = bottom
  [../]
  [./ave_stress_zz]
    type = SideAverageValue
    variable = stress_zz
    boundary = bottom
  [../]
  [./ave_strain_zz]
    type = SideAverageValue
    variable = strain_zz
    boundary = bottom
  [../]
[]

[Outputs]
  exodus = true
  print_perf_log = false
  csv = true
  print_linear_residuals = false
[]
