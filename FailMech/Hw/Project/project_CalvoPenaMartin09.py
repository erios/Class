outFig = "py_CalvoPenaMartin09_pub.png"
# LICENSE.
# Copyright (C) 2018  Edgar Rios
#
# This program is free software: you can redistribute
# it and/or modify it under the terms of the GNU
# General Public License as published by the Free
# Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will
# be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General
# Public License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Numerical package
import numpy as np
# Plots
from matplotlib import pyplot as pl
from scipy import odr


def dam_CalvoPenaMartin09(Psi_k_dev, Xi_k_t, Xi_min_k,
                          Xi_max_k, beta_k):
    # Get new value for Xi_k_t (damage evolution) (eq. 9
    # CalvoPenaMartin09)
    Xi_k_t_new = np.sqrt(2 * Psi_k_dev)
    # Check if the new value is larger than the current maximum
    if Xi_k_t_new > Xi_k_t:
        # If the new value is larger, store it as equivalent
        # strain
        Xi_k_t = Xi_k_t_new

    # Damage criterion (useless for calculations)
    phi_k = Xi_k_t_new == Xi_k_t

    if Xi_k_t < Xi_min_k:
        g_k_dev = 1
    elif Xi_min_k <= Xi_k_t <= Xi_max_k:
        xi = (Xi_k_t - Xi_min_k) / (Xi_max_k - Xi_min_k)
        xi2 = xi * xi
        g_k_dev = 1 - xi2 * (1 - beta_k * (xi2 - 1))
    elif Xi_max_k < Xi_k_t:
        g_k_dev = 0

    return Xi_k_t, g_k_dev


# From the publication
c1 = 0.01
c2 = 0
c3 = 0.124472
c4 = 1.547994

I_4_b_dev = 1.1
I_4_ref_dev = 1.87

beta_g = -1
beta_f = 0.895651
Xi_min_g = 0.108012
Xi_max_g = 0.147898
Xi_min_f = 0.865269
Xi_max_f = 1.281208

idmat = np.eye(3, 3)
Psi_dev_g_tmp = -np.inf
Psi_dev_f_tmp = -np.inf
Xi_g_t = -np.inf
Xi_f_t = -np.inf

# Fibres aligned with pull: theta = 0;
# perpendicular: theta = 90
theta = 0

# Initial value of stretch
lamda_o = 1
# Final value of stretch
lamda_e = 1.716
# Number of steps
lamda_n = 50
# Values of stretch for the calculation
lamda_arr = np.linspace(lamda_o, lamda_e, lamda_n)

# Equivalent to chi when I_4_dev = I_4_ref_dev
chi_ref = c4 * (I_4_ref_dev - I_4_b_dev)
# For stress derivative continuity
c5 = 2 * c3 * I_4_ref_dev ** 0.5 * (
    np.exp(chi_ref) * (c4 * I_4_ref_dev + 1) - 1)
# For stress continuity
c6 = c3 * I_4_ref_dev * (
    1 - np.exp(chi_ref) * (2 * c4 * I_4_ref_dev + 1))
# For energy continuity
c7 = c3 / c4 * (np.exp(chi_ref) - chi_ref - 1)\
    - (c6 * np.log(I_4_ref_dev)
        + 2 * c5 * I_4_ref_dev ** 0.5)

a_0 = np.matrix([[np.cos(np.radians(theta))],
                 [np.sin(np.radians(theta))],
                 [0]])

dat_ran = range(len(lamda_arr))
# List of stress matrices
tsigma = [np.matrix(np.zeros([3, 3])) for I in dat_ran]
tsigma_D = [np.matrix(np.zeros([3, 3])) for I in dat_ran]
# List of energy values
Psi_g_mat = [float() for I in tsigma]
Psi_f_mat = [float() for I in tsigma]
# List of damage values
dam_g_vals = [float() for I in tsigma]
dam_f_vals = [float() for I in tsigma]

for L in dat_ran:
    # The stress is calculated iteratively for
    # each of the stretch values below this line
    lamda = lamda_arr[L]

    dx_1 = lamda
    dx_2 = 1 / np.sqrt(dx_1)

    tF = np.matrix([[dx_1, 0, 0],
                    [0, dx_2, 0],
                    [0, 0, dx_2]])

    tC = np.dot(tF.T, tF)
    tB = np.dot(tF, tF.T)

    tC2 = np.dot(tC.T, tC)
    trC = np.trace(tC)
    tr2tC = trC * trC

    I_4 = float(np.dot(a_0.T, np.dot(tC, a_0)))
    J = np.linalg.det(tF)

    lamda_f = np.sqrt(I_4)

    J__13 = 1 / (J ** (1 / 3))
    lamda_f_dev = J__13 * lamda_f
    I_4_dev = lamda_f_dev * lamda_f_dev

    if I_4_dev <= I_4_b_dev:
        Psi_f_dev = 0
        Phi_f_dev = 0
    elif I_4_b_dev < I_4_dev <= I_4_ref_dev:
        chi = c4 * (I_4_dev - I_4_b_dev)
        Psi_f_dev = c3 / c4 * (np.exp(chi) - chi - 1)
        Phi_f_dev = 2 * c3 * I_4_dev * (np.exp(chi) - 1)
    elif I_4_ref_dev < I_4_dev:
        chi = c4 * (I_4_dev - I_4_b_dev)
        Psi_f_dev = c6 * np.log(I_4_dev) \
            + 2 * np.sqrt(I_4_dev) * c5 + c7
        Phi_f_dev = 2 * (c6 + c5 * lamda_f_dev)

    J__23 = J__13 * J__13
    tB_dev = J__23 * tB
    I_1_dev = J__23 * trC

    Psi_g_dev = c1 * (I_1_dev - 3)

    # Fibre unitary vector
    a = np.dot(tF, a_0) / lamda_f

    dPsidI_1_dev = c1
    dPsidI_2_dev = c2

    # Matrix (ground substance)
    tvarsigma_g1_dev = (dPsidI_1_dev
                        + dPsidI_2_dev * I_1_dev) * tB_dev
    tvarsigma_g2_dev = - dPsidI_2_dev \
        * np.dot(tB_dev, tB_dev)
    tvarsigma_g_dev = tvarsigma_g1_dev + tvarsigma_g2_dev
    # Fibre
    tvarsigma_f_dev = 0.5 * Phi_f_dev * np.outer(a, a)

    tvarsigma_dev = tvarsigma_g_dev \
        + tvarsigma_f_dev

    tsigma_dev = 2/J * (tvarsigma_dev
                        - np.trace(tvarsigma_dev)
                        / 3 * idmat)

    p = - tsigma_dev[1, 1]

    tsigma[L] = tsigma_dev \
        + np.matrix([[p, 0, 0],
                     [0, p, 0],
                     [0, 0, p]])

    Xi_g_t, dam_g = dam_CalvoPenaMartin09(Psi_g_dev, Xi_g_t,
                                          Xi_min_g, Xi_max_g,
                                          beta_g)
    Xi_f_t, dam_f = dam_CalvoPenaMartin09(Psi_f_dev, Xi_f_t,
                                          Xi_min_f, Xi_max_f,
                                          beta_f)

    dam_g_vals[L] = dam_g
    dam_f_vals[L] = dam_f

    tsigma_g_dev = 2/J * (tvarsigma_g_dev
                          - np.trace(tvarsigma_g_dev)
                          / 3 * idmat)
    tsigma_g_dev_D = dam_g * tsigma_g_dev
    tsigma_f_dev = 2/J * (tvarsigma_f_dev
                          - np.trace(tvarsigma_f_dev)
                          / 3 * idmat)
    tsigma_f_dev_D = dam_f * tsigma_f_dev

    tsigma_dev_D = tsigma_g_dev_D + tsigma_f_dev_D

    p = - tsigma_dev_D[1, 1]

    tsigma_D[L] = tsigma_dev_D \
        + np.matrix([[p, 0, 0],
                     [0, p, 0],
                     [0, 0, p]])

pl.subplot(211)
pl.plot(lamda_arr, [VAL[0, 0] for VAL in tsigma],
        label="My undamaged modelling")
pl.plot(lamda_arr, [VAL[0, 0] for VAL in tsigma_D],
        label="My damaged modelling")

import csv


outFig = "CalvoPenaMartin09-Fig4a"
pubLab = outFig
pubFname = "CalvoPenaMartin09-Fig4-model.csv"
with open(pubFname) as fp:
    # Skip heading
    fp.readline()
    # Create CSV iterator
    read = csv.reader(fp)
    # Extract columns as text
    # column1: data[0]
    pub_data = list(zip(*read))
    # Convert to single precision number
    pub_stretch = [float(I) for I in pub_data[0]]
    pub_stress = [float(I) for I in pub_data[1]]
    del pub_data, read
pl.plot(pub_stretch, pub_stress, linestyle="",
        marker="o", fillstyle="none",
        label=pubLab)

pl.ylabel("Stress (MPa)")
pl.grid(True)
pl.legend()

pl.subplot(212)
pl.plot(lamda_arr, [1 - VAL for VAL in dam_g_vals],
        label="ground substance damage")
pl.plot(lamda_arr, [1 - VAL for VAL in dam_f_vals],
        label="fibre damage")
pl.xlabel("Stretch")
pl.ylabel("Damage")
pl.grid(True)
pl.legend()

pl.tight_layout()
pl.subplots_adjust(hspace=1e-5)

# pl.savefig(outFig)
pl.show()
