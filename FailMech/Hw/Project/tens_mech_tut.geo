/************************************************************************/
/* File to create the geometry on MOOSE tutorial for tensor mechanics   */
/*                                                                      */
/* To generate the mesh do:                                             */
/*                                                                      */
/* gmsh -optimize_ho -format msh -"$dims"\                              */
/*      -o "$outFname" "$inFname" 1>/dev/null                           */
/*                                                                      */
/* - replace $dims with 2                                               */
/* - replace $outFname with the desired output name (tens_mech_tut.msh) */
/* - replace inFname with the name of this file (the one that you       */
/*   are reading)                                                       */
/************************************************************************/
/* LICENSE                                                   */
/* Copyright (C) 2017  broncodev                             */
/*                                                           */
/* This program is free software: you can redistribute it    */
/* and/or modify it under the terms of the GNU General       */
/* Public License as published by the Free Software          */
/* Foundation, either version 3 of the License, or (at your  */
/* option) any later version.                                */
/*                                                           */
/* This program is distributed in the hope that it will be   */
/* useful, but WITHOUT ANY WARRANTY; without even the        */
/* implied warranty of MERCHANTABILITY or FITNESS FOR A      */
/* PARTICULAR PURPOSE.  See the GNU General Public License   */
/* for more details.                                         */
/*                                                           */
/* You should have received a copy of the GNU General Public */
/* License along with this program.  If not, see             */
/* <http://www.gnu.org/licenses/>.                           */
/*************************************************************/

// Point tolerance //////////////////////////////
lc = 1e-3;

// Every line preceded by //+ was created with the GUI

// Define parameters (lengths; optional)
//+
len1 = DefineNumber[ 1, Name "Parameters/bottomLength" ];
//+
height = DefineNumber[ 4, Name "Parameters/height" ];
//+
len2 = DefineNumber[ 1.5, Name "Parameters/topLength" ];

// Define the geometry (points and lines)
// A point is defined as {x, y, z, tolerance}
//+
Point(1) = {0, 0, 0, lc};
//+
Point(2) = {len1, 0, 0, lc};
//+
Point(3) = {len2, height, 0, lc};
//+
Point(4) = {0, height, 0, lc};
// A line is defined as {point0, point1}
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 1};

// Set the number of nodes per line (optional; useful for a
// nice mesh)
// Lines 1 and 3 have 6 nodes
// +
Transfinite Line {1, 3} = 6 Using Progression 1;
// Lines 4 and 2 have 20 nodes
//+
Transfinite Line {4, 2} = 20 Using Progression 1;

// Create a closed polygon with the lines
//+
Line Loop(5) = {2, 3, 4, 1};
// Define a surface from the polygon
//+
Plane Surface(6) = {5};
//+
Transfinite Surface {6};
// Ask for quadrilaterals instead of triangles
//+
Recombine Surface {6};

// Create physical groups on the edges (names are arbitrary)
// that can be used to set boundaries.
//+
Physical Line("left") = {4};
//+
Physical Line("bottom") = {1};
//+
Physical Line("right") = {2};
//+
Physical Line("top") = {3};

// Add a physical entity up to the dimension that you
//require (Alex Lindsay)--name is arbitrary
Physical Surface("surface") = {6};
