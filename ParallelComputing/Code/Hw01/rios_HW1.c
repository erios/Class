/**
 *   \file rios_HW1.c
 *   \brief Calculates
 *   bi-dimensional Couette's flow (Homework 1)
 *   \author Edgar Rios \date 2017-01-15
 *   \lastmodified 2017-02-05
 *
 *    to compile:
 *    gcc -O3 -Wall -std=gnu99 -lm
 *      -o rios_HW1.run rios_HW1.c
 *
 *  This is homework 1 for the Parallel scientific
 *  computing course in BSU. See HW1.pdf and
 *  HW1_part2.pdf
 *
 *
 */
/* Interaction with the user */
#include <stdio.h>
/* Strings, memory allocation */
#include <stdlib.h>
/* pow, ceilf */
#include <math.h>

#define NY 21 /* Points in the y direction */

void uy_j(int, float, float, float, float, float,
          float *, float *);

int main(int argc, char *argv[])
{
    /* Check command line arguments */
    if (argc != 3) {
        fprintf(stderr,
                "This program requires two"
                " arguments:\n %s"
                " <Reynolds Number (float)>"
                " <Pressure gradient (float)>\n",
                argv[0]);
        /* perror(); */
    }

    /**********************************************/
    /* Initialise variables */
    /**********************************************/
    /* The notation initialises only the specified values and
       the rest are zero */
    /* float uy_n[21] = {[0] =0}; */
    /* Reynolds number */
    const float Re = atof(argv[1]);
    /* Pressure differential */
    const float dPdx = atof(argv[2]);
    /* Spacing between plates (arbitrary; unit
    value) */
    const float h = 1.0f;
    /* Plate velocity */
    const float uPlate = 1.0f;
    /* Density */
    const float rho = 1.0f;
    /* Inverse of density */
    const float irho = 1 / rho;
    /* Dynamic viscosity */
    const float nu = uPlate * h / Re;
    /* Static viscosity */
    const float mu = nu * rho;
    /* Distance between calculation points */
    const float dy = h / (NY - 1);
    /* Square of the distance between the points
     */
    const float dy2 = dy * dy;
    /* Inverse of dy2 */
    const float idy2 = 1 / dy2;
    /* Time steps */
    const float dt = 0.5f * pow(dy, 2.0) / nu;
    /* Ending time (square of distance over nu) */
    const float timeEnd = h * h / nu;
    /* Number of time steps */
    const int nTimeSteps
    = (int) ceil(timeEnd / dt);

    /* loop variables */
    int n;
    int j;

    /* Dynamic allocation of variables */
    /* Current speed along the y-axis, in the
       x-direction */
    float *u = calloc(NY, sizeof *u);
    /* New speed */
    float *unew = calloc(NY, sizeof *unew);
    /* Ordinate to calculate the speed */
    float *y = calloc(NY, sizeof *y);
    /* Dummy variable to swap speeds */
    float *tmp;

    /* Boundary conditions */
    u[0]         = 0.f;
    unew[0]      = 0.f;
    u[NY - 1]    = uPlate / uPlate;
    unew[NY - 1] = uPlate / uPlate;

    /* Calculate the values for each time step */
    for (n = 1; n <= nTimeSteps; n++) {
        /* Calculate next values for current time
           along the y-axis */
        for (j = 1; j < NY - 1; j++) {
            uy_j(j, dt, idy2, dPdx, mu, irho, u,
                 unew);
        }
        /* swap pointers for each time step */
        tmp  = u;
        u    = unew;
        unew = tmp;
    }

    /* Calculate exact solution and print results
     */
    /* Print header */
    printf(
    "Distance between the plates (h): %.5f\n", h);
    printf("Plate velocity (uPlate): %.5f\n",
           uPlate);
    printf("Density (rho): %.5f\n", rho);
    printf("Dynamic viscosity (nu): %.5f\n", nu);
    printf("Static viscosity (mu): %.5f\n", mu);
    printf(
    "Number of calculation points (NY): %i\n",
    NY);
    printf("Time step (dt): %.5f\n", dt);
    printf(
    "Number of time steps (nTimeSteps): %i\n",
    nTimeSteps);
    printf("Re=%.0f, $\\frac{dP}{dx}=%.0f$\n",
           Re, dPdx);
    printf("Results:\n");
    printf("\"y\"\t\"Exact\"\t\"Numer\" \n");
    for (j = 0; j < NY; j++) {
        y[j] = j * dy;
        /* Exact solution (reuse unew) */
        unew[j] = uPlate * y[j] / h
                  + dPdx / (2.f * mu)
                    * (y[j] * y[j] - h * y[j]);
        /* Print current value */
        printf("%.5f\t%.5f\t%.5f\n", y[j] / h,
               unew[j] / uPlate, u[j] / uPlate);
    }

    /* free allocated memory */
    free(unew);
    free(u);
    free(y);

    /* call gnuplot */
    /* system("gnuplot -persist <gnuplot file>")
     */
    return 0;
}

/* Use array pointer to modify values of the
   array */
void uy_j(int j, float dt, float idy2, float dPdx,
          float mu, float irho, float *u,
          float *unew)
{
    unew[j] = dt * (-irho * dPdx
                    + mu * (u[j + 1] - 2.0f * u[j]
                            + u[j - 1])
                      * idy2)
              + u[j];
}

/* Local Variables:  */
/* flycheck-clang-language-standard: c99 */
/* fill-column: 50 */
/* comment-column: 50 */
/* End:              */
