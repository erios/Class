#!/bin/bash

function plot(){
 # https://stackoverflow.com/questions/18576998/gnuplot-autotitle-columnheader-with-whitespaces
 # set key autotitle columnheader;\
    gnuplot -e ";\
     reset;\
     set terminal epslatex standalone color colortext;\
     set output '$nom_arch.tex';\
     set xlabel '$\frac{u}{U_{p}}$';\
     set ylabel '$\frac{y}{H}$';\
     set key autotitle columnheader;\
     plot '$nom_arch.csv' u 1:2 w lines title 'Exact', '' using 1:3 w points title 'Numer';\
     set output;
"
    # echo "$nom_arch"
    while [[ ! -f "$nom_arch-inc.eps" ]]; do sleep 0.1; done;
    [[ ! -f "$nom_arch-inc" ]] && ln -s "$nom_arch-inc.eps" "$nom_arch-inc";
    latex -interaction nonstopmode "$nom_arch.tex" 2>&1 > /dev/null && \
    dvips -q "$nom_arch.dvi" && \
    ps2eps -f -q "$nom_arch.ps";
# SVG
#     gnuplot -e ";\
#  reset;\
#  set terminal svg;\
#  set output '$nom_arch.svg';\
#  set xlabel 'u/Up';\
#  set ylabel 'y/H';\
#  set key autotitle columnheader;\
#  plot '$nom_arch.csv' u 2:1 w lines title 'Exact', '' using 3:1 w points title 'Numer';
# "
}

function export_org(){
    printf "%s\n" "** Values of velocity when Re=$Re, dP=$dP"
    printf "#+ATTR_LATEX: :width 0.7\columnwidth"
    printf "\n%s\n\n" "[[./$nom_arch.eps]]"
    # printf "%s\n" "#+TBLNAME: tab-Re${Re}dP${dP}";
    printf "%s\n" "#+LATEX: \resizebox{0.5\columnwidth}{!}{"
    awk 'NR==3{print "|-"} NR>1{gsub("\t", "|"); print "|" $0 "|"}' \
        "$nom_arch.csv" | sed 's- ([^"]*)"-"-g';
    printf "\n%s\n" "#+LATEX: }";
    # printf "\n%s\n" "#+RESULTS:";
    # printf "\n%s\n\n" "#+LATEX: \includegraphics{$nom_arch.eps}\caption{Prueba}";
}


# $Re: Reynolds number (Re)
# $dP: Pressure gradient (dPdx)
for Re in 10; do
    for dP in -2.0 0.0 2.0; do
        nom_arch="rios_HW1-Re_$Re,dP_$dP"
        ./rios_HW1.run "$Re" "$dP" > "$nom_arch".csv;
        plot;
        export_org;
    done
    printf "\n";
done

dP=2.0
for Re in 1 5; do
    nom_arch="rios_HW1-Re_$Re,dP_$dP";
    ./rios_HW1.run "$Re" "$dP" > "$nom_arch".csv;
    plot;
    export_org;
done
printf "\n";
# gnuplot -e "set terminal png size 300, 300 enhanced;\
#  set output '$nom_arch.png';\
#  plot '$nom_arch.csv' using 1:2 w l, '' using 1:3 w l";

# paste "rios_HW1-Re="{1,5,10}",dP=2.0".csv | tail -n +4;
# paste "rios_HW1-Re=10,dP="*.csv | tail -n +4;
exit
