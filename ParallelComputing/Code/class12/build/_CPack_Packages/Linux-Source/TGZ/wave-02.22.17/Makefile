SHELL = /bin/bash

PRG = wave
EXEC = ../bin/$(PRG).run

CC=gcc $(LDFLAGS)
DEBUG = -g
CFLAGS = -Wall -O2 -std=gnu99 $(DEBUG)
LDFLAGS = -I$(DIR_LIB) -I$(DIR_DEPS)
LIBS = -lm
# Replace the name of the files <file.c> for ../src/<file.c>
DIR_LIB = ../src
OLD_SRC = $(strip createMesh.c initialize.c main.c io.c solveWave.c	\
          validate.c)
SOURCES = $(patsubst %.c, $(DIR_LIB)/%.c, $(OLD_SRC))
# Replace the name of the files <file.h> for ../include/<file.c>
DIR_DEPS = ../include
OLD_DEPS = $(strip parameter.h initialize.h createMesh.h io.h \
           solveWave.h validate.h)
DEPS = $(patsubst %.h, $(DIR_DEPS)/%.h, $(OLD_DEPS))
# Replace the name of the files <file.o> for ../obj/<file.c>
OBJECTS = $(patsubst $(DIR_LIB)/%.c, ../obj/%.o, $(SOURCES))

# The following ../obj/ has pedagogical purposes
../obj/%.o: $(DIR_LIB)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(EXEC): $(OBJECTS)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

.PHONY: clean
clean:
	rm -f ../*/*.o ../*/*.tar.gz $(EXEC) core

.PHONY: tar
tar:
	cd ../ && tar czf ./bkp/$(PRG)_$(shell date "+%d-%b-%g").tar.gz \
     $(patsubst ../%, ./%, $(DIR_LIB) $(DIR_DEPS))

.PHONY: clean-obj
clean-obj:
	rm -fr ./obj/*.o

.PHONY: test
test: $(EXEC)
	# Test for file permissions
	[ -x $(EXEC) ] && ./$(EXEC) 5
