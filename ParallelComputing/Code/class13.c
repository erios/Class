#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
  /* int *p; */
  /* int i=5; */
  /* p = &i; */
  /* printf("ptr: %i\n*ptr: %d", p, *p); */
  int a[6] = {0};
  int *p;
  int sum;
  int N;

  N = 0;
  sum = 0;

  p = &a[0];
  while (p < &a[N]) {
    sum += *p++;
    N += 1;
  }
  printf("%i", p);
  return 0;
}
