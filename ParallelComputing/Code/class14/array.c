#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
  // Initialise array with zero values (give one
  // element a value)
  int a[6] = {[0] = 0};

  int *p;
  p = a;

  printf("Before %d\n", p);
  *(p++) = 4;

  printf("%i\n", a[4]);
  printf("After %p\n", p);
  printf("After %i\n", *p);

  free(p);
  return 0;
}
