#include <stdio.h>

int main (void)
{
   int i = 5;
   printf("value of i = %d\n",i);

   int j = ++i;
   printf("value of j = %d\n",j);
   printf("value of i after j = ++i :%d\n",i);

   int k = i++;
   printf("value of k = %d\n",k);
   printf("value of i after k = i++ :%d\n",i);

   return (0);
}
