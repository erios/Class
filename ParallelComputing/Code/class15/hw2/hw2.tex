% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{color}
\usepackage{listingsutf8}
\usepackage[style=verbose,backend=bibtex]{biblatex}
\addbibresource{biblio.bib}
\input{preamble.tex}
\usepackage{multicol}
\usepackage{longtable}
\usepackage{pdflscape}
\author{Edgar Rios.}
\date{\today}
\title{Single and double precision calculations.\\\medskip
\large Assignment \#2. Parallel Scientific Computing.}
\hypersetup{
 pdfauthor={Edgar Rios.},
 pdftitle={Single and double precision calculations.},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 24.4.1 (Org mode 9.0.5)},
 pdflang={English}}
\begin{document}

\maketitle

\section{Problem 1: Euler's divergent series}
\label{sec:orgbc1ac06}
\subsection{Maximum computing limits}
\label{sec:org00b8251}
Table \ref{tab:limits} contains the limits for the numerical computations on
the current system, which is presented below:
\begin{multicols}{2}
\begin{itemize}
\item Architecture:          x86\_64
\item CPU op-mode(s):        32-bit, 64-bit
\item Byte Order:            Little Endian
\item CPU(s):                1
\item On-line CPU(s) list:   0
\item Thread(s) per core:    1
\item Core(s) per socket:    1
\item Socket(s):             1
\item NUMA node(s):          1
\item Vendor ID:             GenuineIntel
\item CPU family:            6
\item Model:                 22
\item Stepping:              1
\item CPU MHz:               1729.086
\item BogoMIPS:              3458.17
\item L1d cache:             32K
\item L1i cache:             32K
\item L2 cache:              1024K
\item NUMA node0 CPU(s):     0
\end{itemize}
\end{multicols}

\begin{landscape}
\begin{longtable}{ll}
Symbol & Description (Value)\\
\hline
\endfirsthead
\multicolumn{2}{l}{Continued from previous page} \\
\hline

Symbol & Description (Value) \\

\hline
\endhead
\hline\multicolumn{2}{r}{Continued on next page} \\
\endfoot
\endlastfoot
\hline
SPINT & single precision integer\\
DPINT & double precision integer\\
SPFL & single precision floating point number\\
DPFL & double precision floating point number\\
LLONG\_MAX & largest positive (signed) DPINT\\
 & (9223372036854775807)\\
INT\_MAX & largest positive (signed) SPINT\\
 & (2147483647)\\
ULLONG\_MAX & largest unsigned DPINT\\
 & (18446744073709551615)\\
LLONG\_MIN & most negative (signed) DPINT\\
 & (-9223372036854775808)\\
LONG\_MIN & most negative (signed) SPINT\\
 & (-2147483648)\\
FLT\_EPSILON & difference between 1 and the\\
 & smallest SP floating point that\\
 & is greater than 1\\
 & (1.19209289550781250e-07)\\
FLT\_EPSILON\(^{\text{-1}}\) & Inverse of FLT\_EPSILON\\
 & (8388608)\\
LDBL\_EPSILON & Similar to FLT\_EPSILON, but for\\
 & long double types\\
 & (1.084202172485504434007452800869941711425781250e-19)\\
FLT\_EPSILON\(^{\text{-1}}\) & Similar to FLT\_EPSILON\(^{\text{-1}}\), but for\\
 & long double types\\
 & (9.223372036854775808e+18)\\
FLT\_MAX & largest signed SPFL\\
 & (3.402823466385288598117042e+38)\\
LDBL\_MAX & largest signed DPFL\\
 & (1.1897314953572317650212638530309702051690633222946242004403237339e+4932)\\
\caption{\label{tab:limits}
Symbols description}
\\
\end{longtable}
\end{landscape}

\subsection{Computing benchmark}
\label{sec:org8f79e9d}
\subsubsection{Harmonic numbers}
\label{sec:orgd9cab39}
A harmonic number \(H_{n}\) is defined by the divergent series:
\[H_{n} = \sum_{k=1}^{\n}{\frac{1}{k}}\]

The code was prepared to output a range of values for the harmonic
numbers, based on a maximum given by the user. The ranges were
discretised logarithmically.

\subsubsection{Results}
\label{sec:orgb025c94}
The following tables present data that can be used to predict the
amount of time which would take to finish the calculations with
the available precision. The first column presents a range for \(k\)
and how much it takes to compute each of the ranges.

\begin{table}[htbp]
\centering
\begin{tabular}{rrrr}
n (min) & n (max) & time  (seconds) & accumulated time (s)\\
\hline
1\,(+01) & 1\,(+02) & 3.3333\,(-08) & 3.3333\,(-8)\\
1\,(+02) & 1\,(+03) & 2.1111\,(-08) & 8.7777\,(-8)\\
1\,(+03) & 1\,(+04) & 2.0444\,(-08) & 1.95998\,(-7)\\
1\,(+04) & 1\,(+05) & 2.0956\,(-08) & 4.12952\,(-7)\\
1\,(+05) & 1\,(+06) & 2.0807\,(-08) & 8.46711\,(-7)\\
\end{tabular}
\caption{\label{projectionFlt}
The harmonic series with single precision}

\end{table}

\begin{center}
\includegraphics[width=.9\linewidth]{projectionFlt.png}
\end{center}

\begin{table}[htbp]
\centering
\begin{tabular}{rrrr}
n (min) & n (max) & time (seconds) & accumulated time (s)\\
\hline
1\,(+01) & 1\,(+02) & 2.2222\,(-08) & 2.2222\,(-8)\\
1\,(+02) & 1\,(+03) & 2.1111\,(-08) & 6.5555\,(-8)\\
1\,(+03) & 1\,(+04) & 2.1444\,(-08) & 1.52554\,(-7)\\
1\,(+04) & 1\,(+05) & 2.1611\,(-08) & 3.26719\,(-7)\\
1\,(+05) & 1\,(+06) & 2.1746\,(-08) & 6.75184\,(-7)\\
1\,(+06) & 1\,(+07) & 2.1643\,(-08) & 1.372011\,(-6)\\
1\,(+07) & 1\,(+08) & 2.1590\,(-08) & 2.765612\,(-6)\\
\end{tabular}
\caption{\label{projectionDbl}
The harmonic series with double precision}

\end{table}

\begin{center}
\includegraphics[width=.9\linewidth]{projectionDbl.png}
\end{center}

As it can be seen from the figures, the accumulated time behaves
logarithmically, with the double precision growing at a faster
rate in at least one order of (logarithmic) magnitude. In other
words, it could take forever to calculate.

The single precision results for the series gave the following results:
\begin{itemize}
\item Time: 5.420200e-02
\item Sum: 1.540368e+01
\item Harmonic number: 2 097 153
\end{itemize}

As it has been portrayed, the double precision case would be
unfeasible for my humble computer. Nonetheless, we could at least
calculate to the given limit of \(n=500e6\).
\begin{itemize}
\item Time: 1.079630e+01
\item Sum: 2.060733e+01
\item Harmonic number: 500 000 000
\end{itemize}
\end{document}
