/**
 *   \file benchMark.c
 *   \brief Benchmarking of an infinite series
 *   with single and double precision numbers.
 *   \lastmodified 2018-03-01
 *
 *  This is homework 2 for the Parallel scientific
 *  computing course in BSU. See Homework-2.pdf
 *
 *  \compile
 *  [ ! -d ../build ] && mkdir ../build &&\
 *    cd ../build && cmake .. && make
 *  \run bin/benchMark
 *
 */
/* All headers and general libraries */
#include "headers.h"
/* Check input arguments from StdIn */
#include "checkStdin.h"
/* Print number limits */
#include "printNumLim.h"
/* Calculate the harmonic series values */
#include "harmonicSeries.h"


int main(int argc, char *argv[])
{
    /* Check that the StdIn arguments are fine */
    checkStdin(&argc, argv);
    /* Print limits */
    printNumLim();
    /* Get the harmonic series */
    harmonicSeries(atof(argv[1]),
                   atof(argv[2]),
                   atof(argv[3]));

    return 0;
}
