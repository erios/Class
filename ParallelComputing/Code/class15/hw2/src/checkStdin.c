#include "checkStdin.h"

int checkStdin(int *argc, char *argv[]){
  /* Check command line arguments */
  if (*argc != 4) {
    fprintf(stderr,
            "This program requires one"
            " argument:\n %s"
            " <Number (int) of loops for the"
            " single precision harmonic series"
            "> "
            " <Number (int) of loops for the"
            " double precision harmonic series"
            "> "
            " <Breaking limit (long double) at"
            " which the process stops>",
            argv[0]);
    /* perror(); */
  }

  return 0;
}
