#include <stdint.h>
uint8_t twos_complement(uint8_t val)
{
  return -(unsigned int)val;
}

int main(int argc, char *argv[])
{
  int a = 0b101;
  print("%d", a);

  return 0;
}
