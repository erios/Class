#include "funcs.h"
#include "timer.h"

void solveWave(REAL *RESTRICT unew, const REAL *RESTRICT u, const REAL *RESTRICT uold)
{
  INT i, j;

  for (j = 1; j < NY - 1; j++) {
    for (i = 1; i < NX - 1; i++) {
      unew[IC]
        = 2.0f * u[IC] - uold[IC] + FACTOR * (u[IP1] + u[IM1] + u[JP1] + u[JM1] - 4.0f * u[IC]);
    }
  }
}

void initWave(REAL *RESTRICT u, REAL *RESTRICT uold, REAL *RESTRICT x, REAL *RESTRICT y)
{
    INT i, j;
    for (j = 1; j < NY - 1; j++) {
        for (i = 1; i < NX - 1; i++) {
            u[IC] = 0.1f * (4.0f * x[IC] - x[IC] * x[IC]) * (2.0f * y[IC] - y[IC] * y[IC]);
            // u[IC] =  x[IC] * ( PI-x[IC] ) * y[IC] * ( PI-y[IC] );
            // u[ij] = fabs( cos( x[ij] - 3.0*y[ij] ) );
        }
    }
    for (j = 1; j < NY - 1; j++) {
        for (i = 1; i < NX - 1; i++) {
            uold[IC] = u[IC] + 0.5f * FACTOR * (u[IP1] + u[IM1] + u[JP1] + u[JM1] - 4.0f * u[IC]);
        }
    }
}

void analyticalSoln(INT nTimeSteps, REAL *RESTRICT uAnalytical, REAL *RESTRICT XX,
                    REAL *RESTRICT YY)
{
    INT i, j, m, n, limit;
    limit = 51;

    REAL x, y, t, factor, sum;

    t = (REAL) dt * nTimeSteps;
    printf("ntimesteps=  %d\n", nTimeSteps);

    for (j = JSTART; j < JEND; j++) {
        for (i = ISTART; i < IEND; i++) {
            x   = XX[IC];
            y   = YY[IC];
            sum = 0.0;
            for (m = 1; m < limit; m = m + 2) {
                for (n = 1; n < limit; n = n + 2) {
                    factor = (1.0f / (REAL)(m * m * m * n * n * n));
                    sum    = sum
                          + factor
                            * cos(t * 0.25f * sqrt(cSqrd) * PI * sqrt((REAL) m * m + 4 * n * n))
                            * sin((REAL) m * PI * x * 0.25f) * sin((REAL) n * PI * y * 0.50f);
                }
            }
            uAnalytical[IC] = sum * 0.426050f;
        }
    }
}

void meshGrid(REAL *RESTRICT x, REAL *RESTRICT y)
{
    INT  i, j;
    REAL a;

    for (j = 0; j < NY; j++) {
        a = dx * ((REAL) j);
        for (i = 0; i < NX; i++) {
            x[IC] = dx * ((REAL) i);
            y[IC] = a;
        }
    }
}

void writeOutput(REAL *RESTRICT phi)
{
    INT   i, j;
    FILE *output;
    output = fopen("wave.dat", "w");

    for (j = 0; j < NY; j++) {
        for (i = 0; i < NX; i++) {
            fprintf(output, "%f\n", phi[IC]);
        }
    }
    fclose(output);
}

void print2Display(REAL *phi)
{
    INT i, j;

    for (j = JSTART; j < JEND; j++) {
        for (i = ISTART; i < IEND; i++) {
            printf("%12.8f", phi[IC]);
        }
        printf("\n\n");
    }
}
