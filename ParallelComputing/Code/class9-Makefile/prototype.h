#include "funcs.h"

void solveWave(REAL *RESTRICT unew, const REAL *RESTRICT u,
               const REAL *RESTRICT uold);

void initWave(REAL *RESTRICT u, REAL *RESTRICT uold, REAL *RESTRICT x,
              REAL *RESTRICT y);

void analyticalSoln(INT nTimeSteps, REAL *RESTRICT uAnalytical,
                    REAL *RESTRICT XX, REAL *RESTRICT YY);

void meshGrid(REAL *RESTRICT x, REAL *RESTRICT y);

void writeOutput(REAL *RESTRICT phi);

void print2Display(REAL *phi);

void StartTimer();

double GetTimer();
