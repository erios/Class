/*
 * Numerical and analytical solution of the 2D wave equation
 * C library function difftime only measures time in increments of whole seconds
 * i.e. 5 seconds and not 5.21 seconds.
 *
 * precise time measurements are enabled with the GET_TIME macros that are defined
 * in timer.h
 *
 * Author: Inanc Senocak
 * Date: 06/20/2015
 *
 * to compile:
 * gcc -O3 -std=c99 -lm -DRESTRICT=restrict waveCPU.c
 * to execute: ./a.out <number of time steps>
 *
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>
#include "timer.h"

#define NX 512 /* includes boundary points on both end */
#define NY 256 /* includes boundary points on both end */
#define LX 4.0f /* length of the domain in x-direction  */
#define LY 2.0f /* length of the domain in x-direction  */
#define dx (REAL)(LX / ((REAL)(NX)))
#define cSqrd 5.0f
#define dt (REAL)(0.1f * dx / sqrt(cSqrd))
#define FACTOR (cSqrd * (dt * dt) / (dx * dx))

#define ISTART 65     /* mesh zone to print values to the screen */
#define IEND ISTART + 5   /* mesh zone to print values to the screen */
#define JSTART 115     /* mesh zone to print values to the screen */
#define JEND JSTART + 5  /* mesh zone to print values to the screen */

#define IC (i + j * NX)        /* (i,j)   */
#define IM1 (i + j * NX - 1)   /* (i-1,j) */
#define IP1 (i + j * NX + 1)   /* (i+1,j) */
#define JM1 (i + (j - 1) * NX) /* (i,j-1) */
#define JP1 (i + (j + 1) * NX) /* (i,j+1) */

#ifndef RESTRICT
#define RESTRICT
#endif

#ifndef SINGLE
typedef double REAL;
typedef int    INT;
#define PI 3.14159265358979323846
#else
typedef float REAL;
typedef int   INT;
#define PI 3.1415927f
#endif

void solveWave(REAL *RESTRICT unew, const REAL *RESTRICT u, const REAL *RESTRICT uold)
{
    INT i, j;

    for (j = 1; j < NY - 1; j++) {
        for (i = 1; i < NX - 1; i++) {
            unew[IC]
            = 2.0f * u[IC] - uold[IC] + FACTOR * (u[IP1] + u[IM1] + u[JP1] + u[JM1] - 4.0f * u[IC]);
        }
    }
}

void initWave(REAL *RESTRICT u, REAL *RESTRICT uold, REAL *RESTRICT x, REAL *RESTRICT y)
{
    INT i, j;
    for (j = 1; j < NY - 1; j++) {
        for (i = 1; i < NX - 1; i++) {
            u[IC] = 0.1f * (4.0f * x[IC] - x[IC] * x[IC]) * (2.0f * y[IC] - y[IC] * y[IC]);
            // u[IC] =  x[IC] * ( PI-x[IC] ) * y[IC] * ( PI-y[IC] );
            // u[ij] = fabs( cos( x[ij] - 3.0*y[ij] ) );
        }
    }
    for (j = 1; j < NY - 1; j++) {
        for (i = 1; i < NX - 1; i++) {
            uold[IC] = u[IC] + 0.5f * FACTOR * (u[IP1] + u[IM1] + u[JP1] + u[JM1] - 4.0f * u[IC]);
        }
    }
}

void analyticalSoln(INT nTimeSteps, REAL *RESTRICT uAnalytical, REAL *RESTRICT XX,
                    REAL *RESTRICT YY)
{
    INT i, j, m, n, limit;
    limit = 51;

    REAL x, y, t, factor, sum;

    t = (REAL) dt * nTimeSteps;
    printf("ntimesteps=  %d\n", nTimeSteps);

    for (j = JSTART; j < JEND; j++) {
        for (i = ISTART; i < IEND; i++) {
            x   = XX[IC];
            y   = YY[IC];
            sum = 0.0;
            for (m = 1; m < limit; m = m + 2) {
                for (n = 1; n < limit; n = n + 2) {
                    factor = (1.0f / (REAL)(m * m * m * n * n * n));
                    sum    = sum
                          + factor
                            * cos(t * 0.25f * sqrt(cSqrd) * PI * sqrt((REAL) m * m + 4 * n * n))
                            * sin((REAL) m * PI * x * 0.25f) * sin((REAL) n * PI * y * 0.50f);
                }
            }
            uAnalytical[IC] = sum * 0.426050f;
        }
    }
}

void meshGrid(REAL *RESTRICT x, REAL *RESTRICT y)
{
    INT  i, j;
    REAL a;

    for (j = 0; j < NY; j++) {
        a = dx * ((REAL) j);
        for (i = 0; i < NX; i++) {
            x[IC] = dx * ((REAL) i);
            y[IC] = a;
        }
    }
}

void writeOutput(REAL *RESTRICT phi)
{
    INT   i, j;
    FILE *output;
    output = fopen("wave.dat", "w");

    for (j = 0; j < NY; j++) {
        for (i = 0; i < NX; i++) {
            fprintf(output, "%f\n", phi[IC]);
        }
    }
    fclose(output);
}

void print2Display(REAL *phi)
{
    INT i, j;

    for (j = JSTART; j < JEND; j++) {
        for (i = ISTART; i < IEND; i++) {
            printf("%12.8f", phi[IC]);
        }
        printf("\n\n");
    }
}

INT main(INT argc, char *argv[])
{
    if (argc != 2) {
        perror("Command-line usage: executableName <# time steps>");
        exit(1);
    }

    INT nTimeSteps = atoi(argv[1]);

    REAL *x = calloc(NX * NY, sizeof *x);
    REAL *y = calloc(NX * NY, sizeof *y);

    REAL *unew  = calloc(NX * NY, sizeof *unew);
    REAL *u     = calloc(NX * NY, sizeof *u);
    REAL *uold  = calloc(NX * NY, sizeof *uold);
    REAL *exact = calloc(NX * NY, sizeof *exact);
    REAL *tmp;

    meshGrid(x, y);
    initWave(u, uold, x, y);

    StartTimer();

    for (INT n = 1; n <= nTimeSteps ; n++) {
        solveWave(unew, u, uold);

        tmp  = uold;
        uold = u;
        u    = unew;
        unew = tmp;
    }

    double elapsedTime = GetTimer();
    // obtain the analytical solution

    analyticalSoln(nTimeSteps, exact, x, y);

    printf("|||||||----ANALYTICAL SOLUTION----|||||||||\n");
    print2Display(exact);

    printf("|||||||----NUMERICAL SOLUTION----|||||||||\n");
    print2Display(u);
    printf("||||||||||||||||||||||||||||||\n");
    printf("Time elapsed = %f s\n", elapsedTime);

    //   writeOutput( u );

    free(unew);
    free(u);
    free(uold);
    free(exact);
    free(x);
    free(y);

    return EXIT_SUCCESS;
}

