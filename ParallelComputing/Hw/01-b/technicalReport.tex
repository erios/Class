% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{color}
\usepackage{listings}
\usepackage[style=verbose,backend=bibtex]{biblatex}
\addbibresource{biblio.bib}
\input{preamble.tex}
\usepackage{pdfpages}
\author{Edgar Rios}
\date{\today}
\title{Numerical simulation of 1D Couette Flow\\\medskip
\large Assignment \#1 Part 2. Parallel Scientific Computing.}
\hypersetup{
 pdfauthor={Edgar Rios},
 pdftitle={Numerical simulation of 1D Couette Flow},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 24.4.1 (Org mode 9.0.4)},
 pdflang={English}}
\begin{document}

\maketitle


\section{Short description of the problem:}
\label{sec:org30592a2}
A fluid is forced into motion by a pressure gradient normal to the
direction of flow and a surface in contact with its top layer. It
flows through a channel with constant hydraulic diameter (the
cross-section of the channel does not change). For a Newtonian fluid,
this creates a Couette flow. The velocity profile of the fluid is
given by the expression
\begin{align*}
\frac{\partial}{\partial{}t}u(t,X) =
\frac{-1}{\rho}\frac{\partial{}}{\partial{}x}P(X) +
\mu\,\frac{\partial{}^{2}}{\partial{}y^{2}}\,u(t,X)
\end{align*}
where \(u(t,X)\) is the velocity in the direction of \(x\) in terms of time
(\(t\)) and the position coordinates (\(X\)), \(P\) is the pressure applied
to the fluid, \(\rho\) is the density, \(\mu\) is the static viscosity of the
fluid.

\subsection{Numerical solution}
\label{sec:org13e40a0}

\subsubsection{Difference formula}
\label{sec:org0b5d556}
A numerical solution to the previous non-linear equation can be found
to be:

\begin{align*}
  u_{j}^{n+1} = \Delta(t)\,\left[
  \frac{u_{j+1}^{n}
  - 2\,u_{j}^{n}
  + u_{j-1}^{n}}{(\Delta{y})^{2}}\right] + u_{j}^{n}
\end{align*}
where the superscripts \(n\) and \(n+1\) represent steps in time, and the
sub-indices \(j\), \(j+1\) and \(j-1\) signify discrete positions along the
\(y\) axis for the velocity \(u\). (Reading \cite{Wilkinson} is
recommended for a general understanding of mathematics, and
\cite{Geor:Liu:1981} must be a good book as well).The increments of
time are given by \(\Delta(t)\), which can be calculated with:
\begin{align*}
\Delta(t) \le 0.5\,\frac{(\Delta(y))^2}{\nu}
\end{align*}
In this new equation, \(\nu\) represents the dynamic velocity of the
fluid, which is related to the Reynolds number by \(Re =
\frac{U_p\,h}{\nu}\). From here, one needs to specify the physical
conditions of the problem.

When these physical conditions are met, the step is to solve
numerically. The time to reach steady-state may be calculated with the
ratio between the square of \(h\) (the distance from the fixed wall of
the channel to the moving plate) and \(nu\): \(\frac{h^2}{\nu}\)

\subsubsection{Exact solution}
\label{sec:org69afaae}
An exact solution also exists, and can be used to test the numerical
results. The expression is like this:
$$u\left(y\right)=\frac{\left(y^2-h\,y\right)\,{\it dP}}{2\,{\it dx}
 \,\mu}+\frac{V_{{\it plate}}\,y}{h}$$

\section{Results}
\label{sec:org9c42f72}
\subsection{Values for \(Re=10\) and \(\frac{\partial{}}{\partial{}x}P(X) = -2.0\)}
\label{sec:org7165e3a}
Table tblRe10dP-2.0 shows the result for this analysis. The
conditions are given in the list below, and figure
\ref{fig:pngRe10dP-2.0} gives a graphical representation of it:
\begin{itemize}
\item Distance between the plates (h): 1.00000
\item Plate velocity (uPlate): 1.00000
\item Density (rho): 1.00000
\item Dynamic viscosity (nu): 1.00000
\item Static viscosity (mu): 1.00000
\item Number of calculation points (NY): 21
\item Time step (dt): 0.00125
\item Number of time steps (nTimeSteps): 800
\item Re=1, \(\frac{dP}{dx}=-2\)
\end{itemize}

\begin{longtable}{rrr}
y & Exact & Numer\\
\hline
\endfirsthead
\multicolumn{3}{l}{Continued from previous page} \\
\hline

y & Exact & Numer \\

\hline
\endhead
\hline\multicolumn{3}{r}{Continued on next page} \\
\endfoot
\endlastfoot
\hline
0.00000 & 0.00000 & 0.00000\\
0.05000 & 0.52500 & 0.52498\\
0.10000 & 1.00000 & 0.99995\\
0.15000 & 1.42500 & 1.42493\\
0.20000 & 1.80000 & 1.79991\\
0.25000 & 2.12500 & 2.12489\\
0.30000 & 2.40000 & 2.39987\\
0.35000 & 2.62500 & 2.62486\\
0.40000 & 2.80000 & 2.79985\\
0.45000 & 2.92500 & 2.92484\\
0.50000 & 3.00000 & 2.99984\\
0.55000 & 3.02500 & 3.02484\\
0.60000 & 3.00000 & 2.99985\\
0.65000 & 2.92500 & 2.92486\\
0.70000 & 2.80000 & 2.79987\\
0.75000 & 2.62500 & 2.62489\\
0.80000 & 2.40000 & 2.39991\\
0.85000 & 2.12500 & 2.12493\\
0.90000 & 1.80000 & 1.79995\\
0.95000 & 1.42500 & 1.42498\\
1.00000 & 1.00000 & 1.00000\\
\caption{\label{tblRe10dP-2.0}
Normalised values for the exact and numerical solutions when Re=10 and \(\frac{dP}{dx}=-2\) for the velocity along \(y\).}
\\
\end{longtable}

It is interesting to see how in figure \ref{fig:pngRe10dP-2.0}, the
dynamic effects are much stronger than the pressure.
\begin{figure}[htbp]
\centering
\includegraphics[width=0.7\textwidth]{pngRe10dP-2.0.png}
\caption{\label{fig:pngRe10dP-2.0} Numerical and exact solutions for \(Re=10\) and \(\frac{\partial{}}{\partial{}x}P(X) = -2.0\)}
\end{figure}



\subsection{Values for \(Re=10\) and \(\frac{\partial{}}{\partial{}x}P(X) = [0.0, 2.0, -2.0]\)}
\label{sec:org171a96d}
The solution for these conditions are shown in figure
\ref{fig:pngRe10}. There, one can see that, in fact, the profile
follows the pressure towards the positive direction.
\begin{figure}[htbp]
\centering
\includegraphics[width=0.7\textwidth]{pngRe10.png}
\caption{\label{fig:pngRe10} Numerical and exact solutions for \(Re=10\)}
\end{figure}


\subsection{Values for \(\frac{\partial{}}{\partial{}x}P(X)=2.0\) and \(Re= [1, 5, 10]\)}
\label{sec:org85f91cd}
The solution for these conditions are shown in figure
\ref{fig:pngdP2.0}. Finally, comparing this to \ref{fig:pngRe10}, one
has a clear image of how the dynamics and the pressure interact to
create the velocity profile.
\begin{figure}[htbp]
\centering
\includegraphics[width=0.7\textwidth]{pngdp2.0.png}
\caption{\label{fig:pngdP2.0} Numerical and exact solutions for \(dP=2.0\) and \(\frac{\partial{}}{\partial{}x}P(X) = -2.0\)}
\end{figure}

\section{Appendix A - Data file \(Re=10\) and \(\frac{\partial{}}{\partial{}x}P(X) = -2.0\)}
\label{sec:orgb6f4b11}

\begin{verbatim}
Distance between the plates (h): 1.00000
Plate velocity (uPlate): 1.00000
Density (rho): 1.00000
Dynamic viscosity (nu): 0.10000
Static viscosity (mu): 0.10000
Number of calculation points (NY): 21
Time step (dt): 0.01250
Number of time steps (nTimeSteps): 800
Re=10, $\frac{dP}{dx}=-2$
Results:
"y"	"Exact"	"Numer"
0.00000	0.00000	0.00000
0.05000	0.52500	0.52498
0.10000	1.00000	0.99995
0.15000	1.42500	1.42493
0.20000	1.80000	1.79991
0.25000	2.12500	2.12489
0.30000	2.40000	2.39987
0.35000	2.62500	2.62486
0.40000	2.80000	2.79985
0.45000	2.92500	2.92484
0.50000	3.00000	2.99984
0.55000	3.02500	3.02484
0.60000	3.00000	2.99985
0.65000	2.92500	2.92486
0.70000	2.80000	2.79987
0.75000	2.62500	2.62489
0.80000	2.40000	2.39991
0.85000	2.12500	2.12493
0.90000	1.80000	1.79995
0.95000	1.42500	1.42498
1.00000	1.00000	1.00000
\end{verbatim}

\includepdf[pages=-,fitpaper=true]{../01/HW1.pdf}

\includepdf[pages=-,fitpaper=true]{HW1_part2.pdf}

\includepdf[pages=-,fitpaper=true]{rios_HW1_code.pdf}

\includepdf[pages=-,fitpaper=true]{technicalReport.tex.pdf}


\printbibliography
\end{document}
