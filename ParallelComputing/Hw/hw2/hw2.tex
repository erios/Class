% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{color}
\usepackage{listings}
\usepackage[style=verbose,backend=bibtex]{biblatex}
\addbibresource{biblio.bib}
\input{preamble.tex}
\usepackage{multicol}
\usepackage{longtable}
\usepackage{pdflscape}
\author{Edgar Rios.}
\date{\today}
\title{Single and double precision calculations.\\\medskip
\large Assignment \#2. Parallel Scientific Computing.}
\hypersetup{
 pdfauthor={Edgar Rios.},
 pdftitle={Single and double precision calculations.},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 24.4.1 (Org mode 9.0.5)},
 pdflang={English}}
\begin{document}

\maketitle

\section{Problem 1: Euler's divergent series}
\label{sec:orgc6945a4}
\subsection{Maximum computing limits}
\label{sec:org8d9a189}
Table \ref{tab:limits} contains the limits for the numerical computations on
the current system, which is presented below:
\begin{multicols}{2}
\begin{itemize}
\item Architecture:          x86\_64
\item CPU op-mode(s):        32-bit, 64-bit
\item Byte Order:            Little Endian
\item CPU(s):                1
\item On-line CPU(s) list:   0
\item Thread(s) per core:    1
\item Core(s) per socket:    1
\item Socket(s):             1
\item NUMA node(s):          1
\item Vendor ID:             GenuineIntel
\item CPU family:            6
\item Model:                 22
\item Stepping:              1
\item CPU MHz:               1729.086
\item BogoMIPS:              3458.17
\item L1d cache:             32K
\item L1i cache:             32K
\item L2 cache:              1024K
\item NUMA node0 CPU(s):     0
\end{itemize}
\end{multicols}

\begin{landscape}
\begin{longtable}{ll}
Symbol & Description (Value)\\
\hline
\endfirsthead
\multicolumn{2}{l}{Continued from previous page} \\
\hline

Symbol & Description (Value) \\

\hline
\endhead
\hline\multicolumn{2}{r}{Continued on next page} \\
\endfoot
\endlastfoot
\hline
SPINT & single precision integer\\
DPINT & double precision integer\\
SPFL & single precision floating point number\\
DPFL & double precision floating point number\\
LLONG\_MAX & largest positive (signed) DPINT\\
 & (9223372036854775807)\\
INT\_MAX & largest positive (signed) SPINT\\
 & (2147483647)\\
ULLONG\_MAX & largest unsigned DPINT\\
 & (18446744073709551615)\\
LLONG\_MIN & most negative (signed) DPINT\\
 & (-9223372036854775808)\\
LONG\_MIN & most negative (signed) SPINT\\
 & (-2147483648)\\
FLT\_EPSILON & difference between 1 and the\\
 & smallest SP floating point that\\
 & is greater than 1\\
 & (\$1.19209289550781250\texttimes{}10\(^{\text{07}}\))\\
FLT\_EPSILON\(^{\text{-1}}\) & Inverse of FLT\_EPSILON\\
 & (8388608)\\
LDBL\_EPSILON & Similar to FLT\_EPSILON, but for\\
 & long double types\\
 & (1.084202172485504434007452800869941711425781250\texttimes{}10\(^{\text{-19}}\))\\
FLT\_EPSILON\(^{\text{-1}}\) & Similar to FLT\_EPSILON\(^{\text{-1}}\), but for\\
 & long double types\\
 & (9.223372036854775808\texttimes{}10\(^{\text{+18}}\))\\
FLT\_MAX & largest signed SPFL\\
 & (3.402823466385288598117042\texttimes{}10\(^{\text{+38}}\))\\
LDBL\_MAX & largest signed DPFL\\
 & (1.1897314953572317650212638530309702051690633222946242004403237339\texttimes{}10\(^{\text{+4932}}\))\\
\caption{\label{tab:limits}
Symbols description}
\\
\end{longtable}
\end{landscape}

\subsection{Computing benchmark}
\label{sec:org3201e1f}
\subsubsection{Harmonic numbers}
\label{sec:orgb961149}
A harmonic number \(H_{n}\) is defined by the divergent series:
$$H_{n} = \sum_{k=1}^{n}{\frac{1}{k}}$$

The code was prepared to output a range of values for the harmonic
numbers, based on a maximum given by the user. The ranges were
discretised logarithmically.

\subsubsection{Results}
\label{sec:org9743712}
The following tables present data that can be used to predict the
amount of time which would take to finish the calculations with
the available precision. The first column presents a range for \(k\)
and how much it takes to compute each of the ranges.

\begin{table}[htbp]
\centering
\begin{tabular}{rr}
n & accumulated time (s)\\
\hline
$1\times10^{+01}$ & $5.000000\times10^{-6}$\\
$1\times10^{+02}$ & $9.000000\times10^{-6}$\\
$1\times10^{+03}$ & $2.200000\times10^{-5}$\\
$1\times10^{+04}$ & $1.630000\times10^{-4}$\\
$1\times10^{+05}$ & $1.581000\times10^{-3}$\\
$1\times10^{+06}$ & $1.580200\times10^{-2}$\\
$2.097\times10^{+06}$ & $3.306500\times10^{-2}$\\
\end{tabular}
\caption{\label{projectionFlt}
The harmonic series with single precision}

\end{table}

\begin{center}
\includegraphics[width=.9\linewidth]{projectionFlt.png}
\end{center}

\begin{table}[htbp]
\centering
\begin{tabular}{rr}
n (max) & accumulated time (s)\\
\hline
$1\times10^{+01}$ & $3.000000\times10^{-6}$\\
$1\times10^{+02}$ & $8.000000\times10^{-6}$\\
$1\times10^{+03}$ & $2.700000\times10^{-5}$\\
$1\times10^{+04}$ & $2.330000\times10^{-4}$\\
$1\times10^{+05}$ & $2.165000\times10^{-3}$\\
$1\times10^{+06}$ & $2.165600\times10^{-2}$\\
$1\times10^{+07}$ & $2.158510\times10^{-1}$\\
$1\times10^{+08}$ & $2.157500\times10^{+0}$\\
$1\times10^{+09}$ & $2.157913\times10^{+01}$\\
$1\times10^{+10}$ & $2.158417\times10^{+02}$\\
$1\times10^{+11}$ & $2.158542\times10^{+03}$\\
\end{tabular}
\caption{\label{projectionDbl}
The harmonic series with double precision}

\end{table}

\begin{center}
\includegraphics[width=.9\linewidth]{projectionDbl.png}
\end{center}

As it can be seen from the figures, the accumulated time behaves
logarithmically, with the double precision growing at a faster
rate; twice as much as the single precision. If one looks at the
figure, it is easy to infer that it would take forever before the
could take forever to calculate.

The single precision results for the series gave the following results:
\begin{itemize}
\item Time: 54.20200 ms
\item Sum: 15.40368
\item Harmonic number: 2 097 153
\end{itemize}

As it has been portrayed, the double precision case would be
unfeasible for my humble computer. Nonetheless, we could at least
calculate it to the given limit of \(n=1e10\).
\begin{itemize}
\item Time: 21.58542 s
\item Sum: 25.90565
\item Harmonic number: 100 000 000 000
\end{itemize}

\section{Problem 2. Euler function as an infinite series}
\label{sec:org1a6ecd2}
\subsection{Stopping criteria}
\label{sec:org79c541c}
\subsubsection{Factorials}
\label{sec:orga28517b}
Based on the computational limits of the computer, the maximum
factorial which can be used for the series is 12 for single
precision and 20 for double precision. More than that would create
an overflow for the integer representation.

Besides, it could be that the calculated factorial would derive in
an unrepresentable increment (\(\epsilon\) or \(\infty\)). These two were
prevented too.

Mathematically, the factorial is only a function of non-negative
integers. Thus, calculations were made only for this criterion.
\subsubsection{Exponent}
\label{sec:org8374901}
Besides, the loops where stopped when an overflow was detected by
means of the infinity constant from the float.h library.
\subsection{Rearrangement}
\label{sec:org1382035}
To calculate the exponential function more accurately, a
continued fraction was used. However, the results were not as
expected. Different variations were tested, and still, the results
were not as desired. Debugging the code only showed that the
calculations were right. Still, the results were not accurate.
\subsection{Results}
\label{sec:orgec886bd}
The following table presents the results of the computations.
\begin{landscape}
\begin{longtable}{rlrrlr}
x & SP & math.h SP & Continued fractions & DP & math.h DP\\
\hline
\endfirsthead
\multicolumn{6}{l}{Continued from previous page} \\
\hline

x & SP & math.h SP & Continued fractions & DP & math.h DP \\

\hline
\endhead
\hline\multicolumn{6}{r}{Continued on next page} \\
\endfoot
\endlastfoot
\hline
-20 & NA & $7.582561\times10^{-10}$ & $2.692936\times10^{+02}$ & NA & $7.5825604279119067278\times10^{-10}$\\
-15 & NA & $1.125352\times10^{-07}$ & $2.573818\times10^{+02}$ & NA & $1.1253517471925911451\times10^{-07}$\\
-10 & NA & $1.670170\times10^{-05}$ & $2.484295\times10^{+02}$ & NA & $1.6701700790245659312\times10^{-05}$\\
-5 & NA & $2.478752\times10^{-03}$ & $2.428652\times10^{+02}$ & NA & $2.4787521766663584230\times10^{-03}$\\
-1 & NA & $1.353353\times10^{-01}$ & $2.410684\times10^{+02}$ & NA & $1.3533528323661269190\times10^{-01}$\\
1 & $2.718282\times10^{+00}$ & $1.000000\times10^{+00}$ & $2.410849\times10^{+02}$ & $2.7182818284590452350\times10^{+00}$ & $1.0000000000000000000\times10^{+00}$\\
5 & $1.476039\times10^{+02}$ & $5.459815\times10^{+01}$ & $2.429473\times10^{+02}$ & $1.4841310786833832008\times10^{+02}$ & $5.4598150033144239077\times10^{+01}$\\
10 & $1.534752\times10^{+04}$ & $8.103084\times10^{+03}$ & $2.485901\times10^{+02}$ & $2.1950378849431941491\times10^{+04}$ & $8.1030839275753840076\times10^{+03}$\\
15 & NA & $1.202604\times10^{+06}$ & $2.576147\times10^{+02}$ & $2.8611054127726089096\times10^{+06}$ & $1.2026042841647767777\times10^{+06}$\\
20 & NA & $1.784823\times10^{+08}$ & $2.695911\times10^{+02}$ & $2.2815245875893375820\times10^{+08}$ & $1.7848230096318726084\times10^{+08}$\\
\caption{Results for the exponential function}
\\
\end{longtable}
\end{landscape}
\end{document}
