#ifndef EXPSERIES_H
#define EXPSERIES_H

/* powf, log10 */
#include "headers.h"
#include <math.h>

/* kind of function that returns a series of
   coefficients */
typedef double (*coeff_func)(unsigned n);

int exponential(int loopsFlt);

double calc(coeff_func f_a,
            coeff_func f_b,
            unsigned expansions);

int checkExpBrkLim(int *loops, int *lim);

int euler(int *loopsFlt_ptr, int *x_ptr);

int testFltExp(int *loopsFlt, int *limFlt);

int testDblExp(int *loopsDbl, int *limDbl);

long long factorial(int n);

#endif /* EXPSERIES */
