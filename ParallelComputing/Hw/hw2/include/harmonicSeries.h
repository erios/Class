#ifndef HARMONICSERIES_H
#define HARMONICSERIES_H

/* powf, log10 */
#include <math.h>
#include "headers.h"

int harmonicSeries(int loopsFlt,
                   int loopsDbl,
                   long double brkLim);

int floatHarmonic(int *loops_ptr,
                  clock_t *  start_ptr,
                  clock_t *  end_ptr,
                  double *   cpu_time_used_ptr,
                  double *   single_loop_ptr,
                  double *   cpu_time_ptr);

int dblHarmonic(int *    loops_ptr,
                clock_t *start_ptr,
                clock_t *end_ptr,
                double * cpuTimeUsed_ptr,
                double * singleLoop_ptr,
                double * cpuTime_ptr);

int testFlt();

int testDbl();

int testFltSum(long double *brkLim);

int testDblSum(long double *brkLim);

#endif  /* HARMONICSERIES */
