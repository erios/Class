/**
 *   \file headers.h
 *   \brief This file is loaded by almost all
 *   the source files (.c).
 *
 *  Detailed description. It loads the user
 *   interaction, strings, memory allocation, data
 *   type limits and timing libraries from GNU C.
 *
 */
/* Define everything only once */
#ifndef HEADERS_H
#define HEADERS_H

#define CPS CLOCKS_PER_SEC
#define ARRLEN(a) (sizeof(a)/sizeof(*a))

/* Interaction with the user */
#include <stdio.h>
/* Strings, memory allocation */
#include <stdlib.h>
/* Float limits */
#include <limits.h>
#include <float.h>
/* Timing events */
#include <time.h>

#endif
