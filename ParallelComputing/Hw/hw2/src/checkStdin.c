#include "checkStdin.h"

int checkStdin(int *argc, char *argv[])
{
    /* Check command line arguments */
    if (*argc != 5) {
        fprintf(
        stderr,
        "This program requires 4"
        " arguments:\n %s"
        " <Number (int) of loops for the"
        " single precision harmonic series"
        "> "
        " <Number (int) of loops for the"
        " double precision harmonic series"
        "> "
        " <Breaking limit (long double) at"
        " which the process stops> "
        " <Number (int) of loops for the"
        " (single precision) continued"
        " fractions exp(x)>",
        argv[0]);
        exit(-1);
        /* perror(); */
    }

    return 0;
}
