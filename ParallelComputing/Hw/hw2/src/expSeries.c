/**
 *   \file expSeries.c
 *   \brief This file contains the functions to
 *   calculate and benchmark the euler function as
 *   an infinite series
 *
 *  The euler function is the exponential
 *  function. It is pervasive in most
 *  environments, and there are different ways to
 *  calculate it. The canonical way is by means of
 *  an infinite series. This program tries to show
 *  how much it would take to do it if one uses
 *  single precision numbers as compared to double
 *  precision numbers.
 *
 *  Reference:
 *  http://www.luschny.de/math/factorial/FastFactorialFunctions.htm
 *
 */
#include "expSeries.h"

int exponential(int loopsFlt)
{
    /**
     *  \brief calls the functions to calculate
     * the
     *  Euler's exponential number by means of a
     *  series.
     *
     *  The  series for the exponential number is
     *  e^{x} = \sum_{k=0}^{x}{1/k!}.
     *  This function has a gross way to calculate
     *  it and give an estimate of how long it
     *  would take to calculate it up to a high n
     *  with single and double precision.
     *
     *  \param
     *  loopsFlt -- number of loops to spend on
     *  the calculation for single precision (n)
     *
     *  loopsDbl -- number of loops to spend on
     * the
     *  calculation for double precision (n)
     *
     *  \return (int) returns 0 if everything went
     *  well
     */
    /* Working variables */
    int limFlt = 12;
    int limDbl = 20;
    /* Loops */
    int a[10]
    = {-20, -15, -10, -5, -1, 1, 5, 10, 15, 20};

    for (int i = 0; i < ARRLEN(a); i++) {
        /** The factorial is not defined for
            nonnegative integers,
            and the factorial of 13 is
           6,227,020,800,
            which is larger
            than UINT_MAX (4,294,967,295)*/
        printf("INFO: exp(x = %i)\n", a[i]);
        printf("Single precision\n");
        if (checkExpBrkLim(&a[i], &limFlt) == 0) {
            testFltExp(&a[i], &limFlt);
        }
        printf("From math.h: %0.6e\n",
               expf(a[i]));
        printf("\n");
        /** The factorial is not defined for
            nonnegative integers,
            and the factorial of 21 is
            51,090,942,171,709,440,000, which is
            larger
            than ULLONG_MAX
            (18,446,744,073,709,551,615)*/
        printf("Double precision\n");
        if (checkExpBrkLim(&a[i], &limDbl) == 0) {
            testDblExp(&a[i], &limDbl);
        }
        printf("From math.h: %0.19Le\n",
               expl(a[i]));
        printf("\n");

        euler(&loopsFlt, &a[i]);
        printf("\n");
    }
    /* free memory */
    return 0;
}

int checkExpBrkLim(int *loops_ptr, int *lim_ptr)
{
    if (*loops_ptr < 0) {
        fprintf(stderr,
                "For this routine, n needs to be"
                " greater or equal"
                " to zero. Currently, n is: %i\n",
                *loops_ptr);
        /* exit(EXIT_FAILURE); */
        return 1;
    }

    else if (*loops_ptr > *lim_ptr) {
        fprintf(stderr, "For this routine, n"
                        " needs to be lower or"
                        " equal to %d."
                        " Currently, n is: %d\n",
                *lim_ptr, *loops_ptr);
        /* exit(EXIT_FAILURE); */
        return 1;
    }

    else {
        return 0;
    }
}

int testFltExp(int *loopsFlt_ptr, int *limFlt_ptr)
{
    /**
     *  \brief Brute force calculation of
     * exponential
     *  series with single precision numbers
     *
     *  Read the documentation of exponential().
     *
     *  \param none
     *  \return 0 if all goes well
     */
    int       i       = 0;
    float     sumFlt  = 1.0f;
    float     sumFlt2 = sumFlt;
    long long fact;
    /* to keep the exponential */
    float xPow = 1.0f;

    if (*loopsFlt_ptr > *limFlt_ptr) {
        printf("INFO: An accurate result can"
               " only be represented up to"
               " n = %i\n",
               *limFlt_ptr);
    }

    /* n >= 0 has been previously ascerted
       (needed for the factorial) */
    i = 1;
    do {
        sumFlt = sumFlt2;
        xPow *= *loopsFlt_ptr;
        fact = factorial(i);
        sumFlt2 += xPow / (float) fact;
        i += 1;
    } while (isfinite(xPow) && isfinite(sumFlt2)
             && i <= *limFlt_ptr);
    printf("Euler function with"
           " single precision\n");
    printf("Sum: %0.6e\nIterations: %'d\n",
           sumFlt, i - 1);

    return 0;
}

int testDblExp(int *loopsDbl_ptr, int *limDbl_ptr)
{
    /**
     *  \brief Brute force calculation of
     * exponential
     *  series with single precision numbers
     *
     *  Read the documentation of exponential().
     *
     *  \param none
     *  \return 0 if all goes well
     */
    int         i       = 0;
    long double sumDbl  = 1.0L;
    long double sumDbl2 = sumDbl;
    long long   fact;
    /* to keep the exponential */
    long double xPow = 1.0L;

    if (*loopsDbl_ptr > *limDbl_ptr) {
        printf("INFO: An accurate result can"
               " only be represented up to"
               " n = %i\n",
               *limDbl_ptr);
    }

    /* n >= 0 has been previously ascerted
       (needed for the factorial)*/
    i = 1;
    do {
        sumDbl = sumDbl2;
        xPow *= *loopsDbl_ptr;
        fact = factorial(i);
        sumDbl2 += xPow / (long double) fact;
        i += 1;
    } while (isfinite(xPow) // the exponential
                            // grows faster than
                            // the factorial);
             && i <= *limDbl_ptr);
    printf(
    "Euler function with double precision\n");
    printf("Sum: %0.19Le\nIterations: %'d\n",
           sumDbl, i - 1);

    return 0;
}

int euler(int *loopsFlt_ptr, int *x_ptr)
{
    float denNoRat, sum, prevSum;
    int   i;
    int   x2 = *x_ptr * *x_ptr;
    denNoRat = sum = prevSum = 0.0f;

    /* Use sum as temporary variable (for the else
       if below)
     */
    sum = (2 * *loopsFlt_ptr - 6) * *x_ptr;
    /* Use prevSum as temporary variable */
    prevSum = (4 * *loopsFlt_ptr - 6);

    /* Calculate the case when the exponential is
       a positive integer */
    /* Make sure that the number of loops is
       greater than 0; that there would not be an
       overflow in the multiplication of the
       numerator, and that the non-rational part
       can
       be calculated too */
    if (*loopsFlt_ptr > 0
        && isfinite(*loopsFlt_ptr * *x_ptr)
        && fabs(prevSum) < 1 / FLT_EPSILON) {
        sum
        = (4 * *loopsFlt_ptr - 6); // see below
        i = (int) *loopsFlt_ptr - 1;
        do {
            /* Save previous value */
            prevSum = sum;
            /* Calculate the non-rational part of
               the
               sum */
            denNoRat
            = (4 * i - 6); // 6, 10, 14, 18...
            sum += denNoRat + x2 / prevSum;
            i--;
        } while (i > 2);
        sum
        += 1 + (2 * *x_ptr) / (2 - *x_ptr + sum);
    }
    /* Calculate the case when the exponential is
       a negative integer */
    /* Make sure that the number of loops is
       greater than 0; that there would not be an
       overflow in the multiplication of the
       numerator, and that the non-rational part
       can
       be calculated too */
    /* x_ptr is now in the non-rational part */
    else if (*loopsFlt_ptr < 0
             && isfinite(*loopsFlt_ptr * *x_ptr)
             && isfinite(sum)
             && fabs(1 + 1 / (sum))
                < 1 / FLT_EPSILON) {
        sum = (4 * *loopsFlt_ptr - 6) * *x_ptr;
        i   = *loopsFlt_ptr - 1;
        do {
            prevSum  = sum;
            denNoRat = (4 * i - 6) * *x_ptr;
            sum += denNoRat + 1 / prevSum;
            i--;
        } while (i > 2);
        sum += 1 + 2 / (2 * *x_ptr - 1 + sum);
    }
    /* Give me a break :) (too much for me!) */
    else if (!(
             isfinite(*loopsFlt_ptr * *x_ptr))) {
        printf(
        "WARN: The number of iterations would"
        " overflow. Please choose a new"
        " combination of exponential and "
        "limit\n");
        sum = 0;
    }

    printf(
    "The euler function using continued fractions"
    " is: %0.6e\n",
    sum);

    return 0;
}

long long factorial(int n)
{
    long long f[22]
    = {1, // 0
       1,
       2,
       6,
       24,
       120, // 5
       720,
       5040,
       40320,
       362880,
       3628800, // 10
       39916800,
       479001600,
       6227020800,
       87178291200,
       1307674368000, // 15
       20922789888000,
       355687428096000,
       6402373705728000,
       121645100408832000,
       2432902008176640000, // 20
       /* 51090942171709440000L */};
    return f[n];
}
