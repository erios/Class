/**
 *   \file harmonicSeries.c
 *   \brief Calculates the harmonic series
 *
 *   This file contains the functions to
 *   calculate and benchmark the harmonic
 *   (Euler's) series in single and double
 *   precision.
*/

#include "harmonicSeries.h"

int harmonicSeries(int loopsFlt, int loopsDbl,
                   long double brkLim)
{
    /**
     *  \brief calls the functions to do the
     * harmonic series
     *
     *  The harmonic series is
     * \sum_{k=1}^{\infty}{1/k}.
     *  This function has a gross way to calculate
     *  it and give an estimate of how long it
     *  would take to calculate it up to a high n
     *  with single and double precision.
     *
     *  \param
     *  loopsFlt -- number of loops to spend on
     *  the calculation for single precision (n)
     *
     *  loopsDbl -- number of loops to spend on
     * the
     *  calculation for double precision (n)
     *
     *  \return (int) returns 0 if everything went
     *  well
     */
    /* Working variables */
    /** Clocking variables **/
    // The GNU C Library Reference Manual', for
    //   Version 2.19 (Processor and CPU Time)
    clock_t start, end;
    double  cpuTimeUsed, singleLoop;
    double  cpuTime;

    printf("\nThe harmonic series with single"
           " precision takes:\n");
    printf("n (min)\tn (max)\tseconds\n");
    floatHarmonic(&loopsFlt, &start, &end,
                  &cpuTimeUsed, &singleLoop,
                  &cpuTime);
    printf("\n");

    /* testFlt(); */
    testFltSum(&brkLim);
    printf("\n");

    printf("\nThe harmonic series with double"
           " precision takes:\n");
    printf("n (min)\tn (max)\tseconds\n");
    dblHarmonic(&loopsDbl, &start, &end,
                &cpuTimeUsed, &singleLoop,
                &cpuTime);
    printf("\n");

    /* testDbl(); */
    testDblSum(&brkLim);

    /* free memory */
    return 0;
}

int floatHarmonic(int *    loops_ptr,
                  clock_t *start_ptr,
                  clock_t *end_ptr,
                  double * cpuTimeUsed_ptr,
                  double * singleLoop_ptr,
                  double * cpuTime_ptr)
{
    /**
     *  \brief calculates the time it takes to
     *  calculate the harmonic series up to k =
     *  loops_ptr
     *
     *  The harmonic series is
     * \sum_{k=1}^{\infty}{1/k}.
     *  This function has a gross way to calculate
     *  it and give an estimate of how long it
     *  would take to calculate it up to a high
     *  k. It does two loops: the first one with
     *  the function two times and the second one
     *  with only one call. The difference is the
     *  actual computing time of the series
     *  (without loop overhead).
     *
     *  \param
     *  loops_ptr (int*) -- number of
     *  loops that you want to spend calculating
     *  (n)
     *
     *  start_ptr (clock_t* <time.h>) -- clock
     *  value when a loop to be benchmarked starts
     *
     *  end_ptr (clock_t* <time.h>) -- clock
     *  value when a loop to be benchmarked ends
     *
     *  cpuTimeUsed_ptr (double*) --
     *
     *  \return return type
     */
    /* Loop variables */
    int   i;
    int   j;
    float nextJ;
    /** k = 10^logMinLoop is the minimum number of
       cycles to profile **/
    short int logMinLoop = 1;
    /** 10-base log of the sought loops **/
    float logLoops = log10f(*loops_ptr);
    /** Maximum number of loops for profiling **/
    float logMaxLoop
    = ceilf(log10f((float) *loops_ptr));
    /** Logarithmic scale of time for various k=n
     * **/
    float loopsNow;

    /* Working variables */
    /** single precision series approximation **/
    float sumFlt  = 0;
    float sumFlt2 = 0;

    /* Get the actual minimum number of loops */
    logMinLoop = logLoops <= logMinLoop
                 ? logLoops
                 : logMinLoop;

    /* We should run at least one */
    logMinLoop = logMaxLoop <= logMinLoop
                 ? logMinLoop - 1
                 : logMinLoop;

    /* profile loop many times */
    for (i = logMinLoop; i < logMaxLoop; i++) {
        loopsNow = powf(10.f, (float) i);

        /** set upper limit of loop **/
        nextJ = (float) i + 1.f >= logMaxLoop
                ? logMaxLoop
                : (float) i + 1.f;
        nextJ = powf(10, nextJ);

        /** Get the upper limit **/
        j = loopsNow;

        /** get time before loop (time.h) **/
        *start_ptr = clock();

        /** run loop with the process 2 times
         **/
        do {
            sumFlt2 = sumFlt;
            sumFlt  = sumFlt2 + 1.0f / (float) j;
            j += 1;
            sumFlt2 = sumFlt;
            sumFlt  = sumFlt2 + 1.0f / (float) j;
            j += 1;
        }
        /*** Calculate absolute value of
         * difference ***/
        while (fabs(sumFlt - sumFlt2)
               > FLT_EPSILON
               && j < nextJ);

        /** get time after loop (time.h) **/
        *end_ptr = clock();

        /** get difference **/
        *cpuTimeUsed_ptr
        = ((double) (*end_ptr - *start_ptr))
          / CPS;

        /** do the same, with only one process
         **/
        sumFlt     = 0.0f;
        *start_ptr = clock();
        do {
            sumFlt2 = sumFlt;
            sumFlt  = sumFlt2 + 1.0f / (float) j;
            j += 1;
        } while (fabs(sumFlt - sumFlt2)
                 > FLT_EPSILON
                 && j < nextJ);
        *end_ptr = clock();

        /** The difference takes out the
         *  overhead of the loop **/
        *cpuTime_ptr
        = *cpuTimeUsed_ptr
          - ((double) (*end_ptr - *start_ptr))
            / CPS;

        /* printf("%le\n", *cpuTimeUsed_ptr); */
        printf("%0.0e\t%0.0e\t%0.4le\n", loopsNow,
               nextJ,
               *cpuTime_ptr / (nextJ - loopsNow));

        /* printf("The estimated time to calculate
         * ") */
    }
    /* free memory */
    return 0;
}

int dblHarmonic(int *    loops_ptr,
                clock_t *start_ptr,
                clock_t *end_ptr,
                double * cpuTimeUsed_ptr,
                double * singleLoop_ptr,
                double * cpuTime_ptr)
{
    /**
     *  \brief calculates the time it takes to
     *  calculate the harmonic series up to k =
     *  loops_ptr with double precision numbers
     *
     *  The harmonic series is
     * \sum_{k=1}^{\infty}{1/k}.
     *  This function has a gross way to calculate
     *  it and give an estimate of how long it
     *  would take to calculate it up to a high
     *  k. It does two loops: the first one with
     *  the function two times and the second one
     *  with only one call. The difference is the
     *  actual computing time of the series
     *  (without loop overhead).
     *
     *  \param
     *  loops_ptr (int*) -- number of
     *  loops that you want to spend calculating
     *  (n)
     *
     *  start_ptr (clock_t* <time.h>) -- clock
     *  value when a loop to be benchmarked starts
     *
     *  end_ptr (clock_t* <time.h>) -- clock
     *  value when a loop to be benchmarked ends
     *
     *  cpuTimeUsed_ptr (double*) --
     *
     *  \return return type
     */
    /* Loop variables */
    int         i;
    int         j;
    long double nextJ;
    /** k = 10^logMinLoop is the minimum number of
       cycles to profile **/
    short int logMinLoop = 1;
    /** 10-base log of the sought loops **/
    long double logLoops = log10l(*loops_ptr);
    /** Maximum number of loops for profiling **/
    long double logMaxLoop
    = ceill(log10l((long double) *loops_ptr));
    /** Logarithmic scale of time for various k=n
     * **/
    long double loopsNow;

    /* Working variables */
    /** double precision series approximation **/
    long double sumDbl  = 0;
    long double sumDbl2 = 0;

    /* Get the actual minimum number of loops */
    logMinLoop = logLoops <= logMinLoop
                 ? logLoops
                 : logMinLoop;

    /* We should run at least one */
    logMinLoop = logMaxLoop <= logMinLoop
                 ? logMinLoop - 1
                 : logMinLoop;

    /* profile loop many times */
    for (i = logMinLoop; i < logMaxLoop; i++) {
        loopsNow = powl(10.L, (long double) i);

        /** set upper limit of loop **/
        nextJ
        = (long double) i + 1.L >= logMaxLoop
          ? logMaxLoop
          : (long double) i + 1.L;
        nextJ = powl(10, nextJ);

        /** Get the upper limit **/
        j = loopsNow;

        /** get time before loop (time.h) **/
        *start_ptr = clock();

        /** run loop with the process 2 times
         **/
        do {
            sumDbl2 = sumDbl;
            sumDbl
            = sumDbl2 + 1.0L / (long double) j;
            j += 1;
            sumDbl2 = sumDbl;
            sumDbl
            = sumDbl2 + 1.0L / (long double) j;
            j += 1;
        }
        /*** Calculate absolute value of
         * difference ***/
        while (fabs(sumDbl - sumDbl2)
               > LDBL_EPSILON
               && j < nextJ);

        /** get time after loop (time.h) **/
        *end_ptr = clock();

        /** get difference **/
        *cpuTimeUsed_ptr
        = ((double) (*end_ptr - *start_ptr))
          / CPS;

        /** do the same, with only one process
         **/
        sumDbl     = 0.0L;
        *start_ptr = clock();
        do {
            sumDbl2 = sumDbl;
            sumDbl
            = sumDbl2 + 1.0L / (long double) j;
            j += 1;
        } while (fabs(sumDbl - sumDbl2)
                 > LDBL_EPSILON
                 && j < nextJ);
        *end_ptr = clock();

        /** The difference takes out the overhead
            of the loop **/
        *cpuTime_ptr
        = *cpuTimeUsed_ptr
          - ((double) (*end_ptr - *start_ptr))
            / CPS;

        printf("%0.0Le\t%0.0Le\t%0.4le\n",
               loopsNow, nextJ,
               *cpuTime_ptr
               / (double) (nextJ - loopsNow));
    }
    /* free memory */
    return 0;
}

int testFlt()
{
    /**
     *  \brief DO NOT USE!
     *   Shows that the for loop doesn't work.
     *
     *  This function does not do what it is
     *  supposed to do. It is left here for
     * reference
     *
     */
    float   sumFlt = 0.0f;
    double  cpuTime;
    clock_t end;
    clock_t start = clock();
    for (int j = 1; j < 100; j++) {
        sumFlt += 1.0f / (float) j;
    }
    end     = clock();
    cpuTime = ((double) (end - start)) / CPS;
    printf("%le\n%e\n", cpuTime, sumFlt);

    return 0;
}

int testDbl()
{
    /**
     *  \brief DO NOT USE!
     *   Shows that the for loop doesn't work.
     *
     *  This function does not do what it is
     *  supposed to do. It is left here for
     * reference
     *
     */
    long double sumDbl = 0.0L;
    double      cpuTime;
    clock_t     end;
    clock_t     start = clock();
    for (int j = 1; j < 100; j++) {
        sumDbl
        += (long double) 1.0L / (long double) j;
    }
    end     = clock();
    cpuTime = ((double) (end - start)) / CPS;
    printf("%le\n%Le\n", cpuTime, sumDbl);

    return 0;
}

int testFltSum(long double *brkLim)
{
    /**
     *  \brief Brute force calculation of harmonic
     *  series with single precision numbers
     *
     *  Read the documentation of harmonicSeries.
     *
     *  \param none
     *  \return 0 if all goes well
     */
    float   sumFlt  = 0.0f;
    float   sumFlt2 = 0.0f;
    int     k       = 1;
    double  cpuTime;
    clock_t end;
    /* float ja = FLT_EPSILON; */
    clock_t start = clock();
    printf("Using single precision epsilon\n");
    do {
        sumFlt2 = sumFlt;
        sumFlt  = sumFlt2 + 1.0f / (float) k;
        k += 1;
    } while (fabs(sumFlt - sumFlt2) > FLT_EPSILON
             && k < (float) *brkLim); // added
    // conditional
    // to be
    // consistent
    // with prediction
    end     = clock();
    cpuTime = ((double) (end - start)) / CPS;
    printf(
    "Time: %le\nSum: %e\nHarmonic number: %'d",
    cpuTime, sumFlt, k);

    return 0;
}

int testDblSum(long double *brkLim)
{
    /**
     *  \brief Brute force calculation of harmonic
     *  series with double precision numbers
     *
     *  Read the documentation of harmonicSeries.
     *
     *  \param none
     *  \return 0 if all goes well
     */
    long double sumDbl  = 0.0L;
    long double sumDbl2 = 0.0L;
    long        k       = 1;
    double      cpuTime;
    clock_t     end;
    /* long double ja = LDBL_EPSILON; */
    clock_t start = clock();
    printf("Using double precision epsilon\n");
    do {
        sumDbl2 = sumDbl;
        sumDbl = sumDbl2 + 1.0L / (long double) k;
        k += 1;
    } while (fabsl(sumDbl - sumDbl2)
             > LDBL_EPSILON
             && k < *brkLim); // added break
                              // conditional to
                              // be consistent
                              // with prediction

    end     = clock();
    cpuTime = ((double) (end - start)) / CPS;
    printf("Time: %le\nSum: %Le\nHarmonic "
           "number: %'ld\n",
           cpuTime, sumDbl2, k);

    return 0;
}
