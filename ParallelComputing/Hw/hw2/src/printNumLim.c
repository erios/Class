#include "printNumLim.h"

void printNumLim()
{
    /* Largest numbers */
    printf(
    "The largest positive (signed) integer is:"
    " %lld\n",
    LLONG_MAX);
    // 92,23,372,036,854,775,807
    printf("The largest positive (signed) 32-bit "
           "integer is:"
           " %i\n",
           INT_MAX);
    // 2,147,483,647
    printf("The largest unsigned integer is:"
           " %llu\n",
           ULLONG_MAX);
    // 18,446,744,073,709,551,615

    /* Smallest numbers */
    printf(
    "The most negative (signed) integer is:"
    " %lld\n",
    LLONG_MIN);
    printf(
    "The most negative (signed) 32-bit number is:"
    " %i\n",
    INT_MIN);
    /* Smallest epsilon */
    printf(
    "Epsilon for single precision: %0.17e\n",
    FLT_EPSILON);
    printf(
    "Epsilon for double precision: %0.45Le\n",
    LDBL_EPSILON);

    printf(
    "\nThe maximum number n that one can add in "
    "the "
    "series is given by 1/(n+epsilon)=epsilon. ");
    printf(
    "In single precision, this number is (in this"
    " computer) %.0lf.\n",
    1 / FLT_EPSILON - FLT_EPSILON);
    printf("In double precision, it is %0.45Le\n",
           1 / LDBL_EPSILON - LDBL_EPSILON);
    printf(
    "These are far *behind* from the upper "
    "limits "
    "to represent single and double precision "
    "floating point numbers: %0.24e and %0.64Le, "
    "respectively\n",
    FLT_MAX, LDBL_MAX);
}
