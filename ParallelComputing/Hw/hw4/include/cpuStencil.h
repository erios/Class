#ifndef CPUSTENCIL_H
#define CPUSTENCIL_H

#include "headers.h"

void stencil_id_cpu(double *a,
                    double *b,
                    int *N);

float cpuStencil(int N);

#endif /*  CPUSTENCIL_H */
