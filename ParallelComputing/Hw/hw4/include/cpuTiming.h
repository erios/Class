#ifndef CPUTIMING_H
#define CPUTIMING_H

#include "headers.h"

void cpuAdd (int N,
             double *a,
             double *b,
             double *c);

float cpuTiming(int N);

#endif /*  CPUTIMING_H */
