#ifndef GPUSTENCIL_H
#define GPUSTENCIL_H

#include "headers.h"

__global__ void stencil_id_gpu(double *a,
                     double *b);

float gpuStencil(int N);

#endif /*  GPUSTENCIL_H */
