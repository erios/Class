#ifndef GPUTIMING_H
#define GPUTIMING_H

#include "headers.h"

float gpuTiming(int n);

__global__ void gpuAdd(int n, float *a,
                       float *b, float *c);

#endif /*  GPUTIMING_H */
