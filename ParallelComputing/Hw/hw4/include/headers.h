/**
 *   \file headers.h
 *   \brief This file is loaded by almost all
 *   the source files (.c).
 *
 *  Detailed description. It loads the user
 *   interaction, strings, memory allocation, data
 *   type limits and timing libraries from GNU C.
 *
 */
/* Define everything only once */
#ifndef HEADERS_H
#define HEADERS_H
/* Length of an array */
#define ARRLEN(a) (sizeof(a)/sizeof(*a))
/* Interaction with the user */
#include <stdio.h>
/* Strings, memory allocation */
#include <stdlib.h>
/* Mathematical functions */
#include <math.h>
/* EXIT_SUCCESS and pals */
#include <sys/resource.h>

#endif
