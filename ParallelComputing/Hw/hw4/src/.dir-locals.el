;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((c-mode
  (flycheck-gcc-language-standard . "gnu99")
  (ps-landscape-mode . t)
  (ps-paper-type . "letter")
  (ps-number-of-columns . 2)
  (comment-column . 50)
  (comment-fill-column . 50)
  (fill-column . 50)
  (auto-fill-mode . t)
  ;; (flycheck-clang-language-standard . "gnu99")
  ;; (compile-command . "if [ ! -d ../build ]; then mkdir ../build; fi; cd ../build && cmake .. && make")
  ))
