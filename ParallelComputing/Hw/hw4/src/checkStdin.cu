#include "checkStdin.h"

int checkStdin(int *argc, char *argv[])
{
    /* Check command line arguments */
    if (*argc != 2) {
        fprintf(
        stderr,
        "This program requires 1"
        " argument:\n %s"
        " <type of benchmark (string): can be 1 "
        "(addition) or 1 (stencil)>\n",
        argv[0]);
        exit(-1);
        /* perror(); */
    }

    return 0;
}
