/**
 *   \file cpuStencil.cu
 *   \brief Computes a stencil addition using
 *   CUDA without parallelising computation.
 *   Prints the value of the 100th element
 *   and returns the elapsed time to calculate
 *   it.
 *
 *  A stencil is a nested loop to do iterative
 *  calculations from parts within an array. If
 *  you have an array a[] = {1, 2, 3, 4, 5, 6, 7},
 *  you may want to get the sum (or any other
 *  operation) between {k, k+1, k+2} for each
 *  element k from 0 to n, such that the result
 *  would be b = {6, 9, 12, 18}.
 *
 *  This function uses serial computation by
 *  means of CUDA to do a similar calculation. The
 *  original array has a length specified by an
 *  input parameter and the stencil has a radius
 *  (distance from the current position to the
 *  "sides" of the original array) of 3.
 *
 *  Inputs:
 *  N  --  (integer) length of the stencil
 *
 *  Returns:
 *  elapsedTime  -- (float) time to compute the
 *  stencil
 *
 *  Prints:
 *  The value of the resulting array at index 100
 *
 */

#include "cpuStencil.h"

#define RADIUS 3

void stencil_id_cpu(double *a,
                    double *b,
                    int *N){
  for (int i = RADIUS; i < *N - RADIUS; i++){
    float stencil_sum = 0.0f;
    for (int j = -RADIUS; j <= RADIUS; j++){
      stencil_sum += a[i+j];
    }
    b[i] = stencil_sum;
  }
}

float cpuStencil(int N)
{
  /* Arrays to do the addition */
  double *a, *b;
  /* Timing CUDA events */
  cudaEvent_t timeStart, timeStop;
  // make sure it is of type float, precision is
  // milliseconds (ms) !!!
  float elapsedTime;
  /* Declare the arrays with CUDA allocated
     memory */
  cudaMallocManaged( &a, N * sizeof(*a));
  cudaMallocManaged( &b, N * sizeof(*b));

  for (int i=0; i < N; i++){
    a[i] = 1;
    b[i] = 0;
  }

  cudaDeviceSynchronize();

  // WARNING!!! use events only to time the
  // device
  cudaEventCreate(&timeStart);
  cudaEventCreate(&timeStop);

  // don't worry for the 2nd argument zero, it
  // is
  // about cuda streams
  cudaEventRecord(timeStart, 0);
  stencil_id_cpu(a, b, &N);

  cudaEventRecord(timeStop, 0);
  cudaEventSynchronize(timeStop);

  // WARNING!!! do not simply print
  //(timeStop-timeStart)!!
  /* Retrieve the elapsed time and save into
     elapsedTime */
  cudaEventElapsedTime(&elapsedTime, timeStart,
                       timeStop);

  /* free the timer */
  cudaEventDestroy(timeStart);
  cudaEventDestroy(timeStop);

  printf("%0.4e", b[100]);
  /* free allocated arrays */
  cudaFree(a);
  cudaFree(b);

  return elapsedTime;
}
