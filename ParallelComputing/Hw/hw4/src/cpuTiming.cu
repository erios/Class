/**
 *   \file cpuTiming.cu
 *   \brief Benchmark of a loop addition using
 *   serial CUDA.
 *
 *   There are two predefined arrays which need to
 *   be added. This program calculates the time
 *   that it takes a serial CUDA to compute their
 *   addition element by element. It prints the
 *   100th element. The length of the array is
 *   given as an input.
 *
 *  Inputs:
 *  N  --  (integer) length of the arrays
 *
 *  Returns:
 *  elapsedTime  -- (float) time to compute the
 *  stencil
 *
 *  Prints:
 *  The value of the resulting array at index 100
 *
 */

#include "cpuTiming.h"

void cpuAdd(int N,
            double *a,
            double *b,
            double *c){
  for (int i=0; i < N; i++){
    c[i] = a[i] + b[i];
  }
}

float cpuTiming(int N)
{
  /* Arrays to do the addition */
  double *a, *b, *c;
  /* Timing CUDA events */
  cudaEvent_t timeStart, timeStop;
  // make sure it is of type float, precision is
  // milliseconds (ms) !!!
  float elapsedTime;

  /* Declare the arrays with CUDA allocated
     memory */
  cudaMallocManaged( &a, N * sizeof(*a));
  cudaMallocManaged( &b, N * sizeof(*b));
  cudaMallocManaged( &c, N * sizeof(*c));

  for (int i=0; i < N; i++){
    a[i] = 3.5;
    b[i] = 1.5;
  }

  // WARNING!!! use events only to time the
  // device
  cudaEventCreate(&timeStart);
  cudaEventCreate(&timeStop);

  // don't worry for the 2nd argument zero, it
  // is
  // about cuda streams
  cudaEventRecord(timeStart, 0);
  cpuAdd(N, a, b, c);

  cudaEventRecord(timeStop, 0);
  cudaEventSynchronize(timeStop);

  // WARNING!!! do not simply print
  //(timeStop-timeStart)!!
  /* Retrieve the elapsed time and save into
     elapsedTime */
  cudaEventElapsedTime(&elapsedTime, timeStart,
                       timeStop);

  /* free the timer */
  cudaEventDestroy(timeStart);
  cudaEventDestroy(timeStop);

  printf("%0.4e", c[100]);
  /* free allocated arrays */
  cudaFree(a);
  cudaFree(b);
  cudaFree(c);

  return elapsedTime;
}
