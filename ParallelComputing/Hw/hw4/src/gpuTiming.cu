/*
 * Simple CPU program to add two long vectors
 *
 * Author: Inanc Senocak
 * Contributor: Edgar Rios
 */

#include "gpuTiming.h"

__global__ void gpuAdd(int n, float *a,
                       float *b, float *c)
{
  int tid
    = blockIdx.x * blockDim.x + threadIdx.x;

  if (tid < n) c[tid] = a[tid] + b[tid];
}

float gpuTiming(int n)
{
  /* Parallel definition */
  int blockSize = 256;
  // round up if n is not a multiple of
  // blocksize
  int nBlocks = (n + blockSize - 1) / blockSize;
  /* Arrays to do the addition */
  float *x, *y, *z;
  /* Timing CUDA events */
  cudaEvent_t timeStart, timeStop;
  // make sure it is of type float, precision is
  // milliseconds (ms) !!!
  float elapsedTime;
  /* Declare the arrays with CUDA allocated
     memory */
  cudaMallocManaged(&x, n * sizeof(float));
  cudaMallocManaged(&y, n * sizeof(float));
  cudaMallocManaged(&z, n * sizeof(float));

  for (int i = 0; i < n; i++) {
    x[i] = 3.5;
    y[i] = 1.5;
  }

  // WARNING!!! use events only to time the
  // device
  cudaEventCreate(&timeStart);
  cudaEventCreate(&timeStop);

  // don't worry for the 2nd argument zero, it
  // is
  // about cuda streams
  cudaEventRecord(timeStart, 0);
  gpuAdd<<<nBlocks, blockSize>>>(n, x, y, z);
  /* Make sure that all blocks are finished */
  cudaDeviceSynchronize();

  cudaEventRecord(timeStop, 0);
  cudaEventSynchronize(timeStop);

  // WARNING!!! do not simply print
  //(timeStop-timeStart)!!
  /* Retrieve the elapsed time and save into
     elapsedTime */
  cudaEventElapsedTime(&elapsedTime, timeStart,
                       timeStop);

  /* free the timer */
  cudaEventDestroy(timeStart);
  cudaEventDestroy(timeStop);

  printf("%0.4e", z[100]);
  /* free allocated arrays */
  cudaFree(x);
  cudaFree(y);
  cudaFree(z);

  return elapsedTime;
}
