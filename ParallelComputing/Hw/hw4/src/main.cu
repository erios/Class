/* General utilities */
#include "headers.h"
/* Check input arguments from StdIn */
#include "checkStdin.h"
/* Timing cpu and gpu addition */
#include "cpuTiming.h"
#include "gpuTiming.h"
/* Timing cpu and gpu stencil */
#include "cpuStencil.h"
#include "gpuStencil.h"

int main(int argc, char *argv[]) {
  /* Check that arguments are correct */
  checkStdin(&argc, argv);

  /* Length of the array */
  int n[] = {1e6, 1e7, 5e7};

  if (atoi(argv[1]) == 1){
    /* Print headers */
    printf("# CPU and GPU addition\n");
    printf("cycles\t"           /* column 1 */
           "\tCPU value"        /* column 2 */
           "\tCPU (ms)"         /* column 3 */
           "\tGPU value"        /* column 4 */
           "\tGPU (ms)"         /* column 5 */
           "\n");
    for (int i = 0; i < ARRLEN(n); i++) {
      /* Number of cycles */
      printf("%0.0e",           /* column 1 */
             (float)n[i]);
      /* CPU */
      /* Print elapsed time in ms and value of res[100] */
      /* The function prints a value, and returns a float */
      printf("\t");             /* prepare column 2 */
      printf("\t%0.4e",         /* column 3 */
             cpuTiming(n[i]));
      /* GPU */
      /* Print elapsed time in ms and value of res[100] */
      /* The function prints a value, and returns a float */
      printf("\t");             /* prepare column 4 */
      printf("\t%0.4e",         /* column 5 */
             gpuTiming(n[i]));
      printf("\n");
    }
  }
  else if(atoi(argv[1]) == 2){
    printf("# CPU and GPU stencil\n");
    printf("cycles\t"           /* column 1 */
           "\tCPU value"        /* column 2 */
           "\tCPU (ms)"         /* column 3 */
           "\tGPU value"        /* column 4 */
           "\tGPU (ms)"         /* column 5 */
           "\n");
    for (int i = 0; i < ARRLEN(n); i++) {
      /* Number of cycles */
      printf("%0.0e",           /* column 1 */
             (float)n[i]);
      /* CPU */
      /* Print elapsed time in ms and value of res[100] */
      /* The function prints a value, and returns a float */
      printf("\t");             /* prepare column 2 */
      printf("\t%0.4e",         /* column 3 */
             cpuStencil(n[i]));
      /* GPU */
      /* Print elapsed time in ms and value of res[100] */
      /* The function prints a value, and returns a float */
      printf("\t");             /* prepare column 4 */
      printf("\t%0.4e",         /* column 5 */
             gpuStencil(n[i]));
      printf("\n");
    }
  }
  return EXIT_SUCCESS;
}
