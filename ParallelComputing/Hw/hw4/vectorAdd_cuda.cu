/*
 * Simple CPU program to add two long vectors
 *
 * Author: Inanc Senocak
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/resource.h>

__global__ void vector_add (int n, float *a, float *b, float *c) 
{
      int tid = blockIdx.x*blockDim.x + threadIdx.x;

      if (tid <  n)
         c[tid] = a[tid] + b[tid];
}

int main(void)
{

    int n = 100000000;

    float *x, *y, *z;

    cudaMallocManaged(  &x, n * sizeof (float));
    cudaMallocManaged(  &y, n * sizeof (float));
    cudaMallocManaged(  &z, n * sizeof (float));

    for (int i = 0; i < n; i++){
        x[i] = 3.5;
        y[i] = 1.5;
    }

     cudaEvent_t timeStart, timeStop; //WARNING!!! use events only to time the device
     cudaEventCreate(&timeStart);
     cudaEventCreate(&timeStop);
     float elapsedTime; // make sure it is of type float, precision is milliseconds (ms) !!!


    int blockSize = 256;
    int nBlocks   = (n + blockSize -1) / blockSize; //round up if n is not a multiple of blocksize
    cudaEventRecord(timeStart, 0); //don't worry for the 2nd argument zero, it is about cuda streams
    vector_add <<< nBlocks, blockSize >>> (n, x, y, z);
    cudaDeviceSynchronize();

    printf("z[100] = %4.2f\n",z[0]);

    cudaEventRecord(timeStop, 0);
    cudaEventSynchronize(timeStop);

    //WARNING!!! do not simply print (timeStop-timeStart)!!

    cudaEventElapsedTime(&elapsedTime, timeStart, timeStop);

    printf("elapsed wall time (device) = %3.1f ms\n", elapsedTime);

    cudaEventDestroy(timeStart);
    cudaEventDestroy(timeStop);

    cudaFree(x);
    cudaFree(y);
    cudaFree(z);

    return EXIT_SUCCESS;
}
