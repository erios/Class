#+LANGUAGE: en
# Disable table of contents
#+OPTIONS: toc:nil
# Include date
#+OPTIONS: date:t
# Include author
#+OPTIONS: author:t
# Signal a clear problem in the output if there is a broken link
#+OPTIONS: broken-links:mark
# Toggle inclusion of the creation time into exported file
#+OPTIONS: timestamp:nil
# Smart quotes
#+OPTIONS: ':t
# Export with empashized text
#+OPTIONS: *:t
# Do not include time stamps
#+OPTIONS: <:nil
# Do not preserve line breaks when exporting
#+OPTIONS: \n:nil
# Enable tables
#+OPTIONS: |:t
# Sub- and superscripts only in between {}
#+OPTIONS: ^:{}
# Allow export of math snippets (LaTeX)
#+OPTIONS: tex:t
# Enable \alpha and friends (complete with M-tab)
#+OPTIONS: e:t
# Footnotes
#+OPTIONS: f:t
# After which level should headlines become lists or something else
#+OPTIONS: H:3
# Should headlines have a number? or at what level of headline?
#+OPTIONS: num:t
# Do not include properties
#+OPTIONS: prop:nil
# Do not export tags
#+OPTIONS: tags:nil
# Do not include CLOCK keywords
#+OPTIONS: c:nil
# Do not include the creator (program)
#+OPTIONS: creator:nil
# Do not include :DRAWER: .. :END: blocks
#+OPTIONS: d:nil
# # Set the scientific notation
# #+OPTIONS: :latex-table-scientific-notation
# Show all headlines at start
#+STARTUP: content
# virtual indents according to the outline level
#+STARTUP: indent
# Hide all but one stars at the beginning
#+STARTUP: hidestars
# Align all tables
#+STARTUP: align
# Don't show inline images on startup
#+STARTUP: noinlineimages
# Define footnotes near first reference, but not inline
#+STARTUP: fnlocal
# Display entities as UTF-8 characters (\alpha)
#+STARTUP: entitiespretty

#+LaTeX_HEADER: \input{preamble.tex}

#+LATEX_HEADER: % ***** Colors *****
\definecolor{foreground}{RGB}{220,220,204}
\definecolor{background}{RGB}{62,62,62}
\definecolor{preprocess}{RGB}{250,187,249}
\definecolor{var}{RGB}{239,224,174}
\definecolor{string}{RGB}{154,150,230}
\definecolor{type}{RGB}{225,225,116}
\definecolor{function}{RGB}{140,206,211}
\definecolor{keyword}{RGB}{239,224,174}
\definecolor{comment}{RGB}{180,98,4}
\definecolor{doc}{RGB}{175,215,175}
\definecolor{comdil}{RGB}{111,128,111}
\definecolor{constant}{RGB}{220,162,170}
\definecolor{buildin}{RGB}{127,159,127}

# Bibliography
# https://tex.stackexchange.com/questions/197707/using-bibtex-from-org-mode-bbl-and-aux-files-are-incorrectly-generated
# #+LaTeX_HEADER: \usepackage[backend=bibtex,sorting=none]{biblatex}
# #+LaTeX_HEADER: \addbibresource{biblio.bib}  %% point at your bib file
# #+LaTeX_HEADER: \newcommand{\point}[1]{\noindent \textbf{#1}}
# #+LaTeX_HEADER: \usepackage{csquotes}
# #+LaTeX_HEADER: \usepackage[mla]{ellipsis}
# #+LaTeX_HEADER: \parindent = 0em
# #+LaTeX_HEADER: \setlength\parskip{.5\baselineskip}

#+LATEX_HEADER: \usepackage[style=verbose,backend=bibtex]{biblatex}
#+LATEX_HEADER: \addbibresource{biblio.bib}
