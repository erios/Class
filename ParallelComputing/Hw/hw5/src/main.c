#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

float integrand(const float x){
  return 5 + 3 * sin(x);
}

float exactint(const float x){
  return 5 * x - 3 * cos(x);
}

float trapezoidalInt(const float lowlim,
                     const float uplim,
                     const float div){
  float h, xi, res;

  h = (uplim - lowlim)/div;
  res = (integrand(lowlim) + integrand(uplim)) / 2.0;
  for (int i=1; i < div; i ++){
    xi = lowlim + i * h;
    res += integrand(xi);
  }
  res *= h;
  return res;
}

float simpsonInt(const float lowlim,
                 const float uplim,
                 const float div){
  /* https://wbutassignmentshelp.wordpress.com/
     2012/08/08/
     code-for-simpsons-rule-in-c-programming/ */
  float s1, s2, sum, h;
  sum = 0;
  s1 = 0;
  s2 = 0;
  h = (uplim - lowlim)/div;
  for(int i=1; i<div; i++) {
    if(i%2==0) {
      s1 += integrand(lowlim + i*h);
    }
    else {
      s2 += integrand(lowlim + i*h);
    }
  }
  sum = h/3*(integrand(lowlim)
             + integrand(uplim)
             + 4 * s2 + 2 * s1);
  return sum;
}

int
main(int argc, char *argv[])
{
  /* Inputs */
  int lowlim = atoi(argv[1]);
  int uplim = atoi(argv[2]);
  /* Number of intervals (total) */
  unsigned int nIntervals = atoi(argv[3]);
  /* time var */
  double start, finish, wallTime;
  double elapsedTime;
  /* Create an address to communicate the
     result */
  // BEWARE!!! you cannot use pointers
  /* float *res; */
  double res,
    my_lowlim,
    my_uplim,
    my_integral;
  /* number of processes */
  int nProcs;
  /* process rank */
  int myRank;

  /* initialize MPI. if main(void) */
  //MPI_Init(NULL, NULL);
  /* initialize MPI */
  MPI_Init(&argc, &argv);
  /* get the number of processes */
  MPI_Comm_size(MPI_COMM_WORLD, &nProcs);
  /* get the rank of a process */
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

  /* put a barrier to get the start time */
  MPI_Barrier(MPI_COMM_WORLD);


  if (myRank == 0) {
    scanf("%u", &nIntervals);
  }

  /* I'm sending an address, one address, what
     type, who sends, who receives */
  MPI_Bcast(&nIntervals, 1, MPI_UNSIGNED, 0,
            MPI_COMM_WORLD);
  MPI_Bcast(&lowlim, 1, MPI_UNSIGNED, 0,
            MPI_COMM_WORLD);
  MPI_Bcast(&uplim, 1, MPI_UNSIGNED, 0,
            MPI_COMM_WORLD);

  start = MPI_Wtime();

  my_lowlim = lowlim
    + ((uplim - lowlim) / (double) nProcs) * myRank;
  my_uplim = my_lowlim
    + (uplim - lowlim) / (double) nProcs;

  nIntervals = nIntervals / nProcs;

  /* myRank and nProcs take care of everything! */
  my_integral = trapezoidalInt(my_lowlim,
                               my_uplim,
                               nIntervals);

  /* You are going to do the same with everyone,
     you don't need Send, Recv; 0: */
  MPI_Reduce(&my_integral, &res, 1, MPI_DOUBLE,
             MPI_SUM, 0, MPI_COMM_WORLD);

  if (myRank == 0){
    /* Header */
    printf("Intervals\t"
           "Trapezoidal\tSimpson\tExact\n");
    /* First column (intervals) */
    printf("%d", nIntervals);
  }
  /* Sync before getting time */
  MPI_Barrier(MPI_COMM_WORLD);

  finish = MPI_Wtime();

  elapsedTime = finish - start;
  wallTime = 0;
  /* Calculate the time for everyone */
  MPI_Reduce(&elapsedTime, &wallTime, 1,
             MPI_DOUBLE, MPI_MAX, 0,
             MPI_COMM_WORLD);

  if (myRank == 0){
    /* Second column (trapezoidal integral) */
    printf("\t%10.4e",
           trapezoidalInt(lowlim, uplim,
                          nIntervals));

    /* printf("Time %f\n", wallTime); */
  }

  /* Simpson's rule */
  /* Make sure that the Simpson's rule is
     applicable */
  if(nIntervals%2 != 0 && myRank == 0) {
    nIntervals -=1;
    #ifdef DEBUG
    printf("[WARN:] Modified the number of"
           " intervals to %d\n", nIntervals);
    #endif // DEBUG
  }

  /* myRank and nProcs take care of everything! */
  my_integral = simpsonInt(my_lowlim,
                               my_uplim,
                               nIntervals);

  /* You are going to do the same with everyone,
     you don't need Send, Recv; 0: */
  MPI_Reduce(&my_integral, &res, 1, MPI_DOUBLE,
             MPI_SUM, 0, MPI_COMM_WORLD);
  /* Sync before getting time */
  MPI_Barrier(MPI_COMM_WORLD);

  finish = MPI_Wtime();

  elapsedTime = finish - start;
  wallTime = 0;
  /* Calculate the time for everyone */
  MPI_Reduce(&elapsedTime, &wallTime, 1,
             MPI_DOUBLE, MPI_MAX, 0,
             MPI_COMM_WORLD);

  if (myRank == 0){
    /* Fourth column (Simpson integral) */
    printf("\t%10.4e",
           simpsonInt(lowlim, uplim,
                      nIntervals));

    /* printf("Time %f\n", wallTime); */
    /* Fourth column (exact solution) */
    printf("\t%10.4e",
           exactint(uplim) - exactint(lowlim));

    printf("\n");
  }

  return 0;
}
