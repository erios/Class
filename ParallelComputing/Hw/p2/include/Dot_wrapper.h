#ifndef DOT_WRAPPER_H
#define DOT_WRAPPER_H

#include "headers.h"
#include <cublas.h>
#include "gpuNaiveA_dot_B.h"

float Dot_wrapper(double x_d[], double y_d[],
                 double *dot_d, int n, double *dot,
                 dim3 blocks, dim3 threads);


#endif /* DOT_WRAPPER_H */
