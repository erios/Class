#ifndef FREE_MEM_H
#define FREE_MEM_H

#include "headers.h"
#include <cublas.h>


void Free_mem(double* x_h, double* y_h,
              double* x_d, double* y_d,
              double* dot_d,
              double* dot_h);

#endif /* FREE_MEM_H */
