#ifndef GET_ARGS_H
#define GET_ARGS_H

#include "headers.h"

/*------------------------------------
 * Function:  Get_args
 * Purpose:   Get and check command line args.  If
 *            there's an error quit.
 */
void Get_args(int argc, char* argv[], int* n_p);


#endif /* GET_ARGS_H */
