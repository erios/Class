#ifndef SETUP_H
#define SETUP_H

#include "headers.h"
#include <cublas.h>

/*--------------------------------------------
 * Function:  Setup
 * Purpose:   Allocate and initialize host and
 *            device memory
 */
void Setup(int n, double** x_h_p, double** y_h_p,
           double** x_d_p, double** y_d_p,
           double** dot_d_p, double** dot_h_p);

#endif /* SETUP_H */
