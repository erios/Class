#ifndef CHECKSTDIN_H
#define CHECKSTDIN_H

#include "headers.h"

/* Check command line arguments */
int checkStdin(int *argc, char *argv[]);

/* Make sure that all arguments are bigger
       than zero */
int checkArgs(int *n, int *m, int *k);

#endif  /* CHECKSTDIN_H */
