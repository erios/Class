#ifndef CPUBLASA_DOT_B_H
#define CPUBLASA_DOT_B_H

#include "headers.h"

/* gsl_blas unvailable on BSU RedHawk */
/* void */
/* cpuGslBlasA_dot_B (double *a, */
/*                    double *b, */
/*                    double *c, */
/*                    int *m, int *n, int *k); */

/**
 *  \brief Calculates the dot (inner) product
 *  between two matrices
 *
 *
 *  Uses cblas_ddot to do
 *  c = a . b + c
 *
 *  where a and b are m×n and n×k matrices, and
 *  c is m×k
 *  (1D arrays; flatten matrices: a[m-1] is the
 *  element in the second row, first column)
 *
 *  The input parameter c (pointer) is
 *  modified. Make sure to clean c if necessary.
 *
 *  \param
 *  const double *a -- left matrix
 *  const double *b -- right matrix
 *  double *c       -- result
 *  const int *m    -- # rows (1st dimension) of a
 *                     and c
 *  const int *n    -- # columns of a and rows of b
 *  const int *k    -- # columns of b and c

 *  \return nothing
 */
void
cpuCBlasA_dot_B (const double *a,
                 const double *b,
                 double *c,
                 const int *m,
                 const int *n,
                 const int *k);

#endif /* CPUBLASA_DOT_B_H */
