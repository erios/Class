#ifndef CPUDAXPYA_DOT_B_H
#define CPUDAXPYA_DOT_B_H

#include "headers.h"

  /**
   *  \brief Calculates the dot (inner) product
   *  between two matrices
   *
   *
   *  Uses cblas_daxpy (linear combination) to do
   *  c → a . b + c    (→ means assign)
   *
   *  where a and b are m×n and n×k matrices, and
   *  c is m×k
   *  (1D arrays; flatten matrices: a[m-1] is the
   *  element in the second row, first column)
   *
   *  The input parameter c (pointer) is
   *  modified. Make sure to clean c if necessary.
   *
   *  The linear combination works like this: Let
   *  a have n columns, and b have n rows and k
   *  columns, so that element b[n,k] of b is the
   *  one located in the nth row and kth column of
   *  b. Denote a column i of a as col(a)[i], and
   *  the sum of x[j] from j=1 to j=k as
   *  \sum_{j=1}^{k}{x[j]}. Then, if c is the
   *  inner product of a and b,
   *  col(c)[i] → \sum_{j=1}^{n}{col(a)[j]*b[j,i]}
   *  . This means that the
   *  ith column of c is the
   *    sum of the products of
   *        the ith column of a
   *      times
   *        the jth element of the ith column of b
   *        (b[j,i]).
   *
   *  \param
   *  const double *a -- left matrix
   *  const double *b -- right matrix
   *  double *c       -- result
   *  const int *m    -- # rows (1st dimension) of a
   *                     and c
   *  const int *n    -- # columns of a and rows of b
   *  const int *k    -- # columns of b and c

   *  \return nothing
   */
void
cpuDaxpyA_dot_B(const double *a,
                const double *b,
                double *c,
                const int *m,
                const int *n,
                const int *k);

#endif /* CPUDAXPYA_DOT_B_H */
