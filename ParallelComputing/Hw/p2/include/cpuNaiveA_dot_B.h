#ifndef CPUNAIVE_DOT_B
#define CPUNAIVE_DOT_B

#include "headers.h"

void cpuNaiveA_dot_B(const double *a,
                     const double *b,
                     double *c,
                     const int *m,
                     const int *n,
                     const int *k);


#endif /* CPUNAIVE_DOT_B */
