#ifndef GPUDAXPYA_DOT_B_H
#define GPUDAXPYA_DOT_B_H

#include "headers.h"
/* https://stackoverflow.com/a/15679646 */
#include <cuda_runtime.h>
/* Matrix computations on the GPU CUBLAS and MAGMA
   by example */
#include <cublas_v2.h>
/* simpeCUBLAS.cpp */
/* /usr/local/cuda-7.5/samples/common/inc/helper_cuda.h */
#include "helper_cuda.h"

#include "genMatrix.h"
#include "printMatrix.h"

/**
 *  \brief Squared matrix multiplication
 *  using cuda daxpy
 *
 *  Uses cublasDaxpy to do
 *  c = a . b + c
 *
 *  where a, b and c are m×m matrices
 *  (1D arrays; flatten matrices: a[m-1] is the
 *  element in the second row, first column)
 *
 *  The input parameter c (pointer) is
 *  modified. Make sure to clean c if
 *  necessary. See also cpuDaxpyA_dot_B.c
 *
 *  \param
 *  const double *a -- left matrix (cublasSetVector)
 *  const double *b -- right matrix (double *)
 *  double *c       -- result (cublasSetVector)
 *  const int m     -- size of the squared
 *                     matrices
 *
 *  \return nothing
 */
float cudaSqMatDaxpy(cublasHandle_t handle,
                    const double a[], /* device */
                    const double b[], /* host */
                    double c[],       /* device */
                    const int m);

/**
 *  \brief Sets the variables to do daxpy
 *  multiplication with CUDA
 *
 *  It gets everything ready to do a squared
 *  matrix multiplication by means of CUDA. It
 *  also times the process.
 *
 *  \param
 *  m, n, k -- size of matrix (the same)
 *
 *  \output
 *  prints the time
 *
 *  \return (float) elapsed time in ms
 */
int gpuDaxpyA_dot_B(const int *m,
                    const int *n,
                    const int *k);

#endif /* GPUDAXPYA_DOT_B_H */
