#ifndef GPUNAIVEA_DOT_B_H
#define GPUNAIVEA_DOT_B_H

#include "headers.h"
#include <cublas.h>
#include "Get_args.h"
#include "Setup.h"
#include "Dot_wrapper.h"
#include "printMatrix.h"
#include "Free_mem.h"

/*------------------------------------------------
 * Function:    cudaSqMatDot  (kernel)
 * Purpose:     Implement a dot product of double
 *              precision squared matrices
 *
 * In args:     a, b, m
 * In/out arg:  c
 *
 * Note:        c should be initialized to 0
 *              when the calling function
 */
__global__ void cudaSqMatDot(const double a[],
                             const double b[],
                             double c[],
                             const int m);

/*------------------------------------------------
 * Function:    gpuNaiveA_dot_B
 * Purpose:     Calculates the time to do a dot
 *              product between squared matrices
 *              by means of the naive method. The
 *              matrices have random values.
 *
 * In args:     (from stdin) size of the matrix
 * In/out arg:  None
 * Returns:     Nothing
 * Output:      Time to compute the dot product
 *              (on the screen)
 */
void
gpuNaiveA_dot_B (int argc, char* argv[]);

#endif /* GPUNAIVEA_DOT_B_H */
