/**
 *   \file headers.h
 *   \brief This file is loaded by almost all
 *   the source files (.c).
 *
 *  Detailed description. It loads the user
 *   interaction, strings, memory allocation, data
 *   type limits and timing libraries from GNU C.
 *
 */
/* Define everything only once */
#ifndef HEADERS_H
#define HEADERS_H
/* Interaction with the user */
#include <stdio.h>
/* Strings, memory allocation */
#include <stdlib.h>
/* Mathematical functions (use -lm) */
#include <math.h>
/* EXIT_SUCCESS and pals */
#include <sys/resource.h>
/* Timing events */
#include <time.h>
/*--- Doesn't work: --- */
/* /\* cBLAS (use -lcblas)*\/ */
/* #include <cblas.h> */
/* -------------------- */
/* gsl BLAS */
#include <gsl/gsl_blas.h>
/* gsl cBLAS */
#include <gsl/gsl_cblas.h>
/* /\* cuBLAS *\/ */
/* #include "cublas.h" */
#include "timingcpu.h"

/* Clocks per second for the processor */
#define CPS CLOCKS_PER_SEC
/* Length of an array */
#define ARRLEN(a) (sizeof(a)/sizeof(*a))


#endif
