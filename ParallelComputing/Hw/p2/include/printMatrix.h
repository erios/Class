#ifndef PRINTMATRIX_H
#define PRINTMATRIX_H

#include "headers.h"

void printMatrix2Dd(double *a, int m, int n);

void printMatrix2Df(float *a, int m, int n);

void printMatrix2Di(int *a, int m, int n);

#endif /* PRINTMATRIX_H */
