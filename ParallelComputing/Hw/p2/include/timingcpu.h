// This code was heavily based on:
// http://www.cs.usfca.edu/~peter/cs625/code/
/* File:     timingcpu.h
 *
 * Purpose: Define a function that returns the
 *           number of seconds that have elapsed
 *           since some point in the past.  The
 *           timer should return times with
 *           microsecond accuracy.
 *
 *
 * Example:
 *    #include "timingcpu.h"
 *    . . .
 *    double dtime;
 *    struct timeval start;
 *    . . .
 *    gettimeofday(start, NULL);
 *    . . .
 *    Code to be timed
 *    . . .
 *    dtime = GET_TIME(start);
 *    printf("The code to be timed took"
 *           " %e seconds\n", dtime);
 */
#ifndef _TIMINGCPU_H_
#define _TIMINGCPU_H_

#include <sys/time.h>
double GET_TIME(struct timeval start);

#endif  /* TIMINGCPU_H */
