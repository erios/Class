#include "Dot_wrapper.h"

/*-------------------------------------------------------------------
 * Function:  Dot_wrapper
 * Purpose:   CPU wrapper function for GPU dot product
 * Note:      Assumes x_d, y_d, dot_d have already been
 *            allocated on device.  Also assumes x_d and y_d
 *            have been initialized.
 */
float Dot_wrapper(double x_d[], double y_d[], double *dot_d, int n,
                  double *dot_h, dim3 blocks, dim3 threads) {

  /* Timing CUDA events */
  cudaEvent_t timeStart, timeStop;
  // make sure it is of type float, precision is
  // milliseconds (ms) !!!
  float elapsedTime;

  elapsedTime = 0;

  cudaMemset(dot_d, 0, sizeof(double));

  /* Create and start clock */
  cudaEventCreate(&timeStart);
  cudaEventCreate(&timeStop);
  cudaEventRecord(timeStart, 0);

  /* Invoke kernel */
  cudaSqMatDot << <blocks, threads>>> (x_d, y_d, dot_d, n);
  /* Make sure that all blocks are finished */
  cudaDeviceSynchronize();

  /* Retrieve finishing time */
  cudaEventRecord(timeStop, 0);
  /* Stop the clock */
  cudaEventSynchronize(timeStop);
  // WARNING!!! do not simply print
  // (timeStop-timeStart)!!
  /* Calculate the elapsed time and save into
     elapsedTime */
  cudaEventElapsedTime(&elapsedTime, timeStart, timeStop);

  /* free the timer */
  cudaEventDestroy(timeStart);
  cudaEventDestroy(timeStop);

  cudaMemcpy(dot_h, dot_d, sizeof(double), cudaMemcpyDeviceToHost);

  return elapsedTime;

} /* Dot_wrapper */
