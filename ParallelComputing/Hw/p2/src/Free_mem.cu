#include "Free_mem.h"

/*------------------------------------------------
 * Function:  Free_mem
 * Purpose:   Free host and device memory
 */
void Free_mem(double *x_h, double *y_h, double *x_d, double *y_d, double *dot_d,
              double *dot_h) {

  /* Free device memory */
  cudaFree(x_d);
  cudaFree(y_d);
  cudaFree(dot_d);

  /* Free host memory */
  free(x_h);
  free(y_h);
  free(dot_h);

} /* Free_mem */
