#include "Get_args.h"

/*------------------------------------
 * Function:  Get_args
 * Purpose:   Get and check command line args.  If
 *            there's an error quit.
 */
void Get_args(int argc, char *argv[], int *n_p) {

  if (argc != 2) {
    fprintf(stderr, "usage: %s <size of squared"
                    " matrix>\n",
            argv[0]);
    exit(0);
  }
  *n_p = strtol(argv[1], NULL, 10);
  /* Squared matrix */
  *n_p = *n_p;
} /* Get_args */
