# Copyright (C) 2017  Edgar Rios
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

SHELL = /bin/bash

# Set project base name, for gcc and for NVCC
PRJ = p2
GCC_PRJ = gcc_$(PRJ)
NVCC_PRJ = nvcc_$(PRJ)

# Directory structure and variables
MAIN_DIR = ..
DIR_INC = $(MAIN_DIR)/include
DIR_SRC = $(MAIN_DIR)/src
DIR_BLD = $(MAIN_DIR)/build
DIR_BKP = $(MAIN_DIR)/bkp
DIR_BIN = $(MAIN_DIR)/bin
# Place for executable
PRG = $(DIR_BIN)/$(PRJ).run
GCC_PRG = $(DIR_BIN)/$(GCC_PRJ).run
NVCC_PRG = $(DIR_BIN)/$(NVCC_PRJ).run

# * Check for 32-bit vs 64-bit
# http://www.drdobbs.com/parallel/
# a-gentle-introduction-to-opencl/231002854?pgno=3
PROC_TYPE = $(strip $(shell uname -m | grep 64))
# ** Linux OS
# LIBS=-lOpenCL
ifeq ($(PROC_TYPE),)
ARCH := -m32
else
ARCH := -m64
endif # PROC_TYPE Linux OS

# * Compiler
# gcc command
GCC = gcc
# nvcc command
NVCC = nvcc

# ** Options
# *** System libraries to be included
# (-lm: math, -lcblas: blas)
LIBS = -I$(DIR_INC) -I$(DIR_SRC)
# LIBS = -lm -lcblas -I$(DIR_INC) -I$(DIR_SRC)
# LIBS = -lm -lcublas -I$(DIR_INC) -I$(DIR_SRC)
# In case that BSU RedHawk cannot find the gsl libraries
# LIBS = -lm -lcblas -lgslcblas -L/cm/shared/apps/gsl/2.3/lib -I/cm/shared/apps/gsl/2.3/include/gsl -I$(DIR_INC) -I$(DIR_SRC)
# Unavailable gslblas on BSU RedHawk
# LIBS = -lm -lgslblas -lgslcblas -L/cm/shared/apps/gsl/2.3/lib -I/cm/shared/apps/gsl/2.3/include/gsl -I$(DIR_INC) -I$(DIR_SRC)
# GCC_LIBS = $(LIBS) -lOpenCL
GCC_LIBS = $(LIBS) -lm -lgslcblas
NVCC_LIBS = $(LIBS) -lcublas -I/usr/local/cuda-7.5/samples/common/inc/
# *** Debugging options for compiler
DEBUG = -g -DDEBUG
# *** General options for compiler
# OPTS := $(DEBUG) -DUNIX -o
OPTS := -DUNIX -o
# **** GCC
GCC_OPTS := -Wall -O2 -funroll-loops -std=gnu99\
            $(ARCH) $(GCC_LIBS) $(OPTS)
# **** NVCC
NVCC_OPTS := -arch sm_30 $(NVCC_LIBS) $(OPTS)
# *** Compiling (not linking) options
GCC_CFLAGS := -c $(GCC_OPTS)
NVCC_CFLAGS := -c $(NVCC_OPTS)
# *** Add path to make
# All of this will be in the included directory
# list (for make, not gcc)
VPATH = $(DIR_DEPS):$(DIR_LIB):$(DIR_BLD)

# * Files to be linked or compiled
INCS := $(shell find $(DIR_INC) -type f -name '*.h')
GCC_SRCS := $(wildcard $(DIR_SRC)/*.c)
NVCC_SRCS := $(wildcard $(DIR_SRC)/*.cu)
# # In case that you need to set files manually
# # which are in a directory (and avoid writing
# # the name of the directory manually; could be
# # done with pattern substitution too)
# SRCS := main.cu checkStdin.c
# for each string in $(SRCS), prepend $(DIR_SRC)/
# SRCS := $(foreach src, $(SRCS), $(DIR_SRC)/$(src))
GCC_OBJS := $(GCC_SRCS:$(DIR_SRC)/%=$(DIR_BLD)/%.o)
NVCC_OBJS := $(NVCC_SRCS:$(DIR_SRC)/%=$(DIR_BLD)/%.o)
OBJS := $(NVCC_OBJS) $(GCC_OBJS)

# ------------------------------
# Start to compile
# ------------------------------
# Give a name to the main target, and set its
# dependencies
.PHONY: all
all: $(GCC_PRJ) $(NVCC_PRJ)

.PHONY: test
# test: $(GCC_PRJ) gccrun
test: all gcctest nvcctest

$(GCC_PRJ): dirs $(GCC_OBJS)
	$(GCC) $(GCC_OPTS) $(GCC_PRG) $(GCC_OBJS)

$(NVCC_PRJ): dirs $(OBJS)
	$(NVCC) $(NVCC_OPTS) $(NVCC_PRG) $(NVCC_OBJS)

.PHONY: gcctest
gcctest: $(GCC_PRJ)
	[ -x $(GCC_PRG) ] || chmod +x $(GCC_PRG)
	$(GCC_PRG) 7 5 8

.PHONY: nvcctest
nvcctest: $(NVCC_PRJ)
	[ -x $(NVCC_PRG) ] || chmod +x $(NVCC_PRG)
	$(NVCC_PRG) 150

# Target to compile (without linking) every .c
# For every name within the variable $(OBJS), take
# the name, and substitute $(DIR_BLD) with
# $(DIR_SRC), and get rid of the final .o
# in the file name.
$(NVCC_OBJS): $(DIR_BLD)/%.o: $(DIR_SRC)/% $(INCS)
	$(NVCC) $(NVCC_CFLAGS) "$@" "$<"

$(GCC_OBJS): $(DIR_BLD)/%.o: $(DIR_SRC)/% $(INCS)
	$(GCC) $(GCC_CFLAGS) "$@" "$<"

# Target to create required directories
.PHONY: dirs
dirs:
	[ -d $(DIR_BLD) ] || mkdir $(DIR_BLD)
	[ -d $(DIR_BKP) ] || mkdir $(DIR_BKP)
	[ -d $(DIR_BIN) ] || mkdir $(DIR_BIN)

.PHONY: clean
clean:
	rm -fr $(DIR_BLD) $(DIR_BIN) $(DIR_BKP) $(PRG)

.PHONY: tar
tar:
	rm -fr $(DIR_BKP)/\
	$(PRJ)_$(shell date "+%d-%b-%g").tar.gz

	tar czf $(DIR_BKP)/\
	$(PRJ)_$(shell date "+%d-%b-%g").tar.gz $(DIR_INC)\
	$(DIR_SRC)
