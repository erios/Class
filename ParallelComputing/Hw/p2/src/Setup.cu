#include "Setup.h"

/*--------------------------------------------
 * Function:  Setup
 * Purpose:   Allocate and initialize host and
 *            device memory
 */
void Setup(int n, double **x_h_p, double **y_h_p, double **x_d_p,
           double **y_d_p, double **dot_d_p, double **dot_h_p) {
  int i;
  int n2 = n * n;
  size_t size = n2 * sizeof(double);

  /* Allocate input vectors in host memory */
  *x_h_p = (double *)malloc(size);
  *y_h_p = (double *)malloc(size);
  *dot_h_p = (double *)malloc(size);

  /* Initialize input vectors */
  srandom(1);
  for (i = 0; i < n2; i++) {
    (*x_h_p)[i] = random() / ((double)RAND_MAX);
    (*y_h_p)[i] = random() / ((double)RAND_MAX);
    (*dot_h_p)[i] = random() / ((double)RAND_MAX);
  }

  /* Allocate vectors in device memory */
  cudaMalloc(x_d_p, size);
  cudaMalloc(y_d_p, size);
  cudaMalloc(dot_d_p, size);

  /* Copy vectors from host memory to device memory */
  cudaMemcpy(*x_d_p, *x_h_p, size, cudaMemcpyHostToDevice);
  cudaMemcpy(*y_d_p, *y_h_p, size, cudaMemcpyHostToDevice);
  cudaMemcpy(*dot_d_p, *dot_h_p, size, cudaMemcpyHostToDevice);
} /* Setup */
