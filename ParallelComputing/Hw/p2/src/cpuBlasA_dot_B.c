/*
 * Copyright (C) 2017  e-Dgar
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the
 * terms of the GNU General Public License as
 * published by the Free Software Foundation,
 * either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU
 * General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

/* https://www.gnu.org/software/gsl/manual/html_node/BLAS-Examples.html */
#include "cpuBlasA_dot_B.h"

/* gsl_blas unvailable on BSU RedHawk */
/* void */
/* cpuGslBlasA_dot_B (double *a, double *b, */
/*                    double *c, */
/*                    int *m, int *n, int *k) */
/* { */
/*   gsl_matrix_view A = gsl_matrix_view_array(a, *m, *n); */
/*   gsl_matrix_view B = gsl_matrix_view_array(b, *n, *k); */
/*   gsl_matrix_view C = gsl_matrix_view_array(c, *m, *k); */
/*  */
/*   /\* Compute C = A B *\/ */
/*  */
/*   /\* dgemm: Double General Matrix Multiply *\/ */
/*   gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, */
/*                   1.0, &A.matrix, &B.matrix, */
/*                   0.0, &C.matrix); */
/* } */

void cpuCBlasA_dot_B(const double *a, const double *b, double *c, const int *m,
                     const int *n, const int *k) {
  /**
   *  \brief Calculates the dot (inner) product
   *  between two matrices
   *
   *
   *  Uses cblas_ddot to do
   *  c = a . b + c
   *
   *  where a and b are m×n and n×k matrices, and
   *  c is m×k
   *  (1D arrays; flatten matrices: a[m-1] is the
   *  element in the second row, first column)
   *
   *  The input parameter c (pointer) is
   *  modified. Make sure to clean c if necessary.
   *
   *  \param
   *  const double *a -- left matrix
   *  const double *b -- right matrix
   *  double *c       -- result
   *  const int *m    -- # rows (1st dimension) of a
   *                     and c
   *  const int *n    -- # columns of a and rows of b
   *  const int *k    -- # columns of b and c

   *  \return nothing
   */

  for (int i = 0; i < *m; i++) {
    for (int j = 0; j < *k; j++) {
      /* The length of the vectors is n.
         ddot expects an address for the vectors*/
      c[i * *k + j] = cblas_ddot(*n, /* The length of
                                        the vectors */
                                 &a[i * *n], /* by rows */
                                 1,          /* skip 1 element
                                      to get to next column
                                      a[i+1]
                                      a[i+2]
                                      ...*/
                                 &b[j], /* by columns */
                                 *k);   /* skip *k elements
                                         to get to the
                                         next row (at the
                                         same column) */
    }
  }
}
