/*
 * Copyright (C) 2017  e-Dgar
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the
 * terms of the GNU General Public License as
 * published by the Free Software Foundation,
 * either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU
 * General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

/* https://www.gnu.org/software/gsl/manual/html_node/BLAS-Examples.html */
/* Gokturk Poyrazoglu - Matrix Multiplication */
#include "cpuDaxpyA_dot_B.h"

void cpuDaxpyA_dot_B(const double *a, const double *b, double *c, const int *m,
                     const int *n, const int *k) {
  /**
   *  \brief Calculates the dot (inner) product
   *  between two matrices
   *
   *
   *  Uses cblas_daxpy (linear combination) to do
   *  c → a . b + c    (→ means assign)
   *
   *  where a and b are m×n and n×k matrices, and
   *  c is m×k
   *  (1D arrays; flatten matrices: a[m-1] is the
   *  element in the second row, first column)
   *
   *  The input parameter c (pointer) is
   *  modified. Make sure to clean c if necessary.
   *
   *  The linear combination works like this: Let
   *  a have n columns, and b have n rows and k
   *  columns, so that element b[n,k] of b is the
   *  one located in the nth row and kth column of
   *  b. Denote a column i of a as col(a)[i], and
   *  the sum of x[j] from j=1 to j=k as
   *  \sum_{j=1}^{k}{x[j]}. Then, if c is the
   *  inner product of a and b,
   *  col(c)[i] → \sum_{j=1}^{n}{col(a)[j]*b[j,i]}
   *  . This means that the
   *  ith column of c is the
   *    sum of the products of
   *        the ith column of a
   *      times
   *        the jth element of the ith column of b
   *        (b[j,i]).
   *
   *  \param
   *  const double *a -- left matrix
   *  const double *b -- right matrix
   *  double *c       -- result
   *  const int *m    -- # rows (1st dimension) of a
   *                     and c
   *  const int *n    -- # columns of a and rows of b
   *  const int *k    -- # columns of b and c

   *  \return nothing
   */
  for (int j = 0; j < *n; j++) {
    for (int r = 0; r < *k; r++) {
      /* Remember: we are adding by moving one
         column at a time, not rows */
      /* c[j] = c[j] + a[j]*b[r + j * *k] */
      cblas_daxpy(*m, /* length of
                        column vector a
                        (rows) */
                  b[r + j * *k], /* coefficient
                                    by which
                                    col(a) is
                                    multiplied */
                  &a[j], /* start of
                            column
                            vector */
                  *n, /* distance to
                         next row of a
                         (next element
                         of column
                         vector)*/
                  &c[r], /* start of
                            column
                            vector */
                  *k /* distance to
                        next row of c
                        (next element
                        of column
                        vector) */
                  );
    }
  }
}
