/*
 * Copyright (C) 2017  Edgar Rios
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the
 * terms of the GNU General Public License as
 * published by the Free Software Foundation,
 * either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU
 * General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

/* file:///usr/share/doc/liblapack-doc/explore-html/d9/dcd/daxpy_8f_source.html
 */
/* /usr/share/doc/libblas-doc/cinterface.pdf.gz */
/* /usr/share/doc/gsl-ref-psdoc/gsl-ref.pdf */

/* General utilities */
#include "headers.h"
/* Check if input arguments are right */
#include "checkStdin.h"
/* Matrices with random numbers */
#include "genMatrix.h"
/* Print matrices */
#include "printMatrix.h"
/* Naive matrix multiplication */
/* https://jemnz.com/matrixmultiply.html */
#include "cpuNaiveA_dot_B.h"
/* ddot matrix multiplication */
/* https://jemnz.com/matrixmultiply.html */
#include "cpuBlasA_dot_B.h"
/* daxpy matrix multiplication */
#include "cpuDaxpyA_dot_B.h"

double GET_TIME(struct timeval start) {
  struct timeval t;
  gettimeofday(&t, NULL);
  return (t.tv_sec - start.tv_sec + (t.tv_usec - start.tv_usec)/1000000.0);
}

int main(int argc, char *argv[]) {
  /* Matrices dimensions */
  int n, m, k;
  /* Matrices pointers (arrays) */
  double *A, *B, *C;
  /** Clocking variables **/
  double dtime;
  struct timeval start;

  checkStdin(&argc, argv);

  /* Convert input values from string to
     integer */
  /* Rows of first matrix */
  m = atoi(argv[1]);
  /* Columns of first matrix and rows of second
     matrix */
  n = atoi(argv[2]);
  /* Columns of second matrix */
  k = atoi(argv[3]);

  /* Check arguments */
  checkArgs(&m, &n, &k);

  /* Allocate memory and set to zero */
  A = (double *)calloc(m * n, sizeof *A);
  B = (double *)calloc(n * k, sizeof *B);
  C = (double *)calloc(m * k, sizeof *C);

  /* Set random values in matrices */
  randomMatrix(A, m, n);
  randomMatrix(B, n, k);

  /* Fill second row with 2s (to test result) */
  for (int i = 0; i < n; i++) {
    A[1 * n + i] = 2.0;
  }
  /* Fill third column with 5s (to test result) */
  for (int i = 0; i < n; i++) {
    B[2 + i * k] = 5.0;
  }
#ifdef DEBUG
  /* Print input matrices */
  printf("Matrix A:\n");
  printMatrix2Dd(A, m, n);
  printf("\n");
  printf("Matrix B:\n");
  printMatrix2Dd(B, n, k);
  printf("\n");
#endif

#ifndef DEBUG
  printf("cpuNaiveA_dot_B\tcpuCblasA_dot_B\tcpuDaxpyA_dot_B\n");
#endif

  /* get time before loop (time.h) */
  gettimeofday(&start, NULL);

  /* Multiply with the naïve (basic) code */
  /* C was already set to zero (calloc) */
  cpuNaiveA_dot_B(A, B, C, &m, &n, &k);

  /* get time after loop (time.h) */
  dtime = GET_TIME(start);

/* compile with -DDEBUG to see this: */
#ifdef DEBUG
  /* Print results */
  printf("Result with naive procedure:\n");
  printf("Matrix C:\n");
  printMatrix2Dd(C, m, k);
  printf("Elapsed time: ");
#endif // DEBUG
  printf("%10.4le", dtime);

  /* Clear matrix */
  zeroMatrix(C, m, k);
  /* Multiply with cblas double general matrix */
  /* C := alpha*op( A )*op( B ) + beta*C */
  cblas_dgemm(CblasRowMajor, /* Follow rows */
              CblasNoTrans,  /* No transpose */
              CblasNoTrans,  /* No transpose */
              m, k, n,       /* dim(res)=m,k */
              1.0,           /* alpha */
              A,             /* Matrix 1 */
              n,             /* Columns A */
              B, k,          /* Mat 2, cols */
              0.0,           /* beta */
              C, k);
/* compile with -DDEBUG to see this: */
#ifdef DEBUG
  /* Print results */
  printf("Result with cblas_dgemm procedure:\n");
  printf("Matrix C:\n");
  printMatrix2Dd(C, m, k);
#endif // DEBUG

  /* gsl_blas unvailable on BSU RedHawk */
  /* /\* Multiply with gsl *\/ */
  /* cpuGslBlasA_dot_B(A, B, C, &m, &n, &k); */
  /*  */

  /* Clear matrix */
  zeroMatrix(C, m, k);

  /* get time before loop (time.h) */
  gettimeofday(&start, NULL);

  /* Multiply with cblas_ddot */
  cpuCBlasA_dot_B(A, B, C, &m, &n, &k);

  /* get time after loop (time.h) */
  dtime = GET_TIME(start);

/* compile with -DDEBUG to see this: */
#ifdef DEBUG
  /* Print results */
  printf("Result with cblas_ddot procedure:\n");
  printf("Matrix C:\n");
  printMatrix2Dd(C, m, k);
  printf("Elapsed time: ");
#endif // DEBUG
  printf("\t%10.4le", dtime);

  /* Clear matrix (completely necessary) */
  zeroMatrix(C, m, k);

  /* get time before loop (time.h) */
  gettimeofday(&start, NULL);

  /* Multiply with cblas_ddot */
  cpuDaxpyA_dot_B(A, B, C, &m, &n, &k);

  /* get time after loop (time.h) */
  dtime = GET_TIME(start);

/* compile with -DDEBUG to see this: */
#ifdef DEBUG
  /* Print results */
  printf("Result with cblas_daxpy procedure:\n");
  printf("Matrix C:\n");
  printMatrix2Dd(C, m, k);
  printf("Elapsed time: ");
#endif // DEBUG
  printf("\t%10.4le", dtime);

  printf("\n");

  free(A);
  free(B);
  free(C);

  return 0;
}
