/*
 * Copyright (C) 2017  Edgar Rios
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the
 * terms of the GNU General Public License as
 * published by the Free Software Foundation,
 * either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU
 * General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

/* https://jemnz.com/matrixmultiply.html */
#include "cpuNaiveA_dot_B.h"

void cpuNaiveA_dot_B(const double *a, const double *b, double *c, const int *m,
                     const int *n, const int *k) {
  /**
   *  \brief Calculates the dot (inner) product
   *  between two matrices
   *
   *
   *  Uses the traditional (non-optimised ) way
   *  (three loops)
   *  c = a . b + c
   *
   *  where a and b are m×n and n×k matrices, and
   *  c is m×k
   *  (1D arrays; flatten matrices: a[m-1] is the
   *  element in the second row, first column)
   *
   *  The input parameter c (pointer) is
   *  modified. Make sure to clean c if necessary.
   *
   *  \param
   *  const double *a -- left matrix
   *  const double *b -- right matrix
   *  double *c       -- result
   *  const int *m    -- # rows (1st dimension) of a
   *                     and c
   *  const int *n    -- # columns of a and rows of b
   *  const int *k    -- # columns of b and c

   *  \return nothing
   */
  /* rows(a) */
  for (int i = 0; i < *m; i++) {
    /* cols(b) */
    for (int r = 0; r < *k; r++) {
      /* cols(a), rows(b) */
      for (int j = 0; j < *n; j++) {
        c[i * *k + r] += a[j + i * *n] * b[j * *k + r];
      }
    }
  }
}
