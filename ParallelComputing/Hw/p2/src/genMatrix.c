/*
 * Copyright (C) 2017  Edgar Rios
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the
 * terms of the GNU General Public License as
 * published by the Free Software Foundation,
 * either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU
 * General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "genMatrix.h"
/**
 *  \brief retrieves a value from /dev/urandom
 *
 *  \param none
 *  \return randr_r(&seed), where &seed comes from
 *          /dev/urandom
 */
int randomSeed() {
  /* https://stackoverflow.com/a/4930888 */
  /* man 3 srand */
  unsigned int seed;
  FILE *urandom = fopen("/dev/urandom", "r");
  fread(&seed, sizeof(int), 1, urandom);
  fclose(urandom);
  return rand_r(&seed);
}

/**
 *  \brief Sets all values of an array to random
 *  numbers
 *
 *  Calls reandomSeed() to get a random seed for
 *  srand(), then puts rand() for each value of
 *  the input array
 *
 *  \param
 *          *a -- (double) array (matrix)
 *          m  -- number of rows of a
 *          n  -- number of columns of a
 *
 *  \return nothing (modifies *a)
 */
void randomMatrix(double *a, int m, int n) {
  /* Random seed */
  int rseed;

  /* Get random seed */
  rseed = randomSeed();
  /* Set random seed */
  srand(rseed);

  for (int i = 0; i < m * n; i++) {
    a[i] = (double)rand();
  }
}

/**
 *  \brief Sets all values of an array to zero
 *
 *  \param
 *          *a -- (double) array (matrix)
 *          m  -- number of rows of a
 *          n  -- number of columns of a
 *
 *  \return nothing (modifies *a)
 */
void zeroMatrix(double *a, int m, int n) {
  for (int i = 0; i < m * n; i++) {
    a[i] = 0.0;
  }
}
