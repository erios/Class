/*
 * Copyright (C) 2017  Edgar Rios
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the
 * terms of the GNU General Public License as
 * published by the Free Software Foundation,
 * either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU
 * General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Acknowledgment:
 * Inanc Senocak
 * John Everingham
 * https://wiki.rc.ufl.edu/doc/CUDA_Examples
 * https://devblogs.nvidia.com/parallelforall/six-ways-saxpy/
 * Matrix computations on the GPU CUBLAS and MAGMA by example
 *
 *
 * File:     gpuDdotA_dot_B.cu
 * Purpose:  Implement squared matrix dot product
 *           on a gpu using cuda daxpy
 *
 * Input:    Squared matrix dimension
 *
 * Output:   Prints the elapsed time (and the
 *           matrices if compiled with -DDEBUG)
 *
 */

#include "gpuDdotA_dot_B.h"

float cudaSqMatDdot(cublasHandle_t handle, const double a[], /* device */
                    const double b[],                        /* host */
                    double c[],                              /* device */
                    const int m) {
  /**
   *  \brief Squared matrix multiplication
   *  using cuda ddot
   *
   *  Uses cublasDdot to do
   *  c = a . b + c
   *
   *  where a, b and c are m×m matrices
   *  (1D arrays; flatten matrices: a[m-1] is the
   *  element in the second row, first column)
   *
   *  The input parameter c (pointer) is
   *  modified. Make sure to clean c if
   *  necessary. See also cpuBlasA_dot_B.c
   *
   *  \param
   *  const double *a -- left matrix (cublasSetVector)
   *  const double *b -- right matrix (double *)
   *  double *c       -- result (cublasSetVector)
   *  const int m     -- size of the squared
   *                     matrices
   *
   *  \return nothing
   */
  /* Timing CUDA events */
  cudaEvent_t timeStart, timeStop;
  // make sure it is of type float, precision is
  // milliseconds (ms) !!!
  float elapsedTime, deltaT;

  deltaT = 0;
  elapsedTime = 0;

  /* Initialize timer */
  // WARNING!!! use events only to time the
  // device
  cudaEventCreate(&timeStart);
  cudaEventCreate(&timeStop);

  /* Force squared matrices */
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < m; j++) {
      /* Start clock */
      // the second argument 0 is for the default CUDA
      // stream
      cudaEventRecord(timeStart, 0);

      cublasDdot(handle, m, /* The length of
                               the vectors */
                 &a[i * m], /* by rows
                               (device) */
                 1, /* skip 1 element
                       to get to next
                       column a[i+1]
                       a[i+2]
                       ...*/
                 &b[j], /* by columns
                           (device) */
                 m, /* skip *k elements
                       to get to the
                       next row (at the
                       same column) */
                 &c[i * m + j] /* (device or
                                  host) */
                 );

      /* Make sure that all blocks are finished */
      cudaDeviceSynchronize();

      /* Retrieve finishing time */
      cudaEventRecord(timeStop, 0);
      /* Stop the clock */
      cudaEventSynchronize(timeStop);
      // WARNING!!! do not simply print
      // (timeStop-timeStart)!!
      /* Calculate the elapsed time and save into
         elapsedTime */
      cudaEventElapsedTime(&deltaT, timeStart, timeStop);
      elapsedTime += deltaT;
    }
  }
  /* free the timer */
  cudaEventDestroy(timeStart);
  cudaEventDestroy(timeStop);

  return elapsedTime;

} /* cudaSqMatDdot */

int gpuDdotA_dot_B(const int *m, const int *n, const int *k) {
  /**
   *  \brief Sets the variables to do ddot
   *  multiplication with CUDA
   *
   *  It gets everything ready to do a squared
   *  matrix multiplication by means of CUDA. It
   *  also times the process.
   *
   *  \param
   *  m, n, k -- size of matrix (the same)
   *
   *  \output
   *  prints the time
   *
   *  \return nothing
   */

  /* Host  pointers */
  double *pha, *phb, *phc;
  /* Device pointers */
  double *pda, *pdb;

  /* Timing CUDA events */
  float elapsedTime;

  /* Keep an eye on CUDA */
  /* cudaMalloc status */
  /* cudaError_t cudaStat; */ // not used now
  /* CUBLASfunctionsstatus */
  cublasStatus_t status;

  /* CUBLAScontext */
  cublasHandle_t handle;

  /* Allocate memory */
  pha = (double *)malloc((*m * *n) * sizeof(pha[0]));
  phb = (double *)malloc((*n * *k) * sizeof(phb[0]));
  /* set to zero with calloc */
  phc = (double *)calloc((*m * *k), sizeof(phc[0]));

  /* Make sure that the memory was allocated */
  if (pha == 0) {
    fprintf(stderr, "!!!! host memory"
                    " allocation error (A)\n");
    return EXIT_FAILURE;
  }
  if (phb == 0) {
    fprintf(stderr, "!!!! host memory"
                    " allocation error (B)\n");
    return EXIT_FAILURE;
  }
  if (phc == 0) {
    fprintf(stderr, "!!!! host memory"
                    " allocation error (C)\n");
    return EXIT_FAILURE;
  }

  /* Set random values in matrices */
  randomMatrix(pha, *m, *n);
  randomMatrix(phb, *n, *k);

  /* Initialize cuBLAS context */
  /* status = cublasCreate(&handle); */
  /* if (status != CUBLAS_STATUS_SUCCESS) */
  if (cublasCreate(&handle) != CUBLAS_STATUS_SUCCESS) {
    fprintf(stderr, "!!!! CUBLAS"
                    " initialization error\n");
    return EXIT_FAILURE;
  }

  /* Allocate device memory for the matrices */
  /* (Slower alternative to test success) */
  /* cudaStat = cudaMalloc((void **) &pda, *m * *n * sizeof(pda[0])); */
  /* if (cudaStat != cudaSuccess) */
  if (cudaMalloc((void **)&pda, *m * *n * sizeof(pda[0])) != cudaSuccess) {
    fprintf(stderr, "!!!! device memory"
                    " allocation error (allocate A)\n");
    return EXIT_FAILURE;
  }

  if (cudaMalloc((void **)&pdb, *m * *n * sizeof(pdb[0])) != cudaSuccess) {
    fprintf(stderr, "!!!! device memory"
                    " allocation error (allocate A)\n");
    return EXIT_FAILURE;
  }

  /* ******************** Allocate */

  /* Link the device matrices with the host
     matrices */
  status = cublasSetVector(*m * *n, sizeof(pha[0]), pha, 1, pda, 1);
  if (status != CUBLAS_STATUS_SUCCESS) {
    fprintf(stderr, "!!!! device access"
                    " error (write A)\n");
    return EXIT_FAILURE;
  }

  status = cublasSetVector(*n * *k, sizeof(phb[0]), phb, 1, pdb, 1);
  if (status != CUBLAS_STATUS_SUCCESS) {
    fprintf(stderr, "!!!! device access"
                    " error (write A)\n");
    return EXIT_FAILURE;
  }

  /* ******************** Link */

  /* Not useful for host function */
  /* /\* Per request (problem definition) *\/ */
  /* dim3 block(16, 16); */
  /* dim3 grid((n + 15) / 16, (n + 15) / 16); */

  /* Run the dot product with Ddot */
  /* How do I parallelise this? I know it's
     possible */
  /* cudaSqMatDdot<<<grid, block>>>(pda, pdb, */
  /*                                pdc, m); */
  elapsedTime = cudaSqMatDdot(handle, pda, pdb, phc, *m);

  /* Release device memory */
  cudaFree(pda);
  cudaFree(pdb);
  /* cudaFree(pdc); */

  /* Shutdown */
  cublasDestroy(handle);
  cudaDeviceReset();

/* compile with -DDEBUG to see this: */
#ifdef DEBUG
  /* Print results */
  printf("\ngpuDdotA_dot_B\n");
  printf("====================\n");
  printMatrix2Dd(pha, *n, *n);
  printf("\n");
  printMatrix2Dd(phb, *n, *n);
  printf("\n");
  printMatrix2Dd(phc, *n, *n);
  printf("====================\n");
  printf("Elapsed time = ");
#endif // DEBUG

  /* milliseconds → seconds */
  printf("%10.4e", elapsedTime / (float)1000.0);

  /* Release host memory */
  free(pha);
  free(phb);
  free(phc);

  return EXIT_SUCCESS;

} /* gpuDdotA_dot_B */
