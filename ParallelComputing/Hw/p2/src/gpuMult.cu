#include "headers.h"
#include "Setup.h"
#include "Get_args.h"
#include "Dot_wrapper.h"
#include "gpuNaiveA_dot_B.h"
#include "gpuDaxpyA_dot_B.h"
#include "gpuDdotA_dot_B.h"

int main(int argc, char *argv[]) {
  /* size of squared matrices */
  int n;

  /* Set values given from standard input */
  Get_args(argc, argv, &n);

  /* Print header */
  printf("gpuNaiveA_dot_B (s)"
         "\t" /* separator */
         "gpuDaxpyA_dot_B (s)"
         "\t" /* separator */
         "gpuDdotA_dot_B (s)\n");
  /* Print results */
  /* The function prints the time value */
  gpuNaiveA_dot_B(argc, argv);
  printf("\t"); /* separator */
  /* The function prints the time value */
  gpuDaxpyA_dot_B(&n, &n, &n);
  printf("\t");
  /* The function prints the time value */
  gpuDdotA_dot_B(&n, &n, &n);
  printf("\n");

  return 0;
}
