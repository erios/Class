/*
 * Copyright (C) 2017  Edgar Rios
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the
 * terms of the GNU General Public License as
 * published by the Free Software Foundation,
 * either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU
 * General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

// This code is heavily based on
// http://www.cs.usfca.edu/~peter/cs625/code/
// http://www.shodor.org/media/content/petascale/materials/UPModules/matrixMultiplication/moduleDocument.pdf
/* File:     gpuNaiveA_dot_B.cu
 * Purpose:  Implement squared matrix dot product
 *           on a gpu using cuda
 *
 * Input:    From stdin (standard input): size of
 *           matrices
 * Output:   Result of dot product of two squared
 *           matrices
 */
#include <stdio.h>
#include <stdlib.h>
#include "gpuNaiveA_dot_B.h"

/*------------------------------------------------
 * Function:    cudaSqMatDot  (kernel)
 * Purpose:     Implement a dot product of double
 *              precision squared matrices
 *
 * In args:     a, b, m
 * In/out arg:  c
 *
 * Note:        c should be initialized to 0
 *              when calling the function
 */
__global__ void cudaSqMatDot(const double a[],
                             const double b[],
                             double c[],
                             const int m) {
  /**
   *  \brief Naive squared matrix multiplication
   *  using cuda
   *
   *  Uses nested threads (loops) to do
   *  c = a . b + c
   *
   *  where a, b and c are m×m matrices
   *  (1D arrays; flatten matrices: a[m-1] is the
   *  element in the second row, first column)
   *
   *  The input parameter c (pointer) is
   *  modified. Make sure to clean c if necessary.
   *
   *  \param
   *  const double *a -- left matrix
   *  const double *b -- right matrix
   *  double *c       -- result
   *  const int *m    -- size of the squared
   *                     matrices
   *
   *  \return nothing
   */
  /* Rows of a */
  int i = blockDim.y * blockIdx.y + threadIdx.y;
  /* Columns of b */
  int j = blockDim.x * blockIdx.x + threadIdx.x;

  /* NOTE: Do not use pointers to compare to
     threaded variables */
  if (i < m && j < m) {
    /* Columns of a */
    for (int r = 0; r < m; r++) {
      /* c is flat
         index of first element in row =
         (Number of columns) × (row number) */
      /* row(a) = row(c), */
      c[i * m + j] += a[r + i * m] * b[r * m + j];
    }
  }
} /* cudaSqMatDot */

void gpuNaiveA_dot_B(int argc, char *argv[]) {
  /**
   *  \brief Sets the variables to do naive
   *  multiplication with CUDA
   *
   *  It gets everything ready to do a squared
   *  matrix multiplication by means of CUDA. It
   *  also times the process.
   *
   *  \param (stdin) n -- size of matrix
   *  \return nothing
   */
  /* size of squared matrices */
  int n = 1;

  /* host vectors */
  double *x_h, *y_h, *dot_h;
  /* device vectors */
  double *x_d, *y_d, *dot_d;

  /* Timing CUDA events */
  float elapsedTime;

  /* Set values given from standard input */
  Get_args(argc, argv, &n);

  /* Per request (problem definition) */
  dim3 block(16, 16);
  dim3 grid((n + 15) / 16, (n + 15) / 16);

  /* Allocate and initialise arrays */
  Setup(n, &x_h, &y_h, &x_d, &y_d, &dot_d,
        &dot_h);

  elapsedTime = Dot_wrapper(x_d, y_d, dot_d, n,
                            dot_h, grid, block);

/* compile with -DDEBUG to see this: */
#ifdef DEBUG
  /* Print results */
  printf("\ngpuNaiveA_dot_B\n");
  printf("====================\n");
  printMatrix2Dd(x_h, n, n);
  printf("\n");
  printMatrix2Dd(y_h, n, n);
  printf("\n");
  printMatrix2Dd(dot_h, n, n);
  printf("====================\n");
  printf("Elapsed time = ");
#endif // DEBUG

  /* milliseconds → seconds */
  printf("%10.4e", elapsedTime / (float)1000.0);

  Free_mem(x_h, y_h, x_d, y_d, dot_d, dot_h);

} /* gpuNaiveA_dot_B */
