/*
This function performs the same operation as the gpu daxpy() matrix solver
but it uses the cublas version of daxpy() instead
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/resource.h>
#include <cublas.h>

void daxpygpu_matmult(const int m, const int n, const int k, const double *A,
                      const double *B, double *C) {

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < k; j++) {
      cublasDaxpy(m, B[j + i * k], &A[i], n, &C[j], k);
      cudaDeviceSynchronize();
    }
  }
}
