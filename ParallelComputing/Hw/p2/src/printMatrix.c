/*
 * Copyright (C) 2017  Edgar Rios
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the
 * terms of the GNU General Public License as
 * published by the Free Software Foundation,
 * either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU
 * General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "printMatrix.h"

void printMatrix2Dd(double *a, int m, int n) {
  if (n <= 11 && m <= 20) {
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        printf("%10.3le ", a[n * i + j]);
      }
      printf("\n");
    }
  }
}

void printMatrix2Df(float *a, int m, int n) {
  if (n <= 11 && m <= 20) {
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        printf("%10.3e ", a[n * i + j]);
      }
      printf("\n");
    }
  }
}

void printMatrix2Di(int *a, int m, int n) {
  if (n <= 11 && m <= 20) {
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        printf("%10d ", a[n * i + j]);
      }
      printf("\n");
    }
  }
}
