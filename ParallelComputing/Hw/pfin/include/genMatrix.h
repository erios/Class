#ifndef GENMATRIX_H
#define GENMATRIX_H

#include "headers.h"

/**
 *  \brief retrieves a value from /dev/urandom
 *
 *  \param none
 *  \return randr_r(&seed), where &seed comes from
 *          /dev/urandom
 */
int randomSeed();

/**
 *  \brief Sets all values of an array to random
 *  numbers
 *
 *  Calls reandomSeed() to get a random seed for
 *  srand(), then puts rand() for each value of
 *  the input array
 *
 *  \param
 *          *a -- (float) array (matrix)
 *          m  -- number of rows of a
 *          n  -- number of columns of a
 *
 *  \return nothing (modifies *a)
 */
void randomMatrixf(float *a, int m, int n);

/**
 *  \brief Sets all values of an array to zero
 *
 *  \param
 *          *a -- (float) array (matrix)
 *          m  -- number of rows of a
 *          n  -- number of columns of a
 *
 *  \return nothing (modifies *a)
 */
void zeroMatrixf(float *a, int m, int n);

#endif /* GENMATRIX_H */
