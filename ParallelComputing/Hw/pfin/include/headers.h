/*
 * Copyright (C) 2017  Edgar Rios
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the
 * terms of the GNU General Public License as
 * published by the Free Software Foundation,
 * either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU
 * General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *   \file headers.h
 *   \brief This file is loaded by almost all
 *   the source files (.c).
 *
 *  Detailed description. It loads the user
 *   interaction, strings, memory allocation, data
 *   type limits and timing libraries from GNU C.
 *
 */
/* Define everything only once */
#ifndef HEADERS_H
#define HEADERS_H
/* Interaction with the user */
#include <stdio.h>
/* Strings, memory allocation */
#include <stdlib.h>
/* Mathematical functions (use -lm) */
#include <math.h>
/* EXIT_SUCCESS and pals */
#include <sys/resource.h>
/* Timing events */
#include <time.h>
/*--- Doesn't work: --- */
/* /\* cBLAS (use -lcblas)*\/ */
/* #include <cblas.h> */
/* -------------------- */
/* gsl BLAS */
#include <gsl/gsl_blas.h>
/* gsl cBLAS */
#include <gsl/gsl_cblas.h>
/* /\* cuBLAS *\/ */
/* #include "cublas.h" */
#include "timingcpu.h"
/* Check if input arguments are right */
#include "checkStdin.h"
/* Make sure that the memory was allocated */
#include "checkalloc.h"
/* Matrices with random numbers */
#include "genMatrix.h"
/* Print matrices */
#include "printMatrix.h"
/* Set boundary conditions */
#include "setbound.h"
/* Solve the heat equation on cpu (serial) */
#include "solveheatcpu.h"
/* Estimate derivative in terms of a coordinate */
#include "tempparderiv.h"
/* need it for MPI functions */
#include <mpi.h>

/* Macros **************************************/
/* Length of an array */
#define ARRLEN(a) (sizeof(a)/sizeof(*a))

#endif
