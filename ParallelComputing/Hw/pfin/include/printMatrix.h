#ifndef PRINTMATRIX_H
#define PRINTMATRIX_H

#include "headers.h"

void printMatrix2Dd(double *a, const int m,
                    const int n);

void printMatrix2Df(float *a, const int m,
                    const int n);

void printMatrix2Di(int *a, const int m,
                    const int n);

#endif /* PRINTMATRIX_H */
