/**
 *  \brief sets a value in the first column
 *
 *  This is a function to set the initial boundary
 *  conditions of the 2D Laplace heat transfer
 *  equation.
 *
 *  The function takes a flattened array which
 *  contains a 2d squared matrix, and sets the
 *  elements in the first column to 100.
 *
 *  \param
 *  float *a     -- 1D array (2D matrix)
 *  const int N  -- size of the array (N×N)
 *  const flaot val -- value to set
 *
 *  \return
 *  none, modifies the contents of *a
 */
void
setbound (float *a, const int N, const float val);
