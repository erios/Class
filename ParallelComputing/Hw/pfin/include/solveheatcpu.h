#ifndef SOLVEHEATCPU_H
#define SOLVEHEATCPU_H

void
solveheatcpu(float *teta,        //array
             float *teta_new,    //array
             const int *pts_x,
             const int *pts_y,
             const float *dx,
             const float *dy,
             const float *dt);

#endif /* SOLVEHEATCPU_H */
