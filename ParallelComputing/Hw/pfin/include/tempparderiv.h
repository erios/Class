#ifndef TEMPPARDERIV_H
#define  TEMPPARDERIV_H

float
tempparderiv(const float *teta_e1,
             const float *teta_e,
             const float *teta_e_1,
             const float *de2);

#endif /*  TEMPPARDERIV_H */
