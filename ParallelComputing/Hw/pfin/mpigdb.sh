#!/bin/bash

# $OMPI_MCA_ns_nds_vpid is the OpenMPI equivalent to $LAMRANK
if [ "${OMPI_MCA_ns_nds_vpid}" == "0" ] ; then
    exec gdb -i=mi --annotate=3 $*
else
    xterm -sb -rightbar -e "bash -c \"gdb $*<<EOF
  run
  EOF\""
fi
exit 0
