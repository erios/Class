/*
 * Copyright (C) 2017  Edgar Rios
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the
 * terms of the GNU General Public License as
 * published by the Free Software Foundation,
 * either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU
 * General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "checkStdin.h"

int checkArgs(int *n, int *m, int *k) {
  /* Make sure that all arguments are bigger
     than zero */
  int args[3] = { *n, *m, *k };
  for (int i = 0; i < ARRLEN(args); i++) {
    if (args[i] < 1) {
      fprintf(stderr, "One of the arguments is"
                      " zero");
      exit(-1);
    }
  }
  return 0;
}

int checkStdin(int *argc, char *argv[]) {
  /* Check command line arguments */
  if (*argc != 2) {
    fprintf(stderr,
            "This program requires 1 argument:\n "
            "%s m\n"
            "m: number of points to discretise"
            "the length of the square",
            argv[0]);
    exit(-1);
    /* perror(); */
  }

  return 0;
}
