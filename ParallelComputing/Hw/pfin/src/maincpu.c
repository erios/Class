/*
 * Copyright (C) 2017  Edgar Rios
 *
 * This program is free software: you can
 * redistribute it and/or modify it under the
 * terms of the GNU General Public License as
 * published by the Free Software Foundation,
 * either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU
 * General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 */
/* General utilities */
#include "headers.h"

int
main(int argc, char *argv[])
{
  /*********************************************/
  /*            Variable definition            */
  /*********************************************/
  /** Clocking variables **/
  double dtime;
  struct timeval start;

  /* get time before starting */
  /*  ******************************************/
  gettimeofday(&start, NULL);

  /* Length of the square */
  const float L = 20.0f;
  /* Boundary condition */
  const float val = 100.0f;
  /* Length of the array
     (size of side * size of side/(incx * incy) */
  int pts_x;
  int pts_y;
  /* Increment in x */
  float dx;
  /* Increment in y */
  float dy;
  /* Time step */
  float dt;
  /* Array to hold the temperature */
  /* # of columns : N, index: j */
  /* # of rows : M = N, index: i */
  float *teta;
  /* Temporary array to hold prev temperature */
  float *teta_new;

  /*********************************************/
  /*   Get input variables (number of points)  */
  /*********************************************/
  checkStdin(&argc, argv);
  pts_x = atoi(argv[1]);
  pts_y = pts_x;
  /* Set increments in first dimension (x) */
  dx = L/(pts_x - 1);
  /* Set increments in second dimension (y) */
  dy = dx;
  /* dt <= 1/4*dx² */
  /* http://www.u.arizona.edu/~erdmann/mse350/
     _downloads/2D_heat_equation.pdf */
  dt = 0.25 * dx * dy;

  /*********************************************/
  /*              Allocate memory              */
  /*********************************************/
  teta = (float *) calloc(pts_x * pts_y,
                          sizeof(*teta));
  teta_new = (float *) calloc(pts_x * pts_y,
                             sizeof(*teta_new));
  /* Make sure that the memory was allocated */
  if (teta == 0) {
    fprintf(stderr,
            "\nallocation error (teta)\n");
    return EXIT_FAILURE;
  }

  if (teta_new == 0) {
    fprintf(stderr,
            "\nallocation error (teta_1)\n");
    return EXIT_FAILURE;
  }

  /*********************************************/
  /*             Initial conditions            */
  /*********************************************/
  /* # of columns = pts_x */
  setbound(teta, pts_x, val);
  setbound(teta_new, pts_x, val);

  /*********************************************/
  /*            Solve serial version           */
  /*********************************************/
  solveheatcpu(teta, teta_new,
               &pts_x, &pts_y,
               &dx, &dy, &dt);
  #ifdef DEBUG
    /* Print result ******************************/
    printMatrix2Df(teta, pts_x, pts_y);
  #endif // DEBUG

  #ifdef DEBUG
    float pi = 3.141592653589793;
    float npi;
    zeroMatrixf(teta, pts_y, pts_x);
    /* Analytical solution ***********************/

    /* "Note that the .. solution is NOT an exact
       solution. It is a Fourier Series analytical
       solution.  The hyperbolic sine function
       overflows for large n. Therefore, do not
       accumulate the series solution beyond n=
       101. Also, divide each edge into 10
       intervals maximum to calculate the
       solution. High frequency errors show up on
       at finer intervals" --I. Senocak */
    for (int n=1; n<101; n += 2) {
      npi = n * pi;
      for (int i=0; i<pts_y; i++) {
        for (int j=0; j<pts_x; j++) {
          /* Initial position of the row: i**pts_x */
          /* Column in the current line: j */
          teta[i * pts_x + j] +=
            4 * val / (npi)
            * sin(npi * j * dx / L)
            /* The solution is inverted */
            /* * sinh(npi * i * dy / L) / sinh(npi); */
            * sinh(npi * (L - i * dy) / L) / sinh(npi);
        }
      }
    }
    /* Print result ******************************/
    printf("Analytical\n");
    printMatrix2Df(teta, pts_x, pts_y);
  #endif // DEBUG

  /*********************************************/
  /*                   Free!                   */
  /*********************************************/
  free(teta);
  free(teta_new);

  /* get time at the end (time.h) */
  dtime = GET_TIME(start);
  printf("Serial (CPU; seconds): | %10.3le", dtime);
  return 0;
}
