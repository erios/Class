/*
  Program: exchangeMPI

  C code to test MPI_Send and MPI_Recv and
  different flavors of them A 2D array is
  decomposed with a one dimensional slicing
  strategy.  The portion of the array on each
  process is initialized to the process rank and
  an exchange is performed. Results can be printed
  both before and after the exchange to
  demonstrate the correctness.

  Author: Inanc Senocak
  Contributor: Edgar Rios (implementation of
  Laplace heat plate)

  https://github.com/ljdursi/mpi-tutorial/blob/master/presentation/presentation.md
  https://people.sc.fsu.edu/~jburkardt/c_src/laplace_mpi/laplace_mpi.html

  to compile:
    mpicc -std=gnu99 -I../include -O2 mainmpi.c -o heat2d_mpi.run
  to execute (replace #procs by the number of
  processes):
    mpirun -np #procs ./heat2d_mpi.run

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>
#include <sys/resource.h>
#include <mpi.h>  /* need it for MPI functions */
#include "printMatrix.h"

/* external variables */

void exchange_Blocking (int *phi, const int start, const int end,
                        int src, int dest,
                        const int myRank, const int nProcs, const int NX)
{
  /* See Slide: Message Matching */
  /* this implementation depends on the buffering. Hence, it is not recommended */

  int tag0 = 0;
  int tag1 = 1;

  int e = (end-1)*NX;
  int s = 0;

  /* This escalates  */
  MPI_Send(&phi[e], NX, MPI_INT, dest, tag0, MPI_COMM_WORLD);  // send do not complete until matching receive takes place
  MPI_Recv(&phi[s], NX, MPI_INT, src,  tag0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  s = (start+1)*NX;
  e = end*NX;
  int tmp = dest;
  dest = src;
  src  = tmp;

  MPI_Send(&phi[s], NX, MPI_INT, dest, tag1, MPI_COMM_WORLD);
  MPI_Recv(&phi[e], NX, MPI_INT, src , tag1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

void exchange_nonBlocking (int *phi, const int start, const int end,
                           int src, int dest,
                           const int myRank, const int nProcs, const int NX)
{

  /* this is the recommended implementation with nonblocking communications */

  int tag0 = 0;
  int tag1 = 1;
  int tmp;

  MPI_Request request[4]; //to determine whether an operation completed
  MPI_Status status[4];

  int s, e;

  s = 0;

  MPI_Irecv(&phi[s], NX, MPI_INT, src,  tag0, MPI_COMM_WORLD, &request[0]);

  e = end*NX;
  tmp = dest;
  dest = src;
  src  = tmp;

  MPI_Irecv(&phi[e], NX, MPI_INT, src , tag1, MPI_COMM_WORLD, &request[1]);

  e = (end-1)*NX;
  tmp = dest;
  dest = src;
  src  = tmp;

  MPI_Isend(&phi[e], NX, MPI_INT, dest, tag0, MPI_COMM_WORLD, &request[2]);

  s = (start+1)*NX;
  tmp = dest;
  dest = src;
  src  = tmp;

  MPI_Isend(&phi[s], NX, MPI_INT, dest, tag1, MPI_COMM_WORLD, &request[3]);

  MPI_Waitall(4, request, status);
}

void exchange_SendRecv (float *phi, const int start, const int end,
                        int src, int dest,
                        const int myRank, const int nProcs, const int NX)
{
  int tag0 = 0;
  int tag1 = 1;
  int e = (end-1)*NX;
  int s = 0;

  MPI_Sendrecv(&phi[e], NX, MPI_FLOAT, dest, tag0,
               &phi[s], NX, MPI_FLOAT, src,  tag0,
               MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  s = (start+1)*NX;
  e = end*NX;

  // NOTE WE ARE NOW SWAPPING THE SRC & DEST
  int tmp = dest;
  dest = src;
  src  = tmp;
  MPI_Sendrecv(&phi[s], NX, MPI_FLOAT, dest, tag1,
               &phi[e], NX, MPI_FLOAT, src,  tag1,
               MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

/**
 *  \brief Ensure consistency of 1d decomposition
 *
 *  Sets the right values for each processor
 *  (start and end position of the array).
 *
 *  We are solving a mesh. The mesh is split into
 *  rows (or columns). Each chunk needs to get a
 *  starting and ending position relative to the
 *  overall problem. Ideally, all processes will
 *  deal with the same chunk (block) size, but
 *  depending on the number of divisions and the
 *  number of processes, some may get more than
 *  others.
 *
 *  This function tries to make sure that the
 *  right size is assigned to every process.
 *
 *  \param
 *  const int N -- overall size of the mesh
 *  const int nProcs -- number of processes
 *  const int myRank -- the number assigned to
 *                      the current process
 *  const int nGhostLayers -- number of rows (or
 *   columns) which will be used to exchange
 *   information between blocks. This is defined by
 *   the problem to be solved (from how afar does
 *   every node in the block need to retrieve
 *   data?). For a block in the boundary, it
 *   should normally be 1 less than the rest
 *   (because there would be no exchange of data
 *   with the boundary; if there is, this function
 *   will screw you over).
 *
 *  \output
 *  int *start -- where the current block will
 *                start (hard coded to 0)
 *  int *end -- where the current block will end.
 *              note that (*end - *start) + 1 is the
 *              size of the block plus the
 *              required number of rows (or
 *              columns) for the  exchange (ghost
 *              layers)
 *
 *
 *  \return return type
 *
 */
void
decomposeMesh_1D (const int N,
                  const int nProcs,
                  const int myRank,
                  const int nGhostLayers,
                  int *start, int *end) {
  int pointsPerProcess, remainder;
  /* Check if the number of processes equals the
     length of the dimension (axis) by which the
     problem is split */
  remainder = N % nProcs;
  /* Always set the start to zero (change if
     needed) */
  *start = 0;
  if (remainder == 0) {
    /* n */
    /* If the length of the dimension is a multiple
       of the number of processes, just leave one
       extra row (or column) for those blocks
       which are not next to the boundaries, and
       none for the ones which are away from the
       boundaries (nGhostLayers depends on the
       boundaries; see documentation above).  */
    *end = (N / nProcs) + nGhostLayers - 1;
  }
  else {
    /* ?? I hope that the overall idea is clear
       from the previous comment */
    pointsPerProcess = (N - remainder) / nProcs + 1;
    if (myRank == (nProcs - 1))
      *end = (N - pointsPerProcess * (nProcs-1))
        + nGhostLayers - 1;
    else
      *end = pointsPerProcess + nGhostLayers - 1;
  }
}

/**
 *  \brief calcualtes partial spatial derivatives
 *
 *  It is used to calculate the gradient
 *
 *  \param
 *  teta_e1 : \theta_{i+1} in direction e
 *  teta_e : \theta_{i} in direction e
 *  teta_e_1 : \theta_{i-i} in direction e
 *  de2 : differential of the independent variable
 *        for the derivative
 *
 *  \return float
 */
float
tempparderivmpi(const float *teta_e1,
                const float *teta_e,
                const float *teta_e_1,
                const float *de2) {
  return (*teta_e1
          - 2 * *teta_e
          + *teta_e_1) / *de2;
}

/**
 *  \brief set <val> in the top row
 *
 *  This function is useful to set a boundary
 *  condition in the 2D Laplace heat conduction
 *  problem. It sets a value on the top row of an
 *  array.
 *
 *  \param
 *  float *a -- 2D array
 *  const float *val -- boundary condition
 *  const int N -- # of columns
 *  const int M -- # of rows
 *
 *  \output
 *  modifies the values of *a
 *
 *  \return void
 */
void
setboundmpi (float *a, const float *val,
          const int N, const int M) {
  /* # of rows : M = N, index: i */
  /* # of columns : N, index: j*/
  /* This is the top block */
  /* Move along the fist row */
  for (int j=0; j < N; j++) {
    /* Set values to val */
    a[j] = *val;
  }
}

/**
 *  \brief Solves the Laplace 2D heat equation
 *
 *
 *  \param
 *  teta : \theta (temperature)
 *  pts : number of points in teta
 *  dx : differential of length in direction x
 *  dy : differential of length in direction y
 *  dt : differential of time
 *  start : local start (MPI) of array
 *  end : local end (MPI) of array
 *  src, dest : from where and to where (MPI) the
 *              information is sent
 *
 *  \return return type
 */
void
solveheatmpi (float *teta, float *teta_new,
              const int *pts_x, const int *pts_y,
              const float *dx, const float *dy,
              const float *dt) {
  /* Squared of the increment in x */
  const float dx2 = *dx * *dx;
  /* Squared of the increment in y */
  const float dy2 = dx2;
  /* Temporary variable to swap values */
  float *dum;

  /* along the y-axis */
  for (int i=1; i < *pts_y -1; i++) {
    /* along the x-axis */
    for (int j=1; j < *pts_x -1; j++) {
      /* Estimate temperature ****************/
      /* Initial position of the row: i**pts_x */
      /* Column in the current line: j */
      teta_new[i * *pts_x + j] =
        (
         /* Calculate partial gradient d()/(dx) */
         tempparderivmpi(&teta[(i+1) * *pts_x + j],
                      &teta[    i * *pts_x + j],
                      &teta[(i-1) * *pts_x + j],
                      &dx2)
         /* Calculate partial gradient d()/(dy) */
         + tempparderivmpi(&teta[i * *pts_x + j+1],
                        &teta[i * *pts_x + j],
                        &teta[i * *pts_x + j-1],
                        &dy2)
         ) * *dt + teta[i * *pts_x + j];
    }
  }
}

/**
 *  \brief prints a 2D matrix
 *
 *  \param
 *  float *a -- array (2D matrix)
 *  const int m -- number of columns
 *  const int n -- number of rows
 *  \return return type
 */
void
printmatrixmpi(float *a, const int m,
                    const int n) {
  /* # of rows : N, index: i */
  /* # of columns : M, index: j*/
  if (n <= 11 && m <= 20) {
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < m; j++) {
        printf("%10.3e ", a[m * i + j]);
      }
      printf("\n");
    }
  }
}

/**
 *  \brief makes sure that 1 argument is given
 *  to standard input
 *
 *  \return int (EXIT_SUCCESS)
 */
int checkStdin(int *argc, char *argv[]) {
  /* Check command line arguments */
  if (*argc != 2) {
    fprintf(stderr,
            "This program requires 1 argument:\n "
            "%s m\n"
            "m: number of points to discretise"
            "the length of the square",
            argv[0]);
    exit(-1);
    /* perror(); */
  }

  return EXIT_SUCCESS;
}

int
main(int argc, char *argv[]) {
  /* Timing variable for MPI (and start clock) */
  double wtime;
  wtime = MPI_Wtime();

  /* number of processes */
  int nProcs;
  /* process rank */
  /* (queue in the overal problem of each processor) */
  int myRank;
  /* handles for communication, source process id */
  int src;
  /* handles for communication, destination process id */
  int dest;
  /* start index for each partial domain (block) */
  int start;
  /* end index for each partial domain (block) */
  int end;

  /* initialize MPI */
  MPI_Init(&argc, &argv);
  /* get the number of processes */
  MPI_Comm_size(MPI_COMM_WORLD, &nProcs);

  /*********************************************/
  /*   Definition of cartesian decomposition   */
  /*********************************************/
  /* dimension of Cartesian decomposition 1 =>
     slices */
  int nDims = 1;
  int dimension[nDims];
  int isPeriodic[nDims];
  /* allow system to optimize(reorder) the mapping
     of processes to physical cores */
  int reorder = 1;

  for (int i=0; i < nDims; i++) {
    /* Number of processes in each dimension */
    /* (set all dimensions to a distributed
       number, in this case
       nDims = 1 → nProcs/nDims = nProcs) */
    dimension[i] = nProcs / nDims;

    /* periodicty (boolean) of each dimension
       (no periodicity) */
    isPeriodic[i] = 0;
  }

  /* define a communicator that would be assigned
     a new topology */
  MPI_Comm  comm1D;
  /* Create an ordered cartesian mesh */
  MPI_Cart_create(MPI_COMM_WORLD,
                  nDims, dimension,
                  isPeriodic, reorder,
                  &comm1D);
  /* get the rank of a process after REORDERING! */
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  /* Let MPI find out the rank of processes for
     source and destination */
  MPI_Cart_shift(comm1D, 0, 1, &src, &dest);

  /* Get number of points from user */
  /* ******************************************/
  /* Number of points */
  int NX;
  int NY;
  /* Use process 0 to get the arguments */
  if (myRank == 0) {
    /* Check arguments */
    checkStdin(&argc, argv);
    /* Assign number of points */
    NX = atoi(argv[1]);
  }
  /* Broadcast the number of points to all */
  MPI_Bcast(&NX, 1, MPI_INT, 0, MPI_COMM_WORLD);
  NY = NX;

  int nGhostLayers;
  if (myRank == 0 || myRank == nProcs-1){
    nGhostLayers = 1;
  }
  else {
    nGhostLayers = 2;
  }

  /* Define the starting and ending points for the
     1D mesh with ghost layers */
  /* (see function documentation, please) */
  /* start = 0 (for this case) */
  decomposeMesh_1D(NY, nProcs, myRank,
                   nGhostLayers, &start, &end);

  /* (end - start) + 1 is the size of the block
     (along the splitting dimension) plus the
     required number of rows (or columns) for the
     exchange (ghost layers) */
  int ny = (end - start) + 1 ;

  /*********************************************/
  /*            Variable definition            */
  /*********************************************/
  /* Length of the square */
  const float L = 20;
  /* Length of the array
     (size of side * size of side/(incx * incy) */
  const int pts_x = NX;
  const int pts_y = ny;
  /* Increment in x */
  const float dx = L/(NX - 1);
  /* Increment in y */
  const float dy = L/(NY - 1);
  /* dt <= 1/4*dx² */
  /* http://www.u.arizona.edu/~erdmann/mse350/
     _downloads/2D_heat_equation.pdf */
  const float dt = 0.25 * dx * dy;
  /* Number of time iterations */
  const int t_lim = 20;
  /* Array to hold the temperature */
  float *teta;
  /* Temporary array to hold prev temperature */
  float *teta_new;
  /* Temporary array to switch temp values */
  float *dum;
  /* Boundary condition */
  const float val = 100.0f;

  /*********************************************/
  /*              Allocate memory              */
  /*********************************************/
  /* # of columns : pts_x, index: j */
  /* # of rows : pts_y, index: i */
  /* Use calloc to initialise everything to
     zero */
  teta = (float *) calloc(pts_x * pts_y,
                          sizeof(*teta));
  teta_new = (float *) calloc(pts_x * pts_y,
                              sizeof(*teta_new));
  /* Make sure that the memory was allocated */
  if (teta == 0) {
    fprintf(stderr,
            "\nallocation error (teta)\n");
    return EXIT_FAILURE;
  }

  if (teta_new == 0) {
    fprintf(stderr,
            "\nallocation error (teta_new)\n");
    return EXIT_FAILURE;
  }

  /*********************************************/
  /*             Initial conditions            */
  /*********************************************/
  if (myRank == 0) {
    setboundmpi(teta, &val, pts_x, pts_y);
    setboundmpi(teta_new, &val, pts_x, pts_y);
  }

  /* make sure all processes initialised their
     portion of the problem */
  MPI_Barrier(MPI_COMM_WORLD);

  /* for each instant greater than 1 (0 is
     already set) */
  for (int t=1; t < t_lim; t++) {
    #ifdef DEBUG
      printf("myRank=%d myStart=%d myEnd=%d "
             "my_ny=%d\n",
             myRank, start, end, ny);
      printf("teta(%d):********************\n", t);
      printmatrixmpi(teta, pts_x, pts_y);
    #endif // DEBUG
    solveheatmpi(teta, teta_new,
                 &pts_x, &pts_y,
                 &dx, &dy, &dt);

    #ifdef DEBUG
      printf("\nteta_new:\n");
      printmatrixmpi(teta_new, pts_x, pts_y);
    #endif // DEBUG
    /* swap pointers for each time step */
    dum = teta;
    teta = teta_new;
    teta_new = dum;

    /* make sure all processes calculated their
       portion of the problem */
    MPI_Barrier(MPI_COMM_WORLD);

    /* Exchange data */
    exchange_SendRecv(teta, start, end, src, dest,
                      myRank, nProcs, NX);
  }

  /* Not recommended: */
  //exchange_Blocking( u, start, end, src, dest, myRank, nProcs);
  //exchange_nonBlocking( u, start, end, src, dest, myRank, nProcs);
  #ifdef DEBUG
    if (myRank == nProcs -1 || myRank == 0) {
      printf("myRank=%d myStart=%d myEnd=%d "
             "my_ny=%d\n",
             myRank, start, end, ny);
      printf("********************\n");
      printmatrixmpi(teta, pts_x, pts_y);
    }
  #endif // DEBUG
    //  writeOutput( u );
  free(teta);
  free(teta_new);

  /* Report time */
  /* Stop clock on all processes */
  wtime = MPI_Wtime ( ) - wtime;
  /* Create a new variable to store the sum */
  double tot_time;
  /* Sum the values of all prcesses' clocks, and
     send it to tot_time of process 0  */
  MPI_Reduce(&wtime, &tot_time, 1, MPI_DOUBLE, MPI_SUM, 0, /* You can
                                                        control who
                                                        receives the
                                                        result */
             MPI_COMM_WORLD);
  /* make sure all processes calculated their time */
  MPI_Barrier(MPI_COMM_WORLD);

  if (myRank == 0) {
    printf("MPI time (s): | %10.3le\n", tot_time);
  }
  MPI_Finalize();

  return EXIT_SUCCESS;
}
