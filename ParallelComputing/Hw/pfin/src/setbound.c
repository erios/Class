#include "setbound.h"

/**
 *  \brief sets 100 in the first row
 *
 *  This is a function to set the initial boundary
 *  conditions of the 2D Laplace heat transfer
 *  equation.
 *
 *  The function takes a flattened array which
 *  contains a 2d squared matrix, and sets the
 *  elements in the first row to 100.
 *
 *  \param
 *  float *a     -- 1D array (2D matrix)
 *  const int N  -- size of the array (N×N)
 *
 *  \return
 *  none, modifies the contents of *a
 */
void
setbound (float *a, const int N, const float val){
  /* # of columns : N, index: i*/
  /* # of rows : M = N, index: j */
  /* Go row by row */
  for (int j=0; j < N; j++) {
    a[j] = val;
  }
}
