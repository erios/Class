#include "headers.h"

void
solveheatcpu(float *teta,     //array
          float *teta_new,    //array
          const int *pts_x,
          const int *pts_y,
          const float *dx,
          const float *dy,
          const float *dt) {
  /* Squared of the increment in x */
  const float dx2 = *dx * *dx;
  /* Squared of the increment in y */
  const float dy2 = dx2;
  /* Temporary variable to swap values */
  float *dum;

  /* Time steps (not time) */
  const int t_lim = 20;
  /* for each instant */
  for (int t=1; t < t_lim; t++) {
    #ifdef DEBUG
    printf("teta(%d):********************\n", t);
    printMatrix2Df(teta, *pts_x, *pts_y);
    #endif // DEBUG
    /* along the y-axis */
    for (int i=1; i < *pts_y -1; i++) {
      /* along the x-axis */
      for (int j=1; j < *pts_x -1; j++) {
        /* Estimate temperature ****************/
        /* Initial position of the row: i**pts_x */
        /* Column in the current line: j */
        teta_new[i * *pts_x + j] =
          (
           /* Calculate partial gradient d()/(dx) */
           tempparderiv(&teta[(i+1) * *pts_x + j],
                        &teta[    i * *pts_x + j],
                        &teta[(i-1) * *pts_x + j],
                        &dx2)
           /* Calculate partial gradient d()/(dy) */
           + tempparderiv(&teta[i * *pts_x + j+1],
                          &teta[i * *pts_x + j],
                          &teta[i * *pts_x + j-1],
                          &dy2)
           ) * *dt + teta[i * *pts_x + j];
      }
    }
    #ifdef DEBUG
    printf("\nteta_new:\n");
    printMatrix2Df(teta_new, *pts_x, *pts_y);
    #endif // DEBUG
    /* swap pointers for each time step */
    dum = teta;
    teta = teta_new;
    teta_new = dum;
  }
}
