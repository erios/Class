#include "headers.h"

float
tempparderiv(const float *teta_e1,
             const float *teta_e,
             const float *teta_e_1,
             const float *de2) {
  return (*teta_e1
          - 2 * *teta_e
          + *teta_e_1) / *de2;
}
