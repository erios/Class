/* File:     timingcpu.h
 *
 * Purpose: Define a function that returns the
 *           number of seconds that have elapsed
 *           since some point in the past.  The
 *           timer should return times with
 *           microsecond accuracy.
 *
 *
 * Example:
 *    #include "timingcpu.h"
 *    . . .
 *    double dtime;
 *    struct timeval start;
 *    . . .
 *    gettimeofday(start, NULL);
 *    . . .
 *    Code to be timed
 *    . . .
 *    dtime = GET_TIME(start);
 *    printf("took %e seconds\n", dtime);
 */
#include "headers.h"

double
GET_TIME(struct timeval start) {
  struct timeval t;
  gettimeofday(&t, NULL);
  return (t.tv_sec - start.tv_sec + (t.tv_usec - start.tv_usec)/1000000.0);
}
