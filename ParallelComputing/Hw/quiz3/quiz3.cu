/**
 *   \file rios_HW1.c
 *   \brief Calculates
 *   bi-dimensional Couette's flow (Homework 1)
 *   \author Edgar Rios \date 2017-01-15
 *   \lastmodified 2017-02-05
 *
 *    to compile:
 *    gcc -O3 -Wall -std=gnu99 -lm
 *      -o rios_HW1.run rios_HW1.c
 *
 *  This is homework 1 for the Parallel scientific
 *  computing course in BSU. See HW1.pdf and
 *  HW1_part2.pdf
 *
 *
 */

/* Interaction with the user */
#include <stdio.h>
/* Strings, memory allocation */
#include <stdlib.h>
/* Mathematical functions */
#include <math.h>
/* EXIT_SUCCESS and pals */
#include <sys/resource.h>

/* Points in the y direction (intervals +1) */
#define NY 21

__global__ void
calcSpeed (const int nPts,
           const int nTimeSteps,
           const float dt,
           const float idy2,
           const float dPdx,
           const float mu,
           const float irho,
           float *u,
           float *unew){
  /* Create an index for the threads (blockDim.x)
     is the number of threads per block. */
  int j = blockIdx.x * blockDim.x + threadIdx.x;
  /* Create dummy variable */
  float *tmp;

  /* Although the first loop does not use
     parallelising, it can still be profiled by
     cudaEventRecord, because it's running in a
     device (__global__) function */

  /* Start at second instant (the first one is all
     zero). */
  for (int n = 1; n <= nTimeSteps; n++) {
    if(j>0 && j < nPts - 1){
      /* Calculate next values for current time
         along the y-axis */
      unew[j] = dt * (-irho * dPdx
                      + mu * (u[j + 1] - 2.0f * u[j]
                              + u[j - 1])
                      * idy2)
        + u[j];
    }
    /* swap pointers for each time step */
    tmp  = u;
    u    = unew;
    unew = tmp;
  }
}

/* /\* Use array pointer to modify values of the */
/*    array (CPU Legacy code)*\/ */
/* void uy_j(int j, float dt, float idy2, float dPdx, */
/*           float mu, float irho, float *u, */
/*           float *unew) */
/* { */
/*   unew[j] = dt * (-irho * dPdx */
/*                   + mu * (u[j + 1] - 2.0f * u[j] */
/*                           + u[j - 1]) */
/*                   * idy2) */
/*     + u[j]; */
/* } */

int main(int argc, char *argv[])
{
  /* Check command line arguments */
  /* If the number of points is not defined, and
     it was not provided, crash */
#ifndef NY
  if (argc != 4) {
    fprintf(stderr,
            "This program requires three"
            " arguments:\n %s"
            " <Reynolds Number (float)>"
            " <Pressure gradient (float)>"
            " <Number of points (int)>\n",
            argv[0]);
    /* perror(); */
  }
#endif /* NY */

  /* If the number of points is defined, but
     there are missing arguments, crash */
#ifdef NY
  if (argc != 3) {
    printf("Number of points set to: %d\n", NY);
    fprintf(stderr,
            "This program requires two"
            " arguments:\n %s"
            " <Reynolds Number (float)>"
            " <Pressure gradient (float)>\n",
            argv[0]);
  }
#endif // NY

  /*******************************************/
  /*           Initialise variables          */
  /*******************************************/
  /* The notation initialises only the specified
     values and the rest are zero */
  /* float uy_n[21] = {[0] = 0}; */
  /* Number of points */
  const int nPts = NY;
  /* Reynolds number */
  const float Re = atof(argv[1]);
  /* Pressure differential */
  const float dPdx = atof(argv[2]);
  /* Spacing between plates (arbitrary; unit
     value) */
  const float h = 1.0f;
  /* Plate velocity */
  const float uPlate = 1.0f;
  /* Density */
  const float rho = 1.0f;
  /* Inverse of density */
  const float irho = 1 / rho;
  /* Dynamic viscosity */
  const float nu = uPlate * h / Re;
  /* Static viscosity */
  const float mu = nu * rho;
  /* Distance between calculation points */
  const float dy = h / (nPts - 1);
  /* Square of the distance between the points
   */
  const float dy2 = dy * dy;
  /* Inverse of dy2 */
  const float idy2 = 1 / dy2;
  /* Time steps */
  const float dt = 0.5f * pow(dy, 2.0) / nu;
  /* Ending time (square of distance over nu) */
  const float timeEnd = h * h / nu;
  /* Number of time steps */
  const int nTimeSteps = (int) ceil(timeEnd /
                                    dt);
  /* Profiling (benchmark) time */
  // make sure it is of type float, precision is
  // milliseconds (ms) !!!
  float profTime = 0;

  /* Host containers (to receive values from
     GPU) */
  float *u_h = (float *)calloc(nPts, sizeof(*u_h));
  float *unew_h = (float *)calloc(nPts, sizeof(*unew_h));

  /* CUDA parallel definition ***************/
  /* Number of threads per block */
  /* (more efficiency with multiples of 16) */
  int blockSize = 256;             /* 16×16 */
  /* Number of blocks (round up if n is not a
     multiple of blockSize) */
  int nBlocks = (nPts + blockSize - 1)
    / blockSize;
  /* Timing events */
  cudaEvent_t clkStart, clkStop;

  /* Dynamic allocation of variables *********/
  /* Current speed along the y-axis, in the
     x-direction */
  float *u;
  /* New speed */
  float *unew;
  /* Ordinate to calculate the speed */
  float *y;

  cudaMallocManaged(&u, nPts * sizeof(*u));
  cudaMallocManaged(&unew, nPts * sizeof(*unew));
  cudaMallocManaged(&y, nPts * sizeof(*y));

  /* Assign an event to the timing variables */
  // WARNING!!! use cudaEvent_t only to time the
  // device
  cudaEventCreate(&clkStart);
  cudaEventCreate(&clkStop);

  /*******************************************/
  /*           Boundary conditions           */
  /*******************************************/
  /* Set initial values to zero */
  for (int n=1; n < nPts - 1; n++){
    u[n] = 5.0f;
    unew[n] = 0.0f;
  }
  u[0]         = 0.0f;
  unew[0]      = 0.0f;
  u[nPts - 1]    = uPlate / uPlate;
  unew[nPts - 1] = uPlate / uPlate;

  /*******************************************/
  /* Calculate the values for each time step */
  /*******************************************/
  /* Mark the starting time */
  // the second argument 0 is for the default
  // CUDA stream
  cudaEventRecord(clkStart, 0);

  /* Calculate the speed with CUDA */
  /* (use <<< , >>> to specify a CUDA
     function) */
  calcSpeed<<<nBlocks, blockSize>>>(nPts,
                                    nTimeSteps,
                                    dt, idy2,
                                    dPdx, mu,
                                    irho, u,
                                    unew);
  /* Ask all of the processes to finish */
  cudaDeviceSynchronize();

  /* /\* Mark the finishing time  *\/ */
  /* cudaEventRecord(clkStop, 0); */
  cudaEventSynchronize(clkStop);

  // WARNING!!! do not simply print
  // (timeStop-timeStart)!!
  /* Retrieve the elapsed time and save into
     profTime */
  cudaEventElapsedTime(&profTime, clkStart,
                       clkStop);

  /* free the timer */
  cudaEventDestroy(clkStart);
  cudaEventDestroy(clkStop);

  /* /\* Legacy CPU Code *\/ */
  /* for (int n = 1; n <= nTimeSteps; n++) { */
  /*     /\* Calculate next values for current time */
  /*        along the y-axis *\/ */
  /*     for (int j = 1; j < nPts - 1; j++) { */
  /*         uy_j(j, dt, idy2, dPdx, mu, irho, u, */
  /*              unew); */
  /*     } */
  /*     /\* swap pointers for each time step *\/ */
  /*     tmp  = u; */
  /*     u    = unew; */
  /*     unew = tmp; */
  /* } */

  /*******************************************/
  /*   Calculate exact solution and print    */
  /*******************************************/
  /* Print header */
  printf("Distance between the plates (h): "
         "%.5f\n", h);
  printf("Plate velocity (uPlate): "
         "%.5f\n", uPlate);
  printf("Density (rho): "
         "%.5f\n", rho);
  printf("Dynamic viscosity (nu): "
         "%.5f\n", nu);
  printf("Static viscosity (mu): "
         "%.5f\n", mu);
  printf("Number of calculation points (nPts): "
         "%i\n", nPts);
  printf("Time step (dt): %.5f\n", dt);
  printf("Number of time steps (nTimeSteps): "
         "%i\n", nTimeSteps);
  printf("Re=%.0f, $\\frac{dP}{dx}=%.0f$\n",
         Re, dPdx);
  printf("Results:\n");
  printf("\"y\"\t\"Exact\"\t\"Numer\" \n");

  /* /\* Exchange values between GPU and CPU */
  /*    (you cannot use GPU variables in the CPU) *\/ */
  /* cudaMemcpy(u_h, u, sizeof(float), */
  /*            cudaMemcpyDeviceToHost); */
  /* cudaMemcpy(unew_h, unew, sizeof(float), */
  /*            cudaMemcpyDeviceToHost); */

  for (int j = 0; j < nPts; j++) {
    y[j] = j * dy;
    /* Exact solution (reuse unew) */
    unew[j] = uPlate * y[j] / h
      + dPdx / (2.f * mu)
      * (y[j] * y[j] - h * y[j]);
    /* Print current value */
    printf("%.5f"                 /* column 1 */
           "\t%.5f"               /* column 2 */
           "\t%.5f"               /* column 3 */
           "\n", y[j] / h,
           unew[j] / uPlate, u[j] / uPlate);
  }

  /*******************************************/
  /*          free allocated memory          */
  /*******************************************/
  /* free the timer */
  cudaEventDestroy(clkStart);
  cudaEventDestroy(clkStop);

  /* free allocated arrays */
  cudaFree(unew);
  cudaFree(u);
  cudaFree(y);

  /* call gnuplot */
  /* system("gnuplot -persist <gnuplot file>")
   */
  return EXIT_SUCCESS;
}

/* Local Variables:  */
/* flycheck-clang-language-standard: c99 */
/* fill-column: 50 */
/* comment-column: 50 */
/* End:              */
