/*
 * Numerical and analytical solution of the 1D heat conduction problem on the GPU
 *
 * Author: Inanc Senocak
 * Date: 04/11/2016
 *
 * to compile:
 *
 * nvcc -O3 -o heat.exe gpu_1d_heat.cu -arch=sm_30
 *
 * to execute: ./heat.exe <simulation end time (seconds)>
 *
 * make sure to execute on a GPU that supports compute capability 3.0 or higher
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/resource.h>

#define LX 3.0f    /* length of the domain in x-direction  */
#define NX 100    /* includes boundary points on both end */
#define DX        LX / ( (REAL) (NX-1) )
#define ALPHA     1.0f
#define DT        0.25f * DX*DX / ALPHA
#define BLOCK_SIZE 8 /* # of threads per CUDA block */

#define RESTRICT __restrict__

/*--control the precision of the computations */
#ifndef SINGLE
typedef double REAL;
typedef int   INT;
#define PI 3.141592653589793
#else
typedef float REAL;
typedef int    INT;
#define PI 3.1415927f
#endif

/*
void cpu_solveHeat_1D (REAL *RESTRICT unew, const REAL *RESTRICT u, const REAL *RESTRICT x)
{
    INT i;
    REAL dxi = 1/(DX*DX);
    REAL xc, source;

    for (i=1 ; i<NX-1 ; i++) {
        xc      = x[i];
        source  = -(xc*xc-4.f*xc+2.f)*exp(-xc);  //source term
        unew[i] = ( ALPHA*( u[i+1] - 2.0f*u[i] + u[i-1])*dxi + source ) * DT + u[i] ;
    }
}
*/

__global__ void gpu_solveHeat_1D (REAL *RESTRICT unew, const REAL *RESTRICT u, const REAL *RESTRICT x)
{
/*--use the above cpu_solveHeat_1D to create a GPU kernel--*/
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  REAL dxi = 1/(DX*DX);
  REAL xc, source;
  /* From 1 to NX - 2 */
  if (i>0 && i<NX-1){
    xc      = x[i];
    source  = -(xc*xc-4.f*xc+2.f)*exp(-xc);  //source term
    unew[i] = ( ALPHA*( u[i+1] - 2.0f*u[i] + u[i-1])*dxi + source ) * DT + u[i] ;
  }

}
void exactSolution( REAL *RESTRICT uExact, const REAL *RESTRICT x )
{
    INT i;
    for (i=0; i<NX; i++) {
        uExact[i] = x[i]*x[i]*exp(-x[i]);
    }
}

void meshGrid ( REAL *RESTRICT x )
{
    INT i;
    for (i=0; i<NX; i++) {
        x[i] =  DX * ( (REAL) i );
    }
}

void writeOutput ( const REAL *RESTRICT x, const REAL *RESTRICT uExact, const REAL *RESTRICT u)
{
    INT i;
    FILE *output;
    output = fopen("soln_heat1d.dat","w");

    fprintf(output,"x-coord,   exact,   numerical\n");

    for (i=0; i<NX; i++) {
        fprintf( output, "%10f %10f %10f\n", x[i], uExact[i], u[i] );
    }
    fclose(output);
}

int main(INT argc, char *argv[])
{
    if (argc < 2) {
       perror("Command-line usage: executableName <end Time (seconds)>");
       exit(1);
    }

    REAL endTime = atof(argv[1]);

/*--allocate memory for the following--*/

    REAL *unew, *u, *tmp, *uExact, *x;

    cudaMallocManaged(&unew,   NX * sizeof (*unew));
    cudaMallocManaged(&u,      NX * sizeof (*u));
    cudaMallocManaged(&uExact, NX * sizeof (*uExact));
    cudaMallocManaged(&x,      NX * sizeof (*x));

/*--generate the mesh points--*/

    meshGrid( x );

/*--calculate the exact solution on the CPU--*/

    exactSolution ( uExact, x );

/*--apply boundary conditions to u and unew--*/

    u[0]       = 0.f;
    unew[0]    = 0.f;
    unew[NX-1] = uExact[NX-1];
    u[NX-1]    = uExact[NX-1];

/*--parameters for the CUDA kernel execution--*/

    int nBlocks = (NX + BLOCK_SIZE -1) / BLOCK_SIZE;

    REAL time = 0.f;
    while (time < endTime)
    {
        gpu_solveHeat_1D <<< nBlocks, BLOCK_SIZE >>> ( unew, u, x );
        cudaDeviceSynchronize();
        // swap pointers
        tmp  = u;
        u = unew;
        unew = tmp;
        //increment the time
        time += DT;
    }

    writeOutput( x, uExact, u );

    cudaFree(unew);
    cudaFree(u);
    cudaFree(uExact);
    cudaFree(x);

    return EXIT_SUCCESS;
}
