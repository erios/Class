/* This program demonstrates the use of collective communication * functions in MPI
 *
 * Author: Inanc Senocak
 * Date: 03/02/2012
 * Last edit: 04/20/2016
 *
 * to compile: mpicc -std=c99 -O3 <source.c> -o <executable>
 * to execute: mpirun -np <#processes> ./<executable>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h> /* need it for MPI functions */

int main(void) {

  int nProcs; /* number of processes */
  int myRank; /* process rank */

  MPI_Init(NULL, NULL);                   /* initialize MPI */
  MPI_Comm_size(MPI_COMM_WORLD, &nProcs); /* get the number of processes */
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank); /* get the rank of a process */

  // put a barrier to get the start time
  MPI_Barrier(MPI_COMM_WORLD);

  double start = MPI_Wtime();

  // define a new variable that we want to broadcast
  int number;
  if (myRank == 0) {
    printf("Enter a positive integer value:\n");
    scanf("%d", &number);
    if (number < 0)
      MPI_Abort(MPI_COMM_WORLD, 666);
  }

  // Let's broadcast the "number" to all processes
  MPI_Bcast(&number, 1, MPI_INT, 0, MPI_COMM_WORLD);

  // do some operations on this broadcast number
  int myValue = myRank + number;
  int sum, maxValue;

  MPI_Reduce(&myValue, &sum, 1, MPI_INT, MPI_SUM, 0, /* You can
                                                        control who
                                                        receives the
                                                        result */
             MPI_COMM_WORLD);
  MPI_Reduce(&myValue, &maxValue, 1, MPI_INT, MPI_MAX, 1, /* You can
                                                             control who
                                                             receives the
                                                             result */
             MPI_COMM_WORLD);

  /* There is no barrier → the printed statements can happen out of
     order */
  if (myRank == 0) {
    printf("The global sum = %d\n", sum);
  }
  if (myRank == 1) {
    printf("The maximum value among all processes = %d\n", maxValue);
  }
  // note what happens if I reduce the maximum value to process 1 and try to
  // print it from process 2
  if (myRank == 2) {
    printf("Printing the max value from the wrong process = %d\n", maxValue);
  }
  // However, if I do an Allreduce, there should be no problem
  MPI_Allreduce(&myValue, &maxValue, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
  if (myRank == 2) {
    printf("Printing the max value after Allreduce = %d\n", maxValue);
  }
  /* Scatter *****************************************************************/
  /* Declare pointer to an integer */
  /* (everyone does this) */
  int *array;
  int n = 8;
  int i;

  /* Define one of the nodes to allocate memory */
  /* (only one does this) */
  if (myRank == 0) {
    array = malloc(n * sizeof(int));
    for (i = 0; i < n; i++) {
      array[i] = i;
    }
  }
  int sendCount = n / nProcs;   /*  */
  int localArray[sendCount];

  /* Split the process (localArray) by the amount sendCount */
  MPI_Scatter(array, sendCount, /* Do not exceed what is allocated */
              MPI_INT, localArray, /* Where it's received */
              sendCount, /* Do not exceed what is allocated (does
                            not need to match) */
              MPI_INT, 0, MPI_COMM_WORLD);


  for (i = 0; i < sendCount; i++) {
    localArray[i] = localArray[i] + myRank;
  }

  if (myRank == 0) {
    printf("Array elements before MPI_Scatter:\n");
    for (i = 0; i < n; i++) {
      printf("%d\n", array[i]);
    }
  }
  MPI_Gather(localArray, sendCount, MPI_INT, array, sendCount, MPI_INT, 0,
             MPI_COMM_WORLD);

  if (myRank == 0) {
    printf("Array elements after MPI_Gather:\n");
    for (i = 0; i < n; i++) {
      printf("%d\n", array[i]);
    }
  }

  // put a barrier before recording the finish time
  MPI_Barrier(MPI_COMM_WORLD);

  double finish = MPI_Wtime();

  double elapsedTime = finish - start;
  double wallTime;

  MPI_Reduce(&elapsedTime, &wallTime, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

  if (myRank == 0)
    printf("Wall-clock time = %.3f seconds \n", wallTime);

  MPI_Finalize(); /* don't forget to finalize MPI */

  return 0;
}
