#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/* #include <mpi.h> */

float testFunction(const float x){
  return 3 * x * x;
}

float trapezoidalInt(float lowlim, float uplim, float n){
  float h, xi, res;

  h = (uplim - lowlim)/n;
  res = (testFunction(lowlim) + testFunction(uplim)) / 2.0;
  for (int i=1; i < n; i ++){
    xi = lowlim + i * h;
    res += testFunction(xi);
  }
  res *= h;
  return res;
}

int
main(int argc, char *argv[])
{
  printf("%10.4e", trapezoidalInt(0, 10, 100));
  printf("%10.4e", (10.*10.*10.));
  return 0;
}
