#+INTERLEAVE_PDF: /home/edgar/Documentos/Class/TA/Grading/Eprops/24731302.pdf
#+OPTIONS: toc:nil

#+LATEX_HEADER:  \usepackage{pdflscape,longtable,tabu}
* Notes for page 1
:PROPERTIES:
:UNNUMBERED: t
:interleave_page_note: 1
:END:
**                                                  :Style:
I liked the good format of the \alpha equation
**                                                  :Style:
What happened to the nice style?
**                                              :Structure:
When you present an equation, introduce the variables (what is \alpha, etc.)
**                                                  :Plots:
Nice caption for your plots. It needs to have a figure number. I don't care about whether it fits in your printer or not, but others might.
**                                               :Analysis:
was proportional to resistance

do you mean directly proportional? inversely proportional?

Your plots show something else, except for nickel
**                                                  :Vocab:
Changing resistance

Do you mean change of resistance? If so, the mathematical way of representing this is \Delta(R)
**                                                  :Vocab:
Actual Resistivity \rightarrow Actual resistivity

\rightarrow Literature resistivity [citation] (why not use \(\rho\)?)

You know? I don't actually understand your table. Sorry.
**                                                  :Style:
I would like to vanish your copyright symbol
**                                          :Style:Grammar:
Next set of

The next set of

Add a label to each table and figure. One reason for this is so that they can be referenced.
**                                                  :Table:
I like the fact that you set the units on the table header
**                                                  :Vocab:
difference of

difference between
**                                                  :Vocab:
+efficiently+
**                                                  :Style:
This is for my sanity: two column format or single column with a 70 character length, please.
**                                               :Analysis:
while actual values may be a little off from what we calculated in the experiment.

A bit contradictory...
**                                               :Analysis:
being taken out of the ovens by different groups to find the resistance

Did you let the temperature stabilise before your measurements? How could you test if that was really the case?
**                                                :Grammar:
was +this+ resistance

change to something meaningful
**                                                :Grammar:
my

our

Donating your work is fine, but this was not supposed to be presented as the effort of one. (Applies to other references to 1st person).
**                                               :Analysis:
Overall, we believe with further experimentation that Copper would still be the most
recommended.

I am not objecting: why is that?
