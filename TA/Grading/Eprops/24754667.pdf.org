#+INTERLEAVE_PDF: /home/edgar/Documentos/Class/TA/Grading/Eprops/24754667.pdf
#+OPTIONS: toc:nil tags:nil

I trust that you have the potential to produce a formal report which will get you a passing grade. That is why you have until tomorrow at 16:30 to deliver such a report. This will not happen again for another report. Good luck.

* Notes for page 1
:PROPERTIES:
:UNNUMBERED: t
:interleave_page_note: 1
:END:
** The objectives of this lab is                  :Grammar:
are
** and then to learn                              :Grammar:
and to
** resistivity/conductivity                       :Grammar:
a slash is no substitute for a preposition
** Ohms                                           :Grammar:
ohms
** Celsius                                  :Units:Grammar:
centigrade
