#+OPTIONS: toc:nil
#+INTERLEAVE_PDF: /home/edgar/Documentos/Class/TA/Grading/TensileHardness/GroupA/10.pdf

#+LATEX_HEADER:  \usepackage{pdflscape,longtable,tabu}
#+LATEX_HEADER: \usepackage{siunitx} \sisetup{per-mode=fraction,fraction-function=\tfrac}
* Notes for page 1
:PROPERTIES:
:interleave_page_note: 1
:END:
** Introduction
***                                             :Analysis:
#+BEGIN_QUOTE
Strain is the
deformation of a material that is caused by an applied force or load
#+END_QUOTE
Nope
***                                             :Analysis:
#+BEGIN_QUOTE
Stress is a measurement of strength
that the material has based on how much force is applied and how much the area of the material has
changed
#+END_QUOTE
I wished that you understood the depth of what you just said. Either way, stating the definition of stress like this will not help you for your courses.
***                                             :Analysis:
#+BEGIN_QUOTE
These are related to the modulus of elasticity because these types of deformations show
how stress and strain are related in different types of materials proving whether the materials have an
elastic deformation or a plastic deformation
#+END_QUOTE
You got lost. Go back to the path of light :) .
***                                             :Analysis:
#+BEGIN_QUOTE
due to the fact that In the United Mechanical Test Machine
each metal broke with large amounts of initial plastic deformation.
#+END_QUOTE
We could see that they were ductile, because they had a considerable plastic deformation before braking--reversed argument.
***                                              :Grammar:
#+BEGIN_QUOTE
strain of a material and if a
material
#+END_QUOTE
Say what?
***                                             :Analysis:
I liked most of what you said, but you are missing hardness, I believe
** Methods and materials
***                                               :Struct:
#+BEGIN_QUOTE
These included brass, two types of
aluminum, and steel. These materials were ductile in experiments because they held plastic
deformation before fracturing. To be exact we used Brass 260, Aluminum 6061, Aluminum 2024,
and Steel 1018.
#+END_QUOTE
Besides duplicating what was stated in the introduction, you are repeating yourself here as well.
***                                             :Analysis:
Missing model and type of equipment
* Notes for page 2
:PROPERTIES:
:interleave_page_note: 2
:END:
** Methods and materials
***                                                :Style:
What's up with the different indentation?
***                                              :Grammar:
#+BEGIN_QUOTE
instantaneously load
#+END_QUOTE
Sorry?
***                                               :Struct:
No references?
***                                             :Analysis:
#+BEGIN_QUOTE
This is the final length of the metal right before +fraction+ *fracture* minus the initial length of the metal all over the
initial length multiplied by 100 to get a percent value
#+END_QUOTE
Finally! Someone says what this means
***                                             :Analysis:
Are you sure that you are using the right metric of hardness? HB usually means Brinell.
*** Remark
Let me be clear about this: personally, I would not care any less if you have the same plots and tables as someone else, if the rest of your report (the analysis) is different. However, doing this is called plagiarism, and nobody likes that. In my life, I have committed self-plagiarism, because I didn't know what I was doing. I am telling you now. *Do not ever do this again* under any circumstance. If I ever see it, you'll get my wrath. Right now, go to the person with whom you shared your work and let her/him know. I do not know who took from who, if you did it together or anything. I just know that it is the same material, and this is _not_ to be done.

* Notes for page 5
:PROPERTIES:
:interleave_page_note: 5
:END:
** Discussion
***                                             :Analysis:
#+BEGIN_QUOTE
In this lab we are understanding many different concepts that all relate into one main concept
of mechanical properties. We learn about the many different properties that different materials can
hold and how they relate to each other. We learn about the relationship between stress and strain
#+END_QUOTE
You have said nothing useful with these sentences. Get rid of them.
***                                             :Analysis:
#+BEGIN_QUOTE
Stress comes from the amount of force applied and how it changes the cross-sectional area of the
material. Strain comes from the amount of deformation that is caused by the amount of force or load
applied to a material
#+END_QUOTE
All this belongs to your introduction. It seems to me that you are trying to prove that you learnt the concepts from your book. Further, if that is the case, it may be because of the instructions, which would mean that you misunderstood what I was trying to say, and I don't blame you.

When I said that in the discussion you show what you really learnt, I meant that the learning is shown by means of your discussion, not by restating the concepts. You draw insights from the concepts and your experiments. That is what you really learn.

More importantly, your discussion is supposed to be based on your results. You don't make any refernce to them in your redaction.
***                                             :Analysis:
#+BEGIN_QUOTE
The relationship between the slope of the stress
strain curve and bonding potential is that as a force is applied to a material and begins to deform then
bonding potential begins to decrease as the slope of the curve increases
#+END_QUOTE
This is a little vague, but it seems that you are stating that there is a relationship between the bond energy potential and the mechanical properties. If that is the case, make sure that you understand about what you are talking.
***                                             :Analysis:
#+BEGIN_QUOTE
Overall this lab has taught me the most about the subject compared to previous labs.
#+END_QUOTE
Good for you!
** Final remark
Overall good. Plagiarism not accepted (points off)
