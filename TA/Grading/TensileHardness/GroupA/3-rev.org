#+OPTIONS: toc:nil
#+INTERLEAVE_PDF: /home/edgar/Documentos/Class/TA/Grading/TensileHardness/GroupA/3.pdf

#+LATEX_HEADER:  \usepackage{pdflscape,longtable,tabu}
* Notes for page 1
:PROPERTIES:
:interleave_page_note: 1
:END:
** Introduction
***                                                :Vocab:
#+BEGIN_QUOTE
to from a stress
#+END_QUOTE
to form a stress
***                                             :Analysis:
Thank you for being clear with your goal
***                                               :Struct:
Good structure to introduce the main concepts
***                                             :Analysis:
The toughness is not obtained from the strain v.s. stress curve.
***                                             :Analysis:
#+BEGIN_QUOTE
Since our materials are brittle
#+END_QUOTE
Really?
** Methods and materials
***                                               :Struct:
#+BEGIN_QUOTE
United Mechanical Testing Machine.
#+END_QUOTE
Missing model.
* Notes for page 2
:PROPERTIES:
:interleave_page_note: 2
:END:
** Methods and materials
***                                                :Vocab:
#+BEGIN_QUOTE
The HBR
#+END_QUOTE
The HRB (hardness rockwell B, if I am correct)
***                                              :Grammar:
#+BEGIN_QUOTE
The HBR hardness tests we apply a load directly into our material via a 1/8-inch ball.
#+END_QUOTE
Sorry?
***                                             :Analysis:
#+BEGIN_QUOTE
I am not sure how many times the groups measured their samples to increase
accuracy, however based on how much time we have in lab I would be surprised if any group
measured more than twice.
#+END_QUOTE
I just need to make sure that you know how to report your tests. From this statement, it is still not clear to me. If you measured one time due to time constraints, you can say:
#+BEGIN_QUOTE
Due to time constraints, only one measurement of hardness was performed.
#+END_QUOTE
... or something like that.
*
* Notes for page 3
:PROPERTIES:
:interleave_page_note: 3
:END:
** Results
***                                               :Struct:
#+BEGIN_QUOTE
Using equation 3
#+END_QUOTE
I almost cried (over-statement) to see the first well-referenced equation!
***                                             :Analysis:
#+BEGIN_QUOTE
Using equation 3 I found the modulus of elasticity of Aluminum alloy 6061 to be 1.42 GPa
#+END_QUOTE
Someone can make very wrong assumptions based on this comment. Try to be more specific. For instance:
#+BEGIN_QUOTE
By taking two points of strain v.s. stress, (0.03, \(\SI{30}{MPa}\)) and (0.07, \(\SI{60}{MPa}\)) (see  Figure 1)
#+END_QUOTE
Note that the values are wrong, and I just used them to illustrate the structure.
***                                                :Punct:
#+BEGIN_QUOTE
Using equation 3 I
#+END_QUOTE
Using equation 3, I
***                                                :Units:
Check my comments on units from the previous report
***                                               :Struct:
Please, report with tables
* Notes for page 7
:PROPERTIES:
:interleave_page_note: 7
:END:
** Discussion
***                                             :Analysis:
#+BEGIN_QUOTE
Having the English system and metric system mixed together would cause a lot of errors in my results
#+END_QUOTE
Thanks, that's all that I need to know.
** References

I didn't see any references in the text.

*I am not a reliable reference*.

The reference section looks like this (example):

#+LATEX: \begin{thebibliography}{9}

#+LATEX: \bibitem{lamport94}
#+LATEX:   Leslie Lamport,
#+LATEX:   \textit{The title},
#+LATEX:   Addison Wesley, Massachusetts,
#+LATEX:   2nd edition,
#+LATEX:   1994.

#+LATEX: \bibitem{Callister}
#+LATEX:   Callister,
#+LATEX:   \textit{Materials Science \& Engineering -- An introduction}
#+LATEX:   Wiley & Sons, New York
#+LATEX:   7th edition,
#+LATEX:   2007.
#+LATEX:
#+LATEX: \end{thebibliography}
** Final remark
It shows that you put some effort into your report :) .
