* Configuration                                            :ARCHIVE:noexport:

#+STARTUP: beamer
#+EXCLUDE_TAGS: noexport
#+LATEX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [bigger,xcolor={usenames,dvipsnames,table}]
- Set slides theme
  #+BEAMER_THEME: Darmstadt
  #+OPTIONS: H:2 toc:1

- Make it easier to create columns on slides (switch to ~org-column-mode~)
  #+COLUMNS: %20ITEM %13BEAMER_env(Env) %6BEAMER_envargs(Args) %4BEAMER_col(Col) %7BEAMER_extra(Extra)

- Set a better font for equations
  # https://tex.stackexchange.com/a/34267
  # http://orgmode.org/org.html#Beamer-specific-export-settings
  #+BEAMER_FONT_THEME: serif [onlymath]

  # https://github.com/dfeich/org-babel-examples/blob/master/beamer/beamer-example.org

- Style, symbols, etc.
  # #+INCLUDE: "../Reports/headers.org"
  #+LATEX_HEADER: \input{/home/edgar/.emacs.d/plugins/symbols.tex}

- My external apps
  # #+INCLUDE: "../Reports/headers.org"
  #+LATEX_HEADER: \input{/home/edgar/.emacs.d/plugins/externalapps.tex}
  - Set path for SVG
    #+LATEX_HEADER: \def\epsdir{./}
    #+LATEX_HEADER: \def\svgdir{./}

- Footnotes
  #+OPTIONS: f:t

- Add support for BibLaTeX
  #+LATEX_HEADER: \usepackage[style=numeric-comp,
  #+LATEX_HEADER:  sorting=none,hyperref=true,backref=true,
  #+LATEX_HEADER:  url=true,backend=biber,natbib=true]{%
  #+LATEX_HEADER:  biblatex}
  #+LATEX_HEADER: \addbibresource{../References.bib}

- Long and rotated tables
  #+LATEX_HEADER:  \usepackage{pdflscape,longtable,tabu}

- Bold math symbols
  #+LATEX_HEADER: \usepackage{bm}

- Do not evaluate when exporting
  #+PROPERTY: header-args:latex :exports results :eval no-export :results raw

- Use python3, do not evaluate when exporting
  #+PROPERTY: header-args:python :python python3 :eval no-export

- Do not evaluate blocks when exporting
  #+PROPERTY: header-args :eval no-export

- Change caption head for listings
  #+LATEX_HEADER: \renewcommand\lstlistingname{Block}
  #+LATEX_HEADER: \renewcommand\lstlistlistingname{Code blocks}
  #+LATEX_HEADER: \def\lstlistingautorefname{Blk.}


* Bit of theory[fn::Serway. Physics for Scientists and Engineers with Modern Physics (9th ed.), p.811, 819]

** (Actual) Ohm's law

#+BEGIN_QUOTE
For many materials (including most metals), the ratio of the current density to the electric field is a constant \(\sigma\) that is independent of the electric field producing the current.
#+END_QUOTE

\(J = \sigma\,E\)

- /J/: current density
- \(\sigma\): conductivity
- /E/: electrical field
** Ohm's law as you want to know it
\(V = R\,I\)

- /V/: voltage
- /R/: resistance
- /I/: current

** Life is about balance (a bit of both)

\[\Delta{(V)} = \frac{l}{\sigma\,A}\,I\]

- \(\Delta{(V)}\): electrical potential difference (a.k.a. voltage)
- \(l\): length (surprise!)
- \(\sigma\): conductivity (same as before)
- \(A\): Cross-sectional area normal to \(\Delta{(V)}\)

** What do I care? (Resist!)

- \(R = \frac{l}{\sigma\,A}\)
- \(R \equiv \frac{\Delta{(V)}}{I}\)

Can you solve the riddle? (resistance depends on length)

** What about temperature?

- \(\rho = \frac{1}{\sigma}\)
- \(R = \rho\,\frac{l}{A}\)

- \(\rho = \rho_{0}\,\left(1 + \alpha\,\left(T - T_{0}\right)\right)\)
- \(\alpha = \frac{1}{\rho_{0}}\,\frac{\Delta{(p)}}{\Delta{(T)}}\)

- \(R = R_{0}\,\left(1 + \alpha \, \left(T - T_{0}\right)\right)\)

** Say what?

 \[R = R_{0}\,\left(1 + \alpha\,\left(T - T_{0}\right)\right)\]

- R_{0} :: resistance at temperature \(T_{0}\)
- T_{0} :: reference temperature (usually 20°C)
- R :: resistance at temperature \(T\)
- T :: testing temperature

* Equipment

** The Good

Easier than the tensile machine

** The Bad

It's hot! (careful).

** The Best

Thanks Travis! (he did all the hard work).

** General

- Put on gloves, safety glasses
- Give temperature some time
- Don't let wires touch
- Leave the ovens on
- You get the samples from oven (got insurance?)
- Let them take measurements

* Equipment specifics

** Meter

- Dials of meter only when button is SET
- Upper limit of meter to low value (beeping)
- Match the colors of cables
- You can open one of the drawers below the ovens
- Three scales
  - Left: kilo-ohm
  - Center: ohm
  - Right: milli-ohm

** Ovens

- Oven to the left (buttons)
  1. left
  2. right
  3. increase/decrease
  4. left
- Central oven should be set
- Oven to the right
  - Needs a brick (just the right size)

** Drawer

- Use rollers, not grooves
- For your own sake (let them learn), go little by little
- If they want to make coin pancakes, it's fine.
- Take care of your knees: leave handle down.
